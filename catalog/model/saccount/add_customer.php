<?php
class ModelSaccountAddCustomer extends Model {
  public function existsAsCustomer($customer_id, $seller_id)
  {
    //Consulta si el cliente existe en la tabla de clientes por vendedor
    //oci_sellers_customers

    $sql = (
              "select *  "
              ."from oci_sellers_customers "
              ."where customer_id = '" . (int)$customer_id . "' "
              ."and seller_id = '" . (int)$seller_id . "' "
            );
    $query = $this->db->query($sql);

    //Si la consulta no trajo ningún resultado devuelvo false
    if(count($query->rows) == 0)
    {
      return false;
    } else {
        return true;
      }

  }

  public function addToCustomersOfSeller($customer_id, $seller_id)
  {
    //Agrega al cliente como cliente del vendedor, pero primero tengo que
    //controlar que no exista en la tabla de oci_sellers_customers
    //si NO es CLIENTE del vendedor, agrego registro a la trabla
    //Con fecha actual
    //Si el cliente ya existe en la tabla, no hago nada
    $existe = $this->existsAsCustomer($customer_id, $seller_id);

    if($existe == false)
    {
      $sql = (
                "insert into oci_sellers_customers (id,seller_id,customer_id, date_added, status) "
                ."values (null, '" . (int)$seller_id . "', '" . (int)$customer_id . "', CURRENT_TIMESTAMP, 0 )"
              );
      $query = $this->db->query($sql);
      return "Se agrego nuevo cliente_id: " .$customer_id ." como cliente del vendedor seller_id: " .$seller_id;
    }
  return "NO se puede añadir NUEVAMENTE al cliente_id: " .$customer_id ." por qué YA ES CLIENTE DEL VENDEDOR seller_id " .$seller_id;
  }

  public function getDefaultCustomerGroupSeller($seller_id)
  {
    //Retorna el grupo de clientes Default de un vendedor
    //El ID del grupo defaul sera almacenado en la tabla
    //oc_sellers.seller_group_id
    /*$sql = "select seller_group_id "
          ."from oc_sellers "
          ."where seller_id = '" . (int)$seller_id . "'";*/
    //Consulta en varias tablas
    $sql = "select distinct scg.customer_group_id, cgd.name "
          ."from oci_sellers_customer_groups scg "
          ."join oc_customer_group cg on scg.customer_group_id = cg.customer_group_id "
          ."join oc_customer_group_description cgd on cg.customer_group_id = cgd.customer_group_id "
          ."where seller_id = '" . (int)$seller_id . "' and cgd.name like '%Default' ";
    $query = $this->db->query($sql);
    //return $query->row['seller_group_id'];
    return $query->row['customer_group_id'];
  }
}
?>
