<?php
// Heading
$_['heading_title']        = 'Cuenta de Afiliado';

// Text
$_['text_account']         = 'Cuenta';
$_['text_my_account']      = 'Cuenta de Afiliado';
$_['text_my_tracking']     = 'Información de Seguimiento';
$_['text_my_transactions'] = 'Transacciones';
$_['text_edit']            = 'Editar Información de la Cuenta';
$_['text_password']        = 'Cambiar Contraseña';
$_['text_payment']         = 'Cambiar Preferencias de Pago';
$_['text_tracking']        = 'Código de seguimiento personalizado del Afiliado';
$_['text_transaction']     = 'Historial de Transacciones';