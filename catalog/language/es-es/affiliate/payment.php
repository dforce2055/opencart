<?php
// Heading
$_['heading_title']             = 'Método de Pago';

// Text
$_['text_account']              = 'Cuenta';
$_['text_payment']              = 'Pago';
$_['text_your_payment']         = 'Información de Pago';
$_['text_your_password']        = 'Contraseña';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Transferencia Bancaria';
$_['text_success']              = 'La Cuenta ha sido Actualizada Correctamente.';

// Entry
$_['entry_tax']                 = 'ID Impuesto';
$_['entry_payment']             = 'Método de Pago';
$_['entry_cheque']              = 'Cheque a Nombre de';
$_['entry_paypal']              = 'Email de PayPal';
$_['entry_bank_name']           = 'Nombre del Banco';
$_['entry_bank_branch_number']  = 'Número ABA/BSB (Número de sucursal)';
$_['entry_bank_swift_code']     = 'Código SWIFT';
$_['entry_bank_account_name']   = 'Nombre de Cuenta';
$_['entry_bank_account_number'] = 'Número de Cuenta';