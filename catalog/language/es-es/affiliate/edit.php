<?php
// Heading
$_['heading_title']     = 'Información';

// Text
$_['text_account']      = 'Cuenta';
$_['text_edit']         = 'Editar Información';
$_['text_your_details'] = 'Datos Personales';
$_['text_your_address'] = 'Dirección';
$_['text_success']      = 'La cuenta ha sido actualizada correctamente.';

// Entry
$_['entry_firstname']   = 'Nombre';
$_['entry_lastname']    = 'Apellido';
$_['entry_email']       = 'Email';
$_['entry_telephone']   = 'Teléfono';
$_['entry_fax']         = 'Fax';
$_['entry_company']     = 'Compañia';
$_['entry_website']     = 'Web';
$_['entry_address_1']   = 'Dirección 1';
$_['entry_address_2']   = 'Dirección 2';
$_['entry_postcode']    = 'Código Postal';
$_['entry_city']        = 'Ciudad';
$_['entry_country']     = 'País';
$_['entry_zone']        = 'Provincia/Estado';

// Error
$_['error_exists']      = 'El Email ingresado ya está registrado.';
$_['error_firstname']   = 'El nombre debe contener entre 1 y 32 caracteres.';
$_['error_lastname']    = 'El Apellido debe contener entre 1 y 32 caracteres.';
$_['error_email']       = 'Email Inválido.';
$_['error_telephone']   = 'El Teléfono debe contener entre 3 y 32 caracteres.';
$_['error_address_1']   = 'la Dirección 1 debe contener entre 3 y 128 caracteres.';
$_['error_city']        = 'La Ciudad debe contener entre 2 y 128 caracteres.';
$_['error_country']     = 'Seleccionar País.';
$_['error_zone']        = 'Seleccionar Provincia/Estado.';
$_['error_postcode']    = 'El Código Postal debe contener entre 2 y 10 caracteres.';