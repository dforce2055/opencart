<?php
// Heading
$_['heading_title']                 = 'Programa de Afiliados';

// Text
$_['text_account']                  = 'Cuenta';
$_['text_login']                    = 'Acceso';
$_['text_description']              = '<p>%s es gratuito y permite a los miembros obtener ingresos mediante la colocación de un enlace o enlaces en su Sitio Web. Cualquier venta realizada a los clientes que han hecho click en los enlaces generan la Comisión de Afiliado. La Tasa de Comisión es de %s.</p>';
$_['text_new_affiliate']            = 'Nuevo afiliado';
$_['text_register_account']         = '<p>Nuevo Afiliado.</p><p>Hacer click en Continuar para crear una nueva Cuenta de Afiliado. Tenga en cuenta que esto no está conectado de ninguna manera con su cuenta de cliente.</p>';
$_['text_returning_affiliate']      = 'Acceso Afiliados';
$_['text_i_am_returning_affiliate'] = 'Ya Afiliado. ';
$_['text_forgotten']                = 'Contraseña Olvidada';

// Entry
$_['entry_email']                   = 'Email de Afiliado';
$_['entry_password']                = 'Contraseña';

// Error
$_['error_login']                   = 'Acceso Incorrecto.';
$_['error_approved']                = 'La cuenta requiere Aprobación para poder iniciar sesión.';