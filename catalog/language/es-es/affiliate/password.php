<?php
// Heading
$_['heading_title']  = 'Cambiar Contraseña';

// Text
$_['text_account']   = 'Cuenta';
$_['text_password']  = 'Contraseña';
$_['text_success']   = 'Ca Contraseña se ha Actualizado Correctamente.';

// Entry
$_['entry_password'] = 'Contraseñ';
$_['entry_confirm']  = 'Confirmar Contraseña';

// Error
$_['error_password'] = 'La Contraseña debe contener entre 4 y 20 caracteres.';
$_['error_confirm']  = 'La Contraseña y su Confirmación no coinciden.';