<?php
// Heading
$_['heading_title']      = 'Transacciones';

// Column
$_['column_date_added']  = 'Fecha';
$_['column_description'] = 'Descripción';
$_['column_amount']      = 'Importe (%s)';

// Text
$_['text_account']       = 'Cuenta';
$_['text_transaction']   = 'Transacciónes';
$_['text_balance']       = 'Saldo actual:';
$_['text_empty']         = 'Sin Transacciones.';