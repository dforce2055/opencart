<?php
// Heading
$_['heading_title']   = 'Ha olvidado su contraseña?';

// Text
$_['text_account']    = 'Cuenta';
$_['text_forgotten']  = 'Contraseñ olvidada';
$_['text_your_email'] = 'Email';
$_['text_email']      = 'Introducir el Email asociado a la cuenta. Haga click en enviar para Reestablecer la Contraseña.';
$_['text_success']    = 'Una nueva Contraseña ha sido enviada a la dirección de Email proporcionada.';

// Entry
$_['entry_email']     = 'Email';

// Error
$_['error_email']     = 'El Email no se encuentra registrado, intentar nuevamente.';