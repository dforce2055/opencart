<?php
// Heading
$_['heading_title']  = 'Contacto: Solicitud de adhesión como cliente';

// Text
$_['text_location']  = 'Datos del vendedor';
$_['text_contact']   = 'Formulario para solicitar adhesión como cliente';
$_['text_address']   = 'Dirección:';
$_['text_email']     = 'E-Mail:';
$_['text_telephone'] = 'Teléfono:';
$_['text_fax']       = 'Fax:';
$_['text_message']   = '<p>¡Su solicitud de adhesión como cliente ha sido enviada al vendedor!<br/>We will notify you by mail, when you can already buy products from this seller.</p>';

$_['text_message1']   = 'Mensaje:';

$_['text_thanks']   = 'Gracias';
$_['text_new_customer']   = 'Un nuevo cliente se quiere registrar en su tienda';
$_['pending_authorization']   = '¡El cliente se encuentra pendiente de autorización!';

// Entry Fields
$_['entry_name']     = 'Nombre';
$_['entry_product_name']     = 'Nombre de Producto';
$_['entry_email']    = 'Dirección de E-Mail:';
$_['entry_enquiry']  = 'Consulta:';
$_['query']          = 'Me gustaría ser parte de su grupo de clientes...';
$_['entry_captcha']  = 'Ingrese el código en el recuadro de abajo:';
$_['entry_fname']    = 'Nombre';
$_['product']        = 'Producto';

// Email
$_['email_subject']  = 'Consultas por Producto: %s';

$_['text_error']        = '¡Vendedor no encontrado!';

// Errors
$_['error_name']        = '¡El nombre debe tener entre 3 y 32 caracteres!';
$_['error_email']       = '¡Dirección de E-Mail invalida!';
$_['error_enquiry']     = '¡La consulta debe tener entre 10 y 3000 caracteres!';
$_['error_captcha']     = '¡El código de verificación no coincide con la imagen!';
$_['error_parameters']  = '¡Faltan parametros customer_id and seller_id!';
$_['error_client']      = '¡Usted ya es cliente de este vendedor! Para poder consultar precios y agregar productos al carrito, el vendedor tiene que aprobarlo.';
?>
