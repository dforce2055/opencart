<?php
// Locale
$_['code']                  = 'es';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Si';
$_['text_no']               = 'No';
$_['text_none']             = ' --- Ninguno --- ';
$_['text_select']           = ' --- Seleccionar--- ';
$_['text_all_zones']        = 'Todas las Zonas';
$_['text_pagination']       = 'Página %d con %d productos de %d en total (%d Páginas)';
$_['text_loading']          = 'Cargando...';

// Buttons
$_['button_address_add']    = 'Agregar Direcció';
$_['button_back']           = 'Retroceder';
$_['button_continue']       = 'Continuar';
$_['button_cart']           = 'Agregar al Carro';
$_['button_cancel']         = 'Cancelar';
$_['button_compare']        = 'Comparar este Producto';
$_['button_wishlist']       = 'Agregar a Favoritos';
$_['button_wishlist_no_customers']       = 'Para poder agregar a favoritos debe ser cliente del vendedor';
$_['button_checkout']       = 'Caja';
$_['button_confirm']        = 'Confirmar Compra';
$_['button_coupon']         = 'Aplicar Cupón';
$_['button_delete']         = 'Borrar';
$_['button_download']       = 'Descarga';
$_['button_edit']           = 'Editar';
$_['button_filter']         = 'Refinar Busqueda';
$_['button_new_address']    = 'Nueva Dirección';
$_['button_change_address'] = 'Cambiar Direcci/oacute;n';
$_['button_reviews']        = 'Opiniones';
$_['button_write']          = 'Escribir Opinión';
$_['button_login']          = 'Iniciar sesión';
$_['button_update']         = 'Actualizar';
$_['button_remove']         = 'Eliminar';
$_['button_reorder']        = 'Reordenar';
$_['button_return']         = 'Volver';
$_['button_shopping']       = 'Continuar Comprando';
$_['button_search']         = 'Buscar';
$_['button_shipping']       = 'Solicitar Envío';
$_['button_submit']         = 'Enviar';
$_['button_guest']          = 'Comprar como Invitado';
$_['button_view']           = 'Vista';
$_['button_voucher']        = 'Aplicar Bono';
$_['button_upload']         = 'Subir Archivo';
$_['button_reward']         = 'Aplicar Puntos';
$_['button_quote']          = 'Obtener Cotizaciones';
$_['button_list']           = 'Lista';
$_['button_grid']           = 'Cuadrícula';
$_['button_map']            = 'Ver Google Map';
$_['text_blog']             = 'Blog';
$_['text_store']            = 'Comercio';
$_['text_blog_category']    = 'Categorías del Blog';
$_['text_its_free']         = 'Sin precio';

// Error
$_['error_exception']       = 'Codigo(%s) de error: %s en %s en linea %s';
$_['error_upload_1']        = 'El archivo escede the upload_max_filesize directiva en php.ini.';
$_['error_upload_2']        = 'El archivo excede el MAX_FILE_SIZE directiva especificado en el HTML form.';
$_['error_upload_3']        = 'El archivo subido se ha subido sólo parcialmente.';
$_['error_upload_4']        = 'Ningún archivo fue subido.';
$_['error_upload_6']        = 'Falta una carpeta temporal.';
$_['error_upload_7']        = 'Error al escribir el archivo en el disco.';
$_['error_upload_8']        = 'Extensión de Archivo Incorrecta.';
$_['error_upload_999']      = 'No hay código de error disponible.';
