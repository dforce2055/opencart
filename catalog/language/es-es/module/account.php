<?php
// Heading
$_['heading_title']    = 'Cuenta';

// Text
$_['text_register']    = 'Registro';
$_['text_login']       = 'Iniciar Sesión';
$_['text_logout']      = 'Salir';
$_['text_forgotten']   = 'Contraseña Olvidada';
$_['text_account']     = 'Cuenta';
$_['text_edit']        = 'Editar Cuenta';
$_['text_password']    = 'Contraseña';
$_['text_address']     = 'Libreta de Direcciones';
$_['text_wishlist']    = 'Favoritos';
$_['text_order']       = 'Historial de Pedidos';
$_['text_download']    = 'Descargas';
$_['text_reward']      = 'Puntos de Recompensa';
$_['text_return']      = 'Devoluciones';
$_['text_transaction'] = 'Transaciones';
$_['text_newsletter']  = 'Boletín de Noticias';
$_['text_recurring']   = 'Pagos Recurrentes';