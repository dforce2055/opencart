<?php
// Heading
$_['heading_title']    = 'Afiliado';

// Text
$_['text_register']    = 'Registro';
$_['text_login']       = 'Iniciar Sesión';
$_['text_logout']      = 'Salir';
$_['text_forgotten']   = 'Contraseña Olvidada';
$_['text_account']     = 'Cuenta';
$_['text_edit']        = 'Editar Cuenta';
$_['text_password']    = 'Contraseña';
$_['text_payment']     = 'Opciones de Pago';
$_['text_tracking']    = 'Programa de Afiliados';
$_['text_transaction'] = 'Transaciones';
