<?php
// Heading
$_['heading_title']     = 'Búsqueda';
$_['heading_tag']       = 'Etiqueta - ';

// Text
$_['text_search']       = 'Productos que coinciden con el criterio de búsqueda';
$_['text_keyword']      = 'Palabras clave';
$_['text_category']     = 'Todas las categorías';
$_['text_sub_category'] = 'Buscar en subcategorías';
$_['text_empty']        = 'Ningún producto coincide con el criterio de búsqueda.';
$_['text_quantity']     = 'Cantidad:';
$_['text_manufacturer'] = 'Marca:';
$_['text_model']        = 'Código de producto:';
$_['text_points']       = 'Puntos de Recompensa:';
$_['text_price']        = 'Precio:';
$_['text_tax']          = 'Sin Impuesto:';
$_['text_reviews']      = 'Basado en %s reseñas.';
$_['text_compare']      = 'Comparación de productos (%s)';
$_['text_sort']         = 'Ordenar por:';
$_['text_default']      = 'Defecto';
$_['text_name_asc']     = 'Nombre (A - Z)';
$_['text_name_desc']    = 'Nombre (Z - A)';
$_['text_price_asc']    = 'Precio (Bajo > Alto)';
$_['text_price_desc']   = 'Precio (Alto > Bajo)';
$_['text_rating_asc']   = 'Valoración (Más baja)';
$_['text_rating_desc']  = 'Valoración (Más alta)';
$_['text_model_asc']    = 'Modelo (A - Z)';
$_['text_model_desc']   = 'Modelo (Z - A)';
$_['text_limit']        = 'Mostrar:';

// Entry
$_['entry_search']      = 'Criterio de búsqueda';
$_['entry_description'] = 'Búscar en descripciones de productos';
