<?php
// Text
$_['text_search']              = 'Búsqueda';
$_['text_brand']               = 'Marca';
$_['text_manufacturer']        = 'Fabricante:';
$_['text_model']               = 'Código de producto:';
$_['text_reward']              = 'Puntos de Recompensa:';
$_['text_points']              = 'Precio en Puntos de Recompensa:';
$_['text_stock']               = 'Disponibilidad:';
$_['text_instock']             = 'En Stock';
$_['text_tax']                 = 'Sin Impuesto:';
$_['text_discount']            = ' o más ';
$_['text_option']              = 'Opciones disponibles';
$_['text_minimum']             = 'Este producto tiene una cantidad mínima de %s';
$_['text_reviews']             = '%s reseñas';
$_['text_write']               = 'Escribir una reseña';
$_['text_login']               = 'Por favor <a href="%s">inicie sesión</a> o <a href="%s">regístrese</a> para reseñar';
$_['text_no_reviews']          = 'No hay reseñas para este producto.';
$_['text_note']                = '<span class="text-danger">Nota: </span> El HTML no se traduce!';
$_['text_success']             = 'Gracias por su reseña. Ha sido enviada al webmaster para su aprobación.';
$_['text_related']             = 'Productos relacionados';
$_['text_tags']                = 'Etiquetas:';
$_['text_error']               = '¡No se encontró el producto!';
$_['text_payment_recurring']   = 'Perfil de pago';
$_['text_trial_description']   = '%s cada %d %s(s) para %d pago(s) entonces';
$_['text_payment_description'] = '%s cada %d %s(s) para %d pago(s)';
$_['text_payment_cancel']      = '%s cada %d %s(s) hasta cancelación';
$_['text_day']                 = 'Día';
$_['text_week']                = 'semana';
$_['text_semi_month']          = 'quincena';
$_['text_month']               = 'Mes';
$_['text_year']                = 'Año';

// Entry
$_['entry_qty']                = 'Cantidad';
$_['entry_name']               = 'Su nombre';
$_['entry_review']             = 'Su reseña';
$_['entry_rating']             = 'Valoración';
$_['entry_good']               = 'Bueno';
$_['entry_bad']                = 'Malo';

// Tabs
$_['tab_description']          = 'Descripción';
$_['tab_attribute']            = 'Especificación';
$_['tab_review']               = 'Opiniones (%s)';

// Error
$_['error_name']               = 'Advertencia: ¡Nombre de reseña debe tener entre 3 y 25 caracteres!';
$_['error_text']               = 'Advertencia: ¡Texto de reseña debe tener entre 25 y 1000 caracteres!';
$_['error_rating']             = 'Advertencia: ¡Por favor seleccione una valoración de reseña!';


// Multiseller
$_['column_seller']         = 'Vendedor';
$_['contact_seller']        = 'Contactar al Vendedor';
$_['text_more']             = 'o más';
$_['text_sellers']          = '1 o más Vendedores';
$_['text_seller']           = 'Otro Vendedor';
$_['text_choices']          = 'Otras opciones';
$_['text_seller_rating']    = 'Calificación del Vendedor';
$_['text_price']            = 'Precio';
$_['text_action']           = 'Acción';
$_['btn_details']           = 'Detalles';
$_['text_no_price']  = "¿Por qué no puede ver el precio?";
$_['text_register_to_see_price']          = 'Debe ser cliente de este vendedor para poder comprarle';
$_['text_sing_up']  = "Quiero comprarle a este vendedor";
$_['text_sing_up_help']  = "Para poder comprarle a este vendedor, se tiene que registrat como cliente";
