<?php
// Heading
$_['heading_title']     = 'Comparación de productos';

// Text
$_['text_product']      = 'Detalles de producto';
$_['text_name']         = 'Producto';
$_['text_image']        = 'Imagen';
$_['text_price']        = 'Precio';
$_['text_model']        = 'Modelo';
$_['text_manufacturer'] = 'Marca';
$_['text_availability'] = 'Disponibilidad';
$_['text_instock']      = 'En Stock';
$_['text_rating']       = 'Valoración';
$_['text_reviews']      = 'Basado en %s reseñas.';
$_['text_summary']      = 'Resumen';
$_['text_weight']       = 'Peso';
$_['text_dimension']    = 'Dimensions (L x A x A)';
$_['text_compare']      = 'Comparación de productos (%s)';
$_['text_success']      = 'Éxito: ¡Ha agregado <a href="%s">%s</a> a su <a href="%s">Comparación de productos</a>!';
$_['text_remove']       = 'Éxito: ¡Ha modificado su Comparación de productos!';
$_['text_empty']        = 'No ha seleccionado ningún producto para comparar.';