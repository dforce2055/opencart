<?php
// Text
$_['text_refine']       = 'Refinar la busqueda';
$_['text_product']      = 'Productos';
$_['text_error']        = '¡No se encontro el Vendedor!';
$_['text_empty']        = 'No hay Productos en la lista de este Vendedor.';
$_['text_quantity']     = 'Cantidad:';
$_['text_manufacturer'] = 'Marca:';
$_['text_model']        = 'Código de Producto:';
$_['text_points']       = 'Puntos Recompensa:';
$_['text_price']        = 'Precio:';
$_['text_tax']          = 'Sin Impuestos:';
$_['text_reviews']      = 'Basado en %s Opiniones.';
$_['text_no_reviews']          = 'No hay Opiniones para este Vendedor.';
$_['text_compare']      = 'Compare Productos (%s)';
$_['text_display']      = 'Mostrar:';
$_['text_list']         = 'Lista';
$_['text_grid']         = 'Cuadrícula';
$_['text_sort']         = 'Ordenar por:';
$_['text_default']      = 'Defecto';
$_['text_name_asc']     = 'Nombre (A - Z)';
$_['text_name_desc']    = 'Nombre (Z - A)';
$_['text_price_asc']    = 'Precio (Menor > Mayor)';
$_['text_price_desc']   = 'Precio (Mayor > Menor)';
$_['text_rating_asc']   = 'Valoración (Más baja)';
$_['text_rating_desc']  = 'Valoración (Más alta)';
$_['text_model_asc']    = 'Modelo (A - Z)';
$_['text_model_desc']   = 'Modelo (Z - A)';
$_['text_limit']        = 'Mostrar:';
$_['text_seller'] = 'Vendedor';

// Sellers
$_['column_seller']          = 'Vendedor';
$_['contact_seller']          = 'Contactar al Vendedor';
$_['text_more']          = 'Más';
$_['text_sellers']          = 'Vendedores';
$_['text_seller']          = 'Vendedor';
$_['text_choices']          = 'Otras Opciones';
$_['text_seller_rating']          = 'Calificación del Vendedor';
$_['text_price']          = 'Precio';
$_['text_action']          = 'Acción';
?>
