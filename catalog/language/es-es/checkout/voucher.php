<?php
// Heading
$_['heading_title'] = 'Utilizar Cupón de Regalo';

// Text
$_['text_success']  = 'El Cupón de Regalo se ha aplicado.';

// Entry
$_['entry_voucher'] = 'Introducir el Código del Cupón de Regalo';
$_['entry_voucher'] = 'Introducir el Código del Cupón de Regalo';

// Error
$_['error_voucher'] = 'El Cupón de Regalo es Inválido o el Saldo se ha Agotado.';
$_['error_empty']   = 'Introducir el Código del Cupón de Regalo.';