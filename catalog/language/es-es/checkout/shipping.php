<?php
// Heading
$_['heading_title']        = 'Estimación de Envío y Tasas';

// Text
$_['text_success']         = 'El precio estimado de Envío se ha aplicado.';
$_['text_shipping']        = 'Ingresar el destino para obtener un Precio Estimado de Envío.';
$_['text_shipping_method'] = 'Seleccionar el Método de Envío preferido para usar en esta pedido.';

// Entry
$_['entry_country']        = 'País';
$_['entry_zone']           = 'Provincia';
$_['entry_postcode']       = 'Código Postal';

// Error
$_['error_postcode']       = 'El Código Postal debe contener entre 2 y 10 caracteres.';
$_['error_country']        = 'Seleccionar País.';
$_['error_zone']           = 'Seleccionar Provincia/Estado.';
$_['error_shipping']       = 'Método de Envío Obligatorio.';
$_['error_no_shipping']    = 'No hay Opciones de Envío disponibles. <a href="%s">Contactar</a> para recibir asistencia.';