<?php
// Heading
$_['heading_title'] = 'Utilizar el Código del Cupón';

// Text
$_['text_success']  = 'El Cupón de Descuento se ha aplicado exitosamente.';

// Entry
$_['entry_coupon']  = 'Introducir Cupón';

// Error
$_['error_coupon']  = 'Cupón Inválido, expirado o alcanzó el Límite de Uso.';
$_['error_empty']   = 'Introducir Código de Cupón.';