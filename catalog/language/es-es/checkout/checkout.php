<?php
// Heading
$_['heading_title']                  = 'Pagar';

// Text
$_['text_cart']                      = 'Carro de Compras';
$_['text_checkout_option']           = 'Paso 1: Opciones de Compra';
$_['text_checkout_account']          = 'Paso 2: Datos de la Cuenta de Facturación';
$_['text_checkout_payment_address']  = 'Paso 2: Datos de Facturación';
$_['text_checkout_shipping_address'] = 'Paso 3: Datos de Entrega';
$_['text_checkout_shipping_method']  = 'Paso 4: Método de Envío';
$_['text_checkout_payment_method']   = 'Paso 5: Método de Pago';
$_['text_checkout_confirm']          = 'Paso 6: Confirmar Compra';
$_['text_modify']                    = 'Modificar &raquo;';
$_['text_new_customer']              = 'Nuevo Cliente';
$_['text_returning_customer']        = 'Cliente Registrado';
$_['text_checkout']                  = 'Opciones de Pago:';
$_['text_i_am_returning_customer']   = 'Cliente Registrado';
$_['text_register']                  = 'Registrar Cuenta';
$_['text_guest']                     = 'Comprar como Invitado';
$_['text_register_account']          = 'Al crear una cuenta se podrán realizar compras, revisar los pedidos, el estado, o realizar un seguimiento de las operaciones anteriores.';
$_['text_forgotten']                 = 'Olvidó su Contraseña';
$_['text_your_details']              = 'Datos Personales';
$_['text_your_address']              = 'Dirección';
$_['text_your_password']             = 'Contraseña';
$_['text_agree']                     = 'He leído y estoy de acuerdo con los <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Usar una Nueva Dirección';
$_['text_address_existing']          = 'Usar una Dirección Existente';
$_['text_shipping_method']           = 'Seleccionar Método de Envío.';
$_['text_payment_method']            = 'Seleccionar Método de Pago.';
$_['text_comments']                  = 'Agregar Comentarios al Pedido';
$_['text_recurring']                 = 'Elemento Recurrente';
$_['text_payment_recurring']           = 'Perfil de Pago';
$_['text_trial_description']         = '%s todos %d %s(s) para %d pagos posteriores';
$_['text_payment_description']       = '%s todos %d %s(s) para %d pago(s)';
$_['text_payment_until_canceled_description'] = '%s todos %d %s(s) hasta que se cancele';
$_['text_day']                       = 'día';
$_['text_week']                      = 'semana';
$_['text_semi_month']                = 'quincena';
$_['text_month']                     = 'mes';
$_['text_year']                      = 'año';

// Column
$_['column_name']                    = 'Nombre del Producto';
$_['column_model']                   = 'Modelo';
$_['column_quantity']                = 'Cantidad';
$_['column_price']                   = 'Precio Unitario';
$_['column_total']                   = 'Total';

// Entry
$_['entry_email_address']            = 'Dirección de Email';
$_['entry_email']                    = 'Email';
$_['entry_password']                 = 'Contraseña';
$_['entry_confirm']                  = 'Confirmar Contraseña';
$_['entry_firstname']                = 'Nombre';
$_['entry_lastname']                 = 'Apellido';
$_['entry_telephone']                = 'Teléfono';
$_['entry_fax']                      = 'Fax';
$_['entry_address']                  = 'Seleccionar Dirección';
$_['entry_company']                  = 'Empresa';
$_['entry_customer_group']           = 'Tipo de Cliente';
$_['entry_address_1']                = 'Dirección 1';
$_['entry_address_2']                = 'Dirección 2';
$_['entry_postcode']                 = 'Código Postal';
$_['entry_city']                     = 'Ciudad';
$_['entry_country']                  = 'País';
$_['entry_zone']                     = 'Provincia/Estado';
$_['entry_newsletter']               = 'Suscripción al Boletín de Noticias %s .';
$_['entry_shipping'] 	             = 'Las Direcciones de Entrega y Facturación son los mismas';

// Error
$_['error_warning']                  = 'Hubo un problema al tratar de procesar el Pedido. Si el problema persiste seleccionar otro Método de Pago o ponerse en contacto desde <a href="%s">aquí</a>.';
$_['error_login']                    = 'Acceso Incorrecto.';
$_['error_approved']                 = 'La Cuenta requiere Aprobación para poder Iniciar Sesión.';
$_['error_exists']                   = 'Email ya Registrado.';
$_['error_firstname']                = 'El Nombre debe contener entre 1 y 32 caracteres.';
$_['error_lastname']                 = 'El Apellido debe contener entre 1 y 32 caracteres.';
$_['error_email']                    = 'Email Inválido.';
$_['error_telephone']                = 'El Teléfono debe contener entre 3 y 32 caracteres.';
$_['error_password']                 = 'La Contraseña debe contener entre 3 y 20 caracteres.';
$_['error_confirm']                  = 'La Contraseña y su Confirmación no coinciden.';
$_['error_address_1']                = 'La Dirección 1 debe contener entre 3 y 128 caracteres.';
$_['error_city']                     = 'La Ciudad debe contener entre 2 y 128 caracteres.';
$_['error_postcode']                 = 'El Código Postal debe contener entre 2 y 10 caracteres.';
$_['error_country']                  = 'Seleccionar País.';
$_['error_zone']                     = 'Seleccionar Provincia/Estado.';
$_['error_agree']                    = 'Se debe estar de acuerdo con %s.';
$_['error_address']                  = 'Seleccionar la Dirección.';
$_['error_shipping']                 = 'Método del Envío Obligatorio.';
$_['error_no_shipping']              = 'No hay Opciones de Envío Disponibles. <a href="%s">Contactar</a> para recibir asistencia.';
$_['error_payment']                  = 'Forma de pago Obligatoria.';
$_['error_no_payment']               = 'No hay Opciones de Pago disponibles. <a href="%s">Contactar</a> para recibir asistencia.';
$_['error_custom_field']             = '%s Obligatorio.';

// Multiseller
$_['text_seller']            = 'Producto de';
$_['text_status']            = 'Estado';
$_['text_products']          = 'Productos';
$_['text_review']            = 'Opiniones';
$_['text_rate']              = 'Calificación de este Vendedor';
