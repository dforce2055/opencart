<?php
// Heading
$_['heading_title']        = 'Pedido Realizado.';

// Text
$_['text_basket']          = 'Carro de compras';
$_['text_checkout']        = 'Pagar';
$_['text_success']         = 'Operación Exitosa';
$_['text_customer']        = '<p>El Pedido ha sido procesado Exitosamente.</p><p>Se puede ver el historial de pedidos, yendo a <a href="%s">Cuenta</a> y clickeando en <a href="%s">Historial</a>.</p><p>Si la compra tiene descargas asociadas, puede ir a <a href="%s">Descargas</a>.</p>';
$_['text_guest']           = '<p>El Pedido ha sido procesado Exitosamente.</p><p>Ante cualquier pregunta <a href="%s">contactarse</a>.</p><p>Gracias por Comprar.</p>';