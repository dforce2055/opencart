<?php
// Heading
$_['heading_title'] = 'Utilizar Puntos de Recompensa (Disponibles %s)';

// Text
$_['text_success']  = 'Los Puntos de Recompensa se han aplicado exitosamente.';

// Entry
$_['entry_reward']  = 'Puntos a Utilizar (Máx. %s)';

// Error
$_['error_reward']  = 'Introducir cantidad de Puntos de Recompensa a utilizar.';
$_['error_points']  = 'No hay %s Puntos de Recompensa.';
$_['error_maximum'] = 'El Número Máximo de Puntos que se puede aplicar es %s.';