<?php
// Heading
$_['heading_title']  = 'Contacto';

// Text
$_['text_location']  = 'Ubicación';
$_['text_store']     = 'Tiendas';
$_['text_contact']   = 'Formulario de Contacto';
$_['text_address']   = 'Dirección';
$_['text_telephone'] = 'Teléfono';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Horario';
$_['text_comment']   = 'Comentario';
$_['text_success']   = '<p>La consulta ha sido enviada exitosamente.</p>';

// Entry
$_['entry_name']     = 'Nombre';
$_['entry_email']    = 'Email';
$_['entry_enquiry']  = 'Comentario';
$_['entry_captcha']  = 'Introducir el Siguiente Código';

// Email
$_['email_subject']  = 'Comentario';

// Errors
$_['error_name']     = 'El Nombre debe contener entre 3 y 32 caracteres.';
$_['error_email']    = 'Email Inválido.';
$_['error_enquiry']  = 'El Comentario debe contener entre 10 y 3000 caracteres.';
$_['error_captcha']  = 'El Código de Verificación no coincide con la Imagen.';
