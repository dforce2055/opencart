<?php
// Heading
$_['heading_title'] = 'Salir de la cuenta';

// Text
$_['text_message']  = '<p>El Contenido del Carro de Compras ha sido guardado y será restaurado al volver a entrar en la cuenta. </p>';
$_['text_account']  = 'Cuenta';
$_['text_logout']   = 'Salir';