<?php
// Heading
$_['heading_title']      = 'Puntos de Recompensa';

// Column
$_['column_date_added']  = 'Agregados el';
$_['column_description'] = 'Descripción';
$_['column_points']      = 'Puntos';

// Text
$_['text_account']       = 'Cuenta';
$_['text_reward']        = 'Puntos de Recompensa';
$_['text_total']         = 'El Número total de puntos es:';
$_['text_empty']         = 'Sin Puntos de Recompensa.';