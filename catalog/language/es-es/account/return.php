<?php
// Heading
$_['heading_title']      = 'Devolución de Productos';

// Text
$_['text_account']       = 'Cuenta';
$_['text_return']        = 'Información de Devolución';
$_['text_return_detail'] = 'Detalles de la Devolución';
$_['text_description']   = 'Completar el siguiente formulario para solicitar un Número de Devolución.';
$_['text_order']         = 'Información de Pedido';
$_['text_product']       = 'Información del Producto y Motivo de Devolución';
$_['text_message']       = '<p>Gracias por enviar la Solicitud de Devolución. La solicitud ha sido enviada al departamento correspondiente para su procesamiento.</ P> <p> Se notificará por Email sobre el estado de la Solicitud.</p>';
$_['text_return_id']     = 'ID de Devolución:';
$_['text_order_id']      = 'ID de pedido:';
$_['text_date_ordered']  = 'Fecha de Pedido:';
$_['text_status']        = 'Estado:';
$_['text_date_added']    = 'Fecha Agregado:';
$_['text_comment']       = 'Volver a los Comentarios';
$_['text_history']       = 'Volver al historial';
$_['text_empty']         = 'No ha hecho ninguna Devolució anteriormente.';
$_['text_agree']         = 'He leído y estoy de acuerdo con el <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'ID de Devolución';
$_['column_order_id']    = 'ID de pedido';
$_['column_status']      = 'Estado';
$_['column_date_added']  = 'Fecha Agregado';
$_['column_customer']    = 'Cliente';
$_['column_product']     = 'Nombre de Producto';
$_['column_model']       = 'Modelo';
$_['column_quantity']    = 'Cantidad';
$_['column_price']       = 'Precio';
$_['column_opened']      = 'Abierto';
$_['column_comment']     = 'Comentario';
$_['column_reason']      = 'Razón';
$_['column_action']      = 'Acción';

// Entry
$_['entry_order_id']     = 'ID de pedido';
$_['entry_date_ordered'] = 'Fecha de Pedido';
$_['entry_firstname']    = 'Nombre';
$_['entry_lastname']     = 'Apellido';
$_['entry_email']        = 'Email';
$_['entry_telephone']    = 'Teléfono';
$_['entry_product']      = 'Nombre Producto';
$_['entry_model']        = 'Código';
$_['entry_quantity']     = 'Cantidad';
$_['entry_reason']       = 'Razón de Devolución';
$_['entry_opened']       = 'El Producto ha sido abierto';
$_['entry_fault_detail'] = 'Defectuoso u otros detalles';
$_['entry_captcha']      = 'Introducir el Siguiente Código';

// Error
$_['text_error']         = 'La Solicitud no se pudo encontrar.';
$_['error_order_id']     = 'ID de pedido Obligatorio.';
$_['error_firstname']    = 'El Nombre debe contener entre 1 y 32 caracteres.';
$_['error_lastname']     = 'El Apellido debe contener entre 1 y 32 caracteres.';
$_['error_email']        = 'Email Inválido.';
$_['error_telephone']    = 'El Teléfono debe contener entre 3 y 32 caracteres.';
$_['error_product']      = 'El Nombre del producto debe contener entre 3 y 255 caracteres.';
$_['error_model']        = 'El Modelo del Producto debe contener entre 3 y 64 caracteres.';
$_['error_reason']       = 'Seleccionar Motivo Devolución.';
$_['error_captcha']      = 'El Código de Verificación no coincide con la imagen.';
$_['error_agree']        = 'Se debe estar de acuerdo con la %s.';