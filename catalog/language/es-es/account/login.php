<?php
// Heading
$_['heading_title']                = 'Inicio de Sesión';

// Text
$_['text_account']                 = 'Cuenta';
$_['text_login']                   = 'Iniciar Sesión';
$_['text_new_customer']            = 'Nuevo Cliente';
$_['text_register']                = 'Registrar Cuenta';
$_['text_register_account']        = 'Al crear una cuenta es posible realizar compras rapidamente, revisar los estados de los pedidos , y realizar un seguimiento de las operaciones anteriores. Las cuentas con los datos incompletos serán bloqueadas.';
$_['text_returning_customer']      = 'Cliente Existente';
$_['text_i_am_returning_customer'] = 'Nuevo Cliente';
$_['text_forgotten']               = 'Olvidó su contraseña';

// Entry
$_['entry_email']                  = 'Email';
$_['entry_password']               = 'Contraseña';

// Error
$_['error_login']                  = 'Acceso Incorrecto.';
$_['error_approved']               = 'La cuenta requiere aprobación para poder iniciar sesión.';
