<?php
// header
$_['heading_title']  = 'Resetear Contraseña';

// Text
$_['text_account']   = 'Cuenta';
$_['text_password']  = 'Ingresar Nueva Contraseña.';
$_['text_success']   = 'La Contraseña ha sido Actualizada Exitosamente.';

// Entry
$_['entry_password'] = 'Contraseña';
$_['entry_confirm']  = 'Confirmación';

// Error
$_['error_password'] = 'La Contraseña debe contener entre 4 y 20 caracteres.';
$_['error_confirm']  = 'La Contraseña y su Confirmación no coinciden.';
$_['error_code']     = 'El Código para Resetear la Contraseña es inválido o ha sido utilizado previamente.';