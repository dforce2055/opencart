<?php
// Heading
$_['heading_title']  = 'Cambiar Contraseña';

// Text
$_['text_account']   = 'Cuenta';
$_['text_password']  = 'Tu Contraseña';
$_['text_success']   = 'La Contraseña se ha actualizado correctamente.';

// Entry
$_['entry_password'] = 'Contraseña';
$_['entry_confirm']  = 'Confirma Contraseña';

// Error
$_['error_password'] = 'La Contraseña debe contener entre 4 y 20 caracteres.';
$_['error_confirm']  = 'La Contraseña y su Confirmaci&acute;n no coinciden.';