<?php
// Heading
$_['heading_title']      = 'Información de la Cuenta';

// Text
$_['text_account']       = 'Cuenta';
$_['text_edit']          = 'Editar Información';
$_['text_your_details']  = 'Datos Personales';
$_['text_success']       = 'La cuenta ha sido actualizado correctamente.';

// Entry
$_['entry_firstname']    = 'Nombre';
$_['entry_lastname']     = 'Apellido';
$_['entry_email']        = 'Email';
$_['entry_telephone']    = 'Teléfono';
$_['entry_fax']          = 'Fax';

// Error
$_['error_exists']       = 'Email ya registrado.';
$_['error_firstname']    = 'El nombre debe contener entre 1 y 32 caracteres.';
$_['error_lastname']     = 'El Apellido debe contener entre 1 y 32 caracteres.';
$_['error_email']        = 'Email inválido.';
$_['error_telephone']    = 'El Teléfono debe contener entre 3 y 32 caracteres.';
$_['error_custom_field'] = '%s Obligatorio.';
