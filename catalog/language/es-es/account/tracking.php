<?php
// Heading
$_['heading_title']    = 'Seguimiento de Afiliados';

// Text
$_['text_account']     = 'Cuenta';
$_['text_description'] = 'Para asegurarse que se pagan las Comisiones se debe realizar un seguimiento mediante la colocación de un Código Web. Se pueden utilizar siguientes herramientas para generar enlaces. %s.';

// Entry
$_['entry_code']       = 'Código de Seguimiento';
$_['entry_generator']  = 'Generador de Link de Seguimiento';
$_['entry_link']       = 'Link de Seguimiento';

// Help
$_['help_generator']  = 'Escribir el nombre de un producto alque se desea relacionar.';