<?php
// Heading
$_['heading_title']        = 'Registrar cuenta';

// Text
$_['text_account']         = 'Cuenta';
$_['text_register']        = 'Registrar';
$_['text_account_already'] = 'Si la cuenta ya ha sido registrada ingresar <a href="%s">aquí</a>.';
$_['text_your_details']    = 'Datos Personales';
$_['text_your_address']    = 'Dirección';
$_['text_newsletter']      = 'Boletín';
$_['text_your_password']   = 'Contraseña';
$_['text_agree']           = 'He leído y estoy de acuerdo con el <a href="%s" class="agree"> <b>% s </ b> </a>';

// Entry
$_['entry_customer_group'] = 'Tipo de Cliente';
$_['entry_firstname']      = 'Nombre';
$_['entry_lastname']       = 'Apellido';
$_['entry_email']          = 'Email';
$_['entry_telephone']      = 'Teléfono';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Empresa';
$_['entry_address_1']      = 'Dirección 1';
$_['entry_address_2']      = 'Dirección 2';
$_['entry_postcode']       = 'Código Postal';
$_['entry_city']           = 'Ciudad';
$_['entry_country']        = 'País';
$_['entry_zone']           = 'Provincia/Estado';
$_['entry_newsletter']     = 'Subscribir';
$_['entry_password']       = 'Contraseña';
$_['entry_confirm']        = 'Confirma Contraseña';

// Error
$_['error_exists']         = 'Email ya registrado.';
$_['error_firstname']      = 'El Nombre debe contener entre 1 y 32 caracteres.';
$_['error_lastname']       = 'El Apellido debe contener entre 1 y 32 caracteres.';
$_['error_email']          = 'Email Inválido.';
$_['error_telephone']      = 'El Teléfono debe contener entre 3 y 32 caracteres.';
$_['error_address_1']      = 'La Dirección 1 debe contener entre 3 y 128 caracteres.';
$_['error_city']           = 'La Ciudad debe contener entre 2 y 128 caracteres.';
$_['error_postcode']       = 'El Código Postal debe contener entre 2 y 10 caracteres.';
$_['error_country']        = 'Seleccionar País.';
$_['error_zone']           = 'Seleccionar Provincia/Estado.';
$_['error_custom_field']   = '%s Obligatorio.';
$_['error_password']       = 'La Contraseña debe contener entre 4 y 20 caracteres.';
$_['error_confirm']        = 'La Contraseña y su Confirmación no coinciden.';
$_['error_agree']          = 'Se debe aceptar el %s.';