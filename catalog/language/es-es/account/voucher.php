<?php
// Heading
$_['heading_title']    = 'Compra un Cupón de Regalo';

// Text
$_['text_account']     = 'Cuenta';
$_['text_voucher']     = 'Cupones de Regalo';
$_['text_description'] = 'Este Cupón de regalo será enviado por Email al Destinatario después que el Pedido haya sido pagado.';
$_['text_agree']       = 'Entiendo que los Cupones de Regalo no son reembolsables.';
$_['text_message']     = '<p>Gracias por comprar un Cupón de regalo. Una vez que se haya completado el pedido, se enviará un Email al Destinatario con detalles decómo canjear el Cupón de regalo.</p>';
$_['text_for']         = '%s Cupón Regalo para %s';

// Entry
$_['entry_to_name']    = 'Nombre del Destinatario';
$_['entry_to_email']   = 'Email del Destinatario';
$_['entry_from_name']  = 'Nombre';
$_['entry_from_email'] = 'Email';
$_['entry_theme']      = 'Razón del Cupón Regalo';
$_['entry_message']    = 'Mensaje';
$_['entry_amount']     = 'Importe';

// Help
$_['help_message']     = 'Opcional';
$_['help_amount']      = 'El valor debe estar entre %s y %s';

// Error
$_['error_to_name']    = 'El Nombre de Destinatario debe contener entre 1 y 64 caracteres.';
$_['error_from_name']  = 'El Nombre debe contener entre 1 y 64 caracteres.';
$_['error_email']      = 'Email Inválido.';
$_['error_theme']      = 'Seleccionar Razón.';
$_['error_amount']     = 'El importe debe estar entre %s y %s.';
$_['error_agree']      = 'Se debe estar de acuerdo en que los Cupones de Regalo no son reembolsables.';
