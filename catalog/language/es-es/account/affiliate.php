<?php
// Heading
$_['heading_title']             = 'Información de Afiliado';

// Text
$_['text_account']              = 'Cuenta';
$_['text_affiliate']            = 'Afiliado';
$_['text_my_affiliate']         = 'Mi cuenta de Afiliado';
$_['text_payment']              = 'Información de Pago';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Transferencia Bancaria';
$_['text_success']              = 'Éxito: ¡Su cuenta ha sido creada correctamente¡';
$_['text_agree']                = 'Leí y estoy de acuerdo <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_company']             = 'Compañía';
$_['entry_website']             = 'Sitio Web';
$_['entry_tax']                 = 'ID Impuesto';
$_['entry_payment']             = 'Método de Pago';
$_['entry_cheque']              = 'Beneficiario del cheque';
$_['entry_paypal']              = 'Cuenta de E-mail de Paypal';
$_['entry_bank_name']           = 'Nombre del Banco';
$_['entry_bank_branch_number']  = 'Número ABA/BSB (Número de sucursal)';
$_['entry_bank_swift_code']     = 'Código SWIFT';
$_['entry_bank_account_name']   = 'Nombre de Cuenta';
$_['entry_bank_account_number'] = 'Número de Cuenta';

// Error
$_['error_agree']               = 'Advertencia: ¡Usted debe aceptar el %s!';
$_['error_cheque']              = '¡Es necesario el Nombre del beneficiario del cheque!';
$_['error_paypal']              = '¡El E-Mail de Paypal parece inválido!';
$_['error_bank_account_name']   = '¡Es necesario el Nombre de Cuenta!';
$_['error_bank_account_number'] = '¡Es necesario Número de Cuenta!';
$_['error_custom_field']        = '¡%s necesario!';