<?php
// Heading
$_['heading_title']    = 'Suscribirse al Boletín';

// Text
$_['text_account']     = 'Cuenta';
$_['text_newsletter']  = 'Boletín';
$_['text_success']     = 'La Suscripción al Boletín se ha actualizado exitosamente.';

// Entry
$_['entry_newsletter'] = 'Subscribir';
