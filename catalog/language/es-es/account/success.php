<?php
// Heading
$_['heading_title'] = 'Cuenta Creada Exitosamente.';

// Text
$_['text_message']  = '<p> La nueva cuenta ha sido creada Exitosamente.</p><p>Ahora es Miembro de % s.</p><p>Si existen dudas acerca del funcionamiento de este sistema, enviar un Email con las preguntas pertinentes.</p><p>Una confirmación ha sido enviada al Email proporcionado. Si no es recibida en menos de una hora, <a href="%s"> contactarse </a>.</p>';
$_['text_approval'] = '<p>Gracias por registrarse en % s.</p><p> Se notificará por Email una vez que la cuenta ha sido activada.</p> <p> Si existen dudas acerca del funcionamiento de esta Web, contactarse <a href="%s"> aquí </a>.</p>';
$_['text_account']  = 'Cuenta';
$_['text_success']  = 'Operación Exitosa';
