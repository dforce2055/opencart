<?php
// Heading
$_['heading_title']     = 'Cuenta Descargas';

// Text
$_['text_account']      = 'Cuenta';
$_['text_downloads']    = 'Descargas';
$_['text_empty']        = 'No ha hecho ningún pedido descargable anteriormente.';

// Column
$_['column_order_id']   = 'ID de pedido';
$_['column_name']       = 'Nombre';
$_['column_size']       = 'Peso';
$_['column_date_added'] = 'Agregado el';