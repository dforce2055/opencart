<?php
// Heading
$_['heading_title']         = 'Historial de Pedidos';

// Text
$_['text_account']          = 'Cuenta';
$_['text_ip']               = 'Ip';
$_['text_order']            = 'Información de Pedidos';
$_['text_order_detail']     = 'Detalles del Pedido';
$_['text_invoice_no']       = 'Factura N&deg;:';
$_['text_order_id']         = 'ID de pedido:';
$_['text_date_added']       = 'Fecha:';
$_['text_shipping_address'] = 'Dirección de Envío';
$_['text_shipping_method']  = 'Método de Envío:';
$_['text_payment_address']  = 'Direcció de Pago';
$_['text_payment_method']   = 'Método de Pago:';
$_['text_comment']          = 'Comentarios de Pedidos';
$_['text_history']          = 'Historial de Pedidos';
$_['text_success']          = 'Se ha Agregado <a href="%s">% s </a> al Carro de Compras <a href="%s"> </a>.';
$_['text_empty']            = 'No se ha realizado ningún pedido.';
$_['text_error']            = 'El pedido solicitado no se pudo encontrar.';
$_['text_seller']            = 'Producto de:';
$_['text_status']            = 'Estado:';
$_['text_products']            = 'Productos:';
$_['text_review']            = 'Opiniones';
$_['text_rate']             = 'Calificar Vendedor';
$_['button_Save']            = 'Guardar';

// Column
$_['column_order_id']       = 'ID de pedido';
$_['column_product']        = 'N&deg; de Productos';
$_['column_customer']       = 'Cliente';
$_['column_name']           = 'Nombre del Producto';
$_['column_model']          = 'Modelo';
$_['column_quantity']       = 'Cantidad';
$_['column_price']          = 'Precio';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';
$_['column_date_added']     = 'Fecha Agregado';
$_['column_status']         = 'Estado del Pedido';
$_['column_comment']        = 'Comentario';

// Error
$_['error_reorder']         = '%s actualmente no está disponible para ser pedido.';

// Multiseller
$_['text_seller']            = 'Producto de';
$_['text_status']            = 'Estado';
$_['text_products']          = 'Productos';
$_['text_review']            = 'Opiniones';
$_['text_rate']              = 'Calificación de este Vendedor';
