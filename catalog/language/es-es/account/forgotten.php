<?php
// Heading
$_['heading_title']   = 'Ha olvidado su contraseña?';

// Text
$_['text_account']    = 'Cuenta';
$_['text_forgotten']  = 'Olvidó su Contraseña';
$_['text_your_email'] = 'Email';
$_['text_email']      = 'Introducir el Email asociado con la cuenta. Hacer click en enviar para Reestablecer la Contraseña.';
$_['text_success']    = 'Una nueva contraseña ha sido enviada al Email.';

// Entry
$_['entry_email']     = 'Email';

// Error
$_['error_email']     = 'El Email no se encuentra registrado, intentar nuevamente.';