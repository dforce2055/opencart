<?php
// Heading
$_['heading_title']       = 'Opinión';

// Text
$_['text_success']      = 'Éxito: ¡Ha enviado correctamente la Opinión!';
$_['your_name']    = 'Tu Nombre';
$_['your_review']    = 'Tu Opinión';
$_['btn_cancel']    = 'Cancelar';
$_['btn_save']    = 'Guardar';

// Column
$_['column_product']    = 'Producto';
$_['column_author']     = 'Autor';
$_['column_rating']     = 'Valoración';
$_['column_status']     = 'Estado';
$_['column_date_added'] = 'Fecha de agregación';
$_['column_action']     = 'Acción';

$_['button_Save']            = 'Guardar';

// Entry
$_['entry_product']     = 'Producto:<br/><span class="help">(Autocompletar)</span>';
$_['entry_author']      = 'Autor:';
$_['entry_rating']      = 'Valoración:';
$_['entry_status']      = 'Estado:';
$_['entry_text']        = 'Texto:';
$_['entry_good']        = 'Bueno';
$_['entry_bad']         = 'Malo';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permiso para modificar la Opinión!';
$_['error_seller']     = '¡Se requiere Vendedor!';
$_['error_order']     = '¡Se requiere ID del pedido!';
$_['error_author']      = '¡El nombre debe tener entre 3 y 64 caracteres!';
$_['error_text']        = '¡El texto de la Opinión debe tener al menos 1 caracter!';
$_['error_rating']      = '¡La Opinión requiere una Valoración!';
?>
