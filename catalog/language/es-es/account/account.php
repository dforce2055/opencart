<?php
// Heading
$_['heading_title']      = 'Cuenta';

// Text
$_['text_account']       = 'Cuenta';
$_['text_my_account']    = 'Cuenta';
$_['text_my_orders']     = 'Pedidos';
$_['text_my_affiliate']   = 'Mi Cuenta de Afiliado';
$_['text_my_newsletter'] = 'Boletín';
$_['text_edit']          = 'Modificar Información';
$_['text_password']      = 'Cambiar Contraseña';
$_['text_address']       = 'Modificar Dirección';
$_['text_wishlist']      = 'Modificar Favoritos';
$_['text_order']         = 'Ver Historial de Compras';
$_['text_download']      = 'Descargas';
$_['text_reward']        = 'Puntos';
$_['text_return']        = 'Ver Solicitudes de Devolución';
$_['text_transaction']   = 'Transacciones';
$_['text_newsletter']    = 'Subscribirse/Desubscribirse del Boletín';
$_['text_recurring']     = 'Pagos Recurrentes';
$_['text_transactions']  = 'Transacciones';
$_['text_affiliate_add']  = 'Registrarse para cuenta de afiliado';
$_['text_your_transaction']   = 'Sus Transacciones';
