<?php
// Text
$_['text_success']           = 'El Pedido se ha modificado exitosamente.';

// Error
$_['error_permission']       = 'Sin permiso para acceder a la API.';
$_['error_customer']         = 'Los Detalles de Cliente necesitan configurarse.';
$_['error_payment_address']  = 'Dirección de Pago Obligatoria.';
$_['error_payment_method']   = 'Método de Pago Obligatorio.';
$_['error_shipping_address'] = 'Dirección de Envió Obligatoria.'; 
$_['error_shipping_method']  = 'Método de Envío Obligatorio.';
$_['error_stock']            = 'Los productos marcados con *** no están disponibles en la cantidad deseada o no están en stock.';
$_['error_minimum']          = 'La Cantidad mínima de pedido para %s es %s.';
$_['error_not_found']        = 'El producto no se pudo encontrar.';