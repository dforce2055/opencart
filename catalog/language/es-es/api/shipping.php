<?php
// Text
$_['text_address']       = 'La Dirección de Envío fijada exitosamente.';
$_['text_method']        = 'Método de Envío establecido exitosamente.';

// Error
$_['error_permission']   = 'Sin permiso para acceder a la API.';
$_['error_firstname']    = 'El Nombre debe contener entre 1 y 32 caracteres.';
$_['error_lastname']     = 'El Apellido debe contener entre 1 y 32 caracteres.';
$_['error_address_1']    = 'La Dirección 1 debe contener entre 3 y 128 caracteres.';
$_['error_city']         = 'La Ciudad debe contener entre 3 y 128 caracteres.';
$_['error_postcode']     = 'El Código Postal debe contener entre 2 y 10 caracteres.';
$_['error_country']      = 'Seleccionar País.';
$_['error_zone']         = 'Seleccionar Provincia/Estado.';
$_['error_custom_field'] = '%s Obligatorio.';
$_['error_address']      = 'Dirección de Envío Obligatoria.';
$_['error_method']       = 'Método de Envío Obligatorio.';
$_['error_no_shipping']  = 'No hay Opciones de Envío Disponibles.';