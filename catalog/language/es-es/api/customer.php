<?php
// Text
$_['text_success']       = 'Se han modificado los Clientes.';

// Error
$_['error_permission']   = 'Sin permiso para acceder a la API.';
$_['error_firstname']    = 'El nombre debe contener entre 1 y 32 caracteres.';
$_['error_lastname']     = 'El Apellido debe contener entre 1 y 32 caracteres.';
$_['error_email']        = 'Email Inválido.';
$_['error_telephone']    = 'El Teléfono debe contener entre 3 y 32 caracteres.';
$_['error_custom_field'] = '%s Obligatorio.';