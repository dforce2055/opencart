<?php
// Text
$_['text_success']     = 'Se ha modificado el Carro de Compras.';

// Error
$_['error_permission'] = 'Sin permiso para acceder a la API.';
$_['error_stock']      = 'Los productos marcados con *** no están disponibles en la cantidad deseada o no hay en stock.';
$_['error_minimum']    = 'La Cantidad mínima de pedido para %s es %s.';
$_['error_store']      = 'El producto no se puede comprar.';
$_['error_required']   = '%s Obligatorio.';