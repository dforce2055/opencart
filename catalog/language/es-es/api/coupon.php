<?php
// Text
$_['text_success']     = 'El Cupón de Descuento se ha aplicado.';

// Error
$_['error_permission'] = 'Sin permiso para acceder a la API.';
$_['error_coupon']     = 'Cupón Inválido, Vencido o llegó al Límite de Uso.';