<?php
// Text
$_['text_upload']    = '¡El archivo fue cargado con éxito!';

// Error
$_['error_filename'] = '¡El nombre de archivo debe tener entre 3 y 64 caracteres!';
$_['error_filetype'] = '¡Tipo de archivo inválido!';
$_['error_upload']   = '¡Es necesario subir!';