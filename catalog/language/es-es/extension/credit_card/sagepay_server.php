<?php
// Heading
$_['heading_title']         = 'Servidor de Tarjetas Sagepay';

// Text
$_['text_empty']		    = 'No tienes tarjetas guardadas';
$_['text_account']          = 'Cuenta';
$_['text_card']			    = 'Administrar Servidor de tarjetas SagePay';
$_['text_fail_card']	    = 'Hubo un problema al eliminar su tarjeta SagePay. Comuníquese con el administrador de la tienda para obtener ayuda.';
$_['text_success_card']     = 'Tarjeta SagePay fue eliminada correctamente';
$_['text_success_add_card'] = 'Tarjeta SagePay fue Agregada correctamente';

// Column
$_['column_type']		    = 'Tipo de Tarjeta';
$_['column_digits']	        = 'Ultimos digitos';
$_['column_expiry']	     	= 'Vencimiento';

// Entry
$_['entry_cc_owner']        = 'Títular de la tarjeta';
$_['entry_cc_type']         = 'Tipo de Tarjeta';
$_['entry_cc_number']       = 'Número de Tarjeta';
$_['entry_cc_expire_date']  = 'Fecha de vencimiento';
$_['entry_cc_cvv2']         = 'Código de seguridad (CVV2)';
$_['entry_cc_choice']       = 'Elegir una tarjeta existente';

// Button
$_['button_add_card']       = 'Agregar Tarjeta';
$_['button_new_card']       = 'Agregar nueva tarjeta';



