<?php

$_['heading_title'] = 'Cuadro Tarjetas de Crédito';

$_['text_account'] = 'Cuenta';
$_['text_back'] = 'Volver';
$_['text_delete'] = 'Eliminar';
$_['text_no_cards'] = 'No tiene tarjetas alamacenadas en nuestra base de datos.';
$_['text_card_ends_in'] = '%s tarjeta termina en &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; %s';
$_['text_warning_card'] = '¿Confirma eliminación de la tarjeta? Puede agregarla nuevamente en su próximo pago.';
$_['text_success_card_delete'] = '¡Éxito! Esta tarjeta fue correctamente eliminada.';