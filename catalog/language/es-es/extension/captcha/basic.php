<?php
// Text
$_['text_captcha']  = 'Captcha';

// Entry
$_['entry_captcha'] = 'Ingrese el código de verificación';

// Error
$_['error_captcha'] = '¡El código de verificación no coincide con la imagen!';
