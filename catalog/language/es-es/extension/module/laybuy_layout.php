<?php
// Heading
$_['heading_title']            = 'Lay-Buy Información';

// Text
$_['text_reference_info']      = 'Información de Referencia';
$_['text_laybuy_ref_no']       = 'ID de Referencia de Lay-Buy:';
$_['text_paypal_profile_id']   = 'ID perfil Paypal:';
$_['text_payment_plan']        = 'Plan de pago';
$_['text_status']              = 'Estado:';
$_['text_amount']              = 'Cantidad:';
$_['text_downpayment_percent'] = 'Porcentaje de anticipo:';
$_['text_months']              = 'Meses:';
$_['text_downpayment_amount']  = 'Importe de pago inicial:';
$_['text_payment_amounts']     = 'Montos de pago:';
$_['text_first_payment_due']   = 'Primer Pago:';
$_['text_last_payment_due']    = 'Último Pago:';
$_['text_downpayment']         = 'Pago inicial';
$_['text_month']               = 'Meses';

// Column
$_['column_instalment']        = 'Plazo';
$_['column_amount']            = 'Cantidad';
$_['column_date']              = 'Fecha';
$_['column_pp_trans_id']       = 'ID Transacción de PayPal';
$_['column_status']            = 'Estado';