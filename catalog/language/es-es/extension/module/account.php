<?php
// Heading
$_['heading_title']    = 'Cuenta';

// Text
$_['text_register']    = 'Registrarse';
$_['text_login']       = 'Iniciar Sesión';
$_['text_logout']      = 'Cerrar Sesión';
$_['text_forgotten']   = 'Olvide mi contraseña';
$_['text_account']     = 'Mi cuenta';
$_['text_edit']        = 'Editar Cuenta';
$_['text_password']    = 'Contraseña';
$_['text_address']     = 'Libreta de Direcciones';
$_['text_wishlist']    = 'Favoritos';
$_['text_order']       = 'Historial de Pedidos';
$_['text_download']    = 'Descargas';
$_['text_reward']      = 'Puntos de Recompensa';
$_['text_return']      = 'Devoluciones';
$_['text_transaction'] = 'Transacciones';
$_['text_newsletter']  = 'Boletín de noticias';
$_['text_recurring']   = 'Pagos recurrentes';
