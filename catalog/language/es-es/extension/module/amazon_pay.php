<?php
// Heading
$_['heading_title'] = 'Pagar con Amazon';

// Text
$_['text_module'] = 'Modulo';
$_['text_success'] = 'Éxito: ¡Has modificado el modulo de pago con Amazon!';
$_['text_content_top'] = 'Contenido superior';
$_['text_content_bottom'] = 'Contenido inferior';
$_['text_column_left'] = 'Columna izquierda';
$_['text_column_right'] = 'Column derecha';
$_['text_pwa_button'] = 'Pagar con Amazon';
$_['text_pay_button'] = 'Pagar';
$_['text_a_button'] = 'A';
$_['text_gold_button'] = 'Dorado';
$_['text_darkgray_button'] = 'Gris oscuro';
$_['text_lightgray_button'] = 'Gris claro';
$_['text_small_button'] = 'Pequeño';
$_['text_medium_button'] = 'Mediano';
$_['text_large_button'] = 'Grande';
$_['text_x_large_button'] = 'X-Grande';

//Entry
$_['entry_button_type'] = 'Tipo de Botón';
$_['entry_button_colour'] = 'Color de Botón';
$_['entry_button_size'] = 'Largo de Botón';
$_['entry_layout'] = 'Diseño';
$_['entry_position'] = 'Posición';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Ordenar Pedidos';

//Error
$_['error_permission'] = 'Advertencia: ¡No tien permisos para modificar el Modulo de inicio de Sesión con Amazon!';