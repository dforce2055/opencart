<?php
// Calculator
$_['text_checkout_title']      = 'Pagar en cuotas';
$_['text_choose_plan']         = 'Elegir el plan';
$_['text_choose_deposit']      = 'Elegir depósito';
$_['text_monthly_payments']    = 'pagos mensuales de';
$_['text_months']              = 'meses';
$_['text_term']                = 'Terminos';
$_['text_deposit']             = 'Depósito';
$_['text_credit_amount']       = 'Costo del crédito';
$_['text_amount_payable']      = 'Total a pagar';
$_['text_total_interest']      = 'Total de intereses APR';
$_['text_monthly_installment'] = 'Pagos mensuales';
$_['text_redirection']         = 'Será redirigido a Divido para completar esta solicitud de financiación cuando confirme su pedido';