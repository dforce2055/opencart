<?php
// Text
$_['text_title']          = 'PayPal Pago exprés';
$_['text_canceled']       = 'Éxito: ¡Has Logrado con éxito realizar este pago!';

// Button
$_['button_cancel']       = 'Cancelar Pago recurrente';

// Error
$_['error_not_cancelled'] = 'Error: %s';
$_['error_not_found']     = 'No se pudo cancelar el perfil recurrente';