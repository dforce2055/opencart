<?php
// Text
$_['text_title']                = 'Cuadrad';
$_['text_canceled']             = 'Éxit: ¡Has logrado cancelar exitosamente este pago! Le vamos a envíar un mail de confirmación.';
$_['text_confirm_cancel']       = '¿Esta seguro que quiere cancelar el pago recurrente?';
$_['text_order_history_cancel'] = 'Has cancelado tu perfil recurrente. Su tarjeta ya no se cobrará.';

// Button
$_['button_cancel']             = 'Cancelar el Pago Recurrente';

// Error
$_['error_not_cancelled']       = 'Error: %s';
$_['error_not_found']           = 'No se pudo cancelar el perfil recurrente';