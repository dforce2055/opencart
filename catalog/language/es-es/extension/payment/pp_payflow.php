<?php
// Texto
$_['text_title']				= 'Tarjeta de crédito o débito (Proceso seguro por PayPal)';
$_['text_credit_card']			= 'Detalles de tarjeta de Crédito';
$_['text_start_date']			= '(si está disponible)';
$_['text_issue']				= '(sólo para tarjetas Maestro y Solo)';
$_['text_wait']					= '¡Por favor espere!';

// Entry
$_['entry_cc_owner']			= 'Titular de tarjeta:';
$_['entry_cc_type']				= 'Tipo de Tarjeta:';
$_['entry_cc_number']			= 'Número de tarjeta:';
$_['entry_cc_start_date']		= 'Tarjeta Válida Desde Fecha:';
$_['entry_cc_expire_date']		= 'Fecha de expiración de Tarjeta:';
$_['entry_cc_cvv2']				= 'Código de seguridad de tarjeta (CVV2):';
$_['entry_cc_issue']			= 'Número de emisión de tarjeta:';

// Error
$_['error_required']			= 'Advertencia: Todos los campos de información de pago son necesarios.';
$_['error_general']				= 'Advertencia: Ocurrió un problema general con la transacción. Por favor intente de nuevo.';
$_['error_config']				= 'Advertencia: Error de configuración del módulo de pagos. Por favor verifique las credenciales de inicio de sesión.';
$_['error_address']				= 'Advertencia: Falló una coincidencia de estado, ciudad, código postal, y dirección de pago. Por favor intente de nuevo.';
$_['error_declined']			= 'Advertencia: Esta transaccion fue rechazada. Por favor intente de nuevo.';
$_['error_invalid']				= 'Advertencia: La información de tarjeta de crédito provista es inválida. Por favor intente de nuevo.';