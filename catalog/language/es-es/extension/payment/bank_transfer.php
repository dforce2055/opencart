<?php
// Text
$_['text_title']       = 'Transferencia bancaria';
$_['text_instruction'] = 'Instrucciones de transferencia bancaria';
$_['text_description'] = 'Por favor transfiera el monto total a la siguiente cuenta bancaria.';
$_['text_payment']     = 'Su pedido will no será enviado hasta recibir su pago.';