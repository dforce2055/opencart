<?php
// Text
$_['text_new_card']                     = '+ Agregar Nueva tarjeta';
$_['text_loading']                      = 'Cargando... Por favor espere...';
$_['text_card_details']                 = 'Detalles de tarjeta';
$_['text_saved_card']                   = 'Usar Tarjeta Guardada:';
$_['text_card_ends_in']                 = 'Pagar con tarjeta existente %s que termina en XXXX XXXX XXXX %s';
$_['text_card_number']                  = 'Número de tarjeta:';
$_['text_card_expiry']                  = 'Expiración de tarjeta (MM/YY):';
$_['text_card_cvc']                     = 'Código de seguridad de tarjeta (CVC):';
$_['text_card_zip']                     = 'Código postal Tarjeta:';
$_['text_card_save']                    = '¿Guardar tarjeta para uso futuro?';
$_['text_trial']                        = '%s cada %s %s para %s pagos entonces ';
$_['text_recurring']                    = '%s cada %s %s';
$_['text_length']                       = ' para %s pagos';
$_['text_cron_subject']                 = 'Resumen Square CRON job';
$_['text_cron_message']                 = 'Lista de tareas CRON hechas por su extensión Square:';
$_['text_squareup_profile_suspended']   = ' Sus pagos recurrentes se han suspendido. Por favor contáctenos para más detalles.';
$_['text_squareup_trial_expired']       = ' Su período de prueba ha expirado.';
$_['text_squareup_recurring_expired']   = ' Sus pagos recurrentes han expirado. Éste fue su último pago..';
$_['text_cron_summary_token_heading']   = 'Actualización de token de accesoo:';
$_['text_cron_summary_token_updated']   = 'Token de acceso actualizado exitosamente!';
$_['text_cron_summary_error_heading']   = 'Errores de transacción:';
$_['text_cron_summary_fail_heading']    = 'Transacciones fallidas (Perfiles suspendidos):';
$_['text_cron_summary_success_heading'] = 'Transacciones exitosas:';
$_['text_cron_fail_charge']             = 'Perfil <strong>#%s</strong> no pudo ser cargado con <strong>%s</strong>';
$_['text_cron_success_charge']          = 'Perfil <strong>#%s</strong> fue cargado con <strong>%s</strong>';
$_['text_card_placeholder']             = 'XXXX XXXX XXXX XXXX';
$_['text_cvv']                          = 'CVV';
$_['text_expiry']                       = 'MM/YY';
$_['text_default_squareup_name']        = 'Tarjeta de crédito / débito';
$_['text_token_issue_customer_error']   = 'Estamos experimentando un corte técnico en nuestro sistema de pago. Por favor intente de nuevo más tarde.';
$_['text_token_revoked_subject']        = 'Su Token de acceso a Square fue revocado!';
$_['text_token_revoked_message']        = "El acceso a su cuenta Square por parte de la extensión Square fue revocada mediante el Tablero Square. Debe verificar sus credenciales de aplicación en la configuración de la extensióñ y volver a conectar.";
$_['text_token_expired_subject']        = 'Su token de acceso a Square ha expirado!';
$_['text_token_expired_message']        = "El token que conectaba a la extension de pago Square con su cuenta Square ha expirado. Debe verificar sus credenciales de aplicación y tarea CRON en la configuracióñ de extensióñn y volver a conectar.";

// Error
$_['error_browser_not_supported']       = 'Error: El sistema de pago ya no soporta su navegador web. Por favor actualícelo o utilice otro.';
$_['error_card_invalid']                = 'Error: ¡La tarjeta es inválid!';
$_['error_squareup_cron_token']         = 'Error: No se pudo actualizar el token de acceso. Por favor conecte su extensióñ de Pago Square mediante el panel de administración OpenCart.';

// Warning
$_['warning_test_mode']                 = 'Advertencia: ¡El Modo Sandbox está activado! Las transacciones parecerán proceder, pero no se llevarán a cabo los cargos.';

// Statuses
$_['squareup_status_comment_authorized']    = 'La transacción de tarjeta ha sido autorizada pero no ha sido capturada aún.';
$_['squareup_status_comment_captured']      = 'La transacción de tarjeta fue autorizada y subsecuentemente capturada (o sea, completada).';
$_['squareup_status_comment_voided']        = 'La transacción de tarjeta fue autorizada y subsecuentemente anulada (o sea, cancelada).   ';
$_['squareup_status_comment_failed']        = 'La transacción de tarjeta falló.';

// Override errors
$_['squareup_override_error_billing_address.country']       = 'País de dirección de pago inválido. Por favor modifíquelo e intente de nuevo.';
$_['squareup_override_error_shipping_address.country']      = 'País de dirección de envío inválido. Por favor modifíquelo e intente de nuevo.';
$_['squareup_override_error_email_address']                 = 'Su dirección e-mail de cliente es inválida. Por favor modifíquela e intente de nuevo.';
$_['squareup_override_error_phone_number']                  = 'Su múmero de teléfono de cliente es inválido. Por favor modifíquelo e intente de nuevo.';
$_['squareup_error_field']                                  = ' Campo: %s';