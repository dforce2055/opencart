<?php

$_['text_title'] = 'Tarjeta de Crédito / Débito';
$_['button_confirm'] = 'Confirmar';

$_['text_postcode_check'] = 'Comprobación Código Postal %s';
$_['text_security_code_check'] = 'Comprobación CVV2: %s';
$_['text_address_check'] = 'Comprobación Dirección: %s';
$_['text_not_given'] = 'No dado';
$_['text_not_checked'] = 'No comprobado';
$_['text_match'] = 'Coincide';
$_['text_not_match'] = 'Sin coincidencia';
$_['text_payment_details'] = 'Detalles de pago';

$_['entry_card_type'] = 'Tipo de tarjeta';