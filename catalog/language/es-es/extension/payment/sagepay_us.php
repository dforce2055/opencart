<?php
// Texto
$_['text_title']				= 'Tarjeta de Crédito / Débito (SagePay)';
$_['text_credit_card']			= 'Detalles de tarjeta de Crédito';

// Entry
$_['entry_cc_owner']			= 'Dueño de tarjeta';
$_['entry_cc_number']			= 'Número de tarjeta';
$_['entry_cc_expire_date']		= 'Fecha de expiración de tarjeta';
$_['entry_cc_cvv2']				= 'Código de seguridad de tarjeta (CVV2)';