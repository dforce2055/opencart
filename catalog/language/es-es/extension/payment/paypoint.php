<?php
// Heading
$_['heading_title']				= 'Gracias por comprar con %s .... ';

// Texto
$_['text_title']				= 'Tarjeta de Crédito / Débito (PayPoint)';
$_['text_response']				= 'Respuesta de PayPoint:';
$_['text_success']				= '... su pago fue recibido exitosamente.';
$_['text_success_wait']			= '<b><span style="color: #FF0000">Por favor espere...</span></b> mientras terminamos de procesar su pedido.<br>Si no es redireccionado automáticamente en 10 segundos, por favor haga click <a href="%s">aquí</a>.';
$_['text_failure']				= '... ¡Su pago ha sido cancelado!';
$_['text_failure_wait']			= '<b><span style="color: #FF0000">Por favor espere...</span></b><br>Si no es redireccionado automáticamente en 10 segundos, por favor haga click <a href="%s">aquí</a>.';