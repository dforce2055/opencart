<?php
// Texto
$_['text_title']			 = 'Tarjeta de Crédito / Débito (Worldpay)';
$_['text_credit_card']		 = 'Detalles de tarjeta';
$_['text_card_type']		 = 'Tipo de Tarjeta: ';
$_['text_card_name']		 = 'Nombre de Tarjeta: ';
$_['text_card_digits']		 = 'Últimos dígitos: ';
$_['text_card_expiry']		 = 'Expiración: ';
$_['text_trial']			 = '%s cada %s %s por %s pagos entonces';
$_['text_recurring']		 = '%s cada %s %s';
$_['text_length']			 = ' por %s pagos';
$_['text_confirm_delete']	 = 'Está seguro de que desea eliminar esta tarjeta';
$_['text_card_success']		 = 'Tarjeta removida exitosamente';
$_['text_card_error']		 = 'Ocurrió un error al remover la tarjeta. Por favor contacte al administrador de la tienda por ayuda.';

// Entry
$_['entry_card']			 = 'Tarjeta nueva o existente: ';
$_['entry_card_existing']	 = 'Existente';
$_['entry_card_new']		 = 'Nueva';
$_['entry_card_save']		 = 'Recordar detalles de tarjeta';
$_['entry_cc_cvc']			 = 'Código de verificación de tarjeta (CVC)';
$_['entry_cc_choice']		 = 'Seleccionar una tarjeta existente';

// Button
$_['button_delete_card']	 = 'Borrar tarjeta';

// Error
$_['error_process_order']	 = 'Ocurrió un error al procesar su pedido. Por favor contacte al administrador de la tienda por ayuda.';