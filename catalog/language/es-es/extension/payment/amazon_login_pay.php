<?php
// Heading
$_['heading_title']				= 'Pagar con Amazon';
$_['heading_address']			= 'Por favor seleccione una dirección de envío';
$_['heading_payment']			= 'Por favor seleccione un método de pago';
$_['heading_confirm']			= 'Resumen de pedido';
// Text
$_['text_back']					= 'Volver';
$_['text_cart']					= 'Carrito';
$_['text_confirm']				= 'Confirmar';
$_['text_continue']				= 'Continuar';
$_['text_lpa']					= 'Pagar con Amazon';
$_['text_enter_coupon']			= 'Ingrese su código de cupón aquí. Si no tiene uno, déjelo vacío.';
$_['text_coupon']				= 'Cupón';
$_['text_tax_other']			= 'Impuestos / Otros gastos administrativos';
$_['text_success_title']		= '¡Su pedido ha sido realizado!';
$_['text_payment_success']		= 'Su pedido ha sido realizado con éxito. Detalles debajo';
// Error
$_['error_payment_method']		= 'Por favor seleccione un método de pago';
$_['error_shipping']			= 'Por favor seleccione a método de envío';
$_['error_shipping_address']	= 'Por favor seleccione una dirección de envío';
$_['error_shipping_methods']	= 'Ocurrió un error al obtener su dirección de Amazon. Por favor contacte al administrador de la tienda por ayuda.';
$_['error_no_shipping_methods'] = 'No hay opciones de envío para la dirección seleccionada. Por favor seleccione otra dirección de envío.';
$_['error_process_order']		= 'Ocurrió un error al procesar su pedido. Por favor contacte al administrador de la tienda por ayuda.';
$_['error_login']				= 'Inicio de sesión fallido';
$_['error_login_email']			= 'Inicio de sesión fallido: %s dirección email de la cuenta no coincide con dirección de email de Amazon';
$_['error_minimum']             = 'El monto mínimo de pedido para pagar con Amazon es %s!';
