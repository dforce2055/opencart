<?php
/**
 * @package		OpenCart
 * @author		Meng Wenbin
 * @copyright	Copyright (c) 2010 - 2017, Chengdu Guangda Network Technology Co. Ltd. (https://www.opencart.cn/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.cn
 */

// Heading
$_['heading_title']              = 'Pagar con código QR Wechat';

// Text
$_['text_title']                 = 'Pagar con Wechat';
$_['text_checkout']              = 'Proceso de compra en línea';
$_['text_qrcode']                = 'CódigoQr';
$_['text_qrcode_description']    = 'Por favor escanee el código QR en la aplicación WeChat para pagar su pedido!';
