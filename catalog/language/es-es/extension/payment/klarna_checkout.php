<?php
// Heading
$_['heading_title']				   = 'Proceso de compra en línea Klarna';
$_['heading_title_success']		   = 'Su solicitud de Proceso de compra en línea Klarna ha sido creada!';

// Texto
$_['text_title']				   = 'Proceso de compra en línea Klarna ';
$_['text_basket']				   = 'Carrito de compras';
$_['text_checkout']				   = 'Proceso de compra en línea';
$_['text_success']				   = 'Éxito';
$_['text_choose_shipping_method']  = 'Seleccionar método de envío';
$_['text_sales_tax']			   = 'Impuesto de ventas';
$_['text_newsletter']			   = 'Subscríbase a nuestro boletín informativo';
$_['entry_terms']						 = 'Terminos & Condiciones';
