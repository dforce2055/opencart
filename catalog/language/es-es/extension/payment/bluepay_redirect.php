<?php
// Text
$_['text_title']				= 'Tarjeta de Crédito / Débito (BluePay)';
$_['text_credit_card']			= 'Detalles de tarjeta';
$_['text_description']			= 'Items on %s Order No: %s';
$_['text_card_type']			= 'Tipo de Tarjeta: ';
$_['text_card_name']			= 'Nombre de tarjeta: ';
$_['text_card_digits']			= 'Últimos dígitos: ';
$_['text_card_expiry']			= 'Expiración: ';

// Returned text
$_['text_transaction_error']	= 'Ocurrió un error al procesar su transacción - ';

// Entry
$_['entry_card']				= 'Tarjeta nueva o existente: ';
$_['entry_card_existing']		= 'Existente';
$_['entry_card_new']			= 'Nueva';
$_['entry_card_save']			= 'Recordar detalles de tarjeta';
$_['entry_cc_owner']			= 'Dueño de tarjeta';
$_['entry_cc_number']			= 'Número de tarjeta';
$_['entry_cc_start_date']		= 'Tarjeta válida desde fecha';
$_['entry_cc_expire_date']		= 'Fecha de expiración de tarjeta';
$_['entry_cc_cvv2']				= 'Código de seguridad de tarjeta (CVV2)';
$_['entry_cc_address']			= 'Dirección';
$_['entry_cc_city']				= 'Ciudad';
$_['entry_cc_state']			= 'Estado';
$_['entry_cc_zipcode']			= 'Código postal';
$_['entry_cc_phone']			= 'Teléfono';
$_['entry_cc_email']			= 'E-mail';
$_['entry_cc_choice']			= 'Seleccione una tarjeta existente';