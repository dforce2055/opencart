	<?php
// Texto
$_['text_title']				= 'Tarjeta de Crédito / Débito (SagePay)';
$_['text_credit_card']			= 'Detalles de tarjeta';
$_['text_description']			= 'Items en %s Pedido Nro: %s';
$_['text_card_type']			= 'Tipo de Tarjeta: ';
$_['text_card_name']			= 'Nombre de Tarjeta: ';
$_['text_card_digits']			= 'Últimos dígitos: ';
$_['text_card_expiry']			= 'Expiración: ';
$_['text_trial']				= '%s cada %s %s por %s pagos entonces ';
$_['text_recurring']			= '%s cada %s %s';
$_['text_length']				= ' cada %s pagos';
$_['text_success']				= 'Su pago ha sido autorizado.';
$_['text_decline']				= 'Su pago ha sido rechazado.';
$_['text_bank_error']			= 'Ocurrió un error al procesar su solicitud con el banco.';
$_['text_transaction_error']	= 'Ocurrió un error al procesar su transacción.';
$_['text_generic_error']		= 'Ocurrió un error al procesar su solicitud.';
$_['text_hash_failed']			= 'Verificación de hash fallida. No intente pagar nuevamente puesto que el estado del pago es desconocido. Por favor contacte al vendedor.';
$_['text_link']					= 'Por favor haga click <a href="%s">aquí</a> para continuar';
$_['text_confirm_delete']		= 'Está seguro de que desea eliminar la tarjeta?';

// Entry
$_['entry_card']				= 'Tarjeta nueva o existente: ';
$_['entry_card_existing']		= 'Existente';
$_['entry_card_new']			= 'Nueva';
$_['entry_card_save']			= 'Recordar detalles de Tarjeta para uso futuro';
$_['entry_cc_choice']			= 'Seleccionar una tarjeta existente';

// Button
$_['button_delete_card']		= 'Borrar tarjeta seleccionada';