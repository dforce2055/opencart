<?php
// Texto
$_['text_title']				= 'Tarjeta de crédito o débito (Proceso seguro por PayPal)';
$_['text_wait']					= '¡Por favor espere!';
$_['text_credit_card']			= 'Detalles de tarjeta de Crédito';

// Entry
$_['entry_cc_type']				= 'Tipo de tarjeta';
$_['entry_cc_number']			= 'Número de tarjeta';
$_['entry_cc_start_date']		= 'Tarjeta Válida Desde Fecha';
$_['entry_cc_expire_date']		= 'Fecha de expiración de tarjeta';
$_['entry_cc_cvv2']				= 'Código de seguridad de tarjeta (CVV2)';
$_['entry_cc_issue']			= 'Número de emisión de tarjeta';

// Help
$_['help_start_date']			= '(si está disponible)';
$_['help_issue']				= '(sólo para tarjetas Maestro y Solo)';