<?php
$_['text_title'] = 'Tarjeta de Crédito / Débito';
$_['text_card_details'] = 'Detalles de tarjeta';
$_['text_wait'] = 'Procesando su pago';
$_['text_auth_code'] = 'Código de autorización: %s';
$_['text_postcode_check'] = 'Comprobación Código Postal %s';
$_['text_security_code_check'] = 'Comprobación CVV2: %s';
$_['text_address_check'] = 'Comprobación Dirección: %s';
$_['text_3d_secure_check'] = '3D Secure: %s';
$_['text_not_given'] = 'No dado';
$_['text_not_checked'] = 'No comprobado';
$_['text_match'] = 'Coincide';
$_['text_not_match'] = 'Sin coincidencia';
$_['text_authenticated'] = 'Autenticado';
$_['text_not_authenticated'] = 'No autenticado';
$_['text_authentication_not_completed'] = 'Intentado pero sin completar';
$_['text_unable_to_perform'] = 'No se pudo realizar';
$_['text_transaction_declined'] = 'Su banco ha rechazado la transacción. Por favor utilice otro método de pago.';
$_['text_transaction_failed'] = 'No se pudo procesar el pago. Por favor compruebe los detalles ingresados.';
$_['text_connection_error'] = 'Por favor intente de nuevo más tarde or utilice otro método de pago.';

$_['entry_type'] = 'Tipo de tarjeta';
$_['entry_number'] = 'Número de tarjeta';
$_['entry_expire_date'] = 'Fecha de expiración';
$_['entry_cvv2'] = 'Código de seguridad (CVV2)';

$_['button_confirm'] = 'Confirmar';

$_['error_failure'] = 'No se pudo completar la transacción. Por favor intente de nuevo más tarde or utilice otro método de pago.';