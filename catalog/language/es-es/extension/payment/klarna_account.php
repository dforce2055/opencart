<?php
// Texto
$_['text_title']				= 'Cuenta Klarna - Pague desde %s/mes';
$_['text_terms']				= '<span id="klarna_account_toc"></span><script type="text/javascript">var terms = new Klarna.Terms.Cuenta({el: \'klarna_account_toc\', eid: \'%s\', country: \'%s\'});</script>';
$_['text_information']			= 'Información de cuenta Klarna';
$_['text_additional']			= 'Cuenta de Klarna requiere informacióñ adicional antes de poder procesar su pedido.';
$_['text_male']					= 'Masculino';
$_['text_female']				= 'Femenino';
$_['text_year']					= 'Año';
$_['text_month']				= 'Mes';
$_['text_day']					= 'Día';
$_['text_payment_option']		= 'Opciones de pago';
$_['text_single_payment']		= 'Un pago';
$_['text_monthly_payment']		= '%s - %s por mes';
$_['text_comment']				= 'ID de Factura Klarna: %s' . "\n" . '%s/%s: %.4f';

// Entry
$_['entry_gender']				= 'Género';
$_['entry_pno']					= 'Número Personal';
$_['entry_dob']					= 'Fecha de nacimiento';
$_['entry_phone_no']			= 'Número de teléfono';
$_['entry_street']				= 'Calle';
$_['entry_house_no']			= 'Nro. Casa';
$_['entry_house_ext']			= 'Ext. Casa';
$_['entry_company']				= 'Número de registro de compañía';

// Help
$_['help_pno']					= 'Por favor ingrese su número de Seguridad Social aquí';
$_['help_phone_no']				= 'Por favor ingrese su Número de teléfono.';
$_['help_street']				= 'Por favor tenga en cuenta que el envío sólo se puede hacer a la dirección registrada cuando paga con Klarna.';
$_['help_house_no']				= 'Por favor ingrese su número de casa.';
$_['help_house_ext']			= 'Por favor ingrese su extensión de casa aquí. Ej. A, B, C, Rojo, Azul etc.';
$_['help_company']				= 'Por favor ingrese su número de registro de compañía';

// Error
$_['error_deu_terms']			= 'Debe aceptar la política de privacidad de Klarna (Datenschutz)';
$_['error_address_match']		= 'Dirección de envíoes y facturación deben coincidir si desea utilizar Pagos Klarna.';
$_['error_network']				= 'Ocurrió un error al conectar con Klarna. Por favor intente de nuevo más tarde.';