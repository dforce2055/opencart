<?php
// Heading
$_['heading_title'] = 'Use Puntos de Recompensa (Disponibles %s)';

// Text
$_['text_reward']   = 'Puntos de Recompensa (%s)';
$_['text_order_id'] = 'ID de pedido: #%s';
$_['text_success']  = 'Éxito: ¡Su descuento por Puntos de Recompensa ha sido aplicado!';

// Entry
$_['entry_reward']  = 'Puntos a utilizar (Max %s)';

// Error
$_['error_reward']  = 'Advertencia: ¡Por favor ingrese la cantidad de Puntos de Recompensa a utilizar!';
$_['error_points']  = 'Advertencia: ¡No tiene %s Puntos de Recompensa!';
$_['error_maximum'] = 'Advertencia: ¡El máximo de Puntos de Recompensa aplicable es %s!';