<?php
// Heading
$_['heading_title'] = 'Usar código de cupón';

// Text
$_['text_coupon']   = 'Cupón (%s)';
$_['text_success']  = 'Éxito: ¡Su descuento de cupón ha sido aplicado!';

// Entry
$_['entry_coupon']  = 'Ingrese su cupón aquí';

// Error
$_['error_coupon']  = 'Advertencia: ¡Cupón inválido, expirado, o alcanzó su límite de saldo!';
$_['error_empty']   = 'Advertencia: ¡Por favor ingrese un código de cupón!';