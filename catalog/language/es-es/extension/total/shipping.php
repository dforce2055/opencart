<?php
// Heading
$_['heading_title']        = 'Estimar envío e impuestos';

// Text
$_['text_success']         = 'Éxito: ¡Su estimación de envío ha sido aplicada!';
$_['text_shipping']        = 'Ingrese el destino para estimar el costo de envío.';
$_['text_shipping_method'] = 'Por favor seleccione el método de envío preferido a usar con este pedido.';

// Entry
$_['entry_country']        = 'País';
$_['entry_zone']           = 'Provincia / Estado';
$_['entry_postcode']       = 'Código postal';

// Error
$_['error_postcode']       = 'Código postal debe tener entre 2 y 10 caracteres!';
$_['error_country']        = 'Por favor seleccione un país!';
$_['error_zone']           = 'Por favor seleccione una provincia / estado!';
$_['error_shipping']       = 'Advertencia: ¡Método de envío necesario!';
$_['error_no_shipping']    = 'Advertencia: ¡No hay opciones de envío disponibles. Por favor <a href="%s">contáctenos</a> por asistencia!';