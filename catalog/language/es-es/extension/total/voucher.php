<?php
// Heading
$_['heading_title'] = 'Usar Cupón de Regalo';

// Text
$_['text_voucher']  = 'Cupón de Regalo (%s)';
$_['text_success']  = 'Éxito: ¡Su descuento por cupón de regalo ha sido aplicado!';

// Entry
$_['entry_voucher'] = 'Ingrese su código de Cupón de Regalo aquí';

// Error
$_['error_voucher'] = 'Advertencia: ¡Cupón de regalo inválido o sin saldo restante!';
$_['error_empty']   = 'Advertencia: ¡Por favor ingres un código de Cupón de Regalo!';