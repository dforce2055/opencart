<?php
// Text
$_['text_paid_amazon'] 			= 'Pagar con Amazon';
$_['text_total_shipping'] 		= 'Envió';
$_['text_total_shipping_tax'] 	= 'Impuestos de envió';
$_['text_total_giftwrap'] 		= 'Papel de regalo';
$_['text_total_giftwrap_tax'] 	= 'Impuestos del Papel de Regalo';
$_['text_total_sub'] 			= 'Sub-total';
$_['text_tax'] 					= 'Impuestos';
$_['text_total'] 				= 'Total';
$_['text_gift_message'] 		= 'Mensajes de Regalo';
