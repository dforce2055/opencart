<?php
// Text
$_['text_total_shipping']		= 'Envio';
$_['text_total_discount']		= 'Descuento';
$_['text_total_tax']			= 'Impuesto';
$_['text_total_sub']			= 'Sub-total';
$_['text_total']				= 'Total';
$_['text_smp_id']				= 'Manejador de ventas ID de venta: ';
$_['text_buyer']				= 'Nombre de usuario del Comprador: ';