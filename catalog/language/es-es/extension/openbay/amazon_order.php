<?php
// Text
$_['text_paid_amazon'] 			= 'Pagar con Amazon';
$_['text_total_shipping'] 		= 'Envío';
$_['text_total_shipping_tax'] 	= 'Impuestos de Envío';
$_['text_total_giftwrap'] 		= 'Papel de regalo';
$_['text_total_giftwrap_tax'] 	= 'Impuestos de Papel de regalo';
$_['text_total_sub'] 			= 'Sub-total';
$_['text_tax'] 					= 'Impuestos';
$_['text_total'] 				= 'Total';
$_['text_gift_message'] 		= 'Mensajes de Regalo';
