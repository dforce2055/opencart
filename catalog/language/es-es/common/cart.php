<?php
// Text
$_['text_items']    = '%s Artículo(s) - %s';
$_['text_empty']    = 'El Carro de Compras esta Vacío.';
$_['text_cart']     = 'Ver Carro de Compras';
$_['text_checkout'] = 'Pagar';
$_['text_recurring']  = 'Perfil de Pago';

// Multiseller
$_['text_seller']            = 'Producto de';
