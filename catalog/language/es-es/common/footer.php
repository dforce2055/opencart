<?php
// Text
$_['text_information']  = 'Información';
$_['text_service']      = 'Servicio al Cliente';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contacto';
$_['text_return']       = 'Devoluciones';
$_['text_sitemap']      = 'Mapa del Sitio';
$_['text_manufacturer'] = 'Marca';
$_['text_voucher']      = 'Cupones de Regalo';
$_['text_affiliate']    = 'Afiliados';
$_['text_special']      = 'Ofertas';
$_['text_account']      = 'Cuenta';
$_['text_order']        = 'Historial de Pedidos';
$_['text_wishlist']     = 'Favoritos';
$_['text_newsletter']   = 'Boletín de Noticias';
$_['text_powered']      = 'Diseño y Desarrollo <a href="http://www.indepi.com.ar/">INDEPI</a><br /> %s &copy; %s';
