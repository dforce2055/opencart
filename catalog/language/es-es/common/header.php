<?php
// Text
$_['text_home']          = 'Inicio';
$_['text_wishlist']      = 'Favoritos';//'Favoritos (%s)';
$_['text_shopping_cart'] = 'Carro de Compras';
$_['text_category']      = 'Categorías';
$_['text_account']       = 'Mi Cuenta';
$_['text_register']      = 'Registro';
$_['text_login']         = 'Iniciar Sesión';
$_['text_order']         = 'Historial de Pedidos';
$_['text_transaction']   = 'Transacciones';
$_['text_download']      = 'Descargas';
$_['text_logout']        = 'Salir';
$_['text_checkout']      = 'Pagar';
$_['text_search']        = 'Buscar';
$_['text_all']           = 'Ver Todo';
$_['text_buyeraccount']   = 'Comprador';
$_['text_selleraccount']  = 'Vender con Nosotros';
$_['text_seller_store']   = 'Ir a mi Tienda';
$_['text_viewselleraccount']  = 'Ver todos los Vendedores';
