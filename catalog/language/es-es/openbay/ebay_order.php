<?php
// Text
$_['text_total_shipping']		= 'Envío';
$_['text_total_discount']		= 'Descuento';
$_['text_total_tax']			= 'Tasas';
$_['text_total_sub']			= 'Sub-total';
$_['text_total']				= 'Total';