<?php
// Text
$_['text_paid_amazon'] 			= 'Pagar en Amazon US';
$_['text_total_shipping'] 		= 'Envío';
$_['text_total_shipping_tax'] 	= 'Tasas de Envío';
$_['text_total_giftwrap'] 		= 'Papel regalo';
$_['text_total_giftwrap_tax'] 	= 'Tasas del Papel Regalo';
$_['text_total_sub'] 			= 'Sub-total';
$_['text_tax'] 					= 'Tasas';
$_['text_total'] 				= 'Total';