<?php
// Text
$_['text_subject']      = '%s - Actualización de pedido %s';
$_['text_order_id']     = 'ID de pedido:';
$_['text_date_added']   = 'Fecha añadido:';
$_['text_order_status'] = 'Su pedido fue actualizado al siguiente estado:';
$_['text_comment']      = 'Los comentarios para su pedido son:';
$_['text_link']         = 'Para ver su pedido haga click en el link debajo:';
$_['text_footer']       = 'Por favor conteste este email si tiene alguna duda.';