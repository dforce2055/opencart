<?php
// Text
$_['text_subject']	= '%s - Comentarios de Productos';
$_['text_waiting']	= 'Revisar nuevo Comentario de Productos.';
$_['text_product']	= 'Producto: %s';
$_['text_reviewer']	= 'Comentario: %s';
$_['text_rating']	= 'Clasificación: %s';
$_['text_review']	= 'Texto del Comentario:';