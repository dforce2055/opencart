<?php
// Text
$_['text_subject']          = '%s - Order %s';
$_['text_greeting']         = 'Gracias por su interés en %s productos. Su pedido fue recibido y será procesado una vez confirmado el pago.';
$_['text_link']             = 'Para ver su pedido haga click en el link debajo:';
$_['text_order_detail']     = 'Detalles de pedido';
$_['text_instruction']      = 'Instrucciones';
$_['text_order_id']         = 'ID de pedido:';
$_['text_date_added']       = 'Fecha añadido:';
$_['text_order_status']     = 'Estado de pedido:';
$_['text_payment_method']   = 'Método de pago:';
$_['text_shipping_method']  = 'Método de envío:';
$_['text_email']            = 'E-mail:';
$_['text_telephone']        = 'Teléfono:';
$_['text_ip']               = 'Dirección IP:';
$_['text_payment_address']  = 'Dirección de pago';
$_['text_shipping_address'] = 'Dirección de envío';
$_['text_products']         = 'Productos';
$_['text_product']          = 'Producto';
$_['text_model']            = 'Modelo';
$_['text_quantity']         = 'Cantidad';
$_['text_price']            = 'Precio';
$_['text_order_total']      = 'Totales de pedido';
$_['text_total']            = 'Total';
$_['text_download']         = 'Una vez confirmado su pago puede hacer click en el link debajo para acceder a los descargables de sus productos:';
$_['text_comment']          = 'Los comentarios para su pedido son:';
$_['text_footer']           = 'Por favor conteste a este e-mail si tiene alguna duda.';
