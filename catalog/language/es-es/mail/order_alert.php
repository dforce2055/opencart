<?php
// Text
$_['text_subject']      = '%s - Pedido %s';
$_['text_received']     = 'Ha recibido un pedido.';
$_['text_order_id']     = 'ID de pedido:';
$_['text_date_added']   = 'Fecha añadido:';
$_['text_order_status'] = 'Estado de pedido:';
$_['text_product']      = 'Productos';
$_['text_total']        = 'Totales';
$_['text_comment']      = 'Los comentarios para su pedido son:';
