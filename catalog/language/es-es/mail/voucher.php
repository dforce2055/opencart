<?php
// Text
$_['text_subject']  = 'Se le ha enviado un Cupón de Regalo de %s';
$_['text_greeting'] = 'Felicitaciones, ha recibido un Cupón de Compra de %s';
$_['text_from']     = 'Este Cupón de Regalo ha sido enviado por %s';
$_['text_message']  = 'Con el siguiente mensaje';
$_['text_redeem']   = 'Para canjear este Cupón de Regalo, anotar el Código del Cupón que es <b>%s</b> a continuación, haga click en el siguiente enlace y compre el producto en el que se desea utilizar este Cupón de regalo.';
$_['text_footer']   = 'Responda este Email si tiene dudas o preguntas.';
