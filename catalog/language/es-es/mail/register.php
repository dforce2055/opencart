<?php
// Text
$_['text_subject']        = '%s - Gracias por registrarse';
$_['text_welcome']        = 'Bienvenido y gracias por registrarse en %s!';
$_['text_login']          = 'Su cuenta ha sido creada, puede iniciar sesión usando su dirección de E-mail y contraseña visitando nuestro sitio web o desde la siguiente URL:';
$_['text_approval']       = 'La Cuenta debe ser aprobada antes de que pueda iniciar sesión. Una vez aprobada se puede iniciar sesión utilizando el E-mail y Contraseña registrados, desde la siguiente URL:';
$_['text_service']        = 'Al iniciar sesión, podrá acceder a otros servicios, como la revisión de pedidos anteriores, la impresión de facturas y la edición de la información de su cuenta';
$_['text_thanks']         = 'Gracias,';
$_['text_new_customer']   = 'Nuevo cliente';
$_['text_signup']         = 'Un nuevo cliente se ha registrado:';
$_['text_customer_group'] = 'Grupo de clientes:';
$_['text_firstname']      = 'Nombre:';
$_['text_lastname']       = 'Apellido:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Teléfono:';
