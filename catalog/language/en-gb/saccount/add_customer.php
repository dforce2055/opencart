<?php
// Heading
$_['heading_title']  = 'Contact: Application for membership as a client';

// Text
$_['text_location']  = 'Seller information';
$_['text_contact']   = 'Form to Application for membership as a client';
$_['text_address']   = 'Address:';
$_['text_email']     = 'E-Mail:';
$_['text_telephone'] = 'Telephone:';
$_['text_fax']       = 'Fax:';
$_['text_message']   = '<p>Your request to join as a customer has been sent to the seller!</p><br/>';

$_['text_message1']   = 'Message:';

$_['text_thanks']   = 'Thanks';
$_['text_new_customer']   = 'A new client wants to register in his store';
$_['pending_authorization']   = 'The client is pending authorization!';

// Entry Fields
$_['entry_name']     = 'First Name:';
$_['entry_email']    = 'E-Mail Address:';
$_['entry_enquiry']  = 'Enquiry:';
$_['query']          = 'I would like to be part of your client group...';
$_['entry_captcha']  = 'Enter the code in the box below:';
$_['entry_fname']    = 'Name:';
$_['product']        = 'Product';

// Email
$_['email_subject']  = 'Enquiry for product : %s';

$_['text_error']        = 'Seller not found!';

// Errors
$_['error_name']        = 'Name must be between 3 and 32 characters!';
$_['error_email']       = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']     = 'Enquiry must be between 10 and 3000 characters!';
$_['error_captcha']     = 'Verification code does not match the image!';
$_['error_parameters']  = 'Missing parameters customer_id and seller_id!';
$_['error_client']      = 'You are already a customer of this seller!';
?>
