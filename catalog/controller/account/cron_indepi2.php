<?php
class ControllerAccountCronIndepi2 extends Controller {
  public function index()
  {
    /*--
    -- Volcado de datos para la tabla `oc_user_group`
    --*/


    $sql = "INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
          (16, 'Vendedor_Administrador', '{   "access":["saccount\\/customers", "saccount\\/customers_groups", "saccount\\/users", "saccount\\/extension", "saccount\\/extension_insert", "saccount\\/offer", "saccount\\/edit", "saccount\\/password", "saccount\\/address2", "saccount\\/plan", "saccount\\/uploadimages", "saccount\\/category", "saccount\\/download", "saccount\\/address2", "saccount\\/order", "saccount\\/transaction"],   "modify":["saccount\\/customers", "saccount\\/customers_groups", "saccount\\/users", "saccount\\/extension", "saccount\\/extension_insert", "saccount\\/offer", "saccount\\/edit", "saccount\\/password", "saccount\\/address2", "saccount\\/plan", "saccount\\/uploadimages", "saccount\\/category", "saccount\\/download", "saccount\\/address2", "saccount\\/order", "saccount\\/transaction"] }'),
          (17, 'Vendedor_Tienda', '{   "access":["saccount\\/customers", "saccount\\/customers_groups", "saccount\\/users", "saccount\\/extension", "saccount\\/extension_insert", "saccount\\/offer"],   "modify":["saccount\\/customers", "saccount\\/customers_groups", "saccount\\/users", "saccount\\/extension", "saccount\\/extension_insert", "saccount\\/offer"] }'),
          (18, 'Vendedor_Cuenta', '{   "access":["saccount\\/edit", "saccount\\/password", "saccount\\/address2", "saccount\\/plan", "saccount\\/uploadimages", "saccount\\/category", "saccount\\/download"],    "modify":["saccount\\/edit", "saccount\\/password", "saccount\\/address2", "saccount\\/plan", "saccount\\/uploadimages", "saccount\\/category", "saccount\\/download"] }'),
          (19, 'Vendedor_Pedidos', '{   "access":["saccount\\/address2", "saccount\\/order", "saccount\\/transaction"],   "modify":["saccount\\/address2", "saccount\\/order", "saccount\\/transaction"] }');
          ";

    if ($this->db->query($sql)) {
      echo "New record created successfully";
    } else {
      echo "error al insertar datos"
    }

    if($this->sePuedeInstalar())
    {
      /*
       *  --
       *  -- Base de datos: `opencart`
       *  --
       *  -- Estructura de tabla para la tabla `oci_sellers_users`
       *  --
       */
         $sql = "CREATE TABLE `oci_sellers_users` (
                  `oci_sellers_users_id` int(11) NOT NULL AUTO_INCREMENT,
                  `seller_id` int(11) DEFAULT NULL,
                  `user_group_id` int(11) DEFAULT NULL,
                  `firstname` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
                  `lastname` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
                  `email` varchar(96) COLLATE utf8_spanish_ci DEFAULT NULL,
                  `password` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
                  `fecha_alta` date DEFAULT NULL,
                  `status` int(2) DEFAULT NULL,
                  PRIMARY KEY (`oci_sellers_users_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Table Sellers Users' AUTO_INCREMENT=1 ; ";

         $this->db->query($sql);
         echo "<h1>Instalación satisfactoria<h1>";

    } else {
      echo "<h1>El módulo ya esta instalado, no es necesario reinstalar</h1>";
    }


  }

  public function sePuedeInstalar()
  {
    $consultaExistenciaTablas;
    try
    {
      $consultaExistenciaTablas = $this->db->query("SELECT * FROM oci_sellers_users");

    } catch (Exception $e) {
      echo 'Las tablas no existen, pueden ser creadas...';
      //echo 'Las tablas no existen, pueden ser creadas...',  $e->getMessage(), "\n";
      return true;
    }

    if($consultaExistenciaTablas->num_rows)
    {
      return false;
    }
  }

}
