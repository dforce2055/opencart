<?php
class ControllerAccountCronIndepi extends Controller {
  public function index()
  {
    if($this->sePuedeInstalar())
    {
      /*
       *  --
       *  -- Base de datos: `opencart`
       *  --
       *  -- Estructura de tabla para la tabla `oci_customer_groups`
       *  --
       */
         $sql = "CREATE TABLE IF NOT EXISTS `oci_customer_groups` (
         `customers_groups_id` int(11) NOT NULL AUTO_INCREMENT,
         `customer_id` int(11) NOT NULL,
         `customer_group_id` int(11) NOT NULL,
         `fecha_alta` date NOT NULL,
         `fecha_baja` date DEFAULT NULL,
         PRIMARY KEY (`customers_groups_id`)
       ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Table Customers Groups' COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; " ;

         $this->db->query($sql);

       /*
       * -- --------------------------------------------------------
       * --
       * -- Estructura de tabla para la tabla `oci_sellers_customer_groups`
       * --
       */
         $sql1 = "CREATE TABLE IF NOT EXISTS `oci_sellers_customer_groups` (
           `sellers_customer_groups_id` int(11) NOT NULL AUTO_INCREMENT,
           `seller_id` int(11) NOT NULL,
           `customer_group_id` int(11) NOT NULL,
           PRIMARY KEY (`sellers_customer_groups_id`)
         ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Table Sellers Customers Groups' COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; " ;

           $this->db->query($sql1);

           echo "<h1>Instalación satisfactoria<h1>";

    } else {
      echo "<h1>El módulo ya esta instalado, no es necesario reinstalar</h1>";
    }


  }

  public function sePuedeInstalar()
  {
    $consultaExistenciaTablas;
    try
    {
      $consultaExistenciaTablas = $this->db->query("SELECT * FROM
                                                    oci_customer_groups,
                                                    oci_sellers_customer_groups");


    } catch (Exception $e) {
      echo 'Las tablas no existen, pueden ser creadas...';
      //echo 'Las tablas no existen, pueden ser creadas...',  $e->getMessage(), "\n";
      return true;
    }

    if($consultaExistenciaTablas->num_rows) 
    {
      return false;
    }
  }

}
