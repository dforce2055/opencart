<?php
class ControllerCommonHeader extends Controller {
  public function index() {
    // Analytics
    $this->load->model('setting/extension');

    $data['analytics'] = array();

    $analytics = $this->model_setting_extension->getExtensions('analytics');

    foreach ($analytics as $analytic) {
      if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
        $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
      }
    }

    if ($this->request->server['HTTPS']) {
      $server = $this->config->get('config_ssl');
    } else {
      $server = $this->config->get('config_url');
    }

    if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
      $this->document->addLink(HTTP_SERVER1 . 'image/' . $this->config->get('config_icon'), 'icon');
    }

    $data['title'] = $this->document->getTitle();

    $data['base'] = $server;
    $data['description'] = $this->document->getDescription();
    $data['keywords'] = $this->document->getKeywords();
    $data['links'] = $this->document->getLinks();
    $data['styles'] = $this->document->getStyles();
    $data['scripts'] = $this->document->getScripts('header');
    $data['lang'] = $this->language->get('code');
    $data['direction'] = $this->language->get('direction');

    $data['name'] = $this->config->get('config_name');

    if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
      $data['logo'] = HTTP_SERVER1 . 'image/' . $this->config->get('config_logo');
    } else {
      $data['logo'] = '';
    }

    $this->load->language('common/header');

    $data['text_home'] = $this->language->get('text_home');


    $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
    $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('saccount/account', '', true), $this->customer->getsellerFirstName(), $this->url->link('saccount/logout', '', true));
    $data['text_account'] = $this->language->get('text_account');
    $data['text_register'] = $this->language->get('text_register');
    $data['text_login'] = $this->language->get('text_login');
    $data['text_order'] = $this->language->get('text_order');
    $data['text_transaction'] = $this->language->get('text_transaction');
    $data['text_download'] = $this->language->get('text_download');
    $data['text_logout'] = $this->language->get('text_logout');
    $data['text_checkout'] = $this->language->get('text_checkout');
    $data['text_category'] = $this->language->get('text_category');
    $data['text_extensions'] = $this->language->get('text_extensions');
    $data['text_manageextensions'] = $this->language->get('text_manageextensions');
    $data['text_addextensions'] = $this->language->get('text_addextensions');
    $data['manageextensions'] = $this->url->link('saccount/extension', '', 'SSL');
    $data['addextensions'] = $this->url->link('saccount/extension/insert', '', 'SSL');
    $data['address2'] = $this->url->link('saccount/address2', '', 'SSL');
    $data['messagebox'] = $this->url->link('saccount/messages', '', 'SSL');

    $data['text_plan'] = $this->language->get('text_plan');
    $data['text_my_plan'] = $this->language->get('text_my_plan');
    $data['plan'] = $this->url->link('saccount/plan', '', 'SSL');
    $data['text_order1'] = $this->language->get('text_order1');

    $data['offer'] = $this->url->link('saccount/offer', '', 'SSL');
    $data['attributes'] = $this->url->link('saccount/attribute', '', 'SSL');
    $data['text_attributes'] = $this->language->get('text_attributes');
    $data['text_offer'] = $this->language->get('text_offer');

    /**code added here**/
    $data['option'] = $this->url->link('saccount/option', '', 'SSL');
    $data['category'] = $this->url->link('saccount/category', '', 'SSL');
    $data['text_option'] = $this->language->get('text_option');
    $data['text_category'] = $this->language->get('text_category');
    $data['text_download'] = $this->language->get('text_download');
    $data['text_address2'] = $this->language->get('text_address2');
    $data['text_sellerwelcome'] = $this->language->get('text_sellerwelcome');
    $data['text_edit'] = $this->language->get('text_edit');
    $data['text_password'] = $this->language->get('text_password');
    $data['text_address'] = $this->language->get('text_address');
    //###INDEPI
    $this->load->model('saccount/users');
    $this->load->model('saccount/seller');


    if (isset($this->session->data['user_firstname']))
      $data['user_firstname'] = $this->session->data['user_firstname'];

    /**/

    $data['home'] = $this->url->link('saccount/account');
    $data['wishlist'] = $this->url->link('saccount/wishlist', '', true);
    $data['logged'] = $this->customer->issellerLogged();
    $data['username'] = $this->customer->getUserName();
    $data['account'] = $this->url->link('saccount/account', '', true);
    $data['register'] = $this->url->link('saccount/register', '', true);
    $data['login'] = $this->url->link('saccount/login', '', true);
    $data['order'] = $this->url->link('saccount/order', '', true);
    $data['transaction'] = $this->url->link('saccount/transaction', '', true);
    $data['download'] = $this->url->link('saccount/download', '', true);
    $data['logout'] = $this->url->link('saccount/logout', '', true);
    $data['shopping_cart'] = $this->url->link('checkout/cart');
    $data['checkout'] = $this->url->link('checkout/checkout', '', true);
    $data['contact'] = $this->url->link('information/contact');
    $data['telephone'] = $this->config->get('config_telephone');
    $data['edit'] = $this->url->link('saccount/edit', '', 'SSL');
    $data['password'] = $this->url->link('saccount/password', '', 'SSL');
    $address_id  = $this->customer->getsellerAddressId();		
    $data['address'] = $this->url->link('saccount/address/update', 'address_id=' . $address_id, 'SSL');
    $data['images'] = $this->url->link('saccount/uploadimages', 'seller_id=' . $seller_id, 'SSL');
    $data['plan'] = $this->url->link('saccount/plan', '', 'SSL');
    $data['export'] = $this->url->link('saccount/smartexportimport','', 'SSL');

    $data['language'] = $this->load->controller('common/language');
    $data['currency'] = $this->load->controller('common/currency');
    $data['search'] = $this->load->controller('common/search');
    $data['cart'] = $this->load->controller('common/cart');
    $data['menu'] = $this->load->controller('common/menu');

    return $this->load->view('common/header', $data);
  }
}
