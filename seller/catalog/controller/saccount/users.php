<?php
class ControllerSAccountUsers extends Controller {
  private $error = array();

    public function index() {
      $this->load->language('saccount/users');

      $this->document->setTitle($this->language->get('heading_title'));

      $this->load->model('saccount/users');

      $this->getList();
    }

    public function insert() {
      $this->load->language('saccount/users');
      $this->document->setTitle($this->language->get('heading_title'));
      $this->load->model('saccount/users');

      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
        //SI NO ESXISTE EL MAIL EN LA BBDD SE PERMITE CREAR
        if(!$this->model_saccount_users->existUser($this->request->post['email'])) {
          $this->model_saccount_users->addUser($this->request->post);
          $this->session->data['success'] = $this->language->get('text_success');
        } else {
            $this->session->data['error_already_exists'] = $this->language->get('error_already_exists');
          }
        $this->response->redirect($this->url->link('saccount/users',  $url, 'SSL'));
      }

      $this->getForm();
    }

    public function update() {
      $this->load->language('saccount/users');
      $this->document->setTitle($this->language->get('heading_title'));
      $this->load->model('saccount/users');

      if ($this->request->post) {
        if (isset($this->request->get['user_id']) && $this->validateForm()) {
          $this->model_saccount_users->editUser($this->request->get['user_id'], $this->request->post);
          $this->session->data['success'] = $this->language->get('text_success');

          $url = '';

          if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
          }

          if (isset($this->request->get['filter_email'])) {
            $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
          }

          if (isset($this->request->get['filter_seller_group_id'])) {
            $url .= '&filter_seller_group_id=' . $this->request->get['filter_seller_group_id'];
          }

          if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
          }

          if (isset($this->request->get['filter_approved'])) {
            $url .= '&filter_approved=' . $this->request->get['filter_approved'];
          }

          if (isset($this->request->get['filter_ip'])) {
            $url .= '&filter_ip=' . $this->request->get['filter_ip'];
          }

          if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
          }

          if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
          }

          if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
          }

          if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
          }

          $this->response->redirect($this->url->link('saccount/users', $url, 'SSL'));
        }
      }


      $this->getForm();

    }

    public function delete() {
      $this->load->language('saccount/users');
      $this->load->model('saccount/users');
      $this->document->setTitle($this->language->get('heading_title'));

      if (isset($this->request->post['selected']) && $this->validateDelete()) {
        foreach ($this->request->post['selected'] as $user_id) {
          $this->model_saccount_users->deleteUser($user_id);
        }

        $this->session->data['success'] = $this->language->get('text_success');

        $url = '';

        if (isset($this->request->get['filter_name'])) {
          $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_email'])) {
          $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller_group_id'])) {
          $url .= '&filter_seller_group_id=' . $this->request->get['filter_seller_group_id'];
        }

        if (isset($this->request->get['filter_status'])) {
          $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['filter_approved'])) {
          $url .= '&filter_approved=' . $this->request->get['filter_approved'];
        }

        if (isset($this->request->get['filter_ip'])) {
          $url .= '&filter_ip=' . $this->request->get['filter_ip'];
        }

        if (isset($this->request->get['filter_date_added'])) {
          $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['sort'])) {
          $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
          $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
          $url .= '&page=' . $this->request->get['page'];
        }

        //$this->response->redirect($this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL'));
      }
      $this->getList();
    }

    public function approve1() {
      $this->load->language('saccount/users');

      $this->document->setTitle($this->language->get('heading_title'));

      $this->load->model('saccount/users');

      if (isset($this->request->post['selected']) && $this->validateApprove()) {
      foreach ($this->request->post['selected'] as $user_id) {
        $this->model_saccount_users->approveUser($user_id);
      }

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_seller_group_id'])) {
        $url .= '&filter_seller_group_id=' . $this->request->get['filter_seller_group_id'];
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_ip'])) {
        $url .= '&filter_ip=' . $this->request->get['filter_ip'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL'));
      }

      $this->getList();
    }

    public function approve() {
      $this->load->language('saccount/users');
      $this->load->model('saccount/users');
      $this->document->setTitle($this->language->get('heading_title'));

      $user_id = $_POST['usuario']['id'];
      //Inicializo json
      $json = array();

      if (isset($user_id) && $this->validateApprove()) {
        $this->model_saccount_users->approveUser($user_id);
        $json['success'] = $this->language->get('text_success');
        $json['approved'] = $this->language->get('text_approved');
        $json['user_id'] = $_POST['usuario']['id'];
        $json['user_name'] = $_POST['usuario']['name'];
        $this->session->data['success'] = $this->language->get('text_success');

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
      }
  }

  public function deny() {
    $this->load->language('saccount/users');
    $this->load->model('saccount/users');
    $this->document->setTitle($this->language->get('heading_title'));

    $user_id = $_POST['usuario']['id'];
    //Inicializo json
    $json = array();

    if (isset($user_id) && $this->validateApprove()) {
      $this->model_saccount_users->denyUser($user_id);
      $json['success'] = $this->language->get('text_success');
      $json['deny'] = $this->language->get('text_deny');
      $json['user_id'] = $_POST['usuario']['id'];
      $json['user_name'] = $_POST['usuario']['name'];
      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
}


    private function getList() {
      $this->load->language('saccount/users');
      if (isset($this->request->get['filter_name'])) {
        $filter_name = $this->request->get['filter_name'];
      } else {
        $filter_name = null;
      }

      if (isset($this->request->get['filter_email'])) {
        $filter_email = $this->request->get['filter_email'];
      } else {
        $filter_email = null;
      }

      if (isset($this->request->get['filter_status'])) {
        $filter_status = $this->request->get['filter_status'];
      } else {
        $filter_status = null;
      }

      if (isset($this->request->get['filter_approved'])) {
        $filter_approved = $this->request->get['filter_approved'];
      } else {
        $filter_approved = null;
      }

      if (isset($this->request->get['filter_date_added'])) {
        $filter_date_added = $this->request->get['filter_date_added'];
      } else {
        $filter_date_added = null;
      }

      if (isset($this->request->get['sort'])) {
        $sort = $this->request->get['sort'];
      } else {
        $sort = 'name';
      }

      if (isset($this->request->get['order'])) {
        $order = $this->request->get['order'];
      } else {
        $order = 'ASC';
      }

      if (isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
      } else {
        $page = 1;
      }

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }


      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }


      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $data['breadcrumbs'] = array();

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('text_home'),
      'href'      => $this->url->link('common/home', /* 'user_token=' . $this->session->data['user_token'] . */ 'SSL'),
          'separator' => false
       );

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('text_account'),
           'href'      => $this->url->link('saccount/account', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL'),
           'separator' => ' :: '
       );

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('heading_title'),
      'href'      => $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL'),
          'separator' => ' :: '
       );

      $data['approve'] = $this->url->link('saccount/users/approve1', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL');
      $data['insert'] = $this->url->link('saccount/users/insert', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL');
      $data['delete'] = $this->url->link('saccount/users/delete', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL');

      $data['sellers'] = array();

      $data1 = array(
        'filter_name'              => $filter_name,
        'filter_email'             => $filter_email,
        'filter_status'            => $filter_status,
        'filter_approved'          => $filter_approved,
        'filter_date_added'        => $filter_date_added,
        'sort'                     => $sort,
        'order'                    => $order,
        'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
        'limit'                    => $this->config->get('config_limit_admin')
      );

      //$seller_total = $this->model_saccount_seller->getTotalSellers($data1);

      //$results = $this->model_saccount_seller->getSellers($data1);

        //foreach ($results as $result) {
        //  $action = array();
        //  $this->load->model('saccount/users');
        //  $balance_total = $this->model_sale_seller->getBalanceTotal($result['seller_id']);
        //  $action[] = array(
        //    'text' => $this->language->get('text_edit'),
        //    'href' => $this->url->link('saccount/users/update', /* 'user_token=' . $this->session->data['user_token'] . */ '&seller_id=' . $result['seller_id'] . $url, 'SSL')
        //);

        //$this->load->model('report/seller_transactions');
        //$pending = $this->model_report_seller_transactions->getPending($result['seller_id']);

        //$data['sellers'][] = array(
        //  'seller_id'      => $result['seller_id'],
        //  'name'           => $result['name'],
        //  'payment_status' => $result['payment_status'],
        //  'email'          => $result['email'],
        //  'status'         => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
        //  'approved'       => ($result['approved'] ? $this->language->get('text_yes') : $this->language->get('text_no')),
        //  'ip'             => $result['ip'],
        //  'date_added'     => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
        //  'edit'           => $this->url->link('saccount/users/update', /* 'user_token=' . $this->session->data['user_token'] . */ '&seller_id=' . $result['seller_id'] . $url, 'SSL'),
        //  'approve'        => $this->url->link('saccount/users/approve', /* 'user_token=' . $this->session->data['user_token'] . */ '&seller_id=' . $result['seller_id'] . $url, 'SSL'),
        //  'approvepayment' => $this->url->link('saccount/users/approvepayment', /* 'user_token=' . $this->session->data['user_token'] . */ '&seller_id=' . $result['seller_id'] . $url, 'SSL'),
        //  'approved'       => $result['approved'],
        //  'balance_total'  => $balance_total,
        //   'amount'        => $this->currency->format($pending['pamount'], $this->config->get('config_currency')),
        //   'transaction'   =>$this->url->link('report/seller_transactions/Seller','&seller_id='.$result['seller_id'] /*. '&user_token=' . $this->session->data['user_token'] */,'SSL')
        //);
      //}

      $data['heading_title'] = $this->language->get('heading_title');
      $data['text_enabled'] = $this->language->get('text_enabled');
      $data['text_disabled'] = $this->language->get('text_disabled');
      $data['text_yes'] = $this->language->get('text_yes');
      $data['text_no'] = $this->language->get('text_no');
      $data['text_select'] = $this->language->get('text_select');
      $data['text_default'] = $this->language->get('text_default');
      $data['text_no_results'] = $this->language->get('text_no_results');
      $data['text_login'] = $this->language->get('text_login');
      $data['text_confirm'] = $this->language->get('text_confirm');
      $data['text_login'] = $this->language->get('text_login');


      $data['column_name'] = $this->language->get('column_name');
      $data['column_email'] = $this->language->get('column_email');
      $data['column_seller_group'] = $this->language->get('column_seller_group');
      $data['column_status'] = $this->language->get('column_status');
      $data['column_approved'] = $this->language->get('column_approved');
      $data['column_ip'] = $this->language->get('column_ip');
      $data['column_date_added'] = $this->language->get('column_date_added');
      $data['column_login'] = $this->language->get('column_login');
      $data['column_action'] = $this->language->get('column_action');

      $data['button_approve'] = $this->language->get('button_approve');
      $data['button_insert'] = $this->language->get('button_insert');
      $data['button_delete'] = $this->language->get('button_delete');
      $data['button_filter'] = $this->language->get('button_filter');
      $data['button_edit'] = $this->language->get('button_edit');


      if (isset($this->error['warning'])) {
        $data['error_warning'] = $this->error['warning'];
      } else {
        $data['error_warning'] = '';
      }

      if (isset($this->session->data['error_already_exists'])) {
        $data['error_already_exists'] = $this->session->data['error_already_exists'];

        unset($this->session->data['error_already_exists']);
      } else {
        $data['error_already_exists'] = '';
      }

      if (isset($this->session->data['success'])) {
        $data['success'] = $this->session->data['success'];

        unset($this->session->data['success']);
      } else {
        $data['success'] = '';
      }

      if (isset($this->request->post['selected'])) {
        $data['selected'] = (array)$this->request->post['selected'];
      } else {
        $data['selected'] = array();
      }


      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_seller_group_id'])) {
        $url .= '&filter_seller_group_id=' . $this->request->get['filter_seller_group_id'];
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_ip'])) {
        $url .= '&filter_ip=' . $this->request->get['filter_ip'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if ($order == 'ASC') {
        $url .= '&order=DESC';
      } else {
        $url .= '&order=ASC';
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $data['sort_name'] = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ '&sort=name' . $url, 'SSL');
      $data['sort_email'] = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ '&sort=c.email' . $url, 'SSL');
      $data['sort_seller_group'] = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ '&sort=seller_group' . $url, 'SSL');
      $data['sort_status'] = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ '&sort=c.status' . $url, 'SSL');
      $data['sort_approved'] = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ '&sort=c.approved' . $url, 'SSL');
      $data['sort_ip'] = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ '&sort=c.ip' . $url, 'SSL');
      $data['sort_date_added'] = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ '&sort=c.date_added' . $url, 'SSL');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_seller_group_id'])) {
        $url .= '&filter_seller_group_id=' . $this->request->get['filter_seller_group_id'];
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_ip'])) {
        $url .= '&filter_ip=' . $this->request->get['filter_ip'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      $pagination = new Pagination();
      //$pagination->total = $seller_total; //DEFINO EN 1 para evitar warning, calcular bien paginación
      $pagination->total = $seller_total = 1;
      $pagination->page = $page;
      $pagination->limit = $this->config->get('config_limit_admin');
      $pagination->text = $this->language->get('text_pagination');
      $pagination->url = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ $url . '&page={page}', 'SSL');

      $data['pagination'] = $pagination->render();


      $data['results'] = sprintf($this->language->get('text_pagination'), ($seller_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($seller_total - $this->config->get('config_limit_admin'))) ? $seller_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $seller_total, ceil($seller_total / $this->config->get('config_limit_admin')));


      $data['filter_name'] = $filter_name;
      $data['filter_email'] = $filter_email;

      $data['filter_status'] = $filter_status;
      $data['filter_approved'] = $filter_approved;
      $data['filter_date_added'] = $filter_date_added;

      $this->load->model('setting/store');

      $data['stores'] = $this->model_setting_store->getStores();

      $data['sort'] = $sort;
      $data['order'] = $order;

      $data['header'] = $this->load->controller('common/header');
      $data['column_left'] = $this->load->controller('common/column_left');
      $data['footer'] = $this->load->controller('common/footer');

      //INDEPI
      $this->load->model('saccount/users');
      $this->load->model('saccount/seller');
      $seller_id = $this->customer->getsellerId();

      if (isset($this->session->data['user_id']))
        $data['user_id'] = $this->session->data['user_id'];

      $data['users'] = $this->model_saccount_users->getUsersBySeller($seller_id);
      //Agrego elementos al array de usuarios
      foreach ($data['users'] as &$element) {
        $element['approve'] = "index.php?route=saccount/users/approve";
        $element['deny'] = "index.php?route=saccount/users/deny";
        $element['edit'] = "index.php?route=saccount/users/update&user_id=" . $element['oci_sellers_users_id'];
      }

      $data['seller_id'] = $seller_id;




      $this->response->setOutput($this->load->view('saccount/users_list', $data));
    }

    private function getForm() {
      $data['heading_title'] = $this->language->get('heading_title');

      $data['text_enabled'] = $this->language->get('text_enabled');
      $data['text_disabled'] = $this->language->get('text_disabled');
      $data['text_select'] = $this->language->get('text_select');
      $data['text_none'] = $this->language->get('text_none');
      $data['text_wait'] = $this->language->get('text_wait');
      $data['text_no_results'] = $this->language->get('text_no_results');
      $data['text_add_blacklist'] = $this->language->get('text_add_blacklist');
      $data['text_remove_blacklist'] = $this->language->get('text_remove_blacklist');

      $data['column_ip'] = $this->language->get('column_ip');
      $data['column_total'] = $this->language->get('column_total');
      $data['column_date_added'] = $this->language->get('column_date_added');
      $data['column_action'] = $this->language->get('column_action');

      $data['entry_firstname'] = $this->language->get('entry_firstname');
      $data['entry_aboutus'] = $this->language->get('entry_aboutus');

      $data['entry_image'] = $this->language->get('entry_image');

      $data['text_clear'] = $this->language->get('text_clear');

      $data['text_browse'] = $this->language->get('text_browse');

      $data['text_image_manager'] = $this->language->get('text_image_manager');


      $data['entry_lastname'] = $this->language->get('entry_lastname');
      $data['entry_email'] = $this->language->get('entry_email');
      $data['entry_telephone'] = $this->language->get('entry_telephone');
      $data['entry_fax'] = $this->language->get('entry_fax');
      $data['entry_password'] = $this->language->get('entry_password');
      $data['entry_confirm'] = $this->language->get('entry_confirm');
      $data['entry_newsletter'] = $this->language->get('entry_newsletter');
      $data['entry_seller_group'] = $this->language->get('entry_seller_group');
      $data['entry_status'] = $this->language->get('entry_status');
      $data['entry_company'] = $this->language->get('entry_company');
      $data['entry_company_id'] = $this->language->get('entry_company_id');
      $data['entry_tax_id'] = $this->language->get('entry_tax_id');
      $data['entry_address_1'] = $this->language->get('entry_address_1');
      $data['entry_address_2'] = $this->language->get('entry_address_2');
      $data['entry_city'] = $this->language->get('entry_city');
      $data['entry_postcode'] = $this->language->get('entry_postcode');
      $data['entry_zone'] = $this->language->get('entry_zone');
      $data['entry_country'] = $this->language->get('entry_country');
      $data['entry_default'] = $this->language->get('entry_default');
      $data['entry_amount'] = $this->language->get('entry_amount');
      $data['entry_points'] = $this->language->get('entry_points');
      $data['entry_description'] = $this->language->get('entry_description');


      $data['entry_address_2'] = $this->language->get('entry_address_2');
      $data['entry_postcode2'] = $this->language->get('entry_postcode');
      $data['entry_city2'] = $this->language->get('entry_city');
      $data['entry_country2'] = $this->language->get('entry_country');
      $data['entry_zone2'] = $this->language->get('entry_zone');
      $data['entry_paypalemail'] = $this->language->get('entry_paypalemail');
      $data['entry_username'] = $this->language->get('entry_username');
      $data['entry_commission'] = $this->language->get('entry_commission');
      $data['button_save'] = $this->language->get('button_save');
      $data['button_cancel'] = $this->language->get('button_cancel');
      $data['button_add_address'] = $this->language->get('button_add_address');
      $data['button_add_transaction'] = $this->language->get('button_add_transaction');
      $data['button_add_reward'] = $this->language->get('button_add_reward');
      $data['button_remove'] = $this->language->get('button_remove');

      $data['tab_general'] = $this->language->get('tab_general');
      $data['tab_address'] = $this->language->get('tab_address');
      $data['tab_transaction'] = $this->language->get('tab_transaction');
      $data['tab_reward'] = $this->language->get('tab_reward');
      $data['tab_ip'] = $this->language->get('tab_ip');

      /* $data['user_token'] = $this->session->data['user_token']; */

      $data['entry_bankname'] = $this->language->get('entry_bankname');
      $data['entry_accountnumber'] = $this->language->get('entry_accountnumber');
      $data['entry_accountname'] = $this->language->get('entry_accountname');
      $data['entry_branch'] = $this->language->get('entry_branch');
      $data['entry_ifsccode'] = $this->language->get('entry_ifsccode');
      $data['entry_cheque'] = $this->language->get('entry_cheque');

      if (isset($this->error['cheque'])) {
        $data['error_cheque'] = $this->error['cheque'];
      } else {
        $data['error_cheque'] = '';
      }

      if (isset($this->error['address_2'])) {
          $data['error_address_2'] = $this->error['address_2'];
      } else {
        $data['error_address_2'] = '';
      }



      if (isset($this->error['city2'])) {
          $data['error_city2'] = $this->error['city2'];
      } else {
        $data['error_city2'] = '';
      }

      if (isset($this->error['postcode2'])) {
          $data['error_postcode2'] = $this->error['postcode2'];
      } else {
        $data['error_postcode2'] = '';
      }

      if (isset($this->error['country2'])) {
        $data['error_country2'] = $this->error['country2'];
      } else {
        $data['error_country2'] = '';
      }

      if (isset($this->error['zone2'])) {
        $data['error_zone2'] = $this->error['zone2'];
      } else {
        $data['error_zone2'] = '';
      }

      if (isset($this->error['paypalemail'])) {
        $data['error_paypalemail'] = $this->error['paypalemail'];
      } else {
        $data['error_paypalemail'] = '';
      }

      if (isset($this->request->get['seller_id'])) {
        $data['seller_id'] = $this->request->get['seller_id'];
      } else {
        $data['seller_id'] = 0;
      }

       if (isset($this->error['warning'])) {
        $data['error_warning'] = $this->error['warning'];
      } else {
        $data['error_warning'] = '';
      }

     if (isset($this->error['firstname'])) {
        $data['error_firstname'] = $this->error['firstname'];
      } else {
        $data['error_firstname'] = '';
      }

       if (isset($this->error['lastname'])) {
        $data['error_lastname'] = $this->error['lastname'];
      } else {
        $data['error_lastname'] = '';
      }

       if (isset($this->error['email'])) {
        $data['error_email'] = $this->error['email'];
      } else {
        $data['error_email'] = '';
      }

       if (isset($this->error['telephone'])) {
        $data['error_telephone'] = $this->error['telephone'];
      } else {
        $data['error_telephone'] = '';
      }

       if (isset($this->error['password'])) {
        $data['error_password'] = $this->error['password'];
      } else {
        $data['error_password'] = '';
      }

      if (isset($this->error['seller_id'])) {
       $data['error_seller_id'] = "No me llego el seller ID";
     } else {
       $data['error_seller_id'] = '';
     }

      if (isset($this->error['username12'])) {
      $data['error_username12'] = $this->error['username12'];
      } else {
        $data['error_username12'] = '';
      }

      if (isset($this->error['bank_name'])) {
        $data['error_bankname'] = $this->error['bank_name'];
      } else {
        $data['error_bankname'] = '';
      }

      if (isset($this->error['account_number'])) {
        $data['error_accountnumber'] = $this->error['account_number'];
      } else {
        $data['error_accountnumber'] = '';
      }

      if (isset($this->error['account_name'])) {
        $data['error_accountname'] = $this->error['account_name'];
      } else {
        $data['error_accountname'] = '';
      }

      if (isset($this->error['branch'])) {
        $data['error_branch'] = $this->error['branch'];
      } else {
        $data['error_branch'] = '';
      }

      if (isset($this->error['ifsccode'])) {
        $data['error_ifsccode'] = $this->error['ifsccode'];
      } else {
        $data['error_ifsccode'] = '';
      }

      if (isset($this->error['error_commission'])) {
        $data['error_commission'] = $this->error['commission'];
      } else {
        $data['error_commission'] = '';
      }

       if (isset($this->error['confirm'])) {
        $data['error_confirm'] = $this->error['confirm'];
      } else {
        $data['error_confirm'] = '';
      }

        if (isset($this->error['address'])) {
        $data['error_address'] = $this->error['address'];
      } else {
        $data['error_address'] = array();
      }



      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }


      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_approved'])) {
        $url .= '&filter_approved=' . $this->request->get['filter_approved'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $data['breadcrumbs'] = array();

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('text_home'),
           'href'      => $this->url->link('common/home', /* 'user_token=' . $this->session->data['user_token'] . */ 'SSL'),
           'separator' => false
       );

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('text_account'),
           'href'      => $this->url->link('saccount/account', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL'),
           'separator' => ' :: '
       );

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('heading_title'),
           'href'      => $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL'),
           'separator' => ' :: '
       );

      if (!isset($this->request->get['user_id'])) {
        $data['action'] = $this->url->link('saccount/users/insert', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL');
      } else {
        $data['action'] = $this->url->link('saccount/users/update', '&user_id=' . $this->request->get['user_id'] . $url, 'SSL');
      }

      $data['cancel'] = $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL');

      if (isset($this->request->get['user_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
          $userinfo = $this->model_saccount_users->getUser($this->request->get['user_id']);
          foreach($userinfo as $user_info){}
      }

    if (isset($this->request->post['firstname'])) {
          $data['firstname'] = $this->request->post['firstname'];
    } elseif (!empty($user_info)) {
      $data['firstname'] = $user_info['firstname'];
    } else {
          $data['firstname'] = '';
      }

      if (isset($this->request->post['lastname'])) {
          $data['lastname'] = $this->request->post['lastname'];
      } elseif (!empty($user_info)) {
      $data['lastname'] = $user_info['lastname'];
    } else {
          $data['lastname'] = '';
      }

      if (isset($this->request->post['email'])) {
          $data['email'] = $this->request->post['email'];
      } elseif (!empty($user_info)) {
      $data['email'] = $user_info['email'];
    } else {
          $data['email'] = '';
      }



    $this->load->model('tool/image');
    $this->load->model('saccount/users');
   if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
      $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
    } elseif (!empty($seller_info) && $seller_info['image'] && file_exists(DIR_IMAGE . $seller_info['image'])) {
      $data['thumb'] = $this->model_tool_image->resize($seller_info['image'], 100, 100);
    } else {
      $data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
    }


      if (isset($this->request->post['status'])) {
          $data['status'] = $this->request->post['status'];
      } elseif (!empty($user_info)) {
      $data['status'] = $user_info['status'];
    } else {
          $data['status'] = 1;
      }

      if (isset($this->request->post['password'])) {
      $data['password'] = $this->request->post['password'];
    } else {
      $data['password'] = '';
    }

    if (isset($this->request->post['confirm'])) {
        $data['confirm'] = $this->request->post['confirm'];
    } else {
      $data['confirm'] = '';
    }

    $this->load->model('localisation/country');

    $data['countries'] = $this->model_localisation_country->getCountries();

    //$data['commissions'] = $this->model_sale_seller->getCommissions();

    if (isset($this->request->post['address'])) {
          $data['addresses'] = $this->request->post['address'];
    } elseif (isset($this->request->get['seller_id'])) {
      //$data['addresses'] = $this->model_sale_seller->getAddresses1($this->request->get['seller_id']);
    } else {
      $data['addresses'] = array();
      }




    $data['ips'] = array();

    if (!empty($seller_info)) {
      $results = $this->model_sale_seller->getIpsBySellerId($this->request->get['seller_id']);

      foreach ($results as $result) {
        $blacklist_total = $this->model_sale_seller->getTotalBlacklistsByIp($result['ip']);

        $data['ips'][] = array(
          'ip'         => $result['ip'],
          'total'      => $this->model_sale_seller->getTotalSellersByIp($result['ip']),
          'date_added' => date('d/m/y', strtotime($result['date_added'])),
          'filter_ip'  => $this->url->link('saccount/users', /* 'user_token=' . $this->session->data['user_token'] . */ '&filter_ip=' . $result['ip'], 'SSL'),
          'blacklist'  => $blacklist_total
        );
      }
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    //INDEPI
    $data['user_id'] = $this->session->data['user_id'];
    $data['seller_id'] = $this->session->data['seller_id'];
    $data['permissions'] = $this->session->data['permissions'];
    $data['permissions_db'] = $this->model_saccount_users->getPermissionsUsersDB();


    $this->response->setOutput($this->load->view('saccount/users_form', $data));
  }

  private function validateFormEdit() {
    if (!$this->customer->hasPermission('modify', 'saccount/users')) {
        $this->error['warning'] = $this->language->get('error_permission');
    }

    if ((utf8_strlen($data['firstname']) < 1) || (utf8_strlen($data['firstname']) > 32)) {
        $this->error['firstname'] = $this->language->get('error_firstname');
    }

    if ((utf8_strlen($data['lastname']) < 1) || (utf8_strlen($data['lastname']) > 32)) {
        $this->error['lastname'] = $this->language->get('error_lastname');
    }

    if ((utf8_strlen($data['email']) < 1) || (utf8_strlen($data['email']) > 32)) {
        $this->error['email'] = $this->language->get('error_email');
    }

    if ((utf8_strlen($data['password']) < 1) || (utf8_strlen($data['password']) > 32)) {
        $this->error['password'] = $this->language->get('error_password');
    }

    if (!$this->error) {
        return true;
    } else {
        return false;
  }
}

    private function validateForm() {

      if (!$this->customer->hasPermission('modify', 'saccount/users')) {
          $this->error['warning'] = $this->language->get('error_permission');
      }

      if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
          $this->error['firstname'] = $this->language->get('error_firstname');
      }

      if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
          $this->error['lastname'] = $this->language->get('error_lastname');
      }

    if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
          $this->error['email'] = $this->language->get('error_email');
      }
    if ($this->request->post['password'] || (!isset($this->request->get['seller_id']))) {
        if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
          $this->error['password'] = $this->language->get('error_password');
        }
    }

    if ($this->request->post['seller_id'] == 0) {
          $this->error['seller_id'] = $this->language->get('error_seller_id');
    }


    if ($this->error && !isset($this->error['warning'])) {
      $this->error['warning'] = $this->language->get('error_warning');
    }

    if (!$this->error) {
        return true;
    } else {
        return false;
    }
  }


  protected function validateApprove() {
    if (!$this->customer->hasPermission('modify', 'saccount/users')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    return !$this->error;
  }

    private function validateDelete() {
      if (!$this->customer->hasPermission('modify', 'saccount/users')) {
          $this->error['warning'] = $this->language->get('error_permission');
      }

      if (!$this->error) {
          return true;
      } else {
          return false;
      }
    }

  public function login() {
    $json = array();

    if (isset($this->request->get['seller_id'])) {
      $seller_id = $this->request->get['seller_id'];
    } else {
      $seller_id = 0;
    }

    $this->load->model('saccount/users');

    $seller_info = $this->model_sale_seller->getSeller($seller_id);

    if ($seller_info) {
      $user_token = md5(mt_rand());

      $this->model_sale_seller->editToken($seller_id, $user_token);

      if (isset($this->request->get['store_id'])) {
        $store_id = $this->request->get['store_id'];
      } else {
        $store_id = 0;
      }

      $this->load->model('setting/store');

      $store_info = $this->model_setting_store->getStore($store_id);

      if ($store_info) {
        $this->response->redirect($store_info['url'] . 'index.php?route=saccount/login&user_token=' . $user_token);
      } else {
        $this->response->redirect(HTTPS_CATALOG1 . 'index.php?route=saccount/login&user_token=' . $user_token);
      }
    } else {
      $this->load->language('error/not_found');

      $this->document->setTitle($this->language->get('heading_title'));

      $data['heading_title'] = $this->language->get('heading_title');

      $data['text_not_found'] = $this->language->get('text_not_found');

      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text'      => $this->language->get('text_home'),
        'href'      => $this->url->link('common/home', /* 'user_token=' . $this->session->data['user_token'] . */ 'SSL'),
        'separator' => false
      );

      $data['breadcrumbs'][] = array(
          'text'      => $this->language->get('text_account'),
          'href'      => $this->url->link('saccount/account', /* 'user_token=' . $this->session->data['user_token'] . */ $url, 'SSL'),
          'separator' => ' :: '
      );

      $data['breadcrumbs'][] = array(
        'text'      => $this->language->get('heading_title'),
        'href'      => $this->url->link('error/not_found', /* 'user_token=' . $this->session->data['user_token'] . */ 'SSL'),
        'separator' => ' :: '
      );

      $this->template = 'error/not_found';
      $this->children = array(
        'common/header',
        'common/footer'
      );

      $this->response->setOutput($this->render());
    }
  }
}
?>
