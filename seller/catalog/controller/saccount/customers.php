<?php
class ControllerSAccountCustomers extends Controller {
  private $error = array();

  public function index() {

    if (!$this->customer->issellerLogged()) {
        $this->session->data['redirect'] = $this->url->link('saccount/customers', '', 'SSL');

        $this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
      }
    $this->load->language('saccount/customers');


    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('saccount/customers');
    $this->load->model('saccount/seller');


    $this->getList();
  }

  private function getList() {
    if (isset($this->request->get['sort'])) {
      $sort = $this->request->get['sort'];
    } else {
      $sort = 'od.name';
    }

    if (isset($this->request->get['order'])) {
      $order = $this->request->get['order'];
    } else {
      $order = 'ASC';
    }

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

      $data['breadcrumbs'] = array();

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('text_home'),
           'href'      => $this->url->link('common/home', '', 'SSL'),
           'separator' => false
       );

    $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('text_account'),
           'href'      => $this->url->link('saccount/account', '', 'SSL'),
           'separator' => ' :: '
       );

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('heading_title'),
           'href'      => $this->url->link('saccount/customers', $url, 'SSL'),
           'separator' => ' :: '
       );

    $data['insert'] = $this->url->link('saccount/customers/insert', $url, 'SSL');
    $data['edit'] = $this->url->link('saccount/customers/edit', $url, 'SSL');
    $data['delete'] = $this->url->link('saccount/customers/delete', $url, 'SSL');

    $data['options'] = array();

    $filter_data = array(
      'sort'  => $sort,
      'order' => $order,
      'start' => ($page - 1) * $this->config->get('config_admin_limit'),
      'limit' => $this->config->get('config_admin_limit')
    );

    $option_total = $this->model_saccount_customers->getTotalOptions();

    $results = $this->model_saccount_customers->getallOptions($filter_data);

    foreach ($results as $result) {
      $action = array();

      $action[] = array(
        'text' => $this->language->get('text_edit'),
        'href' => $this->url->link('saccount/customers/edit', '' . '&option_id=' . $result['option_id'] . $url, 'SSL')
      );

      $data['options'][] = array(
        'option_id'  => $result['option_id'],
        'name'       => $result['name'],
        'seller_id'       => $result['seller_id'],
        'approve'       => $result['approve'],
        'sort_order' => $result['sort_order'],
        'edit'        => $this->url->link('saccount/customers/edit', 'option_id=' . $result['option_id'] . $url, 'SSL'),
        'delete'      => $this->url->link('saccount/customers/delete','option_id=' . $result['option_id'] . $url, 'SSL')
      );
    }

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_no_results'] = $this->language->get('text_no_results');

    $data['column_name'] = $this->language->get('column_name');
    $data['column_sort_order'] = $this->language->get('column_sort_order');
    $data['column_action'] = $this->language->get('column_action');

    $data['button_insert'] = $this->language->get('button_insert');
    $data['button_delete'] = $this->language->get('button_delete');
    $data['button_edit'] = $this->language->get('button_edit');


     if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

      if (isset($this->request->post['selected'])) {
      $data['selected'] = (array)$this->request->post['selected'];
    } else {
      $data['selected'] = array();
    }

    $url = '';

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['sort_name'] = $this->url->link('saccount/customers', '' . '&sort=od.name' . $url, 'SSL');
    $data['sort_sort_order'] = $this->url->link('saccount/customers', '' . '&sort=o.sort_order' . $url, 'SSL');

    $url = '';

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $pagination = new Pagination();
    $pagination->total = $option_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_admin_limit');
    $pagination->text = $this->language->get('text_pagination');
    $pagination->url = $this->url->link('saccount/customers', '' . $url . '&page={page}', 'SSL');

    $data['pagination'] = $pagination->render();

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['results'] = sprintf($this->language->get('text_pagination'), ($option_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($option_total - $this->config->get('config_limit_admin'))) ? $option_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $option_total, ceil($option_total / $this->config->get('config_limit_admin')));


    $data['column_left'] = $this->load->controller('common/column_left');
    $data['column_right'] = $this->load->controller('common/column_right');
    $data['content_top'] = $this->load->controller('common/content_top');
    $data['content_bottom'] = $this->load->controller('common/content_bottom');
    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');




    $customers = $this->getCustomers();
    ###Agrego botón de editar usuario
    foreach ($customers as $customer) {
      $data['customers'][] = array(
        'customer_id' => $customer['customer_id'],
        'name'        => $customer['name'],
        'email'       => $customer['email'],
        'date_added'  => $customer['date_added'],
        'status'      => $customer['status'],
        'edit'        => $this->url->link('saccount/customers/edit', 'customer_id=' . $customer['customer_id']),
      );
    }

    $data['customers_groups'] = $this->getCustomersGroups();
    $data['customers_detailed'] = $this->getCustomersDetailed();
    $data['seller_id'] = $this->customer->getsellerId();
    ###Información del grupo por defecto de clientes de este vendedor
    $data['default_customer_group'] = $this->model_saccount_customers->getDefaultCustomerGroupSeller($seller_id);

    $this->response->setOutput($this->load->view('saccount/customers_list', $data));
  }

  private function getCustomers()
  {
    //Retorna array con clientes asociados a un vendedor en particular
    $this->load->model('saccount/customers');


    $seller_id = $this->customer->getsellerId();
    $result = $this->model_saccount_customers->getCustomersOfSeller($seller_id);


    return $result;
  }

  private function getCustomersGroups()
  {
    //Retorna los GRUPOS de clientes que estan asociados a un vendedor en
    //particular
    $this->load->model('saccount/customer_group');

    $seller_id = $this->customer->getsellerId();
    $result = $this->model_saccount_customer_group->getCustomerGroupsOfSeller($seller_id);
    return $result;
  }

  private function getCustomersDetailed()
  {
    //Retorna los GRUPOS de clientes que estan asociados a un vendedor en
    //particular
    $this->load->model('saccount/customer_group');

    $seller_id = $this->customer->getsellerId();
    $result = $this->model_saccount_customer_group->getCustomersDetailedOfSeller($seller_id, 5);
    return $result;
  }

  private function addApproval($customers)
  {
    //Agrego enlace para aprobar, desaprobar a cada cliente de un vendedor
    $clientes = array();
    foreach ($customers as $customer)
    {
      $customer['approved'] = $this->url->link('customers/approved');
      $customer['deny'] = $this->url->link('customers/deny');
      $customer['edit'] = "Editar";
      array_push($clientes, $customer);
    }
    return $clientes;
  }

  public function approve()
  {
    //Aprobar a un cliente que corresponde a un vendedor en particular
    $this->load->language('saccount/customers');
    $this->load->model('saccount/customers');

    $customer_id = $_POST['cliente']['id'];
    $seller_id = $this->customer->getsellerId();

    //Inicializo json
    $json = array();

    if(!empty($customer_id))
    {
      ###Apruebo al cliente
      $this->model_saccount_customers->approveCustomer($customer_id, $seller_id);
      ###Lo agrego al grupo por defecto del vendedor si no pertenece a ningún
      ###grupo de clientes
      if (!$this->model_saccount_customers->isAssingToGroupOfSeller($customer_id, $seller_id))
      {
        $default_customer_group = $this->model_saccount_customers->getDefaultCustomerGroupSeller($seller_id);
        $result = $this->model_saccount_customers->addToGroup($customer_id, $default_customer_group);
      }

      ###Si se registro pro primera vez como cliente de este vendedor, lo tengo
      ###que aprobar también en la tienda principal, y sacar de la tabla de
      ###clientes por aprobar
      if ($this->model_saccount_customers->isANewCustomer($customer_id))
      {
        $this->model_saccount_customers->removeFromCustomerApproval($customer_id);
        $this->model_saccount_customers->approveNewCustomer($customer_id);
      }



      $json['email'] = $this->sendEmailApprove($customer_id, $seller_id);
      $json['success'] = $this->language->get('text_success');
      $json['approved'] = $this->language->get('text_approved');
      $json['customer_id'] = $_POST['cliente']['id'];
      $json['customer_name'] = $_POST['cliente']['name'];
      $json['default_customer_group'] = $default_customer_group;
      $json['assigned_to_customer_group'] = $result;
      $json['seller_id'] = $seller_id;
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));

    /* Esto deberia hacerse
    if (!$this->user->hasPermission('modify', 'customer/customer_approval')) {
      $json['error'] = $this->language->get('error_permission');
    } else {
      $this->load->model('customer/customer_approval');

      if ($this->request->get['type'] == 'customer') {
        $this->model_saccount_customers_approval->approveCustomer($this->request->get['customer_id']);
      } elseif ($this->request->get['type'] == 'affiliate') {
        $this->model_saccount_customers_approval->approveAffiliate($this->request->get['customer_id']);
      }

      $json['success'] = $this->language->get('text_success');
    }
    */
  }

  public function deny()
  {
    //Desaprobar a un cliente que corresponde a un vendedor en particular
    $this->load->language('saccount/customers');
    $this->load->model('saccount/customers');

    $customer_id = $_POST['cliente']['id'];
    $seller_id = $this->customer->getsellerId();

    //Inicializo json
    $json = array();

    if(!empty($customer_id))
    {
      ###Desapruebo al cliente
      $this->model_saccount_customers->denyCustomer($customer_id, $seller_id);
      ###Lo saco del grupo por defecto del vendedor
      ###Lo agrego al grupo por defecto del vendedor si no pertenece a ningún
      ###grupo de clientes
      if (!$this->model_saccount_customers->isAssingToGroupOfSeller($customer_id, $seller_id))
      {
        $default_customer_group = $this->model_saccount_customers->getDefaultCustomerGroupSeller($seller_id);
        $result = $this->model_saccount_customers->removeFromTheGroup($customer_id, $default_customer_group);
      }



      $json['email'] = $this->sendEmailDeny($customer_id, $seller_id);
      $json['success'] = $this->language->get('text_success');
      $json['deny'] = $this->language->get('text_deny');
      $json['customer_id'] = $_POST['cliente']['id'];
      $json['customer_name'] = $_POST['cliente']['name'];
      $json['default_customer_group'] = $default_customer_group;
      $json['unassigned_to_customer_group'] = $result;
      $json['seller_id'] = $seller_id;
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function sendEmailApprove($customer_id, $seller_id)
  {
    //Envío de Email a clientes
    //Cargo las clases necesarias para el envío de Emails
    $this->load->model('saccount/customers');
    $this->load->model('saccount/seller');
    $this->load->model('account/customer');


    //obtengo datos del cliente y del vendedor
    $customer = $this->model_account_customer->getCustomer($customer_id);
    $seller = $this->model_saccount_seller->getSeller($seller_id);


    if (isset($customer) AND isset($seller))
    {
      $subject = "¡Bienvenido!";
      $message = "¡Bienvenido " .$customer['firstname'] ."!\n";
      $message .= "Ha sido autorizado como cliente de " .$seller['firstname'] .", " .$seller['lastname']  .".\n";
      $message .= "Ya podes comprar los productos que este vendedor ofrece. \n\n";
      $message .= $this->language->get('text_thanks') . "\n";
      $message .= $this->config->get('config_name') . "\n\n";


      $mail = new Mail($this->config->get('config_mail_engine'));
      $mail->parameter = $this->config->get('config_mail_parameter');
      $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
      $mail->smtp_username = $this->config->get('config_mail_smtp_username');
      $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
      $mail->smtp_port = $this->config->get('config_mail_smtp_port');
      $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

      //$mail->setTo($seller['selleremail']);
      $mail->setTo($customer['email']);
      $mail->setFrom($this->config->get('config_email'));
      $mail->setReplyTo($this->config->get('config_email'));
      $mail->setSender(html_entity_decode($seller['username'], ENT_QUOTES, 'UTF-8'));
      $mail->setSubject(html_entity_decode(sprintf($subject, $customer['firstname']), ENT_QUOTES, 'UTF-8'));
      $mail->setText($message);
      $mail->send();

      return true;
    }

    return false;
  }

  public function sendEmailDeny($customer_id, $seller_id)
  {
    //Envío de Email a clientes
    //Cargo las clases necesarias para el envío de Emails
    $this->load->model('saccount/customers');
    $this->load->model('saccount/seller');
    $this->load->model('account/customer');


    //obtengo datos del cliente y del vendedor
    $customer = $this->model_account_customer->getCustomer($customer_id);
    $seller = $this->model_saccount_seller->getSeller($seller_id);


    if (isset($customer) AND isset($seller))
    {
      $subject = "¡Malas Noticias!";
      $message = "Por el momento has sido suspendido para comprar productos de ";
      $message .= $seller['firstname'] .", " .$seller['lastname'] .".\n\n";
      $message .= "Te mantenedremos al tanto de cualquier novedad. \n\n";
      $message .= $this->language->get('text_thanks') . "\n";
      $message .= $this->config->get('config_name') . "\n\n";


      $mail = new Mail($this->config->get('config_mail_engine'));
      $mail->parameter = $this->config->get('config_mail_parameter');
      $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
      $mail->smtp_username = $this->config->get('config_mail_smtp_username');
      $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
      $mail->smtp_port = $this->config->get('config_mail_smtp_port');
      $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

      //$mail->setTo($seller['selleremail']);
      $mail->setTo($customer['email']);
      $mail->setFrom($this->config->get('config_email'));
      $mail->setReplyTo($this->config->get('config_email'));
      $mail->setSender(html_entity_decode($seller['username'], ENT_QUOTES, 'UTF-8'));
      $mail->setSubject(html_entity_decode(sprintf($subject, $customer['firstname']), ENT_QUOTES, 'UTF-8'));
      $mail->setText($message);
      $mail->send();

      return true;
    }

    return false;
  }

  public function edit() {
    $this->load->language('saccount/customers');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('saccount/customers');

    $seller_id = $this->customer->getsellerId();

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
      ###OCULTO POR EL MOMENTO LA EDICIÓN DEL CLIENTE DESDE LA TIENDA DEL VENDEDOR
      //$this->model_saccount_customers->editCustomerSimple($this->request->get['customer_id'], $seller_id, $this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_email'])) {
        $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_customer_group_id'])) {
        $url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['filter_ip'])) {
        $url .= '&filter_ip=' . $this->request->get['filter_ip'];
      }

      if (isset($this->request->get['filter_date_added'])) {
        $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('saccount/customers', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    $this->getForm();
  }


  ###Replicación de funcionalidad de modulo de panel de administración
  protected function getForm() {
    $data['text_form'] = !isset($this->request->get['customer_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

    $data['user_token'] = $this->session->data['user_token'];

    if (isset($this->request->get['customer_id'])) {
      $data['customer_id'] = $this->request->get['customer_id'];
    } else {
      $data['customer_id'] = 0;
    }

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->error['firstname'])) {
      $data['error_firstname'] = $this->error['firstname'];
    } else {
      $data['error_firstname'] = '';
    }

    if (isset($this->error['lastname'])) {
      $data['error_lastname'] = $this->error['lastname'];
    } else {
      $data['error_lastname'] = '';
    }

    if (isset($this->error['email'])) {
      $data['error_email'] = $this->error['email'];
    } else {
      $data['error_email'] = '';
    }

    if (isset($this->error['telephone'])) {
      $data['error_telephone'] = $this->error['telephone'];
    } else {
      $data['error_telephone'] = '';
    }

    if (isset($this->error['cheque'])) {
      $data['error_cheque'] = $this->error['cheque'];
    } else {
      $data['error_cheque'] = '';
    }

    if (isset($this->error['paypal'])) {
      $data['error_paypal'] = $this->error['paypal'];
    } else {
      $data['error_paypal'] = '';
    }

    if (isset($this->error['bank_account_name'])) {
      $data['error_bank_account_name'] = $this->error['bank_account_name'];
    } else {
      $data['error_bank_account_name'] = '';
    }

    if (isset($this->error['bank_account_number'])) {
      $data['error_bank_account_number'] = $this->error['bank_account_number'];
    } else {
      $data['error_bank_account_number'] = '';
    }

    if (isset($this->error['password'])) {
      $data['error_password'] = $this->error['password'];
    } else {
      $data['error_password'] = '';
    }

    if (isset($this->error['confirm'])) {
      $data['error_confirm'] = $this->error['confirm'];
    } else {
      $data['error_confirm'] = '';
    }

    if (isset($this->error['custom_field'])) {
      $data['error_custom_field'] = $this->error['custom_field'];
    } else {
      $data['error_custom_field'] = array();
    }

    if (isset($this->error['address'])) {
      $data['error_address'] = $this->error['address'];
    } else {
      $data['error_address'] = array();
    }

    $url = '';

    if (isset($this->request->get['filter_name'])) {
      $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_email'])) {
      $url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_customer_group_id'])) {
      $url .= '&filter_customer_group_id=' . $this->request->get['filter_customer_group_id'];
    }

    if (isset($this->request->get['filter_status'])) {
      $url .= '&filter_status=' . $this->request->get['filter_status'];
    }

    if (isset($this->request->get['filter_ip'])) {
      $url .= '&filter_ip=' . $this->request->get['filter_ip'];
    }

    if (isset($this->request->get['filter_date_added'])) {
      $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['breadcrumbs'] = array();

     $data['breadcrumbs'][] = array(
         'text'      => $this->language->get('text_home'),
         'href'      => $this->url->link('common/home', '', 'SSL'),
         'separator' => false
     );

  $data['breadcrumbs'][] = array(
         'text'      => $this->language->get('text_account'),
         'href'      => $this->url->link('saccount/account', '', 'SSL'),
         'separator' => ' :: '
     );

     $data['breadcrumbs'][] = array(
         'text'      => $this->language->get('heading_title'),
         'href'      => $this->url->link('saccount/customers', $url, 'SSL'),
         'separator' => ' :: '
     );

    if (!isset($this->request->get['customer_id'])) {
      $data['action'] = $this->url->link('saccount/customers/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
    } else {
      $data['action'] = $this->url->link('saccount/customers/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=' . $this->request->get['customer_id'] . $url, true);
    }

    $data['cancel'] = $this->url->link('saccount/customers', 'user_token=' . $this->session->data['user_token'] . $url, true);

    $this->load->model('saccount/customers');

    if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $customer_info = $this->model_saccount_customers->getCustomer($this->request->get['customer_id']);
      ###ME traigo el estado de la tabla de clientes por vendedor oci_sellers_customers
      $customer_info['status'] = $this->model_saccount_customers->getStatusCustomerOfSeller($this->request->get['customer_id'], $seller_id);
    }


    //$data['customer_groups'] = $this->model_saccount_customers->getCustomerGroups();
    ### Traigo los grupos de clientes asignados al vendedor
    $data['customer_groups'] = $this->model_saccount_customers->getCustomersGroupsOfTheSeller($seller_id = $this->customer->getsellerId());

    ###Agrego al array $data los datos de los grupos de pertenencia del cliente
    ###que estamos editando
    $this->load->model('saccount/customers');
    if(isset($this->request->get['customer_id']))
    {
      $data['customer_groups_ids'] = $this->model_saccount_customers->getGrouposPertenenciaCustomerId($this->request->get['customer_id']);
      $data['sellers_assigned'] = $this->model_saccount_customers->getSellersAsigned();

      $data['membership_group'] = $this->model_saccount_customers->getMembershipGroup($this->request->get['customer_id'], $this->customer->getsellerId());
      $membership_group_id = $data['membership_group']['customer_group_id'];
      $data['customers_groups_of_seller'] = $this->model_saccount_customers->getCustomersGroupsOfSellerLessMembershipGroup($membership_group_id, $this->customer->getsellerId());
    }

    if (isset($this->request->post['customer_group_id'])) {
      $data['customer_group_id'] = $this->request->post['customer_group_id'];
    } elseif (!empty($customer_info)) {
      $data['customer_group_id'] = $customer_info['customer_group_id'];
    } else {
      $data['customer_group_id'] = $this->config->get('config_customer_group_id');
    }

    if (isset($this->request->post['firstname'])) {
      $data['firstname'] = $this->request->post['firstname'];
    } elseif (!empty($customer_info)) {
      $data['firstname'] = $customer_info['firstname'];
    } else {
      $data['firstname'] = '';
    }

    if (isset($this->request->post['lastname'])) {
      $data['lastname'] = $this->request->post['lastname'];
    } elseif (!empty($customer_info)) {
      $data['lastname'] = $customer_info['lastname'];
    } else {
      $data['lastname'] = '';
    }

    if (isset($this->request->post['email'])) {
      $data['email'] = $this->request->post['email'];
    } elseif (!empty($customer_info)) {
      $data['email'] = $customer_info['email'];
    } else {
      $data['email'] = '';
    }

    if (isset($this->request->post['telephone'])) {
      $data['telephone'] = $this->request->post['telephone'];
    } elseif (!empty($customer_info)) {
      $data['telephone'] = $customer_info['telephone'];
    } else {
      $data['telephone'] = '';
    }

    // Custom Fields
    $this->load->model('saccount/customers');

    $data['custom_fields'] = array();

    $filter_data = array(
      'sort'  => 'cf.sort_order',
      'order' => 'ASC'
    );

    $custom_fields = $this->model_saccount_customers->getCustomFields($filter_data);

    foreach ($custom_fields as $custom_field) {
      $data['custom_fields'][] = array(
        'custom_field_id'    => $custom_field['custom_field_id'],
        'custom_field_value' => $this->model_saccount_customers->getCustomFieldValues($custom_field['custom_field_id']),
        'name'               => $custom_field['name'],
        'value'              => $custom_field['value'],
        'type'               => $custom_field['type'],
        'location'           => $custom_field['location'],
        'sort_order'         => $custom_field['sort_order']
      );
    }

    if (isset($this->request->post['custom_field'])) {
      $data['account_custom_field'] = $this->request->post['custom_field'];
    } elseif (!empty($customer_info)) {
      $data['account_custom_field'] = json_decode($customer_info['custom_field'], true);
    } else {
      $data['account_custom_field'] = array();
    }

    if (isset($this->request->post['newsletter'])) {
      $data['newsletter'] = $this->request->post['newsletter'];
    } elseif (!empty($customer_info)) {
      $data['newsletter'] = $customer_info['newsletter'];
    } else {
      $data['newsletter'] = '';
    }

    if (isset($this->request->post['status'])) {
      $data['status'] = $this->request->post['status'];
    } elseif (!empty($customer_info)) {
      $data['status'] = $customer_info['status'];
    } else {
      $data['status'] = true;
    }

    if (isset($this->request->post['safe'])) {
      $data['safe'] = $this->request->post['safe'];
    } elseif (!empty($customer_info)) {
      $data['safe'] = $customer_info['safe'];
    } else {
      $data['safe'] = 0;
    }

    if (isset($this->request->post['password'])) {
      $data['password'] = $this->request->post['password'];
    } else {
      $data['password'] = '';
    }

    if (isset($this->request->post['confirm'])) {
      $data['confirm'] = $this->request->post['confirm'];
    } else {
      $data['confirm'] = '';
    }

    $this->load->model('localisation/country');

    $data['countries'] = $this->model_localisation_country->getCountries();

    if (isset($this->request->post['address'])) {
      $data['addresses'] = $this->request->post['address'];
    } elseif (isset($this->request->get['customer_id'])) {
      $data['addresses'] = $this->model_saccount_customers->getAddresses($this->request->get['customer_id']);
    } else {
      $data['addresses'] = array();
    }

    if (isset($this->request->post['address_id'])) {
      $data['address_id'] = $this->request->post['address_id'];
    } elseif (!empty($customer_info)) {
      $data['address_id'] = $customer_info['address_id'];
    } else {
      $data['address_id'] = '';
    }

    // Affliate
    if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $affiliate_info = $this->model_saccount_customers->getAffiliate($this->request->get['customer_id']);
    }

    if (isset($this->request->post['affiliate'])) {
      $data['affiliate'] = $this->request->post['affiliate'];
    } elseif (!empty($affiliate_info)) {
      $data['affiliate'] = $affiliate_info['status'];
    } else {
      $data['affiliate'] = '';
    }

    if (isset($this->request->post['company'])) {
      $data['company'] = $this->request->post['company'];
    } elseif (!empty($affiliate_info)) {
      $data['company'] = $affiliate_info['company'];
    } else {
      $data['company'] = '';
    }

    if (isset($this->request->post['website'])) {
      $data['website'] = $this->request->post['website'];
    } elseif (!empty($affiliate_info)) {
      $data['website'] = $affiliate_info['website'];
    } else {
      $data['website'] = '';
    }

    if (isset($this->request->post['tracking'])) {
      $data['tracking'] = $this->request->post['tracking'];
    } elseif (!empty($affiliate_info)) {
      $data['tracking'] = $affiliate_info['tracking'];
    } else {
      $data['tracking'] = '';
    }

    if (isset($this->request->post['commission'])) {
      $data['commission'] = $this->request->post['commission'];
    } elseif (!empty($affiliate_info)) {
      $data['commission'] = $affiliate_info['commission'];
    } else {
      $data['commission'] = $this->config->get('config_affiliate_commission');
    }

    if (isset($this->request->post['tax'])) {
      $data['tax'] = $this->request->post['tax'];
    } elseif (!empty($affiliate_info)) {
      $data['tax'] = $affiliate_info['tax'];
    } else {
      $data['tax'] = '';
    }

    if (isset($this->request->post['payment'])) {
      $data['payment'] = $this->request->post['payment'];
    } elseif (!empty($affiliate_info)) {
      $data['payment'] = $affiliate_info['payment'];
    } else {
      $data['payment'] = 'cheque';
    }

    if (isset($this->request->post['cheque'])) {
      $data['cheque'] = $this->request->post['cheque'];
    } elseif (!empty($affiliate_info)) {
      $data['cheque'] = $affiliate_info['cheque'];
    } else {
      $data['cheque'] = '';
    }

    if (isset($this->request->post['paypal'])) {
      $data['paypal'] = $this->request->post['paypal'];
    } elseif (!empty($affiliate_info)) {
      $data['paypal'] = $affiliate_info['paypal'];
    } else {
      $data['paypal'] = '';
    }

    if (isset($this->request->post['bank_name'])) {
      $data['bank_name'] = $this->request->post['bank_name'];
    } elseif (!empty($affiliate_info)) {
      $data['bank_name'] = $affiliate_info['bank_name'];
    } else {
      $data['bank_name'] = '';
    }

    if (isset($this->request->post['bank_branch_number'])) {
      $data['bank_branch_number'] = $this->request->post['bank_branch_number'];
    } elseif (!empty($affiliate_info)) {
      $data['bank_branch_number'] = $affiliate_info['bank_branch_number'];
    } else {
      $data['bank_branch_number'] = '';
    }

    if (isset($this->request->post['bank_swift_code'])) {
      $data['bank_swift_code'] = $this->request->post['bank_swift_code'];
    } elseif (!empty($affiliate_info)) {
      $data['bank_swift_code'] = $affiliate_info['bank_swift_code'];
    } else {
      $data['bank_swift_code'] = '';
    }

    if (isset($this->request->post['bank_account_name'])) {
      $data['bank_account_name'] = $this->request->post['bank_account_name'];
    } elseif (!empty($affiliate_info)) {
      $data['bank_account_name'] = $affiliate_info['bank_account_name'];
    } else {
      $data['bank_account_name'] = '';
    }

    if (isset($this->request->post['bank_account_number'])) {
      $data['bank_account_number'] = $this->request->post['bank_account_number'];
    } elseif (!empty($affiliate_info)) {
      $data['bank_account_number'] = $affiliate_info['bank_account_number'];
    } else {
      $data['bank_account_number'] = '';
    }

    if (isset($this->request->post['custom_field'])) {
      $data['affiliate_custom_field'] = $this->request->post['custom_field'];
    } elseif (!empty($affiliate_info)) {
      $data['affiliate_custom_field'] = json_decode($affiliate_info['custom_field'], true);
    } else {
      $data['affiliate_custom_field'] = array();
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    ###Información del grupo por defecto de clientes de este vendedor
    $data['default_customer_group'] = $this->model_saccount_customers->getDefaultCustomerGroupSeller($seller_id);

    $this->response->setOutput($this->load->view('saccount/customers_form', $data));
  }

  protected function validateForm() {
    $this->load->library('cart/customer');
    if (!$this->customer->hasPermission('modify', 'saccount/customers')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
      $this->error['firstname'] = $this->language->get('error_firstname');
    }

    if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
      $this->error['lastname'] = $this->language->get('error_lastname');
    }

    if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
      $this->error['email'] = $this->language->get('error_email');
    }

    $customer_info = $this->model_saccount_customers->getCustomerByEmail($this->request->post['email']);

    if (!isset($this->request->get['customer_id'])) {
      if ($customer_info) {
        $this->error['warning'] = $this->language->get('error_exists');
      }
    } else {
      if ($customer_info && ($this->request->get['customer_id'] != $customer_info['customer_id'])) {
        $this->error['warning'] = $this->language->get('error_exists');
      }
    }

    if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
      $this->error['telephone'] = $this->language->get('error_telephone');
    }

    // Custom field validation
    $this->load->model('saccount/customers');

    $custom_fields = $this->model_saccount_customers->getCustomFields(array('filter_customer_group_id' => $this->request->post['customer_group_id']));

    foreach ($custom_fields as $custom_field) {
      if (($custom_field['location'] == 'account') && $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['custom_field_id']])) {
        $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
      } elseif (($custom_field['location'] == 'account') && ($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
        $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
      }
    }

    if ($this->request->post['password'] || (!isset($this->request->get['customer_id']))) {
      if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
        $this->error['password'] = $this->language->get('error_password');
      }

      if ($this->request->post['password'] != $this->request->post['confirm']) {
        $this->error['confirm'] = $this->language->get('error_confirm');
      }
    }

    if (isset($this->request->post['address'])) {
      foreach ($this->request->post['address'] as $key => $value) {
        if ((utf8_strlen($value['firstname']) < 1) || (utf8_strlen($value['firstname']) > 32)) {
          $this->error['address'][$key]['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen($value['lastname']) < 1) || (utf8_strlen($value['lastname']) > 32)) {
          $this->error['address'][$key]['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($value['address_1']) < 3) || (utf8_strlen($value['address_1']) > 128)) {
          $this->error['address'][$key]['address_1'] = $this->language->get('error_address_1');
        }

        if ((utf8_strlen($value['city']) < 2) || (utf8_strlen($value['city']) > 128)) {
          $this->error['address'][$key]['city'] = $this->language->get('error_city');
        }

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($value['country_id']);

        if ($country_info && $country_info['postcode_required'] && (utf8_strlen($value['postcode']) < 2 || utf8_strlen($value['postcode']) > 10)) {
          $this->error['address'][$key]['postcode'] = $this->language->get('error_postcode');
        }

        if ($value['country_id'] == '') {
          $this->error['address'][$key]['country'] = $this->language->get('error_country');
        }

        if (!isset($value['zone_id']) || $value['zone_id'] == '') {
          $this->error['address'][$key]['zone'] = $this->language->get('error_zone');
        }

        foreach ($custom_fields as $custom_field) {
          if (($custom_field['location'] == 'address') && $custom_field['required'] && empty($value['custom_field'][$custom_field['custom_field_id']])) {
            $this->error['address'][$key]['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
          } elseif (($custom_field['location'] == 'address') && ($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($value['custom_field'][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
            $this->error['address'][$key]['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                    }
        }
      }
    }

    if ($this->request->post['affiliate']) {
      if ($this->request->post['payment'] == 'cheque') {
        if ($this->request->post['cheque'] == '') {
          $this->error['cheque'] = $this->language->get('error_cheque');
        }
      } elseif ($this->request->post['payment'] == 'paypal') {
        if ((utf8_strlen($this->request->post['paypal']) > 96) || !filter_var($this->request->post['paypal'], FILTER_VALIDATE_EMAIL)) {
          $this->error['paypal'] = $this->language->get('error_paypal');
        }
      } elseif ($this->request->post['payment'] == 'bank') {
        if ($this->request->post['bank_account_name'] == '') {
          $this->error['bank_account_name'] = $this->language->get('error_bank_account_name');
        }

        if ($this->request->post['bank_account_number'] == '') {
          $this->error['bank_account_number'] = $this->language->get('error_bank_account_number');
        }
      }

      if (!$this->request->post['tracking']) {
        $this->error['tracking'] = $this->language->get('error_tracking');
      }

      $affiliate_info = $this->model_saccount_customers->getAffliateByTracking($this->request->post['tracking']);

      if (!isset($this->request->get['customer_id'])) {
        if ($affiliate_info) {
          $this->error['tracking'] = $this->language->get('error_tracking_exists');
        }
      } else {
        if ($affiliate_info && ($this->request->get['customer_id'] != $affiliate_info['customer_id'])) {
          $this->error['tracking'] = $this->language->get('error_tracking_exists');
        }
      }

      foreach ($custom_fields as $custom_field) {
        if (($custom_field['location'] == 'affiliate') && $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['custom_field_id']])) {
          $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
        } elseif (($custom_field['location'] == 'affiliate') && ($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
          $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
        }
      }
    }

    if ($this->error && !isset($this->error['warning'])) {
      $this->error['warning'] = $this->language->get('error_warning');
    }

    return !$this->error;
  }

  protected function validateDelete() {
    if (!$this->user->hasPermission('modify', 'customer/customer')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    return !$this->error;
  }

  protected function validateUnlock() {
    if (!$this->user->hasPermission('modify', 'customer/customer')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    return !$this->error;
  }

  public function login() {
    if (isset($this->request->get['customer_id'])) {
      $customer_id = $this->request->get['customer_id'];
    } else {
      $customer_id = 0;
    }

    $this->load->model('customer/customer');

    $customer_info = $this->model_saccount_customers->getCustomer($customer_id);

    if ($customer_info) {
      // Create token to login with
      $token = token(64);

      $this->model_saccount_customers->editToken($customer_id, $token);

      if (isset($this->request->get['store_id'])) {
        $store_id = $this->request->get['store_id'];
      } else {
        $store_id = 0;
      }

      $this->load->model('setting/store');

      $store_info = $this->model_setting_store->getStore($store_id);

      if ($store_info) {
        $this->response->redirect($store_info['url'] . 'index.php?route=account/login&token=' . $token);
      } else {
        $this->response->redirect(HTTP_CATALOG . 'index.php?route=account/login&token=' . $token);
      }
    } else {
      $this->load->language('error/not_found');

      $this->document->setTitle($this->language->get('heading_title'));

      $data['breadcrumbs'] = array();

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('text_home'),
           'href'      => $this->url->link('common/home', '', 'SSL'),
           'separator' => false
       );

    $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('text_account'),
           'href'      => $this->url->link('saccount/account', '', 'SSL'),
           'separator' => ' :: '
       );

       $data['breadcrumbs'][] = array(
           'text'      => $this->language->get('heading_title'),
           'href'      => $this->url->link('saccount/customers', $url, 'SSL'),
           'separator' => ' :: '
       );

      $data['header'] = $this->load->controller('common/header');
      $data['column_left'] = $this->load->controller('common/column_left');
      $data['footer'] = $this->load->controller('common/footer');

      $this->response->setOutput($this->load->view('error/not_found', $data));
    }
  }

  ###Agregamos a la edición del cliente, la posibilidad de modificar los
  ###grupos de pertenencia
  public function addmodsgroups()
  {
    $this->load->model('saccount/customers');
    $this->load->language('saccount/customers');

    ### IMPORTANTE: CONTROLA SI EL VENDEDOR TIENE PERMISISOS
    ### IMPORTANTE: CARGA LIBRERIA EXTERNA
    ### Cargo la libreria de usuario para chequear si tiene permisos
    ### $this->customer->hasPermission('modify', 'saccount/customers')
    $this->load->library('cart/customer');
    //$this->load->library('cart/user');
    //$this->load->library('seller');


    $json = array();
    if (!$this->customer->hasPermission('modify', 'saccount/customers')) {
      $json['error'] = $this->language->get('error_permission');
    } else {
      if($this->request->post['gruposDeClientes'])
      {
        $gruposDeClientes = $this->request->post['gruposDeClientes'];
        $i = 0;
        foreach($gruposDeClientes as $grupo)
        {
          $i++;
          if($grupo['pertenece'] === "true")
          {
            $json['sql'.$i] = $this->model_saccount_customers->addToGroup($this->request->get['customer_id'], $grupo['id']);

          } else if($grupo['pertenece'] === "false")
            {
              $json['sql-no-pertenece'.$i] =$this->model_saccount_customers->removeFromTheGroup($this->request->get['customer_id'], $grupo['id']);
            }
        }

        $json['success'] = $this->language->get('text_success_groups');
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));

  }

}
?>
