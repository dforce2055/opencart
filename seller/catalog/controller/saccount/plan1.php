<?php
class ControllerSAccountPlan1 extends Controller {
	private $error = array();

	public function index() {
		
	

		$this->language->load('saccount/plan1');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
	
      	$data['breadcrumbs'] = array();

      	$data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),     	
        	'separator' => false
      	); 

      	$data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('saccount/account', '', 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);

      	$data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_plan1'),
			'href'      => $this->url->link('saccount/plan1', '', 'SSL'),       	
        	'separator' => $this->language->get('text_separator')
      	);
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_your_details'] = $this->language->get('text_your_details');
		$data['text_your_imagegallery'] = $this->language->get('text_your_imagegallery');
		
		
		$data['text_plan1'] = $this->language->get('text_plan1');

		$data['column_plan'] = $this->language->get('column_plan');
		$data['column_duration'] = $this->language->get('column_duration');
		$data['column_charges'] = $this->language->get('column_charges');
		$data['column_upgrade'] = $this->language->get('column_upgrade');
		$data['column_about'] = $this->language->get('column_about');

		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_video_url'] = $this->language->get('entry_video_url');
		$data['button_add_image'] = $this->language->get('button_add_image');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['button_upload'] = $this->language->get('button_upload');
		
		$data['entry_image'] = $this->language->get('entry_image');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		
	   
	   	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "commission ORDER BY commission_id");
			
	
	   
	   $getmemberships = $query->rows;
	   
	     $data['getmemberships'] = array();

			

			foreach ($getmemberships as $getmemberships) {
			

			             $data['getmemberships'][] = array(
						       
							'commission_id' =>  $getmemberships['commission_id'],
							'per' =>  $getmemberships['per'],
							'product_limit' =>  $getmemberships['product_limit'],
							'duration_id' =>  $getmemberships['duration_id'],
							'commission_name' =>  $getmemberships['commission_name'],
							'amount1' =>  $getmemberships['amount'],
							'amount' =>  $this->currency->format($getmemberships['amount'],$this->config->get('config_currency'))
						 
						 );

			

			}
	   
	  

		$data['action'] = $this->url->link('saccount/plan1', '', 'SSL');

		
		$data['durations']['d'] = 'Day(s)'; 
		$data['durations']['w'] = 'Week(s)'; 
		$data['durations']['m'] = 'Month(s)'; 
		$data['durations']['y'] = 'Year(s)';
		$data['durations']['l'] = 'Lifetime';
		
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
	   $data['currency_code'] = $this->config->get('config_currency');
		
		
		
		$data['back'] = $this->url->link('saccount/account', '', 'SSL');

		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/saccount/plan1.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/saccount/plan1', $data));
		} else {
			$this->response->setOutput($this->load->view('saccount/plan1', $data));
		}	
	}
	
	

	

	
}
?>