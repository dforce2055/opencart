<?php
class ControllerSAccountSuccess extends Controller {
	public function index() {
		$this->load->language('saccount/success');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('saccount/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('saccount/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

						$seller_group_info1=$this->config->get('config_seller_autoapprove');				if($seller_group_info1){			$data['text_message'] = sprintf($this->language->get('text_approval1'), $this->config->get('config_name'), 			$this->url->link('information/contact'));		}		else{		             $data['text_message'] = sprintf($this->language->get('text_message'), 			$this->config->get('config_name'), $this->url->link('information/contact'));			}

		$data['button_continue'] = $this->language->get('button_continue');

		if ($this->cart->hasProducts()) {
			$data['continue'] = $this->url->link('checkout/cart');
		} else {
			$data['continue'] = $this->url->link('saccount/account', '', 'SSL');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('common/success', $data));
	}
}