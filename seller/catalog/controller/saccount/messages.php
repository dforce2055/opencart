<?php 
class ControllerSaccountMessages extends Controller {
	private $error = array();
		
	public function index() {
    	if (!$this->seller->isLogged()) {
      		$this->session->data['redirect'] = $this->url->link('saccount/messages', '', 'SSL');
	  		$this->redirect($this->url->link('saccount/login', '', 'SSL'));
    	}		
		$this->language->load('saccount/messages');		
		$this->load->model('saccount/messages');
    	$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),        	
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/order', $url, 'SSL'),        	
        	'separator' => $this->language->get('text_separator')
      	);

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_order_id'] = $this->language->get('text_order_id');
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['text_date_added'] = $this->language->get('text_date_added');
		$this->data['text_customer'] = $this->language->get('text_customer');
		$this->data['text_enquiry'] = $this->language->get('text_enquiry');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_empty'] = $this->language->get('text_empty');

		$this->data['button_reply'] = $this->language->get('button_reply');
		$this->data['button_view'] = $this->language->get('button_view');
		$this->data['button_reorder'] = $this->language->get('button_reorder');
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$this->data['messages'] = array();
		
		$message_total = $this->model_saccount_messages->getTotalMessages();
		
		$results = $this->model_saccount_messages->getMessages(($page - 1) * 10, 10);
		
		foreach ($results as $result) {
		
			$this->data['messages'][] = array(
				'contactus_id'   => $result['contactus_id'],
				'customer_id'	=> $result['customer_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'contactus_enquiry'     => $result['contactus_enquiry'],
				'messageby'     => $result['messageby'],
				'reply'       => $this->url->link('information/contactuss', 'id=' . $result['customer_id'], 'SSL'),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $message_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('account/order', 'page={page}', 'SSL');
		
		$this->data['pagination'] = $pagination->render();

		$this->data['continue'] = $this->url->link('saccount/account', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/saccount/messages_list.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/saccount/messages_list.tpl';
		} else {
			$this->template = 'default/template/account/messages_list.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
						
		$this->response->setOutput($this->render());				
	}
}
?>