<?php
class ControllerSaccountOffer extends Controller {
	private $error = array();

  	public function index() {
		if (!$this->customer->issellerLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('saccount/offer', '', 'SSL');

	  		$this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
    	}
		$this->load->language('saccount/offer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('saccount/offer');

		$this->getList();


  	}

  	public function insert() {

		if (!$this->customer->issellerLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('saccount/offer/insert', '', 'SSL');

	  		$this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
    	}
    	$this->load->language('saccount/offer');

    	$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('saccount/offer');


    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm1()) {

			$this->model_saccount_offer->addProduct($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name1'])) {
				$url .= '&filter_name1=' . $this->request->get['filter_name1'];
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . $this->request->get['filter_model'];
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}



			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('saccount/extension', $url, 'SSL'));
    	}

    	$this->details();
  	}



  	public function preview() {
		if (!$this->customer->issellerLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('saccount/offer', '', 'SSL');

	  		$this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
    	}
    	$this->load->language('saccount/offer');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('saccount/offer');

		if (isset($this->request->post) && $this->validateForm()) {

				$product_id = $this->request->post['select_product'];


				$url = '';

			if (isset($this->request->post['select_product'])) {
				$url .= '&product_id=' . $this->request->post['select_product'];
			}

				$this->response->redirect($this->url->link('saccount/offer/show', $url, 'SSL'));


		}

		$this->getList();


  	}



  	private function getList() {
		if (isset($this->request->get['filter_name1'])) {
			$filter_name1 = $this->request->get['filter_name1'];
		} else {
			$filter_name1 = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];
		} else {
			$filter_sku = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}



		if (isset($this->request->get['filter_seller'])) {
			$filter_seller = $this->request->get['filter_seller'];
		} else {
			$filter_seller = NULL;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name1'])) {
			$url .= '&filter_name1=' . $this->request->get['filter_name1'];
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . $this->request->get['filter_model'];
		}

		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}



		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', '', 'SSL'),
      		'separator' => false
   		);

		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('saccount/account', '', 'SSL'),
      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('saccount/offer', '', 'SSL'),
      		'separator' => ' :: '
   		);

		$data['insert'] = $this->url->link('saccount/extension/insert', '', 'SSL');

		$data['action'] = $this->url->link('saccount/offer/preview', '', 'SSL');

		$data['products'] = array();

		$data1 = array(
			'filter_name1'	  => $filter_name1,
			'filter_model'	  => $filter_model,
			'filter_sku'	  => $filter_sku,
			'filter_seller'   => $filter_seller,
			'filter_price'	  => $filter_price,
			'filter_quantity' => $filter_quantity,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		$seller_id = $this->customer->getsellerId();
		$this->load->model('tool/image');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['new_offers'] = sprintf($this->language->get('new_offers'), $this->url->link('saccount/offer/insert', '', 'SSL'));
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_image_manager'] = $this->language->get('text_image_manager');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_downloads'] = $this->language->get('column_downloads');
		$data['button_copy'] = $this->language->get('button_copy');
		$data['button_insert'] = $this->language->get('button_insert');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');


			$data['button_next'] = $this->language->get('button_next');

		$data['text_insert'] = $this->language->get('text_insert');

		$data['text_search_product'] = $this->language->get('text_search_product');


		$data['text_product'] = $this->language->get('text_product');


		/**NEW ADDED CODE**/

		$data['column_image'] = $this->language->get('column_image');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_quantity'] = $this->language->get('column_quantity');



		if (isset($this->session->data['token'])) {
			$data['token'] = $this->session->data['token'];

		} else {
			$data['token'] = '';
		}

		/**/

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name1'])) {
			$url .= '&filter_name1=' . $this->request->get['filter_name1'];
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . $this->request->get['filter_model'];
		}

		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}



		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('saccount/offer', 'sort=pd.name' . $url, 'SSL');
		$data['sort_model'] = $this->url->link('saccount/offer', 'sort=p.model' . $url, 'SSL');
		$data['sort_sku'] = $this->url->link('saccount/offer', 'sort=p.sku' . $url, 'SSL');
		$data['sort_price'] = $this->url->link('saccount/offer', 'sort=p.price' . $url, 'SSL');
		$data['sort_quantity'] = $this->url->link('saccount/offer', 'sort=p.quantity' . $url, 'SSL');
		$data['sort_order'] = $this->url->link('saccount/offer', 'sort=p.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name1'])) {
			$url .= '&filter_name1=' . $this->request->get['filter_name1'];
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . $this->request->get['filter_model'];
		}

		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}


		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('saccount/offer_list', $data));

  	}

	public function add() {

		if (!$this->customer->issellerLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('saccount/offer/insert', '', 'SSL');

	  		$this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
    	}
    	$this->load->language('saccount/offer');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('saccount/offer');


    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm1()) {

			$this->model_saccount_offer->addoffer($this->request->post);

			$this->session->data['success'] = "Offer Added Successfully";

			$url = "";
			$this->response->redirect($this->url->link('saccount/offer', $url, 'SSL'));
    	}



  	}

  	public function update() {
		if (!$this->customer->issellerLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('saccount/offer', '', 'SSL');

	  		$this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
    	}
    	$this->load->language('saccount/offer');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('saccount/offer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm2()) {


			$this->model_saccount_offer->editoffer($this->request->get['sproduct_id'],$this->request->post);

			$this->session->data['success'] = "Offer Updated Successfully";

			$url = "";
			$this->response->redirect($this->url->link('saccount/offer', $url, 'SSL'));
    	}





  	}


	public function newform() {

	   $this->load->language('saccount/offer');

    	$data['heading_title'] = $this->language->get('heading_title');


		$this->document->setTitle($this->language->get('heading_title'));

    	$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
    	$data['text_none'] = $this->language->get('text_none');
    	$data['text_yes'] = $this->language->get('text_yes');
    	$data['text_no'] = $this->language->get('text_no');
		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_image_manager'] = $this->language->get('text_image_manager');
		$data['text_browse'] = $this->language->get('text_browse');
		$data['text_clear'] = $this->language->get('text_clear');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

        $data['entry_captcha'] = $this->language->get('entry_captcha');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_license'] = $this->language->get('entry_license');
		$data['entry_free'] = $this->language->get('entry_free');
		$data['entry_commerical'] = $this->language->get('entry_commerical');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
    	$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_sku'] = $this->language->get('entry_sku');
		$data['entry_upc'] = $this->language->get('entry_upc');
		$data['entry_location'] = $this->language->get('entry_location');
		$data['entry_minimum'] = $this->language->get('entry_minimum');
		$data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
    	$data['entry_shipping'] = $this->language->get('entry_shipping');
    	$data['entry_date_available'] = $this->language->get('entry_date_available');
    	$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_stock_status'] = $this->language->get('entry_stock_status');
    	$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_points'] = $this->language->get('entry_points');
		$data['entry_option_points'] = $this->language->get('entry_option_points');
		$data['entry_subtract'] = $this->language->get('entry_subtract');
    	$data['entry_weight_class'] = $this->language->get('entry_weight_class');
    	$data['entry_weight'] = $this->language->get('entry_weight');
		$data['entry_dimension'] = $this->language->get('entry_dimension');
		$data['entry_length'] = $this->language->get('entry_length');
    	$data['entry_image'] = $this->language->get('entry_image');
    	$data['entry_download'] = $this->language->get('entry_download');

		/**NEW ADDED CODE **/
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		/**END**/

		$data['download_image1'] = $this->language->get('download_image1');
		$data['download_image2'] = $this->language->get('download_image2');
		$data['button_add_download'] = $this->language->get('button_add_download');
    	$data['entry_category'] = $this->language->get('entry_category');

		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_attribute'] = $this->language->get('entry_attribute');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_option'] = $this->language->get('entry_option');
		$data['entry_option_value'] = $this->language->get('entry_option_value');
		$data['entry_required'] = $this->language->get('entry_required');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_seller_group'] = $this->language->get('entry_seller_group');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_priority'] = $this->language->get('entry_priority');
		$data['entry_tag'] = $this->language->get('entry_tag');
		$data['entry_seller_group'] = $this->language->get('entry_seller_group');
		$data['entry_reward'] = $this->language->get('entry_reward');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_seller_country_origin'] = $this->language->get('entry_seller_country_origin');
		$data['entry_seller_product_cost'] = $this->language->get('entry_seller_product_cost');
		$data['entry_seller_shipping_method'] = $this->language->get('entry_seller_shipping_method');
		$data['entry_seller_prefered_shipping_method'] = $this->language->get('entry_seller_prefered_shipping_method');
		$data['entry_seller_shipping_cost'] = $this->language->get('entry_seller_shipping_cost');
		$data['entry_seller_total'] = $this->language->get('entry_seller_total');
		$data['entry_seller_company'] = $this->language->get('entry_seller_company');
		$data['entry_seller_description'] = $this->language->get('entry_seller_description');
		$data['entry_seller_contact_name'] = $this->language->get('entry_seller_contact_name');
		$data['entry_seller_telephone'] = $this->language->get('entry_seller_telephone');
		$data['entry_seller_fax'] = $this->language->get('entry_seller_fax');
		$data['entry_seller_email'] = $this->language->get('entry_seller_email');
		$data['entry_seller_paypal_email'] = $this->language->get('entry_seller_paypal_email');
		$data['entry_seller_address'] = $this->language->get('entry_seller_address');
		$data['entry_seller_country_zone'] = $this->language->get('entry_seller_country_zone');
		$data['entry_seller_store_url'] = $this->language->get('entry_seller_store_url');
		$data['entry_seller_product_url'] = $this->language->get('entry_seller_product_url');
		$data['entry_seller_name'] = $this->language->get('entry_seller_name');
		$data['entry_seller_wholesale'] = $this->language->get('entry_seller_wholesale');
		$data['entry_image1'] = $this->language->get('entry_image1');
		$data['entry_image2'] = $this->language->get('entry_image2');
		$data['tab_seller'] = $this->language->get('tab_seller');
    	$data['button_save'] = $this->language->get('button_save');
    	$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_add_attribute'] = $this->language->get('button_add_attribute');
		$data['button_add_option'] = $this->language->get('button_add_option');
		$data['button_add_option_value'] = $this->language->get('button_add_option_value');
		$data['button_add_discount'] = $this->language->get('button_add_discount');
		$data['button_add_special'] = $this->language->get('button_add_special');
		$data['button_add_image'] = $this->language->get('button_add_image');
		$data['button_remove'] = $this->language->get('button_remove');

    	$data['tab_documentation'] = $this->language->get('tab_documentation');
    	$data['tab_description'] = $this->language->get('tab_description');
		$data['tab_downloads'] = $this->language->get('tab_downloads');
    	$data['tab_image'] = $this->language->get('tab_image');


       $data['tab_general'] = $this->language->get('tab_general');
    	$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_attribute'] = $this->language->get('tab_attribute');
		$data['tab_option'] = $this->language->get('tab_option');
		$data['tab_discount'] = $this->language->get('tab_discount');
		$data['tab_special'] = $this->language->get('tab_special');
    	$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_links'] = $this->language->get('tab_links');
		$data['tab_reward'] = $this->language->get('tab_reward');
		$data['tab_design'] = $this->language->get('tab_design');


 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
    		$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

 		if (isset($this->error['name'])) {
			$data['error_names'] = $this->error['name'];
		} else {
			$data['error_names'] = "";
		}

 		if (isset($this->error['price'])) {
			$data['error_prices'] = $this->error['price'];
		} else {
			$data['error_prices'] = "";
		}




		$url = '';

		if (isset($this->request->get['filter_name1'])) {
			$url .= '&filter_name1=' . $this->request->get['filter_name1'];
		}

		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . $this->request->get['filter_model'];
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->post['captcha'])) {
			$data['captcha'] = $this->request->post['captcha'];
		} else {
			$data['captcha'] = '';
		}

		if (isset($this->error['download'])) {
			$data['error_download'] = $this->error['download'];
		} else {
			$data['error_download'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', '', 'SSL'),
			'separator' => false
   		);

		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('saccount/account',$url, 'SSL'),
      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title1'),
			'href'      => $this->url->link('saccount/offer',$url, 'SSL'),
      		'separator' => ' :: '
   		);





		 if (!isset($this->request->get['sproduct_id'])) {
			$data['action'] = $this->url->link('saccount/offer/add',  $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('saccount/offer/update','&sproduct_id=' . $this->request->get['sproduct_id'] . $url, 'SSL');
		}




		$this->load->model('saccount/offer');





		$data['insert'] = $this->url->link('saccount/offer/newform',  $url, 'SSL');

		$data['cancel'] = $this->url->link('saccount/offer',$url, 'SSL');

		$this->load->model('saccount/offer');


		if (isset($this->request->get['sproduct_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
		$product_info = $this->model_saccount_offer->getoffer($this->request->get['sproduct_id']);
		}





		if (isset($this->request->post['description'])) {
      		$data['description'] = $this->request->post['description'];
    	} elseif (isset($this->request->get['sproduct_id'])) {
			$data['description'] = $product_info['description'];
		} else {
      		$data['description'] = '';
    	}




		if (isset($this->request->post['price'])) {
      		$data['price'] = $this->request->post['price'];
		}elseif (isset($this->request->get['sproduct_id'])) {
			$data['price'] = $product_info['price'];
		}  else {
      		$data['price'] = '';
    	}



		if (isset($this->request->post['name'])) {
      		$data['name'] = $this->request->post['name'];
    	} elseif (isset($this->request->get['sproduct_id'])) {
			$data['name'] = $product_info['name'];
		} else {
      		$data['name'] = "";
		}









		if (isset($this->request->post['productimage'])) {
			$data['productimage'] = $this->request->post['productimage'];
		} elseif (isset($this->request->get['sproduct_id'])) {

			$graphic_file= explode('/',$product_info['graphic_file']);
			$data['productimage'] = array_pop($graphic_file);;


		} else {
			$data['productimage'] = '';
		}

		$this->load->model('tool/image');


		if (isset($this->request->post['steam_gift'])) {
			$data['steam_gift'] = $this->request->post['steam_gift'];
		}elseif (isset($this->request->get['sproduct_id'])) {
			$data['steam_gift'] = $product_info['steam_gift'];
		} else {
			$data['steam_gift'] = 0;
		}

    	if (isset($this->request->post['cd_key'])) {
      		$data['cd_key'] = $this->request->post['cd_key'];
    	}elseif (isset($this->request->get['sproduct_id'])) {
			$data['cd_key'] = $this->model_saccount_offer->getProductkeys($this->request->get['sproduct_id']);
		}  else {
      		$data['cd_key'] = array();
    	}



		$data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);



		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('saccount/new_form', $data));
  	}

  	public function show() {



	   $this->load->language('saccount/offer');

    	$data['heading_title1'] = $this->language->get('heading_title1');
		$this->document->setTitle($this->language->get('heading_title1'));
    	$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
    	$data['text_none'] = $this->language->get('text_none');
    	$data['text_yes'] = $this->language->get('text_yes');
    	$data['text_no'] = $this->language->get('text_no');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_reg'] = $this->language->get('entry_reg');
		$data['entry_reg1'] = $this->language->get('entry_reg1');
		$data['entry_upc'] = $this->language->get('entry_upc');

		$data['b_no'] = $this->language->get('b_no');
		$data['b_yes'] = $this->language->get('b_yes');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
    		$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}


		$url = '';

		if (isset($this->request->get['product_id'])) {
			$url .= '&product_id=' . $this->request->get['product_id'];
		}



  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', '', 'SSL'),
			'separator' => false
   		);

		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('saccount/account','', 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('saccount/offer','', 'SSL')
   		);

		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title1'),
			'href'      => $this->url->link('saccount/offer/show',$url, 'SSL')
   		);




	     $data['action'] = $this->url->link('saccount/offer/details',$url, 'SSL');



		$data['insert'] = $this->url->link('saccount/extension/insert',  '', 'SSL');

		$data['cancel'] = $this->url->link('saccount/offer','', 'SSL');

		$this->load->model('saccount/product');

		if (isset($this->request->get['product_id'])) {
      		$product_info = $this->model_saccount_product->getProduct($this->request->get['product_id']);
    	}



		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (!empty($product_info)) {
			$data['product_description'] = $product_info['name'];
		} else {
			$data['product_description'] = "";
		}

		if (!empty($product_info)) {
			$data['model'] = $product_info['model'];
		} else {
			$data['model'] = "";
		}



		if (!empty($product_info)) {
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
		} else {
      		$data['description'] = '';
    	}




		if (!empty($product_info)) {
			$image= explode('/',$product_info['image']);
			$data['productimage'] = array_pop($image);;
		} else {
			$data['productimage'] = '';
		}

		$this->load->model('tool/image');

		if (!empty($product_info) && $product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}



		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('saccount/offer_form', $data));

  	}



	public function details() {
		if (!$this->customer->issellerLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('saccount/offer', '', 'SSL');

	  		$this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
    	}
	   $this->load->language('saccount/offer');

	   $this->document->setTitle($this->language->get('heading_title2'));

    	$data['heading_title1'] = $this->language->get('heading_title2');

    	$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
    	$data['text_none'] = $this->language->get('text_none');
    	$data['text_yes'] = $this->language->get('text_yes');
    	$data['text_no'] = $this->language->get('text_no');
		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_image_manager'] = $this->language->get('text_image_manager');
		$data['text_browse'] = $this->language->get('text_browse');
		$data['text_clear'] = $this->language->get('text_clear');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');

        $data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_sku'] = $this->language->get('entry_sku');
		$data['entry_upc'] = $this->language->get('entry_upc');
		$data['entry_ean'] = $this->language->get('entry_ean');
		$data['entry_jan'] = $this->language->get('entry_jan');
		$data['entry_isbn'] = $this->language->get('entry_isbn');
		$data['entry_mpn'] = $this->language->get('entry_mpn');
		$data['entry_location'] = $this->language->get('entry_location');
		$data['entry_minimum'] = $this->language->get('entry_minimum');
		$data['entry_shipping'] = $this->language->get('entry_shipping');
		$data['entry_date_available'] = $this->language->get('entry_date_available');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_points'] = $this->language->get('entry_points');
		$data['entry_option_points'] = $this->language->get('entry_option_points');
		$data['entry_subtract'] = $this->language->get('entry_subtract');
		$data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$data['entry_weight'] = $this->language->get('entry_weight');
		$data['entry_dimension'] = $this->language->get('entry_dimension');
		$data['entry_length_class'] = $this->language->get('entry_length_class');
		$data['entry_length'] = $this->language->get('entry_length');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
		$data['entry_download'] = $this->language->get('entry_download');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_filter'] = $this->language->get('entry_filter');
		$data['entry_related'] = $this->language->get('entry_related');
		$data['entry_attribute'] = $this->language->get('entry_attribute');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_option'] = $this->language->get('entry_option');
		$data['entry_option_value'] = $this->language->get('entry_option_value');
		$data['entry_required'] = $this->language->get('entry_required');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_priority'] = $this->language->get('entry_priority');
		$data['entry_tag'] = $this->language->get('entry_tag');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_reward'] = $this->language->get('entry_reward');
		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_recurring'] = $this->language->get('entry_recurring');



        $data['button_save'] = $this->language->get('button_save');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_attribute_add'] = $this->language->get('button_attribute_add');
		$data['button_option_add'] = $this->language->get('button_option_add');
		$data['button_option_value_add'] = $this->language->get('button_option_value_add');
		$data['button_discount_add'] = $this->language->get('button_discount_add');
		$data['button_special_add'] = $this->language->get('button_special_add');
		$data['button_image_add'] = $this->language->get('button_image_add');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_recurring_add'] = $this->language->get('button_recurring_add');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_attribute'] = $this->language->get('tab_attribute');
		$data['tab_option'] = $this->language->get('tab_option');
		$data['tab_recurring'] = $this->language->get('tab_recurring');
		$data['tab_discount'] = $this->language->get('tab_discount');
		$data['tab_special'] = $this->language->get('tab_special');
		$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_links'] = $this->language->get('tab_links');
		$data['tab_reward'] = $this->language->get('tab_reward');
		$data['tab_design'] = $this->language->get('tab_design');
		$data['tab_openbay'] = $this->language->get('tab_openbay');


		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');



 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
    		$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

   		if (isset($this->error['price'])) {
			$data['error_price'] = $this->error['price'];
		} else {
			$data['error_price'] = "";
		}


		$url = '';

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['product_id'])) {
			$url .= '&product_id=' . $this->request->get['product_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', '', 'SSL')
   		);

		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('saccount/account','', 'SSL')
   		);



   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('saccount/offer','', 'SSL')
   		);

		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title1'),
			'href'      => $this->url->link('saccount/offer/show',$url, 'SSL')
   		);


		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title2'),
			'href'      => $this->url->link('saccount/offer/details',$url, 'SSL')
   		);



         if(isset($this->request->get['product_id'])){
			$data['action'] = $this->url->link('saccount/offer/insert',$url, 'SSL');
			$data['product_id'] = $this->request->get['product_id'];
		 }
		 else{
			$data['action'] = $this->url->link('saccount/offer','', 'SSL');
			$data['product_id'] =0;
		}


		$data['insert'] = $this->url->link('saccount/extension/insert',  '', 'SSL');

		$data['cancel'] = $this->url->link('saccount/offer','', 'SSL');

		$this->load->model('saccount/offer');

		if (isset($this->request->get['product_id'])) {
      		$product_info = $this->model_saccount_offer->getProduct1($this->request->get['product_id']);
    	}



    	if (isset($this->request->post['price'])) {
      		$data['price'] = $this->request->post['price'];
    	} elseif (!empty($product_info)) {
			$data['price'] = $product_info['sprice'];
		} else {
      		$data['price'] = '';
    	}

		if (isset($this->request->post['quantity'])) {
      		$data['quantity'] = $this->request->post['quantity'];
    	} elseif (!empty($product_info)) {
			$data['quantity'] = $product_info['squantity'];
		} else {
      		$data['quantity'] = 1;
    	}

    	if (isset($this->request->post['status'])) {
      		$data['status'] = $this->request->post['status'];
    	} elseif (!empty($product_info)) {
			$data['status'] = $product_info['status'];
		} else {
      		$data['status'] = 1;
    	}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('saccount/attribute');

		if (isset($this->request->post['product_attribute'])) {
			$product_attributes = $this->request->post['product_attribute'];
		}elseif (!empty($product_info)) {
			$product_attributes = $this->model_saccount_offer->getProductAttributes($this->request->get['product_id']);



		} else {
			$product_attributes = array();
		}

		$data['product_attributes'] = array();

		foreach ($product_attributes as $product_attribute) {
			$attribute_info = $this->model_saccount_attribute->getAttribute($product_attribute['attribute_id']);

			if ($attribute_info) {
				$data['product_attributes'][] = array(
					'attribute_id'                  => $product_attribute['attribute_id'],
					'name'                          => $attribute_info['name'],
					'product_attribute_description' => $product_attribute['product_attribute_description']
				);
			}
		}


		$this->load->model('account/customer_group');
		###Retorna los grupos de clientes pertenecientes a un vendedor en particular
		$data['customer_groups'] = $this->model_account_customer_group->getSellerCustomersGroups($this->customer->getsellerId());

		if (isset($this->request->post['product_discount'])) {
			$data['product_discounts'] = $this->request->post['product_discount'];
		} elseif (isset($this->request->get['product_id'])) {
			$data['product_discounts'] = $this->model_saccount_offer->getProductDiscounts($this->request->get['product_id']);
		} else {
			$data['product_discounts'] = array();
		}

		if (isset($this->request->post['product_special'])) {
			$data['product_specials'] = $this->request->post['product_special'];
		} elseif (isset($this->request->get['product_id'])) {
			$data['product_specials'] = $this->model_saccount_offer->getProductSpecials($this->request->get['product_id'],$this->customer->getsellerId());
		} else {
			$data['product_specials'] = array();
		}




		if (isset($this->request->post['product_option'])) {
			$product_options = $this->request->post['product_option'];
		} elseif(isset($this->request->get['product_id'])) {
			$product_options = $this->model_saccount_offer->getProductOptions($this->request->get['product_id'],$this->customer->getsellerId());
		} else {
			$product_options = array();
		}

		$data['product_options'] = array();

		foreach ($product_options as $product_option) {
			$product_option_value_data = array();

			if (isset($product_option['product_option_value'])) {
				foreach ($product_option['product_option_value'] as $product_option_value) {
					$product_option_value_data[] = array(
						'product_option_value_id' => $product_option_value['product_option_value_id'],
						'option_value_id'         => $product_option_value['option_value_id'],
						'quantity'                => $product_option_value['quantity'],
						'subtract'                => $product_option_value['subtract'],
						'price'                   => $product_option_value['price'],
						'price_prefix'            => $product_option_value['price_prefix'],
						'points'                  => $product_option_value['points'],
						'points_prefix'           => $product_option_value['points_prefix'],
						'weight'                  => $product_option_value['weight'],
						'weight_prefix'           => $product_option_value['weight_prefix']
					);
				}
			}

			$data['product_options'][] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => isset($product_option['value']) ? $product_option['value'] : '',
				'required'             => $product_option['required']
			);
		}


		$data['option_values'] = array();

		foreach ($product_options as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
				if (!isset($data['option_values'][$product_option['option_id']])) {
					$data['option_values'][$product_option['option_id']] = $this->model_saccount_offer->getOptionValues($product_option['option_id']);
				}
			}
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('saccount/detail_form', $data));
  	}

	public function upload() {
		$this->language->load('saccount/offer');
		$json = array();

		if (!empty($this->request->files['file']['name'])) {
			$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

			if ((strlen($filename) < 3) || (strlen($filename) > 128)) {
        		$json['error'] = $this->language->get('error_filename');
	  		}

			$allowed = array();

			$filetypes = explode("\n", $this->config->get('config_file_offer_allowed'));

			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}

			if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
       		}

			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}

		if (!$json) {
			if (is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
				//$file = basename($filename) . '.' . md5(rand());
				$file = basename($filename);

				// Hide the uploaded file name so people can not link to it directly.
				$json['file'] = $file;

				move_uploaded_file($this->request->files['file']['tmp_name'], DIR_IMAGE .'data/'. $file);
			}

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->setOutput(json_encode($json));
	}

	public function download() {
		$this->language->load('saccount/offer');


		$json = array();

		if (!empty($this->request->files['file']['name'])) {
			$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

			if ((strlen($filename) < 3) || (strlen($filename) > 128)) {
        		$json['error'] = $this->language->get('error_filename');
	  		}

			$allowed = array();

			$filetypes = explode(',','zip');

			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}

			if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
				$json['error'] = $this->language->get('error_filetype1');
       		}

			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}

		if (!$json) {
			$this->load->model('saccount/download');
			$data = array();
			if (is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
				$file = basename($filename) . '.' . md5(rand());
				// Hide the uploaded file name so people can not link to it directly.
				$json['file']		 = basename($filename);
				$json['download_id'] = $file;
				move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);
				if (file_exists(DIR_DOWNLOAD . $file)) {
					$data['download']	= $file;
					$data['mask']		= $this->request->files['file']['name'];
					$download_id		= $this->model_saccount_download->addDownload($data);
					$json['download_id']= $download_id;
				}
			}

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->setOutput(json_encode($json));
	}

  	private function validateForm() {




		if (empty($this->request->post['select_product'])) {
        		$this->error['warning'] = "Invalid Product";
      	}

		$this->load->model('saccount/product');

    	$maxproducts = $this->model_saccount_product->getTotalProducts1();
		$assignLimit = $this->model_saccount_product->getAssignLimit();

		if ($maxproducts > $assignLimit - 1) {
			$this->error['warning'] = $this->language->get('error_max_warning');
		}

		if (isset($this->request->post['selected'])) {
			if (($maxproducts + (isset($this->request->post['selected']) ? count($this->request->post['selected']) : 0)) > $assignLimit) {
				$this->error['warning'] = $this->language->get('error_max_warning');
			}
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}



	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name1'])) {
			$this->load->model('saccount/offer');

			if (isset($this->request->get['filter_name1'])) {
				$filter_name1 = $this->request->get['filter_name1'];
			} else {
				$filter_name1 = '';
			}


			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 20;
			}

			$data = array(
				'filter_name'         => $filter_name1,
				'start'               => 0,
				'limit'               => $limit
			);



			$results = $this->model_saccount_offer->getProducts($data);

			foreach ($results as $result) {

				$json[] = array(
					'product_id' => $result['product_id'],
					'name'       => html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'),
					'model'      => $result['model'],
					'price'      => $result['price']
				);
			}
		}

		$this->response->setOutput(json_encode($json));
	}


	private function validateForm2() {


	$this->load->model('saccount/offer');





		if (empty($this->request->post['name'])) {
        		$this->error['name'] = "Name is required";
      	}

		if (empty($this->request->post['price'])) {
        		$this->error['price'] = "Price is required";
      	}


		if (isset($this->request->post['cd_key'])) {

			$cd_key = $this->model_saccount_offer->getProductkeys($this->request->get['sproduct_id']);

			$array2  = array();

			$array1  = array();

			$arrays1  = explode("\n",$this->request->post['cd_key']);

			foreach($arrays1 as $key => $arr){

			  if($arr!= ""){

			   $array1[] = $arr;

			  }

			}

			foreach($cd_key as $ckey){

			  $array2[] = $ckey['cd_key'];

			}

			$final_array = array();

			foreach($array1 as $key=>$val){

			if(in_array($val,$array2)){


			         $final_array[] = $val;
			  }
			}



			$result  = count($final_array);

			if($result >0 ){

			  $this->error['warning'] = "Duplicate key detected";


			}



		}



		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}


	private function validateForm1() {


	$this->load->model('saccount/offer');




		if (empty($this->request->post['price'])) {
        		$this->error['price'] = "Price is required";
      	}





		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}
}
?>
