<?php
class ModelSAccountCustomers extends Model {
  public function getOptions1f($data = array(),$seller) {    $sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON     (o.option_id = od.option_id)";        if ($seller) {          $sql .= " WHERE ( o.seller_id = '" . $this->customer->getsellerId().  "' OR o.seller_id =0 ) AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'";       } else {        $sql .= " WHERE od.language_id = '" . (int)$this->config->get('config_language_id') . "'";       }        if (isset($data['filter_name']) && !is_null($data['filter_name'])) {      $sql .= " AND LCASE(od.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";    }    $sort_data = array(      'od.name',      'o.type',      'o.sort_order'    );          if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {      $sql .= " ORDER BY " . $data['sort'];      } else {      $sql .= " ORDER BY od.name";      }        if (isset($data['order']) && ($data['order'] == 'DESC')) {      $sql .= " DESC";    } else {      $sql .= " ASC";    }        if (isset($data['start']) || isset($data['limit'])) {      if ($data['start'] < 0) {        $data['start'] = 0;      }                if ($data['limit'] < 1) {        $data['limit'] = 20;      }            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];    }            $query = $this->db->query($sql);    return $query->rows;  }
  public function addOption($data) {
    $this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "',
    sort_order = '" . (int)$data['sort_order'] . "',seller_id='".(int)$this->customer->getsellerId()."'");
    $option_id = $this->db->getLastId();
    foreach ($data['option_description'] as $language_id => $value) {
      $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int)$option_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
    }
    if (isset($data['option_value'])) {
      foreach ($data['option_value'] as $option_value) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "',
        image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$option_value['sort_order'] . "'");
        $option_value_id = $this->db->getLastId();
        foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
          $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language_id . "', option_id = '" . (int)$option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
        }
      }
    }
  }

  public function editOption($option_id, $data) {
    $this->db->query("UPDATE `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "',
    sort_order = '" . (int)$data['sort_order'] . "',seller_id='".(int)$this->customer->getsellerId()."' WHERE option_id = '" . (int)$option_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int)$option_id . "'");
    foreach ($data['option_description'] as $language_id => $value) {
      $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int)$option_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
    }
    $this->db->query("DELETE FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int)$option_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "option_value_description WHERE option_id = '" . (int)$option_id . "'");
    if (isset($data['option_value'])) {
      foreach ($data['option_value'] as $option_value) {
        if ($option_value['option_value_id']) {
          $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_value_id = '" . (int)$option_value['option_value_id'] . "', option_id = '" . (int)$option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$option_value['sort_order'] . "'");
        } else {
          $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$option_value['sort_order'] . "'");
        }
        $option_value_id = $this->db->getLastId();
        foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
          $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language_id . "', option_id = '" . (int)$option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
        }
      }
    }
  }

  public function deleteOption($option_id) {
    $this->db->query("DELETE FROM `" . DB_PREFIX . "option` WHERE option_id = '" . (int)$option_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int)$option_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int)$option_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "option_value_description WHERE option_id = '" . (int)$option_id . "'");
  }


  /** code added here**/
  public function getOption($option_id) {
    $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od
    ON (o.option_id = od.option_id) WHERE o.option_id = '" . (int)$option_id . "' AND
    od.language_id = '" . (int)$this->config->get('config_language_id') . "'AND o.seller_id ='" .$this->customer->getsellerId(). "'");
    return $query->row;
  }

  public function getOptions($data = array(),$seller) {
    $sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON
    (o.option_id = od.option_id)";

    if ($seller) {
        $sql .= " WHERE o.seller_id IN('" . $seller . "') AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'";
      } else {
        $sql .= " WHERE od.language_id = '" . (int)$this->config->get('config_language_id') . "'";
      }

    if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
      $sql .= " AND LCASE(od.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
    }

    $sort_data = array(
      'od.name',
      'o.type',
      'o.sort_order'
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY od.name";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }



    $query = $this->db->query($sql);

    return $query->rows;
  }


  public function getallOptions($data = array()) {
    $sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON
    (o.option_id = od.option_id)";


      $sql .= " WHERE od.language_id = '" . (int)$this->config->get('config_language_id') . "' AND o.seller_id='".$this->customer->getsellerId()."'";


    if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
      $sql .= " AND LCASE(od.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
    }

    $sort_data = array(
      'od.name',
      'o.type',
      'o.sort_order'
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY od.name";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }



    $query = $this->db->query($sql);

    return $query->rows;
  }


  public function getOptions1($data = array(),$seller) {
    $sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON
    (o.option_id = od.option_id)";

    if ($seller) {
        $sql .= " WHERE o.seller_id = '" . $this->customer->getsellerId() . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'";
      } else {
        $sql .= " WHERE od.language_id = '" . (int)$this->config->get('config_language_id') . "'";
      }

    if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
      $sql .= " AND LCASE(od.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
    }

    $sort_data = array(
      'od.name',
      'o.type',
      'o.sort_order'
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY od.name";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }



    $query = $this->db->query($sql);

    return $query->rows;
  }

  /**/

  /**/

  public function getOptionDescriptions($option_id) {
    $option_data = array();

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int)$option_id . "'");

    foreach ($query->rows as $result) {
      $option_data[$result['language_id']] = array('name' => $result['name']);
    }

    return $option_data;
  }

  public function getOptionValues($option_id) {
    $option_value_data = array();

    $option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '" . (int)$option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ov.sort_order ASC");

    foreach ($option_value_query->rows as $option_value) {
      $option_value_data[] = array(
        'option_value_id' => $option_value['option_value_id'],
        'name'            => $option_value['name'],
        'image'           => $option_value['image'],
        'sort_order'      => $option_value['sort_order']
      );
    }

    return $option_value_data;
  }

  public function getOptionValueDescriptions($option_id) {
    $option_value_data = array();

    $option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int)$option_id . "'");

    foreach ($option_value_query->rows as $option_value) {
      $option_value_description_data = array();

      $option_value_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value_description WHERE option_value_id = '" . (int)$option_value['option_value_id'] . "'");

      foreach ($option_value_description_query->rows as $option_value_description) {
        $option_value_description_data[$option_value_description['language_id']] = array('name' => $option_value_description['name']);
      }

      $option_value_data[] = array(
        'option_value_id'          => $option_value['option_value_id'],
        'option_value_description' => $option_value_description_data,
        'image'                    => $option_value['image'],
        'sort_order'               => $option_value['sort_order']
      );
    }

    return $option_value_data;
  }

  public function getTotalOptions() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "option` where seller_id = '" . $this->customer->getsellerId(). "'");
    return $query->row['total'];
  }


  public function getCustomersOfSeller($seller_id)
  {
    //Retorna los clientes que estan asociados a un vendedor en particular
    //Traigo el estado de aprobación desde oci_sellers_customers.status
    //Esta tabla contiene los clientes del vendedor, sin contar el grupo de pertenencia
    $sql = (
             "select sc.customer_id, CONCAT(c.firstname, ' ', c.lastname)as name, c.email, sc.date_added, sc.status "
             ."from oci_sellers_customers sc "
             ."join oc_customer c on c.customer_id = sc.customer_id "
             ."where seller_id = '" . (int)$seller_id . "' "
           );
    $query = $this->db->query($sql);

    return $query->rows;
  }


  public function approveCustomer($customer_id, $seller_id)
  {
    //Aprueba Clientes dentro de la tabla oci_sellers_customers.status
    $sql = "update `oci_sellers_customers` set status = '1' "
          ."where customer_id = '" . (int)$customer_id . "' "
          ."and seller_id = '" . (int)$seller_id . "' ";
    $this->db->query($sql);
  }

  public function denyCustomer($customer_id, $seller_id)
  {
    //Desaprueba Clientes dentro de la tabla oci_sellers_customers.status
    $sql = "update `oci_sellers_customers` set status = '0' "
          ."where customer_id = '" . (int)$customer_id . "' "
          ."and seller_id = '" . (int)$seller_id . "' ";
    $this->db->query($sql);
  }

  public function getDefaultCustomerGroupSeller($seller_id)
  {
    //Retorna el grupo de clientes Default de un vendedor
    //El ID del grupo defaul sera almacenado en la tabla
    //oc_sellers.seller_group_id
    /*$sql = "select seller_group_id "
          ."from oc_sellers "
          ."where seller_id = '" . (int)$seller_id . "'";*/
    //Consulta en varias tablas
    $sql = "select distinct scg.customer_group_id, cgd.name "
          ."from oci_sellers_customer_groups scg "
          ."join oc_customer_group cg on scg.customer_group_id = cg.customer_group_id "
          ."join oc_customer_group_description cgd on cg.customer_group_id = cgd.customer_group_id "
          ."where seller_id = '" . (int)$seller_id . "' and cgd.name like '%Default' ";
    $query = $this->db->query($sql);
    //return $query->row['seller_group_id'];
    return $query->row['customer_group_id'];
  }
  ###Replicación de funcionalidad de modulo de panel de administración
  public function addToGroup($customer_id, $customer_group_id)
  {
    //Añade un cliente, a un grupo de clientes
    //Consulta si ya pertenece al grupo, si NO pertenece, agrego registro a la trabla
    //Con fecha actual
    //Si el cliente ya pertenece al grupo, no hace nada
    $pertenece = $this->consultaPertenencia($customer_id, $customer_group_id);
    $seller_id = $this->getSellersByCustomerGroupId($customer_group_id);
    if($pertenece == false)
    {
      $sql = (
                "insert into oci_customer_groups (customers_groups_id, customer_id, customer_group_id, fecha_alta, fecha_baja) "
                ."values (null, '" . (int)$customer_id . "', '" . (int)$customer_group_id . "', CURRENT_DATE, null )"
              );
      $query = $this->db->query($sql);

      //Agrego al cliente, a la tabla de clientes por vendedor oci_sellers_customers
      //Siempre y cuando no exista como cliente
      if(isset($seller_id))
      {
        $this->addToCustomersOfSeller($customer_id, $seller_id);
      }
      return "Se agrego al cliente_id: " .$customer_id ." al grupo_id: " .$customer_group_id;
    }
    return "NO se puede añadir NUEVAMENTE al cliente_id: " .$customer_id ." por qué YA PERTENECE al grupo " .$customer_group_id;
  }

  public function removeFromTheGroup($customer_id, $customer_group_id)
  {
    //Elimina de manera lógica a un cliente de un grupo de clientes
    //Primero busco el registro, si el cliente ya pertenece al group
    //Se le da una baja lógica agregando fecha_baja = CURRENT_DATE
    //Si NO pertenece al grupo NO hace nada
    $pertenece = $this->consultaPertenencia($customer_id, $customer_group_id);
    $seller_id = $this->getSellersByCustomerGroupId($customer_group_id);
    if($pertenece == true)
    {
      $sql = (
                "update oci_customer_groups "
                ."set fecha_baja = CURRENT_DATE "
                ."where customer_id = '" . (int)$customer_id . "' and customer_group_id = '" . (int)$customer_group_id . "' "
              );
      $query = $this->db->query($sql);

      //Si ya existe el cliente en la tabla de clientes por vendedor oci_sellers_customers
      //Y no esta asociado a ningún grupo del vendedor, le cambio el estado a "deshabilitado"
      /*###Desactivo funcionalidad, Un cliente puede no estar asociado a ningún
        ###Grupo y seguir estando activo, se controla desde la vista que el client
        ###Pertenezca como mínimo a un solo grupo
      if(isset($seller_id))
      {
        $this->disableCustomerOfTheSeller($customer_id, $seller_id);
      }*/

      return "Se Elimino al cliente_id: " .$customer_id ." del grupo_id: " .$customer_group_id;
    }
    return "NO se puede eliminar al cliente_id: " .$customer_id ." por qué no pertenece al grupo " .$customer_group_id;
  }

  public function consultaPertenencia($customer_id, $customer_group_id)
  {
    //Dado un customer_id y un customer_group_id, consulto pertenencia
    //si existe registro devuelve true
    //si no existe devuelve false
    $query = $this->db->query(
    "select distinct c.customer_id, cg.customer_group_id, cg.fecha_alta, cg.fecha_baja "
    ."from oc_customer c "
    ."join oci_customer_groups cg on cg.customer_id = c.customer_id "
    ."join oc_customer_group_description cgd on cgd.customer_group_id = cg.customer_group_id "
    ."where c.customer_id = '" . (int)$customer_id . "' and cg.customer_group_id = '" . (int)$customer_group_id . "' and cg.fecha_baja is null ");

    //return $query->rows;
    //Si la consulta no trajo ningún resultado devuelvo false
    if(count($query->rows) > 0)
    {
      return true;
    } else {
        return false;
      }
  }

  public function consultaBaja($customer_id, $customer_group_id)
  {
    //Dado un customer_id y un cutomer_group_id, consulto si esta dado de
    //baja (fecha_baja is not null)
    //si existe registro devuelve true
    //si no existe devuelve false
    $query = $this->db->query(
    "select distinct c.customer_id, cg.customer_group_id, cgd.name, cg.fecha_alta, cg.fecha_baja "
    ."from oc_customer c "
    ."join oci_customer_groups cg on cg.customer_id = c.customer_id "
    ."join oc_customer_group_description cgd on cgd.customer_group_id = cg.customer_group_id "
    ."where c.customer_id = '" . (int)$customer_id . "' and cg.customer_group_id = '" . (int)$customer_group_id . "' and cg.fecha_baja is not null ");

    //Si la consulta no trajo ningún resultado devuelvo false
    if(count($query->rows) == 0)
    {
      return false;
    } else {
        return true;
      }
  }

  public function getGrouposPertenenciaCustomerId($customer_id)
  {
    //Dado un customer_id devuelvo todos los id a los cuales pertenecer
    //un cliente
    $sql = (
              "select cg.customer_group_id "
              ."from oc_customer c "
              ."join oci_customer_groups cg on cg.customer_id = c.customer_id "
              ."where cg.customer_id = '" . (int)$customer_id . "' and cg.fecha_baja is null "
            );
    $query = $this->db->query($sql);
    return $query->rows;
  }

  /*
  ###Al eliminar un Grupo, es necesario actualizar la tabla donde registramos
  ###los grupos de pertenencia de un cliente
  ###Esta es la consulta original
  ### REGLAS DE INTEGRIDAD
  ###ok, entonces antes de eliminar, tengo que verificar que el grupo no tenga
  ## ASOCIADO NINGUN VENDEDOR y NINGUN CLIENTE
  ###Dado un customer_group_id, consulto si tiene algún cliente o
  ###vendedor asociado.
  ###Para lo cual consulto si hay registro con ese ID en las tablas
  ###oci_customer_groups, oci_sellers_customer_groups
  */

  public function getTotalCustomersGroupsByCustomerGroupId($customer_group_id)
  {
    //Primer consulta grupo con clientes asociados
    $sql = (
              "select count(*) as total "
              ."from oci_customer_groups cg "
              ."where cg.customer_group_id = '" . (int)$customer_group_id . "' and cg.fecha_baja is null "
            );
    $query = $this->db->query($sql);
    return $query->row['total'];
  }

  public function getTotalSellerCustomersGroupsByCustomerGroupId($customer_group_id)
  {
    //segunda consulta grupo con vendedores asociados
    $sql = (
              "select count(*) as total "
              ."from oci_sellers_customer_groups cgs "
              ."where cgs.customer_group_id = '" . (int)$customer_group_id . "' "
            );
    $query = $this->db->query($sql);
    return $query->row['total'];
  }

  public function getSellersAsigned()
  {
    //Retorna los grupos de clientes asignados a cada vendedor
    $sql = (
              "select scg.seller_id, scg.customer_group_id, concat(s.firstname,' ', s.lastname) as name "
              ."from oci_sellers_customer_groups scg "
              ."join oc_sellers s on scg.seller_id = s.seller_id "
            );
    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getSellersByCustomerGroupId($customer_group_id)
  {
    //Retorna el id del vendedor de un grupo de clientes pasado por parametro
    $sql = (
              "select seller_id "
              ."from oci_sellers_customer_groups cgs "
              ."where cgs.customer_group_id = '" . (int)$customer_group_id . "' "
            );
    $query = $this->db->query($sql);
    return $query->row['seller_id'];
  }

  public function itIsCustomerOfTheSeller($customer_id, $seller_id)
  {
    //Dado un customer_id y un $seller_id devuelvo todos los customer_group_id
    //a los cuales pertenecer un cliente
    $sql = (
              "select cg.customer_group_id  "
              ."from oc_customer c "
              ."join oci_customer_groups cg on cg.customer_id = c.customer_id "
              ."join oci_sellers_customer_groups scg on scg.customer_group_id = cg.customer_group_id "
              ."where cg.customer_id = '" . (int)$customer_id . "' "
              ."and cg.fecha_baja is null "
              ."and scg.seller_id = '" . (int)$seller_id . "' "
            );
    $query = $this->db->query($sql);

    //Si la consulta no trajo ningún resultado devuelvo false
    if(count($query->rows) == 0)
    {
      return false;
    } else {
        return true;
      }
  }

  public function existsAsCustomer($customer_id, $seller_id)
  {
    //Consulta si el cliente existe en la tabla de clientes por vendedor
    //oci_sellers_customers

    $sql = (
              "select *  "
              ."from oci_sellers_customers "
              ."where customer_id = '" . (int)$customer_id . "' "
              ."and seller_id = '" . (int)$seller_id . "' "
            );
    $query = $this->db->query($sql);

    //Si la consulta no trajo ningún resultado devuelvo false
    if(count($query->rows) == 0)
    {
      return false;
    } else {
        return true;
      }

  }

  public function addToCustomersOfSeller($customer_id, $seller_id)
  {
    //Agrega al cliente como cliente del vendedor, pero primero tengo que
    //controlar que no exista en la tabla de oci_sellers_customers
    //si NO es CLIENTE del vendedor, agrego registro a la trabla
    //Con fecha actual
    //Si el cliente ya existe en la tabla, no hago nada
    $existe = $this->existsAsCustomer($customer_id, $seller_id);

    if($existe == false)
    {
      $sql = (
                "insert into oci_sellers_customers (id,seller_id,customer_id, date_added, status) "
                ."values (null, '" . (int)$seller_id . "', '" . (int)$customer_id . "', CURRENT_TIMESTAMP, 0 )"
              );
      $query = $this->db->query($sql);
      return "Se agrego nuevo cliente_id: " .$customer_id ." como cliente del vendedor seller_id: " .$seller_id;
    }
  return "NO se puede añadir NUEVAMENTE al cliente_id: " .$customer_id ." por qué YA ES CLIENTE DEL VENDEDOR seller_id " .$seller_id;
  }

  public function disableCustomerOfTheSeller($customer_id, $seller_id)
  {
    //Cambia el estado estado del cliente en la tabla de clientes por vendedor oci_sellers_customers
    //Primero Controla que el cliente no este asociado a ningún grupo del vendedor
    //Después controla que el cliente exista en la tabla de oci_sellers_customers
    //Si NO esta asociado a ningún grupo de clientes del vendedor y
    //Existe en la tabla oci_sellers_customers, le cambio el estado a "deshabilitado"

    $esClienteDelVendedor = $this->itIsCustomerOfTheSeller($customer_id, $seller_id);
    $existeComoCliente = $this->existsAsCustomer($customer_id, $seller_id);

    if(($esClienteDelVendedor == false) AND ($existeComoCliente == true))
    {
      $sql = (
                "update oci_sellers_customers set status = '0' "
                ."where customer_id = '" . (int)$customer_id . "' "
                ."and seller_id = '" . (int)$seller_id . "' "
              );
      $query = $this->db->query($sql);
      return "Se DESHABILITO al cliente_id: " .$customer_id ." como cliente del vendedor seller_id: " .$seller_id;
    }
    return "El cliente_id: " .$customer_id ." no es cliente del vendedor seller_id: " .$seller_id;
  }


  public function getCustomer($customer_id) {
    $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

    return $query->row;
  }

  public function getCustomerByEmail($email) {
    $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

    return $query->row;
  }

  public function getStatusCustomerOfSeller($customer_id, $seller_id)
  {
    //Retorna el estado de un cliente en la tabla de clientes por vendedor oci_sellers_customers
    $query = $this->db->query("select status from oci_sellers_customers where seller_id = '1' and customer_id = '13'");

    return $query->row['status'];
  }

  public function getMembershipGroup($customer_id, $seller_id)
  {
    ### retorga grupo de pertenencia de un cliente y un seller dados
    $sql = "select cg.customer_id, cg.customer_group_id, cgd.name, cgd.description, seller_id, fecha_alta,fecha_baja "
          ."from oci_customer_groups cg "
          ."join oc_customer_group_description cgd on cg.customer_group_id = cgd.customer_group_id "
          ."join oci_sellers_customer_groups scg on cg.customer_group_id = scg.customer_group_id "
          ."where cg.customer_id = '" .(int)$customer_id ."' "
          ."and scg.seller_id = '" .(int)$seller_id ."' "
          ."and cg.fecha_baja is NULL "
          ."and cgd.language_id = '" .(int)$this->config->get('config_language_id') ."' ";
    $query = $this->db->query($sql);

    return $query->row;
  }

  public function getCustomersGroupsOfSellerLessMembershipGroup($membership_group, $seller_id)
  {
    ###Retorga grupos de pertenencia de un vendedor salvo el grupo al cual pertenece
    ###el cliente
    $sql = "select * "
          ."from oci_sellers_customer_groups scg "
          ."join oc_customer_group cg on scg.customer_group_id = cg.customer_group_id "
          ."join oc_customer_group_description cgd on cg.customer_group_id = cgd.customer_group_id "
          ."where seller_id = '" .(int)$seller_id ."' "
          ."and language_id = '" .(int)$this->config->get('config_language_id') ."' "
          ."and cg.customer_group_id != '" .(int)$membership_group ."' ";

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getCustomers($data = array()) {
    $sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cgd.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id)";

    if (!empty($data['filter_affiliate'])) {
      $sql .= " LEFT JOIN " . DB_PREFIX . "customer_affiliate ca ON (c.customer_id = ca.customer_id)";
    }

    $sql .= " WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

    $implode = array();

    if (!empty($data['filter_name'])) {
      $implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
    }

    if (!empty($data['filter_email'])) {
      $implode[] = "c.email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
    }

    if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
      $implode[] = "c.newsletter = '" . (int)$data['filter_newsletter'] . "'";
    }

    if (!empty($data['filter_customer_group_id'])) {
      $implode[] = "c.customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
    }

    if (!empty($data['filter_affiliate'])) {
      $implode[] = "ca.status = '" . (int)$data['filter_affiliate'] . "'";
    }

    if (!empty($data['filter_ip'])) {
      $implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
    }

    if (isset($data['filter_status']) && $data['filter_status'] !== '') {
      $implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
    }

    if (!empty($data['filter_date_added'])) {
      $implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
    }

    if ($implode) {
      $sql .= " AND " . implode(" AND ", $implode);
    }

    $sort_data = array(
      'name',
      'c.email',
      'customer_group',
      'c.status',
      'c.ip',
      'c.date_added'
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY name";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getCustomerGroups($data = array()) {
    $sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

    $sort_data = array(
      'cgd.name',
      'cg.sort_order'
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY cgd.name";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getCustomersGroupsOfTheSeller($seller_id)
  {
    ###Retorna los grupos de pertenecia de un vendedor
    $sql = "select seller_id, cg.customer_group_id, approval, sort_order, "
          ."language_id, name, description "
          ."from oci_sellers_customer_groups scg "
          ."join oc_customer_group cg on scg.customer_group_id = cg.customer_group_id "
          ."join oc_customer_group_description cgd on cg.customer_group_id = cgd.customer_group_id "
          ."where seller_id = '" .(int)$seller_id ."' and language_id = '2' ";

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function isAssingToGroupOfSeller($customer_id, $seller_id)
  {
    ### Retorna grupos de pertenencia de un cliente y un vendedor dados
    $sql = "select cgd.name "
          ."from oci_customer_groups cg "
          ."join oci_sellers_customer_groups scg on cg.customer_group_id = scg.customer_group_id "
          ."join oc_customer_group_description cgd on cg.customer_group_id = cgd.customer_group_id "
          ."where seller_id = '" .(int)$seller_id ."' "
          ."and customer_id = '" .(int)$customer_id ."' "
          ."and fecha_baja is NULL ";

      $query = $this->db->query($sql);
      //Si la consulta trajo un resultado, retorno true
      if ($query->row)
      {
        return true;
      } else {
        return false;
      }

  }

  public function getCustomerGroupDescriptions($customer_group_id) {
    $customer_group_data = array();

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");

    foreach ($query->rows as $result) {
      $customer_group_data[$result['language_id']] = array(
        'name'        => $result['name'],
        'description' => $result['description']
      );
    }

    return $customer_group_data;
  }

  public function getCustomFields($data = array()) {
    if (empty($data['filter_customer_group_id'])) {
      $sql = "SELECT * FROM `" . DB_PREFIX . "custom_field` cf LEFT JOIN " . DB_PREFIX . "custom_field_description cfd ON (cf.custom_field_id = cfd.custom_field_id) WHERE cfd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
    } else {
      $sql = "SELECT * FROM " . DB_PREFIX . "custom_field_customer_group cfcg LEFT JOIN `" . DB_PREFIX . "custom_field` cf ON (cfcg.custom_field_id = cf.custom_field_id) LEFT JOIN " . DB_PREFIX . "custom_field_description cfd ON (cf.custom_field_id = cfd.custom_field_id) WHERE cfd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND cfd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
    }

    if (!empty($data['filter_customer_group_id'])) {
      $sql .= " AND cfcg.customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
    }

    $sort_data = array(
      'cfd.name',
      'cf.type',
      'cf.location',
      'cf.status',
      'cf.sort_order'
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY cfd.name";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getCustomFieldValues($custom_field_id) {
    $custom_field_value_data = array();

    $custom_field_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "custom_field_value cfv LEFT JOIN " . DB_PREFIX . "custom_field_value_description cfvd ON (cfv.custom_field_value_id = cfvd.custom_field_value_id) WHERE cfv.custom_field_id = '" . (int)$custom_field_id . "' AND cfvd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY cfv.sort_order ASC");

    foreach ($custom_field_value_query->rows as $custom_field_value) {
      $custom_field_value_data[$custom_field_value['custom_field_value_id']] = array(
        'custom_field_value_id' => $custom_field_value['custom_field_value_id'],
        'name'                  => $custom_field_value['name']
      );
    }

    return $custom_field_value_data;
  }

  public function getAddress($address_id) {
    $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "'");

    if ($address_query->num_rows) {
      $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

      if ($country_query->num_rows) {
        $country = $country_query->row['name'];
        $iso_code_2 = $country_query->row['iso_code_2'];
        $iso_code_3 = $country_query->row['iso_code_3'];
        $address_format = $country_query->row['address_format'];
      } else {
        $country = '';
        $iso_code_2 = '';
        $iso_code_3 = '';
        $address_format = '';
      }

      $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

      if ($zone_query->num_rows) {
        $zone = $zone_query->row['name'];
        $zone_code = $zone_query->row['code'];
      } else {
        $zone = '';
        $zone_code = '';
      }

      return array(
        'address_id'     => $address_query->row['address_id'],
        'customer_id'    => $address_query->row['customer_id'],
        'firstname'      => $address_query->row['firstname'],
        'lastname'       => $address_query->row['lastname'],
        'company'        => $address_query->row['company'],
        'address_1'      => $address_query->row['address_1'],
        'address_2'      => $address_query->row['address_2'],
        'postcode'       => $address_query->row['postcode'],
        'city'           => $address_query->row['city'],
        'zone_id'        => $address_query->row['zone_id'],
        'zone'           => $zone,
        'zone_code'      => $zone_code,
        'country_id'     => $address_query->row['country_id'],
        'country'        => $country,
        'iso_code_2'     => $iso_code_2,
        'iso_code_3'     => $iso_code_3,
        'address_format' => $address_format,
        'custom_field'   => json_decode($address_query->row['custom_field'], true)
      );
    }
  }

  public function getAddresses($customer_id) {
    $address_data = array();

    $query = $this->db->query("SELECT address_id FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

    foreach ($query->rows as $result) {
      $address_info = $this->getAddress($result['address_id']);

      if ($address_info) {
        $address_data[$result['address_id']] = $address_info;
      }
    }

    return $address_data;
  }

  public function getAffiliate($customer_id) {
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = '" . (int)$customer_id . "'");

    return $query->row;
  }

  public function editCustomerSimple($customer_id, $seller_id, $data)
  {
    //Edita un cliente, de manera simplificada, el administrador no tiene la
    //potestad de editar los datos de un cliente. Solo habilitarlo o no en su
    //Tienda y a los grupos de pertenencia
    $sql = "update oci_sellers_customers "
          ."set status = '" . (int)$data['status'] ."' "
          ."where customer_id = '" . (int)$customer_id . "' and seller_id = '" .(int)$seller_id ."' ";

    $query = $this->db->query($sql);
    return $query->row;
  }

  public function editCustomer($customer_id, $data) {
    $this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : json_encode(array())) . "', newsletter = '" . (int)$data['newsletter'] . "', status = '" . (int)$data['status'] . "', safe = '" . (int)$data['safe'] . "' WHERE customer_id = '" . (int)$customer_id . "'");

    if ($data['password']) {
      $this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE customer_id = '" . (int)$customer_id . "'");
    }

    $this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");

    if (isset($data['address'])) {
      foreach ($data['address'] as $address) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "address SET address_id = '" . (int)$address['address_id'] . "', customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($address['firstname']) . "', lastname = '" . $this->db->escape($address['lastname']) . "', company = '" . $this->db->escape($address['company']) . "', address_1 = '" . $this->db->escape($address['address_1']) . "', address_2 = '" . $this->db->escape($address['address_2']) . "', city = '" . $this->db->escape($address['city']) . "', postcode = '" . $this->db->escape($address['postcode']) . "', country_id = '" . (int)$address['country_id'] . "', zone_id = '" . (int)$address['zone_id'] . "', custom_field = '" . $this->db->escape(isset($address['custom_field']) ? json_encode($address['custom_field']) : json_encode(array())) . "'");

        if (isset($address['default'])) {
          $address_id = $this->db->getLastId();

          $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
        }
      }
    }

    if ($data['affiliate']) {
      $this->db->query("REPLACE INTO " . DB_PREFIX . "customer_affiliate SET customer_id = '" . (int)$customer_id . "', company = '" . $this->db->escape($data['company']) . "', website = '" . $this->db->escape($data['website']) . "', tracking = '" . $this->db->escape($data['tracking']) . "', commission = '" . (float)$data['commission'] . "', tax = '" . $this->db->escape($data['tax']) . "', payment = '" . $this->db->escape($data['payment']) . "', cheque = '" . $this->db->escape($data['cheque']) . "', paypal = '" . $this->db->escape($data['paypal']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "', status = '" . (int)$data['affiliate'] . "', date_added = NOW()");
    }
  }

  public function isANewCustomer($customer_id)
  {
    ###Retorna true si el cliente recien se acaba de registrar
    $sql = "select * "
          ."from oc_customer_approval "
          ."where customer_id = '" .(int)$customer_id ."' ";

    $query = $this->db->query($sql);
    //Si la consulta trajo un resultado, retorno true
    if ($query->row)
    {
      return true;
    } else {
      return false;
    }
  }

  public function removeFromCustomerApproval($customer_id)
  {
    ###Elimino el cliente de la tabla oc_customer_approval
    $sql = "delete from oc_customer_approval "
          ."where customer_id = '" .(int)$customer_id  ."' ";

    $this->db->query($sql);
  }

  public function approveNewCustomer($customer_id)
  {
    ###Aprueba Clientes dentro de la tabla oc_customer
    $sql = "update `oc_customer` set status = '1' "
          ."where customer_id = '" . (int)$customer_id . "' ";

    $this->db->query($sql);
  }
}
?>
