<?php
class ModelSAccountCustomersGroups extends Model {
  public function addCustomerGroup($data) {

    $seller_id = (int)$data['seller_id'];
    $seller_username = $data['seller_username'];

    //inserto el nuevo grupo
    $sql = "insert into " . DB_PREFIX . "customer_group set approval = '"
           . (int)$data['approval'] . "', sort_order = '"
           . (int)$data['sort_order'] . "'";
    $this->db->query($sql);
    //Obtengo el id del nuevo grupo creado
    $customer_group_id = $this->db->getLastId();

    //Redefino la consulta y agrego la descripción del nuevo grupo en tantos
    //idiomas como haya cargado
    //Se agrega prefijo de vendedor para saber a quien creo el grupo
    foreach ($data['customer_group_description'] as $language_id => $value) {
      $this->db->query("insert into " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" .$seller_username ."_" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
    }

    //Agrego el nuevo grupo de clientes al vendedor
    $sql = (
              "insert into oci_sellers_customer_groups (sellers_customer_groups_id, seller_id, customer_group_id) "
              ."values (NULL, '" . (int)$seller_id ."', '" . (int)$customer_group_id . "'); "
            );
    $query = $this->db->query($sql);
    //return $sql;
    return "Nuevo grupo_de_clientes_id: " .$customer_group_id ." asignado al vendedor_id: " .$seller_id;
  }

  public function editCustomerGroup($customer_group_id, $data) {
    //Se agrega prefijo de vendedor para saber a quien creo el grupo
    $seller_username = $data['seller_username'];

    //$this->db->query("UPDATE " . DB_PREFIX . "customer_group SET approval = '" . (int)$data['approval'] . "', company_id_display = '" . (int)$data['company_id_display'] . "', company_id_required = '" . (int)$data['company_id_required'] . "', tax_id_display = '" . (int)$data['tax_id_display'] . "', tax_id_required = '" . (int)$data['tax_id_required'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE customer_group_id = '" . (int)$customer_group_id . "'");

    $this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");

    foreach ($data['customer_group_description'] as $language_id => $value) {
      $this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" .$seller_username ."_" . str_replace($seller_username, "", $this->db->escape($value['name'])) . "', description = '" . $this->db->escape($value['description']) . "'");
    }
  }

  public function deleteCustomerGroup($customer_group_id) {
    $this->db->query("DELETE FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . (int)$customer_group_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE customer_group_id = '" . (int)$customer_group_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE customer_group_id = '" . (int)$customer_group_id . "'");
    //Borrar grupo de las tablas intermedias oci_customer_groups && oci_sellers_customer_groups
    $this->db->query("UPDATE oci_customer_groups set fecha_baja = CURRENT_DATE WHERE customer_group_id = '" . (int)$customer_group_id . "'");
    $this->db->query("DELETE FROM oci_sellers_customer_groups WHERE customer_group_id = '" . (int)$customer_group_id . "'");
  }

  public function getCustomerGroup($customer_group_id) {
    $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cg.customer_group_id = '" . (int)$customer_group_id . "' AND cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

    return $query->row;
  }

  public function getCustomerGroups($data = array()) {
    $sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

    $sort_data = array(
      'cgd.name',
      'cg.sort_order'
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else {
      $sql .= " ORDER BY cgd.name";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
      if ($data['start'] < 0) {
        $data['start'] = 0;
      }

      if ($data['limit'] < 1) {
        $data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getCustomerGroupDescriptions($customer_group_id) {
    $customer_group_data = array();

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");

    foreach ($query->rows as $result) {
      $customer_group_data[$result['language_id']] = array(
        'name'        => $result['name'],
        'description' => $result['description']
      );
    }

    return $customer_group_data;
  }

  public function getTotalCustomerGroups() {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_group");

    return $query->row['total'];
  }

  public function getCustomerGroupsOfSeller($seller_id)
  {
    //Retorna los GRUPOS de clientes que estan asociados a un vendedor en
    //particular
    $sql = (
             "select distinct scg.customer_group_id, cgd.name "
             ."from oci_sellers_customer_groups scg "
             ."join oc_customer_group cg on cg.customer_group_id = scg.customer_group_id "
             ."join oc_customer_group_description cgd on cgd.customer_group_id = cg.customer_group_id "
             ."where scg.seller_id = '" . (int)$seller_id . "' "
           );

    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getCustomersDetailedOfSeller($seller_id, $customer_id)
  {
    //Retorna los GRUPOS de clientes que estan asociados a un vendedor en
    //particular con detalles
    //customer_id, name, email, customer_group_id, group_name, fecha_alta,
    //fecha_baja, status
    $sql = (
             "select distinct cg.customer_id, concat(c.firstname, ' ', c.lastname) as name, c.email, "
              ."cg.customer_group_id, cgd.name as group_name, cg.fecha_alta, "
              ."cg.fecha_baja, if(cg.fecha_baja is not null, 'activo', 'inactivo')as status "
             ."from oci_customer_groups cg "
             ."join oc_customer c on cg.customer_id = c.customer_id "
             ."join oci_sellers_customer_groups scg on cg.customer_group_id = scg.customer_group_id "
             ."join oc_customer_group_description cgd on cg.customer_group_id = cgd.customer_group_id "
             ."where scg.seller_id = '" . (int)$seller_id . "' and cg.customer_id = '" . (int)$customer_id . "' "
             //."where scg.seller_id = '" . (int)$seller_id . "' "
             //."order by cg.customer_id "
           );
    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getTotalStoresByCustomerGroupId($customer_group_id) {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_customer_group_id' AND `value` = '" . (int)$customer_group_id . "' AND store_id != '0'");
    return $query->row['total'];
  }

  public function getTotalCustomersByCustomerGroupId($customer_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		return $query->row['total'];
	}

  public function getTotalCustomersSeller($customer_group_id)
  {
    //Retorna la cantidad de clientes asignados a un grupo de vendedor
    $sql = (
              "select count(*) as total "
              ."from oci_customer_groups cg "
              ."where cg.customer_group_id = '" . (int)$customer_group_id . "'"
            );
    $query = $this->db->query($sql);
    return $query->row['total'];
  }

  public function getTotalCustomersGroupsByCustomerGroupId($customer_group_id)
  {
    //Primer consulta grupo con clientes asociados
    $sql = (
              "select count(*) as total "
              ."from oci_customer_groups cg "
              ."where cg.customer_group_id = '" . (int)$customer_group_id . "' and cg.fecha_baja is null "
            );
    $query = $this->db->query($sql);
    return $query->row['total'];
  }

  public function getTotalSellerCustomersGroupsByCustomerGroupId($customer_group_id)
  {
    //segunda consulta grupo con vendedores asociados
    $sql = (
              "select count(*) as total "
              ."from oci_sellers_customer_groups cgs "
              ."where cgs.customer_group_id = '" . (int)$customer_group_id . "' "
            );
    $query = $this->db->query($sql);
    return $query->row['total'];
  }
}
?>
