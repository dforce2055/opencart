<?php
class ModelAccountCustomerGroup extends Model {
  public function getCustomerGroup($customer_group_id) {
    $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cg.customer_group_id = '" . (int)$customer_group_id . "' AND cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

    return $query->row;
  }

  public function getCustomerGroups() {
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY cg.sort_order ASC, cgd.name ASC");

    return $query->rows;
  }

  public function getSellerCustomersGroups($seller_id)
  {
    ###Retorna todos los grupos pertenecientes a un vendedor en particular
    ##pasado por parametro
    ###Agrego condición para que no traiga el grupo por defecto
    $sql = "select * "
          ."from oc_customer_group cg "
          ."left join oc_customer_group_description cgd on (cg.customer_group_id = cgd.customer_group_id) "
          ."left join oci_sellers_customer_groups osc on cg.customer_group_id = osc.customer_group_id "
          ."where cgd.language_id = '" . (int)$this->config->get('config_language_id') . "' and osc.seller_id = '". $seller_id ."' and cgd.name not like '%_Default'"
          ."order by cg.sort_order asc, cgd.name asc ";

    $query = $this->db->query($sql);

    return $query->rows;
  }
}
