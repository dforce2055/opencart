<?php
// Heading
$_['heading_title']      = 'Dashboard';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'My Account';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_address']       = 'Modify your address book entries';
$_['text_wishlist']      = 'Modify your wish list';
$_['text_order']         = 'View your order history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Your Reward Points';
$_['text_return']        = 'View your return requests';
$_['text_transaction']   = 'Your Transactions';
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';


$_['text_extensions'] = 'My Store';
$_['text_manageextensions'] = 'Manage Products';
$_['text_addextensions']    = 'Add new Product';
$_['text_getpaid']    = 'How you wish to be paid ?';
$_['text_messagebox']    = 'MessageBox';
$_['text_checkmessage']    = 'Received and Sent messages';

$_['text_download'] = 'Manage Downloads';

$_['text_plan']      = 'Upgarde Your Plan';

$_['text_export'] = 'Upload Products in Bulk';

$_['text_images'] = 'Upload Images';

$_['text_plan']      = 'Upgrade your plan';

$_['text_my_plan']     = 'My Plan';


/**code added here**/
$_['text_option']                        = 'Manage option';
$_['text_attributes']                    = 'Manage Attributes';

$_['text_offer']                         = 'Add already listed product';
$_['text_category']                      = 'Manage Category';
$_['error_agree']                        = 'Warning: You must agree to the %s!';
$_['text_manage_customers']              = 'Manage Customers';
$_['text_manage_customers_groups']       = 'Manage Customers Groups';
$_['text_manage_users']                  = 'Manage Users';
$_['text_manage_price_list']             = 'Manage Price List';
/**/
?>
