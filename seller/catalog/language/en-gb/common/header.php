<?php
// Text
$_['text_home']          = '<i class="fa fa-home"></i>';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'View Your Order History';
$_['text_transaction']   = 'Your Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';

$_['text_extensions'] = 'My Store';
$_['text_manageextensions'] = 'Manage Products';
$_['text_addextensions']    = 'Add new Product';
$_['text_getpaid']    = 'How you wish to be paid ?';
$_['text_messagebox']    = 'MessageBox';
$_['text_checkmessage']    = 'Received and Sent messages';

$_['text_download'] = 'Manage Downloads';

$_['text_plan']      = 'Upgarde Your Plan';

$_['text_export'] = 'Upload Products in Bulk';

$_['text_images'] = 'Upload Images';

$_['text_plan']      = 'Upgrade your plan';

$_['text_my_plan']     = 'My Plan';


/**code added here**/
$_['text_option']       = 'Manage option';
$_['text_attributes'] = 'Manage Attributes';

$_['text_offer']       = 'Add already listed product';
$_['text_category']    = 'Manage Category';
$_['text_address2']    = 'How you wish to be paid ?';
$_['text_order1']         = 'My Orders';
$_['text_sellerwelcome']         = 'Welcome you are logged in as ';
$_['text_welcome']               = 'Welcome ';
$_['text_logged']               = ' you are logged in store of ';

$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_address']       = 'Modify your address book entries';

$_['text_manage_customers']              = 'Manage Customers';
$_['text_manage_customers_groups']       = 'Manage Customers Groups';
$_['text_manage_users']                  = 'Manage Users';
$_['text_manage_price_list']             = 'Manage Price List';

// ZMULTISELLER
$_['text_buyeraccount']   = 'Buyer';
$_['text_selleraccount']  = 'Sell with us';
$_['text_seller_store']   = 'Go to your Store';
$_['text_viewselleraccount']  = 'View All Seller';
