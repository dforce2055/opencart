<?php
// Texto
$_['text_information']  = 'Información';
$_['text_service']      = 'Servicio al cliente';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contáctenos';
$_['text_return']       = 'Devoluciones';
$_['text_sitemap']      = 'Mapa del sitio';
$_['text_manufacturer'] = 'Marcas';
$_['text_voucher']      = 'Certificados de regalos';
$_['text_affiliate']    = 'Afiliado';
$_['text_special']      = 'Especiales';
$_['text_account']      = 'Mi cuenta';
$_['text_order']        = 'Historial de pedidos';
$_['text_wishlist']     = 'Favoritos';
$_['text_newsletter']   = 'Boletín de Noticias';
$_['text_powered']      = 'Powered By <a href="http://gsptechnologies.com/">INDEPI</a><br /> %s &copy; %s';