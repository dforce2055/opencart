<?php
// Heading
$_['heading_title']    = 'Administrador de Imágenes';

// Texto
$_['text_uploaded']    = 'Éxito: ¡Se ha cargado el archivo!';
$_['text_directory']   = 'Éxito: ¡Directorio creado!';
$_['text_delete']      = 'Éxito: ¡Su archivo o directorio ha sido borrado!';

// Entry
$_['entry_search']     = 'Buscar..';
$_['entry_folder']     = 'Nombre de carpeta';

// Button
$_['button_insert']                 = 'Agregar nuevo';
$_['button_delete']                 = 'Borrar';
$_['button_save']                   = 'Guardar';
$_['button_cancel']                 = 'Cancelar';
$_['button_cancel_recurring']       = 'Cancelar Pagos Recurrentes';
$_['button_continue']               = 'Continuar';
$_['button_clear']                  = 'Limpiar';
$_['button_close']                  = 'Cerrar';
$_['button_enable']                 = 'Habilitar';
$_['button_disable']                = 'Deshabilitar';
$_['button_filter']                 = 'Filtrar';
$_['button_send']                   = 'Enviar';
$_['button_edit']                   = 'Editar';
$_['button_copy']                   = 'Copiar';
$_['button_back']                   = 'Volver';
$_['button_upload']                 = 'Subir';
$_['button_view_images']            = 'Ver sus Imágenes';
$_['button_remove']                 = 'Eliminar';
$_['button_refresh']                = 'Refrescar';

$_['button_parent']                 = 'Padre';
$_['button_folder']                 = 'Nueva Carpeta';
$_['button_search']                 = 'Buscar';
$_['button_view']                   = 'Ver';
$_['button_install']                = 'Instalar';
$_['button_uninstall']              = 'Desinstalar';
$_['button_link']                   = 'Link';
$_['button_shipping']               = 'Aplicar método de envío';
$_['button_payment']                = 'Aplicar método de Pago';
$_['button_coupon']                 = 'Aplicar Cupón';
$_['button_voucher']                = 'Aplicar Cupón';
$_['button_reward']                 = 'Aplicar Puntos';

// Error
$_['error_permission'] = 'Advertencia: ¡Permisos denegados!';
$_['error_filename']   = 'Advertencia: ¡El nombre de archivo debe contener entre 3 y 255!';
$_['error_folder']     = 'Advertencia: ¡El nombre de carpeta ebe contener entre 3 y 255!';
$_['error_exists']     = 'Advertencia: ¡Existe un archivo o directorio con el mismo nombre!';
$_['error_directory']  = 'Advertencia: ¡El directorio no existe!';
$_['error_filetype']   = 'Advertencia: ¡Tipo de archivo incorrecto!';
$_['error_upload']     = 'Advertencia: ¡El archivo no pudo ser cargado por alguna razón desconocida!';
$_['error_delete']     = 'Advertencia: ¡No se puede borrar el directorio!';
