<?php
// Heading
$_['heading_title']    = 'Mantenimiento';

// Texto
$_['text_maintenance'] = 'Mantenimiento';
$_['text_message']     = '<h1 style="text-align:center;">Actualmente estamos realizando algunas tareas de mantenimiento. <br/>Volveremos lo antes posible. Por favor vuelva a intentar más tarde.</h1>';