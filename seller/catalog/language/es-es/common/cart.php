<?php
// Texto
$_['text_items']     = '%s artículo(s) - %s';
$_['text_empty']     = '¡Su carro de compras esta vacío!';
$_['text_cart']      = 'Ver carro de compras';
$_['text_checkout']  = 'Pagar';
$_['text_recurring'] = 'Perfil de Pago';