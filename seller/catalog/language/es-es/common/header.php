<?php
// Texto
$_['text_home']          = 'Inicio';
$_['text_wishlist']      = 'Favoritos (%s)';
$_['text_shopping_cart'] = 'Carrito de Compras';
$_['text_category']      = 'Categorias';
$_['text_account']       = 'Mi Cuenta';
$_['text_register']      = 'Registro';
$_['text_login']         = 'Iniciar Sesión';
$_['text_order']         = 'Ver su Historial de Pedidos';
$_['text_transaction']   = 'Sus Transacciones';
$_['text_download']      = 'Descargas';
$_['text_logout']        = 'Cerrar Sesión';
$_['text_checkout']      = 'Comprar';
$_['text_search']        = 'Buscar';
$_['text_all']           = 'Mostrar Todo';

$_['text_extensions'] = 'Mi Tienda';
$_['text_manageextensions'] = 'Administrar Productos';
$_['text_addextensions']    = 'Agregar Nuevo Producto';
$_['text_getpaid']    = '¿Cómo desea que le paguen?';
$_['text_messagebox']    = 'Buzón de Mensajes';
$_['text_checkmessage']    = 'Reciba y envíe mensajes';

$_['text_download'] = 'Administrar Descargas';

$_['text_plan']      = 'Actualizar su Plan';

$_['text_export'] = 'Subir Productos en Masa';

$_['text_images'] = 'Subir Imágenes';

$_['text_plan']      = 'Actualizar su Plan';

$_['text_my_plan']     = 'Mi Plan';


/**code agregados here**/
$_['text_option']       = 'Administrar Opciones';
$_['text_attributes'] = 'Administrar Atributos';

$_['text_offer']       = 'Agregar un Producto de la Lista';
$_['text_category']    = 'Administrar Categorías';
$_['text_address2']    = '¿Cómo desea que le paguen?';
$_['text_order1']         = 'Mis Pedidos';
$_['text_sellerwelcome']         = 'Bienvenido, inicio sesión como ';
$_['text_welcome']               = 'Bienvenido ';
$_['text_logged']               = ' inicio sesión en la tienda de ';

$_['text_edit']          = 'Editar información de su cuenta';
$_['text_password']      = 'Cambiar contraseña';
$_['text_address']       = 'Modificar las entradas de la libreta de direcciones';

$_['text_manage_customers']              = 'Administrar Clientes';
$_['text_manage_customers_groups']       = 'Administrar Grupos de Clientes';
$_['text_manage_users']                  = 'Administrar Usuarios';
$_['text_manage_price_list']             = 'Administrar Lista de Precios';


// ZMULTISELLER
$_['text_buyeraccount']   = 'Comprador';
$_['text_selleraccount']  = 'Vender con Nostros';
$_['text_seller_store']   = 'Ir a mi Tienda';
$_['text_viewselleraccount']  = 'Ver todos los Vendedores';
$_['text_your_transaction']   = 'Sus Transacciones';
