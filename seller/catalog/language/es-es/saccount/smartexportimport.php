<?php
// Heading
$_['heading_title']     = 'Subir en masa';

// Texto
$_['text_success']      = 'Éxito: ¡Ha importado sus productos exitosamente!';
$_['text_nochange']     = 'No se cambiaron los datos del servidor..';
$_['text_log_details']  = 'Vea también \'System > Error Logs\' para más detalles.';

// Entry
$_['entry_restore']     = 'Importar desde planilla de cálculo:';
$_['entry_description'] = 'Maneje sus productos Use el SmartImport desde un archivo de EXCEL.';
$_['entry_exportway_sel'] = 'por favor seleccione el método para exportar sus productos:';
$_['entry_start_id'] = 'id de producto inicial:';
$_['entry_end_id'] = 'id de producto final:';
$_['entry_start_index'] = 'cantidad por lote:';
$_['entry_end_index'] = 'Serial de lote:';


// Button labels
$_['text_acount']     = 'Cuenta';

$_['button_import']     = 'Subir';

$_['button_export']     = 'SmartExport';
$_['button_export_pid']     = 'exportar por id de producto';
$_['button_export_page']     = 'exportar por lote';
$_['btn_download_format']     = 'Descargar Formato';


//Error
$_['error_exist_product'] = '¡La ID de producto %s ya existe en su base de datos, por favor verifique el archivo de Excel!';
$_['error_permission']          = 'Advertencia: ¡No tiene permiso para modificar importaciones!';
$_['error_upload']              = '¡El archivo subido no es una planilla de cálculo válida o sus valores no están en los formatos esperados!';
$_['error_sheet_count']         = 'Importar: Número de hojas de cálculo inválido, se esperaban 8 hojas de cálculo';
$_['error_categories_header']   = 'Importar: Encabezado inválido en la hoja de cálculo Categorías';
$_['error_filtergroups_header']   = 'Importar: Encabezado inválido en la hoja de cálculo Filtrar Grupo';
$_['error_filters_header']   = 'Importar: Encabezado inválido en la hoja de cálculo Filtrar';
$_['error_products_header']     = 'Importar: Encabezado inválido en la hoja de cálculo Productos';
$_['error_descriptions_header']     = 'Importar: Encabezado inválido en la hoja de cálculo Descripciones';
$_['error_additionalimages_header']     = 'Importar: Encabezado inválido en la hoja de cálculo de imágenes adicionales';
$_['error_product_options_header']      = 'Importar: Encabezado inválido en la hoja de cálculo OpcionesDeProducto';
$_['error_options_header']      = 'Importar: Encabezado inválido en la hoja de cálculo Opciones';
$_['error_option_values_header']      = 'Importar: Encabezado inválido en la hoja de cálculo ValoresDeOpción';
$_['error_attributes_header']   = 'Importar: Encabezado inválido en la hoja de cálculo Atributos';
$_['error_specials_header']     = 'Importar: Encabezado inválido en la hoja de cálculo Especiales';
$_['error_discounts_header']    = 'Importar: Encabezado inválido en la hoja de cálculo Descuentos';
$_['error_rewards_header']      = 'Importar: Encabezado inválido en la hoja de cálculo Recompensas';
$_['error_select_file']         = 'Importar: Por favor seleccione un archivo antes de clickear \'Importar\'';
$_['error_post_max_size']       = 'Importar: Tamaño de archivo mayor a %1 (ver parámetro de PHP \'post_max_size\')';
$_['error_upload_max_filesize'] = 'Importar: Tamaño de archivo mayor a %1 (ver parámetro de PHP \'upload_max_filesize\')';
$_['error_pid_no_data']         = 'no hay productos entre la id inicial y la id final.';
$_['error_page_no_data']        = 'no hay más datos de productos.';
$_['error_param_not_number']        = 'parámetros deben ser numéricos.';
?>
