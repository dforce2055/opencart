<?php
// Heading 
$_['heading_title']     = 'Libreta de direcciones';

// Texto
$_['text_account']      = 'Cuenta';
$_['text_address_book'] = 'Entradas en la Libreta de direcciones';
$_['text_edit_address'] = 'Editar dirección';
$_['text_insert']       = 'Su dirección fue insertada correctamente';
$_['text_update']       = 'Su dirección fue actualizada correctamente';
$_['text_delete']       = 'Su dirección fue eliminada correctamente';

// Entry
$_['entry_firstname']   = 'Nombre:';
$_['entry_lastname']    = 'Apellido:';
$_['entry_company']     = 'Compañía:';
$_['entry_company_id']  = 'ID Compañía:';
$_['entry_tax_id']      = 'ID Impuesto:';
$_['entry_address_1']   = 'Dirección 1:';
$_['entry_address_2']   = 'Dirección 2:';
$_['entry_postcode']    = 'Código Postal:';
$_['entry_city']        = 'Ciudad:';
$_['entry_country']     = 'País:';
$_['entry_zone']        = 'Provincia / Estado:';
$_['entry_default']     = 'Dirección por defecto:';

// Error
$_['error_delete']      = 'Advertencia: ¡Debe tener al menos una dirección!';
$_['error_default']     = 'Advertencia: ¡No se puede eliminar la dirección por defecto!';
$_['error_firstname']   = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']    = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_vat']         = '¡Número IVA inválido!';
$_['error_address_1']   = '¡La Dirección debe tener entre 3 y 128 caracteres!';
$_['error_postcode']    = '¡El código debe tener entre 2 y 10 caracteres!';
$_['error_city']        = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_country']     = '¡Por favor seleccione País!';
$_['error_zone']        = '¡Por favor seleccione provincia / estado!';
?>