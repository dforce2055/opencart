<?php
// Heading 
$_['heading_title']   = '¿Olvidó su contraseña?';

// Texto
$_['text_account']    = 'Cuenta';
$_['text_forgotten']  = 'Contraseña Olvidada';
$_['text_password']  = 'Contraseña:';
$_['text_your_email'] = 'Su Dirección de E-Mail';
$_['text_email']      = 'Ingrese el E-mail asociado a su cuenta. Click en enviar, para que le enviemos su contraseña por e-mail.';
$_['text_success']    = 'Éxito: Una nueva contraseña ha sido enviada a su e-mail.';

// Entry
$_['entry_email']     = 'Dirección de E-Mail:';

// Error
$_['error_email']     = 'Advertencia: ¡Su Dirección de E-Mail no se encuentra registrada! Por favor intente de nuevo.';
?>