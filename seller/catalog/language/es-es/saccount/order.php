<?php
// Heading
$_['heading_title']         = 'Pedido Historial';

// Texto
$_['text_account']          = 'Cuenta';
$_['text_order']            = 'Información de pedido';
$_['text_order_detail']     = 'Detalles de Pedido';
$_['text_invoice_no']       = 'Nro. de factura:';
$_['text_order_id']         = 'ID de Pedido:';
$_['text_status']           = 'Estado:';
$_['text_date_added']       = 'Fecha añadido:';
$_['text_customer']         = 'Cliente:';
$_['text_shipping_address'] = 'Dirección de Envío';
$_['text_shipping_method']  = 'Método de Envío:';
$_['text_payment_address']  = 'Dirección de pago';
$_['text_payment_method']   = 'Método de Pago:';
$_['text_products']         = 'Productos:';
$_['text_total']            = 'Total:';
$_['text_comment']          = 'Commentarios de pedido';
$_['text_history']          = 'Historial de pedido';
$_['text_success']          = '¡Ha agregado los productos del pedido ID #%s a su carro!';
$_['text_empty']            = '¡No se encontraron pedidos!';
$_['text_error']            = '¡No se pudo encontrar el pedido solicitado!';
$_['entry_date_start']            = 'Fecha inicio';
$_['entry_date_end']            = 'Fecha finalización';
$_['button_filter']            = 'Filtrar';

// Column
$_['column_order_id']       = 'ID de Pedido';
$_['column_product']        = 'Nro. de Productos';
$_['column_customer']       = 'Cliente';
$_['column_name']           = 'Nombre de Producto';
$_['column_model']          = 'Modelo';
$_['column_quantity']       = 'Cantidad';
$_['column_price']          = 'Precio';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';
$_['column_date_added']     = 'Fecha añadido';
$_['column_status']         = 'Estado del Pedido';
$_['column_comment']        = 'Comentario';
$_['entry_date_start']       = 'Fecha de Inicio';
#order_info
$_['order_status']          = 'Estado del Pedido';
$_['text_comment']          = 'Comentario';
$_['update_status']         = 'Actualizar Estado';
$_['change_status']         = 'Cambiar Estado';

?>
