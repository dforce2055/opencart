<?php
// Heading
$_['heading_title']       = 'Sus Opciones';
$_['heading_title1']     = 'Agregar Opción';

// Texto
$_['text_success']        = '¡Éxito: Ha modificado sus opciones!';
$_['text_choose']         = 'Seleccionar';
$_['text_select']         = 'Lista desplegable';
$_['text_radio']          = 'Radio';
$_['text_checkbox']       = 'Casilla de verificación';
$_['text_image']          = 'Imagen';
$_['text_input']          = 'Entrada';
$_['text_text']           = 'Texto';
$_['text_textarea']       = 'Textarea';
$_['text_file']           = 'Archivo';
$_['text_date']           = 'Fecha';
$_['text_datetime']       = 'Fecha &amp; Hora';
$_['text_time']           = 'Time';
$_['text_image_manager']  = 'Administrar Imagen';
$_['text_browse']         = 'Buscar Archivos';
$_['text_clear']          = 'Limpiar Imagen';
$_['text_account']          = 'Cuenta';

// Column
$_['column_name']         = 'Nombre de opción';
$_['column_sort_order']   = 'Ordenar Pedidos';
$_['column_action']       = 'Acción';

// Entry
$_['entry_name']         = 'Nombre de opción:';
$_['entry_type']         = 'Tipo:';
$_['entry_value']        = 'Nombre del Valor de la opción';
$_['entry_image']        = 'Imagen:';
$_['entry_sort_order']   = 'Ordenar Pedidos:';

// Error
$_['error_permission']   = 'Advertencia: ¡No tiene permiso para modificar opciones!';
$_['error_name']         = '¡El nombre de opción debe tener entre 1 y 128 caracteres!';
$_['error_type']         = 'Advertencia: ¡Valores de opción necesarios!';
$_['error_option_value'] = '¡Nombre de opción de valor debe tener entre 1 y 128 caracteres!';
$_['error_product']      = 'Advertencia: ¡Esta opción no puede ser eliminada porque está actualmente asignada a %s producto(s)!';

/**code agregados here**/
// Button
$_['button_insert']           = 'Insertar';
$_['button_delete']           = 'Borrar';
$_['button_filter']           = 'Filtrar';
$_['button_save']             = 'Guardar';
$_['button_edit']             = 'Editar';
$_['button_copy']             = 'Copiar';
$_['button_back']             = 'Volver';
$_['button_cancel']           = 'Cancelar';
$_['button_remove']           = 'Remover';
$_['button_backup']           = 'Respaldo';
$_['button_upload']           = 'Subir';
$_['button_view_images']            = 'Ver sus Imágenes';
$_['button_submit']           = 'Enviar';
$_['button_add_discount']     = 'Agregar Descuentos';
$_['button_add_special']      = 'Agregar Oferta';
$_['button_add_image']        = 'Agregar Imagen';
$_['button_reset']            = 'Reiniciar';
$_['button_add_option_value']            = 'Agregar Opción value';
$_['text_edit']         = 'Editar';
$_['text_no_results']         = 'No se encontraron resultados';

$_['text_modify']      = '¡Ha modificado la opción con éxito!';

//mail
$_['text_subject']  = '%s - Nueva opción esperando aprobación';
$_['text_welcome']  = 'Nueva opción publicada en %s!';
$_['text_login']    = 'Su cuenta ha sido creada, ahora puede iniciar sesión usando su dirección de email y contraseñn visitando nuestra sitio web o desde la siguiente URL:';
$_['text_approval'] = 'Nueva opción esperando ser aprobada. Inicie sesión como administrador y verifice la opcióñ a aprobar.';
$_['text_services'] = 'Al iniciar sesión, podrá acceder a otros servicios, como la revisión de pedidos anteriores, la impresión de facturas y la edición de la información de su cuenta.';
$_['text_thanks']   = 'Gracias,';


/*end*/
?>
