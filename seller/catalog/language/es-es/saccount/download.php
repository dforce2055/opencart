<?php
// Heading
$_['heading_title']    = 'Sus Descargas';
$_['heading_title1']    = 'Agregar Descargas';
$_['text_account']       = 'Cuenta';
$_['text_account_already']       = 'Cuenta';

// Texto
$_['text_success']     = 'Éxito: ¡Has modificado tus descargas!';
$_['text_upload']      = '¡El archivo ah sido cargado correctamente!';
$_['text_loading']      = 'Cargando...';
$_['text_add']      = 'Agregar Descarga';

$_['text_edit']      = 'Editar';

$_['text_no_results']      = 'No se encontraron resultados';

// Column
$_['column_name']      = 'Nombre de las Descargas';
$_['column_remaining'] = 'Descargas totales permitidas';
$_['column_action']    = 'Acción';

$_['column_date_added'] = 'Fecha añadido';

// Help
$_['help_filename']     = 'Puede cargarlo a través del botón de subir o utilizar FTP para cargar en el directorio de descarga e ingresar los detalles a continuación.';
$_['help_mask']         = 'Se recomienda que el nombre de archivo y la máscara sean diferentes para evitar que las personas intenten enlazar directamente a sus descargas.';


// Entry
$_['entry_name']        = 'Nombre de la Descarga';
$_['entry_filename']    = 'Nombre de Archivo';
$_['entry_mask']        = 'Máscara';

$_['entry_remaining']  = 'Descargas totales permitidas:';
$_['entry_update']     = 'Sacar los clientes previos:<br /><span class="help">Compruebe esto para actualizar las versiones compradas anteriormente también.</span>';

// Error
// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar las descargas!';
$_['error_name']        = 'El nombre de las descarga debe tener entre 3 y 64 caracterés!';
$_['error_upload']      = '¡La carga es necesaria!';
$_['error_filename']    = 'El nombre de archivo debe contener entre 3 y 128 caracterés!';
$_['error_exists']      = '¡El archivo no existe!';
$_['error_mask']        = '¡La máscara debe tener entre 3 y 128 caracterés!';
$_['error_filetype']    = '¡Tipo de archivo inválido!';
$_['error_product']     = 'Advertencia: ¡Esta descarga no puede ser eliminada, actualmente esta asociada a %s productos!';

// Button
$_['button_insert']           = 'Insertar';
$_['button_delete']           = 'Eliminar';
$_['button_filter']           = 'Filtrar';
$_['button_save']             = 'Guardar';
$_['button_edit']             = 'Editar';
$_['button_copy']             = 'Copiar';
$_['button_back']             = 'Volver';
$_['button_cancel']           = 'Cancelar';
$_['button_remove']           = 'Remover';
$_['button_backup']           = 'Respaldo';
$_['button_upload']           = 'Subir';
$_['button_view_images']            = 'Ver sus Imágenes';
$_['button_submit']           = 'Enviar';
$_['button_add_discount']     = 'Agregar Descuentos';
$_['button_add_special']      = 'Agregar Especiales';
$_['button_add_image']        = 'Agregar Imagen';
$_['button_reset']            = 'Reiniciar';
$_['button_add_option_value']            = 'Agregar opción de valor';
?>
