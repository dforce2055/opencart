<?php
// Heading
$_['heading_title']      = 'Tablero';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_account_already']       = 'Cuenta';
$_['text_my_account']    = 'Mi Cuenta';
$_['text_my_orders']     = 'Mis Pedidos';
$_['text_my_newsletter'] = 'Boletín de Noticias';
$_['text_edit']          = 'Editar información de su cuenta';
$_['text_password']      = 'Cambiar contraseña';
$_['text_address']       = 'Modificar Dirección';
$_['text_wishlist']      = 'Modificar la lista de Deseados';
$_['text_order']         = 'Ver historial de Pedidos';
$_['text_download']      = 'Descargas';
$_['text_reward']        = 'Sus puntos de recompensa';
$_['text_return']        = 'Ver solicitudes de devolución';
$_['text_transaction']   = 'Sus transacciones';
$_['text_newsletter']    = 'Subscribir / desuscribir al newsletter';


$_['text_extensions'] = 'Mi Tienda';
$_['text_manageextensions'] = 'Administrar Productos';
$_['text_addextensions']    = 'Agregar nuevo Producto';
$_['text_getpaid']    = '¿Cómo le gustaría que le paguemos?';
$_['text_messagebox']    = 'Buzón de mensajes';
$_['text_checkmessage']    = 'Reciba y envíe mensajes';

$_['text_download'] = 'Administrar Descargas';

$_['text_plan']      = 'Actualizar su Plan';

$_['text_export'] = 'Subir Productos en Masa';

$_['text_images'] = 'Subir Imágenes';

$_['text_plan']      = 'Actualizar su Plan';

$_['text_my_plan']     = 'Mi Plan';


/**code agregados here**/
$_['text_option']                        = 'Administrar Opciones';
$_['text_attributes']                    = 'Administrar Atributos';

$_['text_offer']                         = 'Agregar un Producto de la Lista';
$_['text_category']                      = 'Administrar Categorías';
$_['error_agree']                        = 'Advertencia: ¡Debe aceptar el %s!';
$_['text_your_transaction']              = 'Sus Transacciones';
$_['text_manage_customers']              = 'Administrar Clientes';
$_['text_manage_customers_groups']       = 'Administrar Grupos de Clientes';
$_['text_manage_users']                  = 'Administrar Usuarios';
$_['text_manage_price_list']             = 'Administrar Lista de Precios';
/**/
?>
