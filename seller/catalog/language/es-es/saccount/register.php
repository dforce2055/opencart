<?php
// Heading
$_['heading_title']        = 'Crear Su Cuenta';

// Texto
$_['text_account']         = 'Cuenta';
$_['text_register']        = 'Registrar';
$_['text_account_already'] = 'Si ya tiene una cuenta, por favor inicie sesión en la <a href="%s">página de inicio de sesión</a>.';
$_['text_your_details']    = 'Sus Detalles Personales';
$_['text_your_address']    = 'Su Dirección';
$_['text_newsletter']      = 'Boletín de Noticias';
$_['text_your_password']   = 'Su contraseña';
$_['text_paid']   = '¿Cómo desea que le paguen?';

$_['text_desc']   = 'Descripción';


$_['text_agree']           = 'He leído y acepto los <a class="agree" href="%s" alt="%s"><b>%s</b></a>';

// Entry
$_['entry_username']       = 'Nombre de usuario:';

$_['entry_aboutus']       = 'Acerca de nosotros:';


$_['entry_firstname']      = 'Nombre:';
$_['entry_lastname']       = 'Apellido:';
$_['entry_email']          = 'E-Mail:';
$_['entry_telephone']      = 'Teléfono:';
$_['entry_fax']            = 'Fax:';
$_['entry_business_name']  = 'Nombre de negocio:';
$_['entry_vat_number']     = 'Número VAT:';
$_['entry_company']        = 'Compañía:';
$_['entry_customer_group'] = 'Tipo de negocio:';
$_['entry_company_id']     = 'ID Compañía';
$_['entry_tax_id']         = 'ID Impuesto:';
$_['entry_address_1']      = 'Dirección:';
$_['entry_address_2']      = 'Dirección:';
$_['entry_postcode2']       = 'Código postal:';
$_['entry_cheque']           = 'Nombre de beneficiario de cheque:';
$_['entry_country2']        = 'País:';
$_['entry_postcode']       = 'Código postal:';
$_['entry_city']           = 'Ciudad:';
$_['entry_country']        = 'País:';
$_['entry_zone']           = 'Provincia / Estado:';
$_['entry_zone2']           = 'Provincia / Estado:';
$_['entry_newsletter']     = 'Subscribir:';
$_['entry_password']       = 'Contraseña:';
$_['entry_confirm']        = 'Confirmar contraseña:';
$_['entry_paypalorcheque'] = 'Paypal o cheque:';
$_['entry_paypalemail']	   = 'E-Mail de Paypal:';
$_['entry_captcha']  = 'Ingrese el código en el recuadro de abajo:';
$_['entry_bankname']	   = 'Nombre del banco:';
$_['entry_accountnumber']  = 'Número: de cuenta:';
$_['entry_accountname']	   = 'Nombre de cuenta:';
$_['entry_branch']	   = 'Número ABA/BSB (Número de sucursal):';
$_['entry_ifsccode']	   = 'Código IFSC:';
// Error
$_['error_exists']         = '¡Advertencia: La dirección de E-Mail ya está registrada!';
$_['error_userexists']     = '¡Advertencia: El nombre de usuario ya está registrado';
$_['error_available']      = 'Felicitaciones: Nombre de usuario disponible';
$_['error_firstname']      = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_username']       = '¡El nombre de usuario debe tener entre 4 y 32 caracteres!';
$_['error_invalidusername']= '¡Reingrese el nombre de usuario! ¡Formato inccorrecto!( sólo se permiten letras, números, caracteres especiales !@$^ )';
$_['error_lastname']       = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']          = '¡La dirección de E-Mail parece inválida!';
$_['error_telephone']      = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_password']       = '¡La contraseña debe tener entre 4 y 20 caracteres!';
$_['error_confirm']        = '¡La contraseña no coincide con la confirmación!';
$_['error_company_id']     = '¡ID de compañía necesario!';
$_['error_tax_id']         = '¡ID de impuestos necesario!';
$_['error_vat']            = '¡Número IVA inválido!';
$_['error_address_1']      = '¡La dirección debe tener entre 3 y 128 caracteres!';
$_['error_city']           = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_postcode']       = 'íEl código postal debe tener entre 2 y 10 caracteres!';
$_['error_country']        = '¡Por favor seleccione un País!';
$_['error_paypalemail']    = 'Paypal !La dirección de E-Mail parece inválida!';
$_['error_captcha']  = '¡El código de verificación no coincide con la imagen!';
$_['error_address_2']      = '¡La dirección debe tener entre 3 y 128 caracteres!';
$_['error_cheque']           = '¡El nombre del beneficiario debe tener entre 2 y 128 caracteres!';
$_['error_postcode2']       = '¡El código postal debe tener entre 2 y 10 caracteres!';
$_['error_country2']        = '¡Por favor seleccione un país!';
$_['error_zone']           = '¡Por favor seleccione provincia / estado!';
$_['error_zone2']           = '¡Por favor seleccione provincia / estado!';
$_['error_agree']          = '¡Advertencia: Debe aceptar los %s!';
$_['error_bankname']      = '¡El nombre de banco debe tener entre 1 y 32 caracteres!';
$_['error_accountnumber']      = '¡El número de cuenta debe tener entre 3 y 32 caracteres!';
$_['error_accountname']      = '¡El nombre de cuenta debe tener entre 1 y 32 caracteres!';
$_['error_branch']      = '¡El número de sucursal debe tener entre 3 y 132 caracteres!';
$_['error_ifsccode']      = '¡El código IFSC debe tener entre 3 y 32 caracteres!';

// ZMULTISELLER
$_['text_check_availability']      = 'Chequear disponibilidad';
$_['text_select_plan']             = 'Seleccionar Plan';
$_['text_check_our_plan']          = 'Chequear Sus Planes';
$_['text_create_my_account']       = 'Crear Mi Cuenta';
?>
