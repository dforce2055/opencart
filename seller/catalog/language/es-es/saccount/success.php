<?php
// Heading
$_['heading_title'] = '¡Su Cuenta ha sido Creada!';

// Texto
$_['text_message']  = '<p>¡Felicitaciones! ¡Su nueva cuenta ha sido creada exitosamente!</p> <p>Si tiene alguna pregunta sobre la utilización de la tienda online, por favor envíe un e-mail al administrador de la tienda.</p> <p>Una confimación ha sido enviada a su dirección de e-mail. Si dentro de una hora no ha recibido nada, por favor <a href="%s">contáctenos</a>.</p>';
$_['text_approval'] = '<p>Gracias por registrarse en %s!</p><p>Será notificado por e-mail, cuando su cuenta sea aprobada por el administrador de la tienda.</p><p>Si tiene alguna pregunta sobre la utilización de la tienda online, por favor <a href="%s">contactese con el dueño de la tienda</a>.</p>';
$_['text_account']  = 'Cuenta';
$_['text_approval1'] = '<p>Gracias por registrarse en %s!</p><p>Su cuenta Ha sido aprobado.</p><p>Si tiene alguna pregunta sobre la utilización de la tienda online, por favor <a href="%s">contáctese con el administrador de la tienda</a>.</p>';
$_['text_success']  = 'Éxito';
?>
