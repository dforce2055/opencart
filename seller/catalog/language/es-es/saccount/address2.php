<?php
// Heading 
$_['heading_title']     = '¿Como le gustaría que le paguen?';

// Texto
$_['text_account']      = 'Cuenta';
$_['text_address_book'] = 'Entradas en la Libreta de direcciones';
$_['text_edit_address'] = 'Editar dirección';
$_['text_insert']       = 'Su dirección fue insertada correctamente';
$_['text_update']       = 'Su dirección fue actualizada correctamente';
$_['text_delete']       = 'Su dirección fue eliminada correctamente';

// Entry
$_['entry_firstname']   = 'Nombre:';
$_['entry_lastname']    = 'Apellido:';
$_['entry_company']     = 'Compañía:';
$_['entry_company_id']  = 'ID Compañía:';
$_['entry_tax_id']      = 'ID Impuesto:';
$_['entry_address_1']   = 'Dirección 1:';
$_['entry_address_2']   = 'Dirección :';
$_['entry_postcode']    = 'Código Postal:';
$_['entry_city']        = 'Ciudad:';
$_['entry_country']     = 'País:';
$_['entry_zone']        = 'Provincia / Estado:';
$_['entry_default']     = 'Dirección por defecto:';
$_['entry_paypalemail']	   = 'E-Mail de Paypal:';
$_['entry_bankname']	   = 'Bank Nombre:';
$_['entry_accountnumber']  = 'Numero de Cuenta:';
$_['entry_accountname']	   = 'Nombre de Cuenta:';
$_['entry_branch']	   = 'Número ABA/BSB (Número de sucursal):';
$_['entry_ifsccode']	   = 'ódigo IFSC:';
$_['entry_cheque']           = 'Nombre del Beneficiario del cheque:';
// Error
$_['error_delete']      = 'Advertencia: ¡Debe tener al menos una dirección!';
$_['error_default']     = 'Advertencia: ¡No se puede eliminar la dirección por defecto!';
$_['error_firstname']   = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']    = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_vat']         = '¡El número IVA es inválido!';
$_['error_address_1']   = '¡La Dirección debe tener entre 3 y 128 caracteres!';
$_['error_postcode2']    = '¡El código debe tener entre 2 y 10 caracteres!';
$_['error_city2']        = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_country2']     = '¡Por favor seleccione País!';
$_['error_zone2']        = '¡Por favor seleccione provincia / estado!';
$_['error_paypalemail']    = '¡La dirección de E-Mail parece invalida!';
$_['error_address_2']      = '¡La Dirección debe tener entre 3 y 128 caracteres!';
$_['error_bankname']      = '¡El nombre del banco debe tener entre 1 y 32 caracteres!';
$_['error_accountnumber']      = '¡El número de cuenta debe tener entre 3 y 32 caracteres!';
$_['error_accountname']      = '¡El nombre de cuenta debe tener entre 1 y 32 caracteres!';
$_['error_cheque']           = '¡El nombre del beneficiario debe tener entre 2 y 128 caracteres!';
$_['error_branch']      = '¡El nombre de la sucursal debe tener entre 3 y 132 caracteres!';
$_['error_ifsccode']      = '¡El código IFSC debe tener entre 3 y 32 caracteres!';
?>