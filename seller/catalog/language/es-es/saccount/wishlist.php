<?php
// Heading 
$_['heading_title'] = 'Mi Lista de Deseos';

// Texto
$_['text_account']  = 'Cuenta';
$_['text_instock']  = 'En Stock';
$_['text_wishlist'] = 'Lista de Deseos (%s)';
$_['text_login']    = '¡Debe <a href="%s">iniciar sesión</a> o <a href="%s">crear una cuenta</a> para guardar <a href="%s">%s</a> en su <a href="%s">lista de deseos!</a>!';
$_['text_success']  = '¡Éxito: Ha agregado <a href="%s">%s</a> a su <a href="%s">lista de deseos</a>!';
$_['text_remove']   = '¡Éxito: Ha modificado su lista de deseos!';
$_['text_empty']    = 'Su lista de deseos está vacía.';

// Column
$_['column_image']  = 'Imagen';
$_['column_name']   = 'Nombre de Producto';
$_['column_model']  = 'Modelo';
$_['column_stock']  = 'Stock';
$_['column_price']  = 'Precio Unitario';
$_['column_action'] = 'Acción';
?>