<?php
// Texto
$_['heading_title']     = 'Agregar producto existente';

$_['button_next']     = 'Siguiente';

$_['b_no']     = 'No, crear un producto nuevo';

$_['b_yes']     = 'Sí, continuar';

$_['entry_reg']      = '1. Asegúrese que tanto producto como nombre sean correctos.';

$_['entry_reg1']     = '2. Descripción del producto:';

$_['text_form']     = 'Agregar Productos';

$_['text_insert']     = 'Crear un Producto Nuevo.';

$_['text_search']     = 'Buscar su producto:';
$_['text_search_product']     = 'Buscar su producto:';


$_['text_account']     = 'Cuenta';

$_['text_product']     = '<p class="info">Por favor seleccione un producto existente de nuestra base de datos.</p>
								<p>De esta forma no necesita llenar descripciones, subir fotos, etc.
								Le ahorra tiempo y su oferta aún se ve genial. Si no encuentra el item puede.</p>';

$_['heading_title1']     = 'Detalles de producto';

$_['heading_title2']     = 'Agregar detalles';

$_['new_extensions']    = 'Agregar nuevo Producto <a href="%s">aquí</a>.';

$_['text_search']       = 'Buscar';
$_['text_brand']        = 'Marca';
$_['text_manufacturer'] = 'Marca:';
$_['text_model']        = 'Código de Producto:';
$_['text_reward']       = 'Puntos de Recompensa:';
$_['text_points']       = 'Precio en puntos de recompensa:';
$_['text_stock']        = 'Disponibilidad:';
$_['text_instock']      = 'En Stock';
$_['text_price']        = 'Precio:';
$_['text_tax']          = 'Sin Impuestos:';
$_['text_discount']     = '%s o más %s';
$_['text_option']       = 'Opciones Disponibles';
$_['text_qty']          = 'Cantidad:';
$_['text_minimum']      = 'Este producto tiene una cantidad mínima de %s';
$_['text_or']           = '- O -';
$_['text_reviews']      = '%s reseñas';
$_['text_write']        = 'Escribir una reseña';
$_['text_no_reviews']   = 'No hay reseñas para este producto.';
$_['text_note']         = '<span style="color: #FF0000;">Nota:</span> HTML no est&aacuete; traducido!';
$_['text_share']        = 'Compartir';
$_['text_success']      = 'Gracias por publicar su producto.';



$_['text_upload']       = '¡El archivo fue subido correctamente!';
$_['text_wait']         = '¡Por favor espere!';
$_['text_tags']         = 'Etiquetas';
$_['text_error']        = '¡Producto no encontrado!';
$_['text_no_results']   = '¡No se encontraron productos!';
$_['text_browse']         = 'Buscar';
$_['text_clear']         = 'Limpiar';
$_['text_enabled']         = 'Yes';
$_['text_disabled']         = 'No';
$_['text_edit']         = 'Editar';

$_['text_delete']      = '¡Ha modificado productos!';

$_['text_modify']      = '¡Ha modificado productos exitosamente!';
//button
$_['button_cancel']  = 'Volver';
$_['button_save']  = 'Enviar';


// Entry
$_['column_name']        = 'Nombre de Producto';
$_['column_status']      = 'Estado';
$_['column_action']      = 'Acción';
$_['column_date_added']  = 'Fecha añadido';
$_['column_downloads']   = 'Descargas';

// Entry
$_['entry_name']        = 'Nombre de Producto:';
$_['entry_model']        = 'Modelo:';
$_['entry_description'] = 'Descripción:';
$_['entry_category']    = 'Categoría de Producto:';
$_['entry_image']		= 'Imagen:(Click en el botón para cambiar)';

$_['entry_image1']    = 'Imagen';
$_['entry_image2']    = 'Nombre de Imagen';
$_['button_add_image']    = 'Agregar Imagen';

$_['download_image1']    = 'Nombre de la Descarga';
$_['download_image2']    = 'Nombre del archivo';
$_['button_add_download']    = 'Agregar Descarga';

$_['entry_price']       = 'Precio:';
$_['entry_status']      = 'Publicar:';
$_['entry_license']      = 'Licencia:';
$_['entry_rating']      = 'Valoración:';
$_['entry_good']        = 'Bueno';
$_['entry_bad']         = 'Malo';
$_['entry_captcha']     = 'Ingrese el código en el recuadro de abajo:';
$_['button_upload']     = 'Subir Imagen';
$_['entry_free']        = 'Gratis';
$_['entry_commerical']  = 'Comercial';
$_['entry_captcha']  = 'Captcha: Ingrese el código en el recuadro de abajo)';

// Tabs
$_['tab_description']   = 'Descripción';
$_['tab_documentation'] = 'Documentación';
$_['tab_downloads']     = 'Descargas';
$_['tab_image']         = 'Imágenes';

// Error
$_['error_name']        = 'Advertencia: ¡El nombre de Producto debe tener entre 3 y 25 caracteres!';
$_['error_model']       = 'Advertencia: ¡El modelo del producto debe tener entre 3 y 25 caracteres!';
$_['error_text']        = 'Advertencia: ¡La descripción del producto debe tener entre 25 y 1000 caracteres!';
$_['error_rating']      = 'Advertencia: ¡Por favor seleccione una valoración de reseña!';
$_['error_captcha']     = 'Advertencia: ¡El código de verificación no coincide con la imagen!';
$_['error_upload']      = '¡Es necesario subir!';
$_['error_filename']    = '¡El nombre de archivo debe tener entre 3 y 128 caracteres!';
$_['error_filetype']    = '¡Tipo de archivo Inválido!';
$_['error_filetype1']   = '¡Se permite únicamente archivos zip!';
$_['error_price']		= '¡Precio Inválido!';
$_['error_download']	= 'Advertencia: ¡Debe tener almenos una descarga!';
$_['error_download1']	= 'Advertencia: ¡Debe seleccionar almenos un archivo!';
$_['error_warning']		= 'Advertencia: ¡verifique todos los errores!';

//mail
$_['text_subject']  = '%s - Nuevo producto en espera de aprobación';
$_['text_welcome']  = 'Nuevo producto publicado en %s!';
$_['text_login']    = 'Su cuenta ha sido creada, ahora puede iniciar sesión usando su dirección de email y contraseñn visitando nuestra sitio web o desde la siguiente URL:';
$_['text_approval'] = 'Nuevo producto en espera de aprobación.Inicie Sesión como administrador para controlar y aprobar productos';
$_['text_services'] = 'Al iniciar sesión, podrá acceder a otros servicios, como la revisión de pedidos anteriores, la impresión de facturas y la edición de la información de su cuenta.';
$_['text_thanks']   = 'Gracias,';



/***NEW CODE ADDED**/
// Tab
$_['tab_data']                = 'Datos';
$_['tab_design']              = 'Diseño';
$_['tab_discount']            = 'Descuentos';
$_['tab_general']             = 'General';
$_['tab_links']               = 'Enlaces';
$_['tab_image']               = 'Imagen';
$_['tab_option']              = 'Opción';
$_['tab_special']             = 'Ofertas';
// Column
$_['column_name']            = 'Nombre de Producto';
$_['column_model']           = 'Modelo';
$_['column_image']           = 'Imagen';
$_['column_price']           = 'Precio';
$_['column_quantity']        = 'Cantidad';
$_['column_status']          = 'Estado';
$_['column_action']          = 'Acción';



// Entry
$_['entry_name']             = 'Nombre de Producto:';
$_['entry_description']      = 'Descripción:';
$_['entry_manufacturer']     = 'Fabricante:';
$_['entry_model']            = 'Modelo:';
$_['entry_sku']              = 'SKU:';
$_['entry_upc']              = 'UPC:';
$_['entry_location']         = 'Ubicación:';
$_['entry_quantity']         = 'Cantidad:';
$_['entry_price']            = 'Precio:';
$_['entry_tax_class']        = 'Impuesto Clase:';
$_['entry_weight_class']     = 'Peso Clase:';
$_['entry_weight']           = 'Peso:';
$_['entry_length']           = 'Largo Clase:';
$_['entry_dimension']        = 'Dimensiones (L x A x A):';
$_['entry_image']            = 'Imagen:';
$_['entry_date_start']       = 'Fecha Inicio:';
$_['entry_date_end']         = 'Fecha Fin:';
$_['entry_priority']         = 'Prioridad:';
$_['entry_attribute']        = 'Atributos:';
$_['entry_attribute_group']  = 'Grupo de atributos:';
$_['entry_text']             = 'Texto:';
$_['entry_required']         = 'Necesario:';
$_['entry_category']         = 'Categorías:';
$_['entry_download']         = 'Descargas:';
$_['entry_related']          = 'Productos relacionados:<br /><span class="help">(Autocompletar)</span>';
$_['entry_tag']          	 = 'Etiquetas de producto: <br /><span class="help">separar con coma</span>';
$_['entry_customer_group']   = 'Grupo de Clientes:';
$_['entry_sort_order']       = 'Ordenar Pedidos:';

// text
$_['text_none']               = ' --- Ninguna --- ';
$_['text_select']             = ' --- Por favor Seleccione --- ';
$_['text_select_all']         = 'Seleccionar Todo';
$_['text_unselect_all']       = 'Deseleccionar Todo';

// Button
$_['button_insert']           = 'Insertar';
$_['button_delete']           = 'Borrar';
$_['button_filter']           = 'Filtrar';
$_['button_save']             = 'Guardar';
$_['button_edit']             = 'Editar';
$_['button_copy']             = 'Copiar';
$_['button_back']             = 'Volver';
$_['button_cancel']           = 'Cancelar';
$_['button_remove']           = 'Remover';
$_['button_backup']           = 'Respaldo';
$_['button_upload']           = 'Subir';
$_['button_view_images']            = 'Ver sus Imágenes';
$_['button_submit']           = 'Enviar';
$_['button_add_discount']     = 'Agregar Descuentos';
$_['button_add_special']      = 'Agregar Oferta';
$_['button_add_image']        = 'Agregar Imagen';
$_['button_reset']            = 'Reiniciar';
$_['button_add_option_value']            = 'Agregar Opciones de Valor';


// Texto
$_['entry_option']           = 'Opción:';
$_['entry_option_value']     = 'Opciones de Valor:';
$_['entry_required']         = 'Necesario:';
$_['entry_status']           = 'Estado:';
$_['entry_sort_order']       = 'Ordenar Pedidos:';
$_['entry_category']         = 'Categorías:';
$_['entry_download']         = 'Descargas:';
$_['entry_related']          = 'Relacionado Productos:<br /><span class="help">(Autocompletar)</span>';
$_['entry_tag']          	 = 'Producto Etiquetas<br /><span class="help">separar con coma</span>';
$_['entry_reward']           = 'Puntos de Recompensa:';
$_['entry_layout']           = 'Anular Diseño:';
$_['entry_option_points']    = 'Puntos:';
$_['entry_subtract']         = 'Sustraer Stock:';

$_['button_add']                    = 'Agregar Nuevo';
$_['button_delete']                 = 'Borrar';
$_['button_save']                   = 'Guardar';
$_['button_cancel']                 = 'Cancelar';
$_['button_cancel_recurring']       = 'Cancelar Pagos Recurrentes';
$_['button_continue']               = 'Continuar';
$_['button_clear']                  = 'Limpiar';
$_['button_close']                  = 'Cerrar';
$_['button_enable']                 = 'Habilitar';
$_['button_disable']                = 'Deshabilitar';
$_['button_filter']                 = 'Filtrar';
$_['button_send']                   = 'Enviar';
$_['button_edit']                   = 'Editar';
$_['button_copy']                   = 'Copiar';
$_['button_back']                   = 'Volver';
$_['button_remove']                 = 'Remover';
$_['button_refresh']                = 'Refrescar';
$_['button_backup']                 = 'Respaldo';
$_['button_restore']                = 'Restaurar';
$_['button_download']               = 'Descargar';
$_['button_rebuild']                = 'Reconstruir';
$_['button_upload']                 = 'Subir';
$_['button_submit']                 = 'Enviar';
$_['button_invoice_print']          = 'Imprimir Facturas';
$_['button_shipping_print']         = 'Imprimir Lista de Envios';
$_['button_address_add']            = 'Agregar Dirección';
$_['button_attribute_add']          = 'Agregar Atributo';
$_['button_banner_add']             = 'Agregar Banner';
$_['button_custom_field_value_add'] = 'Agregar Campo Personalizado';
$_['button_product_add']            = 'Agregar Producto';
$_['button_filter_add']             = 'Agregar Filtrar';
$_['button_option_add']             = 'Agregar Opción';
$_['button_option_value_add']       = 'Agregar Opciones de Valor';
$_['button_recurring_add']          = 'Agregar Recurrente';
$_['button_discount_add']           = 'Agregar Descuentos';
$_['button_special_add']            = 'Agregar Oferta';
$_['button_image_add']              = 'Agregar Imagen';
$_['button_geo_zone_add']           = 'Agregar Geo Zona';
$_['button_history_add']            = 'Agregar Historial';
$_['button_transaction_add']        = 'Agregar Transacción';
$_['button_route_add']              = 'Agregar Ruta';
$_['button_rule_add']               = 'Agregar Regla';
$_['button_module_add']             = 'Agregar Modulo';
$_['button_link_add']               = 'Agregar Link';
$_['button_approve']                = 'Aprobar';
$_['button_reset']                  = 'Reiniciar';
$_['button_generate']               = 'Generar';
$_['button_voucher_add']            = 'Agregar Cupón';
$_['button_reward_add']             = 'Agregar Puntos de Recompensa';
$_['button_reward_remove']          = 'Remover Puntos de Recompensa';
$_['button_commission_add']         = 'Agregar Comisión';
$_['button_commission_remove']      = 'Remover Comisión';
$_['button_credit_add']             = 'Agregar Crédito';
$_['button_credit_remove']          = 'Remover Crédito';
$_['button_ip_add']                 = 'Agregar IP';
$_['button_parent']                 = 'Padre';
$_['button_folder']                 = 'Nueva Carpeta';
$_['button_search']                 = 'Buscar';
$_['button_view']                   = 'Ver';
$_['button_install']                = 'Instalar';
$_['button_uninstall']              = 'Desinstalar';
$_['button_login']                  = 'Iniciar Sesión en la Tienda';
$_['button_unlock']                 = 'Desbloquear Cuenta';
$_['button_link']                   = 'Link';
$_['button_currency']               = 'Refrescar los Valores Actuales';
$_['button_apply']                  = 'Aplicar';

// Tab
$_['tab_address']                   = 'Dirección';
$_['tab_admin']                     = 'Administrador';
$_['tab_attribute']                 = 'Atributos';
$_['tab_customer']                  = 'Detalles de Cliente';
$_['tab_data']                      = 'Datos';
$_['tab_design']                    = 'Diseño';
$_['tab_discount']                  = 'Descuentos';
$_['tab_general']                   = 'General';
$_['tab_history']                   = 'Historial';
$_['tab_ftp']                       = 'FTP';
$_['tab_ip']                        = 'IP Direcciones';
$_['tab_links']                     = 'Enlaces';
$_['tab_log']                       = 'Registro';
$_['tab_image']                     = 'Imagen';
$_['tab_option']                    = 'Opción';
$_['tab_server']                    = 'Servidor';
$_['tab_store']                     = 'Tienda';
$_['tab_special']                   = 'Ofertas';
$_['tab_local']                     = 'Local';
$_['tab_mail']                      = 'Mail';
$_['tab_module']                    = 'Modulo';
$_['tab_order']                     = 'Detalles de Pedido';
$_['tab_payment']                   = 'Detalles de Pago';
$_['tab_product']                   = 'Productos';
$_['tab_reward']                    = 'Puntos de Recompensa';
$_['tab_shipping']                  = 'Detalles de Envio';
$_['tab_total']                     = 'Totales';
$_['tab_transaction']               = 'Transacciones';
$_['tab_voucher']                   = 'Cupones';
$_['tab_sale']                      = 'Ventas';
$_['tab_marketing']                 = 'Marketing';
$_['tab_online']                    = 'Personas Online';
$_['tab_activity']                  = 'Actividad Reciente';
$_['tab_recurring']                 = 'Recurrente';
$_['tab_action']                    = 'Acción';
$_['tab_google']                    = 'Google';
$_['error_max_warning']		= 'Advertencia: ¡Alcanzó su límite de productos!';



?>
