<?php
// Heading 
$_['heading_title']    = 'Compra un Certificado de Regalo';

// Texto
$_['text_account']     = 'Cuenta';
$_['text_voucher']     = 'Cupón de regalo.';
$_['text_description'] = 'Este certificado de regalo ser&aexcl; enviado por e-mail al destinatario una que el pedido est&eexcl; pago.';
$_['text_agree']       = 'Entiendo que los certificados de regalo no son reembolsables.';
$_['text_message']     = '<p>¡Gracias por comprar un certificado de regalo! Una vez completado su pedido, se le enviar&aexcl; un email a su destinatario con detalles de c&oexcl;mo your canjear su cup&oexcln;.</p>';
$_['text_for']         = '%s Certificado de Regalo para %s';

// Entry
$_['entry_to_name']    = 'Nombre del destinatario:';
$_['entry_to_email']   = 'E-mail del destinatario:';
$_['entry_from_name']  = 'Su nombre:';
$_['entry_from_email'] = 'Su E-mail:';
$_['entry_theme']      = 'Tema del Certificado de Regalo:';
$_['entry_message']    = 'Mensaje:<br /><span class="help">(Opcional)</span>';
$_['entry_amount']     = 'Monto:<br /><span class="help">(El valor debe estar entre %s y %s)</span>';

// Error
$_['error_to_name']    = '¡El nombre del destinatario debe tener entre 1 y 64 caracteres!';
$_['error_from_name']  = '¡Su nombre debe tener entre 1 y 64 caracteres!';
$_['error_email']      = '¡La dirección de E-Mail parece inválida!';
$_['error_theme']      = '¡Debe seleccionar un tema!';
$_['error_amount']     = '¡Monto debe tener entre %s y %s!';
$_['error_agree']      = '¡Advertencia: Debe aceptar que los certificados de regalo no son reembolsables!';
?>

