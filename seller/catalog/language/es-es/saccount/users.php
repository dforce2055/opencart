<?php
// Heading
$_['heading_title']         = 'Usuarios';

// Text
$_['text_account']          = 'Cuenta';
$_['text_success']          = 'Éxito: ¡Pudo modificar los Usuarios!';
$_['text_default']          = 'Defecto';
$_['text_approved']         = 'Has aprobado %s cuentas!';
$_['text_wait']             = 'Por favor espere!';
$_['text_balance']          = 'Balance:';
$_['text_add_blacklist']    = 'Agregar a la Lista Negra';
$_['text_remove_blacklist'] = 'Eliminar de la Lista Negra';

$_['text_browse']          = 'Vistazo';
$_['text_image_manager']     = 'Aministrador de Im&acute;genes';
$_['text_clear']          = 'Limpiar';

$_['text_login']            = 'Iniciar sesión en la tienda';

$_['column_date_added']     = 'Fecha de agregación';
$_['entry_bankname']     = 'Nombre del Banco:';
$_['entry_accountnumber']  = 'Número de Cuenta:';
$_['entry_accountname']     = 'Nombre de la Cuenta:';
$_['entry_branch']       = 'ABA/BSB number (Branch Number):';
$_['entry_ifsccode']     = 'Código IFSC:';
$_['entry_business_name']  = 'Nombre del Negocio:';
$_['entry_vat_number']     = 'úmero VAT:';
$_['entry_id1']         = 'ID 1:';
$_['entry_id2']         = 'ID 2:';
$_['entry_image']       = 'Imagen:';
$_['button_upload']       = 'Subir archivo:';
$_['entry_cheque']        = 'Nombre del beneficiario del cheque:';
$_['text_status_enabled']         = 'Habilitado';
$_['text_status_disabled']        = 'Deshabilitado';
$_['text_deny']                   = 'Deshabilitado';
$_['text_approved']               = 'Habilitado';

// Button
$_['button_insert']         = 'Agregar Usuario';
$_['button_approve']        = 'Aprobar Usuario';
$_['button_deny']           = 'Desaprobar Usuario';
// Column
$_['column_name']           = 'Nombre';
$_['column_email']          = 'E-Mail';
$_['column_seller_group'] = 'Grupo de Vendedor';
$_['column_status']         = 'Estado';
$_['column_login']          = 'Iniciar sesión en la tienda';
$_['column_approved']       = 'Aprobado';
$_['column_date_added']     = 'Fecha de agregación';
$_['column_description']    = 'Descripción';
$_['column_amount']         = 'Monto';
$_['column_points']         = 'Puntos';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Cuentas Totales';
$_['column_action']         = 'Acción';

// Entry
$_['entry_firstname']       = 'Nombre:';
$_['entry_aboutus']       = 'Acerca de Nosotros:';

$_['entry_image']       = 'Imagen:';
$_['button_upload']       = 'Subir Archivo';

$_['text_upload']       = '¡Tu archivo se cargo correctamente!';


$_['entry_lastname']        = 'Apellido:';
$_['entry_email']           = 'E-Mail:';
$_['entry_telephone']       = 'Teléfono:';
$_['entry_fax']             = 'Fax:';
$_['entry_newsletter']      = 'Boletín de Noticias:';
$_['entry_seller_group']  = 'Grupo de Vendedor:';
$_['entry_status']          = 'Estado:';
$_['entry_password']        = 'Contraseña:';
$_['entry_confirm']         = 'Confirmar:';
$_['entry_company']         = 'Compañia:';
$_['entry_company_id']      = 'ID Compañia::';
$_['entry_tax_id']          = 'ID Impuesto:';
$_['entry_address_1']       = 'Dirección 1:';
$_['entry_address_2']       = 'Dirección 2:';
$_['entry_city']            = 'Ciudad:';
$_['entry_postcode']        = 'Código Postal:';
$_['entry_country']         = 'País:';
$_['entry_zone']            = 'Provincia:';
$_['entry_default']         = 'Dirección por defecto:';
$_['entry_amount']          = 'Monto:';
$_['entry_points']          = 'Puntos:<br /><span class="help">Use (-) para restar puntos</span>';
$_['entry_description']     = 'Descripción:';
$_['entry_permissions']     = 'Permisos';

// Error
$_['error_warning']         = 'Advertencia: ¡Por favor, resive el formulario cuidadosamente para ver si hay errores!';
$_['error_permission']      = 'Advertencia: ¡No tiene permiso para modificar Vendedores!';
$_['error_exists']          = 'Advertencia: ¡La direcció de E-Mail ya esta registrada!';
$_['error_firstname']       = '¡El nombre debe tener entre 1 y 32 caractéres!';
$_['error_username']        = '¡El nombre de usuario debe tener entre 1 y 32 caractéres!';
$_['error_lastname']        = '¡El apellido debe tener entre 1 y 32 caractéres!';
$_['error_email']           = '¡La direcció de E-Mail es invalida!';
$_['error_telephone']       = '¡El teléfono debe tener entre 3 y 32 caractéres!';
$_['error_password']        = '¡La contraseña debe tener entre 4 Y 20 caractéres!';
$_['error_confirm']         = '¡La contraseña y la contraseña de confirmación no coinciden!';
$_['error_company_id']      = '¡ID de compañ&íacute;a; requerido!';
$_['error_tax_id']          = '¡ID de Impuesto requerido!';
$_['error_vat']             = '¡Número de VAT invalido!';

$_['error_commission']             = 'Requiere Comissión!';


$_['error_address_1']       = '¡Dirección 1debe tener entre 3 y 128 caractéres!';
$_['error_city']            = '¡Ciudad debe tener entre 2 and 128 caractéres!';
$_['error_postcode']        = '¡Código postal debe tener entre 2 y 10 caractéres para este País!';
$_['error_country']         = '¡Por favor seleccione un País!';
$_['error_zone']            = '¡Por favor seleccione una provincia!';

$_['entry_paypalemail']     = 'Paypal E-Mail:';
$_['entry_username']     = 'Nombre de Usuario:';
$_['entry_commission']     = 'Comisión:';
// Error
$_['error_postcode2']    = 'Código postal debe tener entre 2 y 10 caractéres para este País!';
$_['error_city2']        = 'Ciudad debe tener entre 2 and 128 caractéres!';
$_['error_country2']     = '¡Por favor seleccione un País!';
$_['error_zone2']        = '¡Por favor seleccione una provincia!';
$_['error_paypalemail']    = 'Paypal E-Mail Invalido!';
$_['error_address_2']      = '¡Dirección 1debe tener entre 3 y 128 caractéres!';

$_['error_bankname']      = '¡Nombre del banco debe tener entre 1 y 32 caractéres!';
$_['error_accountnumber']      = '¡Nú debe tener entre 3 y 32 caractéres!';
$_['error_accountname']      = '¡Nombre de cuenta debe tener entre 3 y 32 caractéres!';
$_['error_branch']      = 'Branch name debe tener entre 3 and 132 caractéres!';
$_['error_ifsccode']      = '¡Código IFSC debe tener entre 3 y 32 caractéres!';
$_['error_cheque']           = '¡El beneficiario del cheque debe tener entre 2 y 128 caractéres!!';

$_['error_already_exists']           = '¡La dirección de mail ya se encuentra registrada!';
$_['error_autodele']                 = '¡No puede eliminar su propia cuenta!';


// ZMULTISELLER
$_['text_pending_balance']                 = 'Balance Pendiente';
$_['text_view_transaction']                = 'Ver Transacciones';
$_['text_payment_status']                  = 'Estado de Pagos';
$_['text_click_to_view']                   = 'Click aquí para ver';
$_['text_paid']                            = 'Pago';
$_['text_unpaid']                          = 'Impago';
$_['text_approved_payment']                = 'Aprobar el Pago';
$_['text_txn_id']                          = 'Txn Id';
$_['payment_history']                      = 'Historial de Pago';
$_['customers_groups']                      = 'Grupos de Clientes';
$_['text_is_it_assigned_to_the_seller']    = 'Asignado al vendedor';

?>
