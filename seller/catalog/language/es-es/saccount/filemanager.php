<?php
// Heading
$_['heading_title']    = 'Administrar Imagen';

// Texto
$_['text_uploaded']    = 'Éxito: ¡Su archivo ha sido subido!';
$_['text_directory']   = 'Éxito: ¡Directorio Creado!';
$_['text_delete']      = 'Éxito: ¡Su archivo o directorio ha sido eliminado!';

// Entry
$_['entry_search']     = 'Buscar..';
$_['entry_folder']     = 'Nombre de Carpeta';
$_['button_view_images']            = 'Ver sus Imágenes';

// Error
$_['error_permission'] = 'Advertencia: ¡Permiso Denegado!';
$_['error_filename']   = 'Advertencia: ¡El Nombre de Archivo debe contener entre 3 y 255 caracteres!';
$_['error_folder']     = 'Advertencia: ¡El Nombre de Carpeta debe contener entre 3 y 255 caracteres!';
$_['error_exists']     = 'Advertencia: ¡Archivo o Directorio con el mismo nombre ya existe!';
$_['error_directory']  = 'Advertencia: ¡El Directorio no existe!';
$_['error_filetype']   = 'Advertencia: ¡Tipo de archivo incorrecto!';
$_['error_upload']     = 'Advertencia: ¡El archivo no se pudo cargar. Razón desconocida.!';
$_['error_delete']     = 'Advertencia: ¡No se puede borrar éste directorio!';
