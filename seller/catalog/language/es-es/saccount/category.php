<?php
// Heading
$_['heading_title']          = 'Sus Categorías';
$_['heading_title1']     = 'Agregar Categoría';

// Texto
$_['text_success']           = 'éxito: Has modificado las Categorías!';
$_['text_default']           = 'Defecto';
$_['text_image_manager']     = 'Administrar imágenes';
$_['text_browse']            = 'Buscar Archivos';
$_['text_clear']             = 'Borrar imagen';

// Column
$_['column_name']            = 'Nombre de Categoría';
$_['column_sort_order']      = 'Ordenar Pedidos';
$_['column_action']          = 'Acción';

// Entry
$_['entry_name']             = 'Nombre de Categoría';
$_['entry_description']      = 'Descripción';
$_['entry_meta_title'] 	     = 'Meta Etiqueta Título';
$_['entry_meta_keyword'] 	 = 'Meta Etiqueta Palabras clave';
$_['entry_meta_description'] = 'Meta Etiqueta Descripción';
$_['entry_keyword']          = 'Palabras clave SEO';
$_['entry_parent']           = 'Padre';
$_['entry_filter']           = 'Filtros';
$_['entry_store']            = 'Tiendas';
$_['entry_image']            = 'Imagen';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'Columnas';
$_['entry_sort_order']       = 'Ordenar por Pedido';
$_['entry_status']           = 'Estado';
$_['entry_layout']           = 'Anular el diseño';

// Error
$_['error_warning']          = 'Advertencia: ¡Por favor controle el formulario en busca de errores!';
$_['error_permission']       = 'Advertencia: ¡No tiene permisos para modificar Categorías!';
$_['error_name']             = '¡El nombre de la Categoría debe tener entre 2 y 32 caracteres!';
$_['error_meta_title']       = 'Meta Título debe tener entre 3 y 255 caracteres!';


// Button
$_['button_insert']           = 'Insertar';
$_['button_delete']           = 'Borrar';
$_['button_save']             = 'Guardar';
$_['button_cancel']           = 'Cancelar';
$_['button_clear']            = 'Borrar registro';
$_['button_close']            = 'Cerrar';
$_['button_filter']           = 'Filtro';
$_['button_send']             = 'Enviar';
$_['button_edit']             = 'Editar';
$_['button_copy']             = 'Copiar';
$_['button_back']             = 'Volver';
$_['button_remove']           = 'Remover';
$_['button_backup']           = 'Respaldo';
$_['button_restore']          = 'Restaurar';
$_['button_upload']           = 'Subir';
$_['button_view_images']            = 'Ver sus Imágenes';
$_['button_submit']           = 'Enviar';
$_['button_invoice']          = 'Imprimir Factura';
$_['button_add_address']      = 'Agregar Dirección';
$_['button_add_attribute']    = 'Agregar Atributo';
$_['button_add_banner']       = 'Agregar Banner';
$_['button_add_product']      = 'Agregar Producto';
$_['button_add_voucher']      = 'Agregar Cupón';
$_['button_add_option']       = 'Agregar Opción';
$_['button_add_option_value'] = 'Agregar Opción Value';
$_['button_add_discount']     = 'Agregar Descuento';
$_['button_add_special']      = 'Agregar Especiales';
$_['button_add_image']        = 'Agregar Imagen';
$_['button_add_geo_zone']     = 'Agregar Geo Zone';
$_['button_add_history']      = 'Agregar Historial';
$_['button_add_transaction']  = 'Agregar Transacción';
$_['button_add_total']        = 'Agregar Total';
$_['button_add_reward']       = 'Agregar Puntos de Recompensa';
$_['button_add_route']        = 'Agregar Ruta';
$_['button_add_rule' ]        = 'Agregar Regla';
$_['button_add_module']       = 'Agregar Modulo';
$_['button_add_link']         = 'Agregar Enlace';
$_['button_update_total']     = 'Update Totales';
$_['button_approve']          = 'Aprobar';
$_['button_reset']            = 'Reajustar';

// Texto
$_['text_yes']                = 'Si';
$_['text_no']                 = 'No';
$_['text_enabled']            = 'Habilitado';
$_['text_disabled']           = 'Deshabilitado';
$_['text_none']               = ' --- Ninguno --- ';
$_['text_select']             = ' --- Por favor selecciones --- ';
$_['text_select_all']         = 'Seleccionar todo';
$_['text_unselect_all']       = 'Deseleccionar todo';
$_['text_all_zones']          = 'Todas las zonas';
$_['text_default']            = ' <b>(Defecto)</b>';
$_['text_close']              = 'Cerrado';

$_['text_no_results']         = '¡Sin resultados!';
$_['text_separator']          = ' > ';
$_['text_edit']               = 'Editar';
$_['text_view']               = 'Ver';
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_account']               = 'Cuenta';

// Tab
$_['tab_address']             = 'Dirección';
$_['tab_admin']               = 'Administrador';
$_['tab_attribute']           = 'Atributos';
$_['tab_coupon_history']      = 'Historial de cupones';
$_['tab_customer']            = 'Detalles de cliente';
$_['tab_data']                = 'Información';
$_['tab_design']              = 'Diseño';
$_['tab_discount']            = 'Descuento';
$_['tab_general']             = 'General';
$_['tab_fraud']               = 'Fraude';
$_['tab_ip']                  = 'Direcciones IP';
$_['tab_links']               = 'Enlaces';
$_['tab_image']               = 'Imágnes';
$_['tab_option']              = 'Opciones';
$_['tab_server']              = 'Servidor';
$_['tab_store']               = 'Tienda';
$_['tab_special']             = 'Especiales';
$_['tab_local']               = 'Localización';
$_['tab_mail']                = 'Mail';
$_['tab_module']              = 'Modulo';
$_['tab_order']               = 'Detalles de pedidos';
$_['tab_order_history']       = 'Historial de ordenes';
$_['tab_payment']             = 'Detalles de Pago';
$_['tab_product']             = 'Productos';
$_['tab_return']              = 'Detalles de devoluciones';
$_['tab_return_history']      = 'Historial de devoluciones';
$_['tab_reward']              = 'Puntos de recompensa';
$_['tab_shipping']            = 'Detalles de envío';
$_['tab_total']               = 'Totales';
$_['tab_transaction']         = 'Transacciones';
$_['tab_voucher']             = 'Cupones';
$_['tab_voucher_history']     = 'Historial de cupones';


/**code agregados here**/
// Button
$_['button_insert']           = 'Insertar';
$_['button_delete']           = 'Borrar';
$_['button_filter']           = 'Filtrar';
$_['button_save']             = 'Guardar';
$_['button_edit']             = 'Editar';
$_['button_copy']             = 'Copiar';
$_['button_back']             = 'Volver';
$_['button_cancel']           = 'Cancelar';
$_['button_remove']           = 'Remover';
$_['button_backup']           = 'Respaldo';
$_['button_upload']           = 'Subir';
$_['button_submit']           = 'Enviar';
$_['button_add_discount']     = 'Agregar descuento';
$_['button_add_special']      = 'Agregar Especiales';
$_['button_add_image']        = 'Agregar Imagen';
$_['button_reset']            = 'Reajustar';
$_['button_add_option_value']            = 'Agregar opción de valor';
$_['text_edit']         = 'Editar';
$_['text_no_results']         = 'No se encontraron resultados';
$_['text_success']      = 'Gracias por publicar su categoría. La misma fue enviada para su aprobación con un administrador';

$_['text_modify']      = '¡Se ha modificado la categoría!';

// Help
$_['help_filter']            = '(Autocompletar)';
$_['help_keyword']           = 'No utilice espacios. Reemplaze los espacios por "-" y asegúrese de que la palabra clave sea única de manera globlal.';
$_['help_top']               = 'Mostrar en la barra superior. Sólo funciona para los rubros principales.';
$_['help_column']            = 'Número de columnas que se utilizará para los 3 rubros inferiores. Solo funciona para los rubros principales. ';

//mail
$_['text_subject']  = '%s - Una nueva categoría, aguarda a ser aprobada.';
$_['text_welcome']  = '¡Nueva categoría publicada por %s!';
$_['text_login']    = 'Su cuenta ha sido creada, ahora puede iniciar sesión con su dirección de email y contraseña, visitando nuestro sitio web o siguiendo el enlace a continuación URL:';
$_['text_approval'] = 'Una nueva categoría, aguarda a ser aprobada. Inicie sesión como administrador para poder aprobarla.';
$_['text_services'] = 'Al iniciar sesión, podrá acceder a otros servicios, como la revisión de pedidos, la impresión de facturas y la edición de la información de su cuenta.';
$_['text_thanks']   = 'Gracias,';


/*end*/

?>
