<?php
// Heading
$_['heading_title']          = 'Subir Imágenes';
$_['heading_title1']         = 'Agregar Imágenes';
$_['text_account']       = 'Cuenta';
$_['text_account_already']       = 'Cuenta';
$_['text_edit']               = 'Agregar Imágenes';
$_['text_no_results']         = 'No se encontraron resultados';
$_['text_upload']    = 'Subir';
$_['text_view_images'] = 'Ver sus imágenes';


// Column
$_['column_parent']          = 'Carpeta Padre';
$_['column_foldername']      = 'Nombre de Carpeta';
$_['column_action']          = 'Acción';

// Entry
$_['entry_parentfolder']   = 'Carpeta Padre:';
$_['entry_foldername'] 	   = 'Nombre de Carpeta:';

// Error
$_['error_warning']          = 'Advertencia: ¡Por favor controle cuidadosamente el formulario en busca de errores!';
$_['error_permission']       = 'Advertencia: ¡No tiene permisos para modificar categor&iacuete;as!';
$_['error_name']             = '¡El Nombre de Carpeta debe tener entre 2 y 32 caracteres!';


// Button
$_['button_insert']           = 'Insertar';
$_['button_delete']           = 'Borrar';
$_['button_save']             = 'Guardar';
$_['button_cancel']           = 'Cancelar';
$_['button_submit']           = 'Enviar';
$_['btn_view_images']         = 'Ver sus Imágenes';
$_['btn_upload']              = 'Subir';


// Tab
$_['tab_general']             = 'General';


$_['text_success']      = 'Exitosamente Creado';

$_['text_modify']      = '¡Ha modificado exitosamente la carpeta!';
$_['text_deleted']      = '¡Ha eliminado exitosamente la carpeta de imagenes!';
/*end*/

?>
