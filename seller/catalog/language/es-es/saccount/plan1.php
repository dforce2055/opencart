<?php
// Heading
$_['heading_title']     = 'Nuestros planes';

// Texto
$_['text_plan1']      = 'Planes';
$_['text_account']      = 'Cuenta';
$_['text_edit']         = 'Editar información';
$_['text_your_details'] = 'Sus Detalles Personales';
$_['text_your_imagegallery'] = 'Galería de imágenes';
$_['text_success']      = 'Éxito: Su cuenta fue actualizada con éxito.';

$_['column_plan']  = 'Nombre de plan';
$_['column_duration']  = 'Duración';
$_['column_charges']  = 'Cargos';
$_['column_upgrade']  = 'Mejorar';
$_['column_about']  = 'Límite de productos';

// Entry
$_['entry_firstname']  = 'Nombre:';
$_['entry_lastname']   = 'Apellido:';
$_['entry_email']      = 'E-Mail:';
$_['entry_telephone']  = 'Teléfono:';
$_['entry_fax']        = 'Fax:';
$_['entry_video_url']        = 'URL de video:';

// Error
$_['error_exists']     = '¡Advertencia: La dirección de E-Mail ya está registrada!';
$_['error_firstname']      = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']       = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']      = '¡La dirección de E-Mail parece inválida!';
$_['error_telephone']  = '¡El teléfono debe tener entre 3 y 32 caracteres!';


$_['text_upload']       = '¡El archivo fue subido correctamente!';
$_['text_wait']         = '¡Por favor espere!';
$_['text_tags']         = 'Etiquetas';
$_['text_error']        = '¡Producto no encontrado!';
$_['text_no_results']   = '¡No se encontraron productos!';
$_['text_browse']         = 'Buscar';
$_['text_clear']         = 'Limpiar';
$_['text_enabled']         = 'Si';
$_['text_disabled']         = 'No';
$_['text_edit']         = 'Editar';
$_['text_free']         = 'Gratis';

$_['entry_image']		= 'Imagen:<br/>Nota: Máximo 200kb';

$_['entry_image1']    = 'Imagen';
$_['entry_image2']    = 'Nombre de Imagen';
$_['button_add_image']    = 'Agregar Imagen';
$_['btn_current_plan']    = 'Plan Actual';
$_['btn_upgrade']         = 'Mejorar';

$_['text_for'] 		   = 'Grupo de Clientes-%s';


?>
