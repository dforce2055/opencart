<?php
// Heading 
$_['heading_title']      = 'Sus Puntos de Recompensa';

// Column
$_['column_date_added']  = 'Fecha añadido';
$_['column_description'] = 'Descripción';
$_['column_points']      = 'Puntos';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_reward']        = 'Puntos de Recompensa';
$_['text_total']         = 'Su total de puntos de recompensa es:';
$_['text_empty']         = '¡No tiene puntos de recompensa!';
?>