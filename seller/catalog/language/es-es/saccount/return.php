<?php
// Heading 
$_['heading_title']      = 'Devoluciones de Productos';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_return']        = 'Información de devolución';
$_['text_return_detail'] = 'Detalles de devolución';
$_['text_description']   = '<p>Por favor complete el formulario siguiente para solicitar un número de RMA. </p>';
$_['text_order']         = 'Información de pedido';
$_['text_product']       = 'Información de producto &amp; Razón de devolución';
$_['text_message']       = '<p>Gracias por enviar su pedido de devolución. Su pedido ha sido enviado al departamento correspondiente para su procesamiento. </p><p> El estado de su pedido le será notificado vía e-mail.</p>';
$_['text_return_id']     = 'ID de devolución:';
$_['text_order_id']      = 'ID de pedido:';
$_['text_date_ordered']  = 'Fecha de pedido:';
$_['text_status']        = 'Estado:';
$_['text_date_added']    = 'Fecha añadido:';
$_['text_customer']      = 'Cliente:';
$_['text_comment']       = 'Comentarios de devolución';
$_['text_history']       = 'Historial de devoluciones';
$_['text_empty']         = '¡No has hecho devoluciones!';
$_['text_error']         = '¡No se pudieron encontrar las devoluciones solicitadas!';

// Column
$_['column_product']     = 'Nombre de producto';
$_['column_model']       = 'Modelo';
$_['column_quantity']    = 'Cantidad';
$_['column_price']       = 'Precio';
$_['column_opened']      = 'Abierto';
$_['column_comment']     = 'Comentario';
$_['column_reason']      = 'Razón';
$_['column_action']      = 'Acción';
$_['column_date_added']  = 'Fecha añadido';
$_['column_status']      = 'Estado';

// Entry
$_['entry_order_id']     = 'ID de pedido:';
$_['entry_date_ordered'] = 'Fecha de pedido:';
$_['entry_firstname']    = 'Nombre:';
$_['entry_lastname']     = 'Apellido:';
$_['entry_email']        = 'E-Mail:';
$_['entry_telephone']    = 'Teléfono:';
$_['entry_product']      = 'Nombre de producto:';
$_['entry_model']        = 'Código de producto:';
$_['entry_quantity']     = 'Cantidad:';
$_['entry_reason']       = 'Razón de devolución:';
$_['entry_opened']       = 'El producto está abierto:';
$_['entry_fault_detail'] = 'Defecto u otros detalles:';
$_['entry_captcha']      = 'Ingrese el código en el recuadro de abajo:';

// Error
$_['error_order_id']     = '¡ID de pedido necesario!';
$_['error_firstname']    = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']     = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']        = '¡La dirección de E-Mail parece inválida!';
$_['error_telephone']    = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_product']      = '¡El nombre de producto debe tener entre 3 y 255 caracteres!';
$_['error_model']        = '¡El modelo de producto debe tener entre 3 y 64 caracteres!';
$_['error_reason']       = '¡Debe seleccionar una razón de devolución!';
$_['error_captcha']      = '¡El código de verificación no coincide con la imagen!';
?>