<?php
// Heading
$_['heading_title']                = 'Inicio de Sesión de Vendedor';

// Texto
$_['text_account']                 = 'Cuenta';
$_['text_login']                   = 'Iniciar Sesión';
$_['text_new_seller']            = 'Nuevo Vendedor';
$_['text_register']                = 'Registrar Cuenta';
$_['text_register_account']        = 'Al crear una cuenta se podrán realizar compras, revisar los pedidos, el estado, o realizar un seguimiento de las operaciones anteriores.';
$_['text_returning_seller']      = 'Vendedor Recurrente';
$_['text_i_am_returning_seller'] = 'Soy un Vendedor Recurrente';
$_['text_register_two']               = 'Registarse';
$_['text_forgotten']               = 'Contraseña Olvidada';
$_['text_forgotten_two']               = '¿Olvido su Contraseña?';
$_['text_back_to_store']               = 'Volver a la Tienda';

// Entry
$_['entry_email']                  = 'Dirección de E-Mail:';
$_['entry_password']               = 'Contraseña:';

// Error
$_['error_login']                  = 'Advertencia: No coincide la dirección de E-mail y / o la contraseña.';
$_['error_approved']               = 'Advertencia: Su cuenta requiere aprobación primero, antes de iniciar sesión.';

// ZMULTISELLER
$_['text_register']               = 'Registrarse';
$_['text_lost_password']          = '¿Olvido su contraseña?';
$_['text_back_to_the_store']      = 'Volver a la Tienda';
?>
