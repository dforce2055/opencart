<?php
// Heading
$_['heading_title']  = 'Contáctenos';
$_['text_account']       = 'Cuenta';
$_['text_account_already']       = 'Cuenta';

// Text
$_['text_location']  = 'Nuestra ubicación';
$_['text_contact']   = 'Formulario de Contacto';
$_['text_address']   = 'Dirección:';
$_['text_email']     = 'E-Mail:';
$_['text_telephone'] = 'Teléfono:';
$_['text_fax']       = 'Fax:';
$_['text_message']   = '<p>¡Su consulta se ha enviado correctamente al propietario de la tienda!</p>';

$_['text_message1']   = 'Mensaje:';

$_['text_thanks']   = 'Gracias';

// Entry Fields
$_['entry_name']     = 'Nombre';
$_['entry_product_name']     = 'Nombre de Producto';
$_['entry_email']    = 'Dirección de E-Mail:';
$_['entry_enquiry']  = 'Consulta:';
$_['entry_captcha']  = 'Ingrese el código en el recuadro de abajo:';

$_['entry_fname']     = 'Nombre';

// Email
$_['email_subject']  = 'Consultas por Producto : %s';

$_['text_error']        = '¡Vendedor no encontrado!';

// Errors
$_['error_name']     = '¡El nombre debe tener entre 3 y 32 caracteres!';
$_['error_email']    = '¡Dirección de E-Mail invalida!';
$_['error_enquiry']  = '¡La consulta debe tener entre 10 y 3000 caracteres!';
$_['error_captcha']  = '¡El código de verificación no coincide con la imagen!';
?>
