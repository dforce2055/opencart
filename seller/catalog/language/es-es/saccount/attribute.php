<?php
// Heading
$_['heading_title']          = 'Atributos';

$_['text_list']          = 'Atributos';

$_['text_account']          = 'Cuenta';

//Button
$_['button_insert']          = 'Insertar';
$_['button_delete']          = 'Eliminar';
$_['button_Save']            = 'Guardar';

// Texto
$_['text_success']           = 'éxito: ¡Ha modificado los atributos!';

$_['text_no_results']           = '¡No se encontraron resultados!';

$_['text_edit']              = 'Editar';

// Column
$_['column_name']            = 'Nombre de Atributo';
$_['column_attribute_group'] = 'Groupo de Atributo';
$_['column_sort_order']      = 'Ordenar pedidos';
$_['column_action']          = 'Acción';

// Entry
$_['entry_name']            = 'Nombre: Atributo';
$_['entry_attribute_group'] = 'Groupo de Atributo:';
$_['entry_sort_order']      = 'Ordenar pedidos:';

// Button
$_['button_add']           = 'Agregar';
$_['button_delete']           = 'Borrar';
$_['button_save']             = 'Guardar';
$_['button_cancel']           = 'Cancelar';
$_['button_clear']            = 'Borrar registro';
$_['button_close']            = 'Cerrar';
$_['button_filter']           = 'Filtro';
$_['button_send']             = 'Enviar';
$_['button_edit']             = 'Editar';
$_['button_copy']             = 'Copiar';
$_['button_back']             = 'Volver';
$_['button_remove']           = 'Remover';
$_['button_backup']           = 'Respaldo';
$_['button_restore']          = 'Restaurar';
$_['button_upload']           = 'Subir';
$_['button_view_images']            = 'Ver sus Imágenes';
$_['button_submit']           = 'Enviar';
$_['button_invoice']          = 'Imprimir Factura';
$_['button_add_address']      = 'Agregar Dirección';
$_['button_add_attribute']    = 'Agregar Atributo';
$_['button_add_banner']       = 'Agregar Banner';
$_['button_add_product']      = 'Agregar Producto';
$_['button_add_voucher']      = 'Agregar Cupón';
$_['button_add_option']       = 'Agregar Opción';
$_['button_add_option_value'] = 'Agregar Opción de Valor';
$_['button_add_discount']     = 'Agregar Descuento';
$_['button_add_special']      = 'Agregar Especiales';
$_['button_add_image']        = 'Agregar Imagen';
$_['button_add_geo_zone']     = 'Agregar Geo Zone';
$_['button_add_history']      = 'Agregar Historial';
$_['button_add_transaction']  = 'Agregar Transacción';
$_['button_add_total']        = 'Agregar Total';
$_['button_add_reward']       = 'Agregar Puntos de Recompensa';
$_['button_add_route']        = 'Agregar Ruta';
$_['button_add_rule' ]        = 'Agregar Regla';
$_['button_add_module']       = 'Agregar Modulo';
$_['button_add_link']         = 'Agregar Enlace';
$_['button_update_total']     = 'Update Totales';
$_['button_approve']          = 'Aprobar';
$_['button_reset']            = 'Reajustar';

// Error
$_['error_permission']      = 'Advertencia: ¡No tiene permisos para modificar Atributos!';
$_['error_name']            = '¡El atributo debe tener entre 3 y 64 caracteres!';
$_['error_product']         = 'Advertencia: ¡Este atributo no se puede eliminar, esta actualmente asignado a %s productos!';
?>
