<?php
// Heading 
$_['heading_title']     = 'Información de mi Cuenta';

// Texto
$_['text_account']      = 'Cuenta';
$_['text_edit']         = 'Editar Información';
$_['text_your_details'] = 'Sus detalles personales';
$_['text_success']      = 'Éxito: ¡la información de su cuenta ha sido actualizada!.';

// Entry
$_['entry_firstname']  = 'Nombre:';
$_['entry_lastname']   = 'Apellido:';
$_['entry_email']      = 'E-Mail:';
$_['entry_telephone']  = 'Teléfono:';
$_['entry_fax']        = 'Fax:';

$_['text_desc']   = 'Descripción';
$_['entry_aboutus']       = 'Acerca de Nosotros:';

// Error
$_['error_exists']     = 'Advertencia: ¡La dirección de E-Mail ya se encuentra registrada!';
$_['error_firstname']  = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']   = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']      = '¡La dirección de E-Mail parece invalida!';
$_['error_telephone']  = '¡El teléfono debe tener entre 3 y 32 caracteres!';


$_['text_upload']       = '¡El archivo fue subido correctamente!';
$_['text_wait']         = '¡Por favor espere!';
$_['text_tags']         = 'Etiquetas';
$_['text_error']        = '¡Producto no encontrado!';
$_['text_no_results']   = '¡No se encontraron productos!';
$_['text_browse']         = 'Buscar';
$_['text_clear']         = 'Limpiar';
$_['text_enabled']         = 'Si';
$_['text_disabled']         = 'No';
$_['text_edit']         = 'Editar';

$_['entry_image']		= 'Imagen:(Click en el botón para cambiar)';

$_['entry_image1']    = 'Imagen';
$_['entry_image2']    = 'Nombre de Imagen';
$_['button_add_image']    = 'Agregar Imagen';
?>