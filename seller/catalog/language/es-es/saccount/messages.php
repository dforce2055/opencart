<?php
// Heading 
$_['heading_title']         = 'Buzón de Mensajes';

// Texto
$_['text_account']          = 'Cuenta';
$_['text_order']            = 'Pedido de Información';
$_['text_order_detail']     = 'Detalles de Pedido';
$_['text_invoice_no']       = 'Nro. de factura:';
$_['text_order_id']         = 'ID de Pedido:';
$_['text_status']           = 'Estado:';
$_['text_date_added']       = 'Fecha añadido:';
$_['text_customer']         = 'Cliente:';
$_['text_shipping_address'] = 'Dirección de Envío';
$_['text_shipping_method']  = 'Método de Envio:';
$_['text_payment_address']  = 'Dirección de Pago';
$_['text_payment_method']   = 'Método de Pago:';
$_['text_enquiry']         = 'investigación:';
$_['text_total']            = 'Total:';
$_['text_comment']          = 'Comentarios del Pedido';
$_['text_history']          = 'Historial del Pedido';
$_['text_success']          = '¡Ha agregado exitosamente los productos del pedido ID #%s a su carro de compras!';
$_['text_empty']            = '¡No ha hecho ningún pedido anteriormente!';
$_['text_error']            = '¡No se pudo encontrar el pedido solicitado!';
$_['button_reply']           = 'Responder';
// Column
$_['column_name']           = 'Nombre de Producto';
$_['column_model']          = 'Modelo';
$_['column_quantity']       = 'Cantidad';
$_['column_price']          = 'Precio';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';
$_['column_date_added']     = 'Fecha añadido';
$_['column_status']         = 'Estado';
$_['column_comment']        = 'Comentario';
?>
