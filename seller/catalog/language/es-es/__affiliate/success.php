<?php
// Heading
$_['heading_title'] = 'Tu cuenta de afiliado ha sido creada.';

// Texto
$_['text_message']  = '<p>¡Felicitaciones. La cuenta ha sido creada exitosamente.</p> <p>Ante alguna duda acerca del funcionamiento de esta Web, por favor enviar un Email de Contacto.</p> <p>Una confirmación ha sido enviada al Email proporcionado. Si no es recibido en una hora <a href="%s">contactarse a través del formulario de contacto</a>.</p>';
$_['text_approval'] = '<p>Gracias por registrarse como afiliado.</p><p>Se notificará por Email una vez que su cuenta ha sido activada.</p><p>Si existen dudas acerca del funcionamiento de este sistema de afiliados <a href="%s">contactarse</a>.</p>';
$_['text_account']  = 'Cuenta';
$_['text_success']  = 'Éxito';
