<?php
// Heading
$_['heading_title']             = 'Programa de Afiliados';

// Texto
$_['text_account']              = 'Cuenta';
$_['text_register']             = 'Registro de Afiliados';
$_['text_account_already']      = 'Si ya se ha creado la cuenta ir a <a href="%s">Iniciar Sesión</a>.';
$_['text_signup']               = 'Para crear una cuenta de afiliado, llenar el formulario.';
$_['text_your_details']         = 'Datos Personales';
$_['text_your_address']         = 'Dirección';
$_['text_payment']              = 'Información de Pago';
$_['text_your_password']        = 'Contraseña';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Transferencia bancaria';
$_['text_agree']                = 'He leído y estoy de acuerdo con el <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_firstname']           = 'Nombre';
$_['entry_lastname']            = 'Apellido';
$_['entry_email']               = 'Email';
$_['entry_telephone']           = 'Teléfono';
$_['entry_fax']                 = 'Fax';
$_['entry_company']             = 'Empresa';
$_['entry_website']             = 'Web';
$_['entry_address_1']           = 'Dirección 1';
$_['entry_address_2']           = 'Dirección 2';
$_['entry_postcode']            = 'Código Postal';
$_['entry_city']                = 'Ciudad';
$_['entry_country']             = 'País';
$_['entry_zone']                = 'Provincia/Estado';
$_['entry_tax']                 = 'ID Impuesto';
$_['entry_payment']             = 'Método de Pago';
$_['entry_cheque']              = 'Nombre del beneficiario de cheque';
$_['entry_paypal']              = 'Email de PayPal';
$_['entry_bank_name']           = 'Nombre del Banco';
$_['entry_bank_branch_number']  = 'Número ABA/BSB (Número de sucursal)';
$_['entry_bank_swift_code']     = 'Código SWIFT';
$_['entry_bank_account_name']   = 'Nombre de la Cuenta';
$_['entry_bank_account_number'] = 'Número de Cuenta';
$_['entry_password']            = 'Contraseña';
$_['entry_confirm']             = 'Confirmar Contraseña';

// Error
$_['error_exists']              = 'Advertencia: Email ya registrado.';
$_['error_firstname']           = 'El nombre debe tener entre 1 y 32 caracteres.';
$_['error_lastname']            = 'El Apellido debe estar entre 1 y 32 caracteres.';
$_['error_email']               = 'Email Inválido.';
$_['error_telephone']           = 'Teléfono debe tener entre 3 y 32 caracteres.';
$_['error_password']            = 'La contraseña debe tener entre 4 y 20 caracteres.';
$_['error_confirm']             = 'Confirmación de la contraseña no coincide con la contraseña.';
$_['error_address_1']           = 'Dirección 1 debe estar entre 3 y 128 caracteres.';
$_['error_city']                = 'Ciudad debe tener entre 2 y 128 caracteres.';
$_['error_country']             = 'Seleccionar País.';
$_['error_zone']                = 'Seleccionar Provincia/Estado.';
$_['error_postcode']            = 'Código Postal debe tener entre 2 y 10 caracteres.';
$_['error_agree']               = 'Advertencia: Usted debe estar de acuerdo con la %s.';