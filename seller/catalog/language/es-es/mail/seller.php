<?php
// Texto
$_['text_subject']  = '%s - Gracias por registrarse';

$_['text_approvalsubject']  = '%s Su solicitud aguarda a ser aprobada.';

$_['text_welcome']  = '¡Bienvenido y gracias por registrarse como %s !';
$_['text_login']    = 'Su cuenta ha sido creada, puede iniciar sesión usando su dirección de E-mail y contraseña visitando nuestro sitio web o desde la siguiente URL:';
$_['text_approval'] = 'La Cuenta debe ser aprobada antes de que pueda iniciar sesión. Una vez aprobada se puede iniciar sesión utilizando el E-mail y Contraseña registrados, desde la siguiente URL:';
$_['text_services'] = 'Al iniciar sesión, podrá acceder a otros servicios, como vender productos, revisión de pedidos, administrar clientes y la edición de la información de su cuenta';
$_['text_thanks']   = 'Gracias,';


// To Admin
$_['new_seller_subject']     = '¡Un nuevo vendedor se ha registrado!';
$_['peding_authorization']   = '¡El vendedor se encuentra pendiente de autorización!';
$_['seller_name']            = 'Nombre';
$_['seller_username']        = 'Nombre de usuario';
$_['seller_email']           = 'E-Mail';
$_['seller_telephone']       = 'Teléfono';
?>
