<?php
// Texto
$_['text_subject']      = '%s - Pedido %s';
$_['text_received']     = 'Has recibido un pedido.';
$_['text_order_id']     = 'ID de pedido:';
$_['text_date_added']   = 'Fecha añadido:';
$_['text_order_status'] = 'Estado del Pedido:';
$_['text_product']      = 'Productos';
$_['text_total']        = 'Totales';
$_['text_comment']      = 'Los comentarios para tu pedido son:';
