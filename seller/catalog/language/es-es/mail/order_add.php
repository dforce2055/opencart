<?php
// Texto
$_['text_subject']          = '%s - Pedido %s';
$_['text_greeting']         = 'Gracias por el interés en %s productos. Se ha recibido el pedido, el mismo sera preparado una vez que se procesado el pago.';
$_['text_link']             = 'Para ver su pedido, haga click en el siguiente enlace:';
$_['text_order_detail']     = 'Detalles de Pedido';
$_['text_instruction']      = 'Instrucciones';
$_['text_order_id']         = 'ID de pedido:';
$_['text_date_added']       = 'Fecha añadido:';
$_['text_order_status']     = 'Estado del Pedido:';
$_['text_payment_method']   = 'Método de Pago:';
$_['text_shipping_method']  = 'Método de Envio:';
$_['text_email']            = 'E-mail:';
$_['text_telephone']        = 'Teléfono:';
$_['text_ip']               = 'Dirección IP:';
$_['text_payment_address']  = 'Dirección de Pago';
$_['text_shipping_address'] = 'Dirección Envio';
$_['text_products']         = 'Productos';
$_['text_product']          = 'Producto';
$_['text_model']            = 'Modelo';
$_['text_quantity']         = 'Cantidad';
$_['text_price']            = 'Precio';
$_['text_order_total']      = 'Pedido Totales';
$_['text_total']            = 'Total';
$_['text_download']         = 'Una vez que su pago se haya confirmado, puede hacer click en el siguiente enlace para acceder a los descargables del producto:';
$_['text_comment']          = 'Los comentarios para su pedido son:';
$_['text_footer']           = 'Por favor, responda a este e-mail si tiene alguna duda.';