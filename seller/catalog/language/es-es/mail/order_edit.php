<?php
// Texto
$_['text_subject']      = '%s - Pedido Actualizado %s';
$_['text_order_id']     = 'ID de pedido:';
$_['text_date_added']   = 'Fecha añadido:';
$_['text_order_status'] = 'Su pedido ha sido actualizado al siguiente estado:';
$_['text_comment']      = 'Los comentarios para su pedido son:';
$_['text_link']         = 'Para ver su pedido, haga click en el siguiente enlace:';
$_['text_footer']       = 'Por favor, responda a este e-mail si tiene alguna pregunta.';