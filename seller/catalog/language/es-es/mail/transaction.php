<?php
// Texto
$_['text_subject']  = '%s - Comisión de Afiliado';
$_['text_received'] = '¡Felicitaciones! Va a recibir una comisión de pago por %s programa de afiliado';
$_['text_amount']   = 'Has recibido:';
$_['text_total']    = 'El total de su comisión es:';