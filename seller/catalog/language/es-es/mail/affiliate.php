<?php
// Texto
$_['text_subject']		        = '%s - Programa de Afiliados';
$_['text_welcome']		        = 'Gracias por unirse al %s Programa de Afiliados.';
$_['text_login']                = 'Su cuenta ha sido creada, puede iniciar sesión usando su dirección de E-mail y contraseña visitando nuestro sitio web o desde la siguiente URL:';
$_['text_approval']		        = 'La Cuenta debe ser aprobada antes de que pueda iniciar sesión. Una vez aprobada se puede iniciar sesión utilizando el E-mail y Contraseña registrados, desde la siguiente URL:';
$_['text_services']		        = 'Al iniciar sesión, podrá acceder a otros servicios, como la revisión de pedidos anteriores, la impresión de facturas y la edición de la información de su cuenta';
$_['text_thanks']		        = 'Gracias,';
$_['text_new_affiliate']        = 'Nuevo Afiliado';
$_['text_signup']		        = 'Un Nuevo Afiliado ha firmado:';
$_['text_store']		        = 'Comercio:';
$_['text_firstname']	        = 'Nombre:';
$_['text_lastname']		        = 'Apellido:';
$_['text_company']		        = 'Empresa:';
$_['text_email']		        = 'Email:';
$_['text_telephone']	        = 'Teléfono:';
$_['text_website']		        = 'Sitio Web:';
$_['text_order_id']             = 'ID de pedido:';
$_['text_transaction_subject']  = '%s - Comisión de Afiliado';
$_['text_transaction_received'] = 'Comisión recibida %s.';
$_['text_transaction_total']    = 'Total de Comisiones de %s.';
