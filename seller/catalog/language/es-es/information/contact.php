<?php
// Heading
$_['heading_title']  = 'Contactenos';

// Texto
$_['text_location']  = 'Nuestra dirección';
$_['text_store']     = 'Nuestras Tiendas';
$_['text_contact']   = 'Formulario de Contacto';
$_['text_address']   = 'Direcciones';
$_['text_telephone'] = 'Teléfono';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Horarios';
$_['text_comment']   = 'Comentarios';
$_['text_success']   = '<p>Su requerimiento fue enviado al propietario del la tienda!</p>';

// Entry
$_['entry_name']     = 'Su Nombre';
$_['entry_email']    = 'E-Mail';
$_['entry_enquiry']  = 'Pedido';

// Email
$_['email_subject']  = 'Pedido %s';

// Errors
$_['error_name']     = 'Nombre debe tener entre 3 y 32 caracteres!';
$_['error_email']    = 'E-Mail inválido!';
$_['error_enquiry']  = 'Pedido debe tener entre 10 y 3000 caracteres!';