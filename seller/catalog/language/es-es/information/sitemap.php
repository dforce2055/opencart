<?php
// Heading
$_['heading_title']    = 'Mapa del Sitio';

// Texto
$_['text_special']     = 'Ofertas';
$_['text_account']     = 'Mi Cuenta';
$_['text_edit']        = 'Información de la Cuenta';
$_['text_password']    = 'Clave';
$_['text_address']     = 'Agenda';
$_['text_history']     = 'Historial de �rdenes';
$_['text_download']    = 'Descargas';
$_['text_cart']        = 'Carrito de Compras';
$_['text_checkout']    = 'Finalizar Compra';
$_['text_search']      = 'Búsqueda';
$_['text_information'] = 'Información';
$_['text_contact']     = 'Contáctenos';