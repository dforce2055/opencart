<?php
// Texto
$_['text_success']     = 'La Moneda ha sido Actualizada.';

// Error
$_['error_permission'] = 'Sin permiso para acceder a la API.';
$_['error_currency']   = 'Código de Moneda Inválido.';