<?php
// Texto
$_['text_success']     = 'El Cupón de Regalo se ha aplicado exitosamente.';
$_['text_cart']        = 'Se ha modificado el Carro de Compras.';

$_['text_for']         = '%s Cupón de Regalo para %s';

// Error
$_['error_permission'] = 'Sin permiso para acceder a la API.';
$_['error_voucher']    = 'Cupón de Regalo Inválido o Saldo Agotado.';
$_['error_to_name']    = 'Nombre de Receptor es debe contener entre 1 y 64 caracteres.';
$_['error_from_name']  = 'El Nombre debe contener entre 1 y 64 caracteres.';
$_['error_email']      = 'Email Inválido.';
$_['error_theme']      = 'Seleccionar Razón.';
$_['error_amount']     = 'El Importe debe estar entre %s y %s.';