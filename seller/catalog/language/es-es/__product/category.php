<?php
// Texto
$_['text_refine']       = 'Refinar Búsqueda';
$_['text_product']      = 'Productos';
$_['text_error']        = 'Categoría no existente!';
$_['text_empty']        = 'No hay productos para listar en esta categoría.';
$_['text_quantity']     = 'Cant:';
$_['text_manufacturer'] = 'Fabricante:';
$_['text_model']        = 'Modelo:';
$_['text_points']       = 'Puntos Recompensa:';
$_['text_price']        = 'Precio:';
$_['text_tax']          = 'Sin Impuesto:';
$_['text_compare']      = 'Compare Producto (%s)';
$_['text_sort']         = 'Ordenar por:';
$_['text_defaul t']      = 'Defecto';
$_['text_name_asc']     = 'Nombre (A - Z)';
$_['text_name_desc']    = 'Nombre (Z - A)';
$_['text_price_asc']    = 'Precio (Menor > Mayor)';
$_['text_price_desc']   = 'Precio (Mayor > Menor)';
$_['text_rating_asc']   = 'Calificación (Asc.)';
$_['text_rating_desc']  = 'Calificación (Desc.)';
$_['text_model_asc']    = 'Modelo (A - Z)';
$_['text_model_desc']   = 'Modelo (Z - A)';
$_['text_limit']        = 'Mostrar:';