<?php
// Heading
$_['heading_title']     = 'Comparación de Productos';

// Texto
$_['text_product']      = 'Detalle de Producto';
$_['text_name']         = 'Producto';
$_['text_image']        = 'Imagen';
$_['text_price']        = 'Precio';
$_['text_model']        = 'Modelo';
$_['text_manufacturer'] = 'Fabricanted';
$_['text_availability'] = 'Disponibilidad';
$_['text_instock']      = 'En Stock';
$_['text_rating']       = 'Calificación';
$_['text_reviews']      = 'Basada en %s revisiones.';
$_['text_summary']      = 'Resumen';
$_['text_weight']       = 'Peso';
$_['text_dimension']    = 'Dimensiones (L x A x H)';
$_['text_compare']      = 'Productos Comparados (%s)';
$_['text_success']      = 'Agregó <a href="%s">%s</a> a su <a href="%s"> comparación de Productos</a>!';
$_['text_remove']       = 'Modificó su comparación de productos!';
$_['text_empty']        = 'No eligió ning&uacuten producto para comparar.';