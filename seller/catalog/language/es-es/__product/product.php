<?php
// Texto
$_['text_search']              = 'B&uacutesqueda';
$_['text_brand']               = 'Marca';
$_['text_manufacturer']        = 'Fabricante:';
$_['text_model']               = 'Modelo:';
$_['text_reward']              = 'Puntos de Recompensa:';
$_['text_points']              = 'Precio en Puntos:';
$_['text_stock']               = 'Disponibilidad:';
$_['text_instock']             = 'En Stock';
$_['text_tax']                 = 'Sin Impuesto:';
$_['text_discount']            = ' o mas ';
$_['text_option']              = 'Opciones Disponibles';
$_['text_minimum']             = 'Este producto tiene una cantidad mínima de %s';
$_['text_reviews']             = '%s Revisiones';
$_['text_write']               = 'Escriba una revisión';
$_['text_login']               = 'Por favor <a href="%s">ingrese</a> o <a href="%s">registrese</a> para dejar una revisión';
$_['text_no_reviews']          = 'No hay revisiones para este producto.';
$_['text_note']                = '<span class="text-danger">Nota:</span> HTML No está traducido!';
$_['text_success']             = 'Gracias por su revisión. Fue enviada al administrador para aprobación. ';
$_['text_related']             = 'Productos Relacionados';
$_['text_tags']                = 'Marcadores:';
$_['text_error']               = 'Producto no encontrado!';
$_['text_payment_recurring']   = 'Perfil Pagos';
$_['text_trial_description']   = '%s cada %d %s(s) para %d pago(s) posteriores';
$_['text_payment_description'] = '%s cada %d %s(s) para %d pago(s)';
$_['text_payment_cancel']      = '%s cada %d %s(s) hasta cancelación';
$_['text_day']                 = 'día';
$_['text_week']                = 'semana';
$_['text_semi_month']          = 'quincena';
$_['text_month']               = 'mes';
$_['text_year']                = 'año';

// Entry
$_['entry_qty']                = 'Cant';
$_['entry_name']               = 'Su Nombre';
$_['entry_review']             = 'Su Revisión';
$_['entry_rating']             = 'Calificación';
$_['entry_good']               = 'Buena';
$_['entry_bad']                = 'Mala';

// Tabs
$_['tab_description']          = 'Descripción';
$_['tab_attribute']            = 'Especificación';
$_['tab_review']               = 'Revisiones (%s)';

// Error
$_['error_name']               = 'Atención: Nombre de Revisión debe tener entre 3 y 25 caracteres!';
$_['error_text']               = 'Atención:El texto de Revisiones debe tener entre 25 y 1000 caracteres!';
$_['error_rating']             = 'Atención: Por favor, seleccione una calificación!';