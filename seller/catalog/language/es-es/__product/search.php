<?php
// Heading
$_['heading_title']     = 'Buscar';
$_['heading_tag']       = 'Marcador - ';

// Texto
$_['text_search']       = 'Productos que cumplen el criterio de b&uacutesqueda';
$_['text_keyword']      = 'Palabras Clave';
$_['text_category']     = 'Todas las Categorías';
$_['text_sub_category'] = 'Buscar en subcategorías';
$_['text_empty']        = 'No hay productos que cumplan el criterio de busqueda.';
$_['text_quantity']     = 'Cant: ';
$_['text_manufacturer'] = 'Marca:';
$_['text_model']        = 'Modelo:';
$_['text_points']       = 'Puntos de recompensa:';
$_['text_price']        = 'Precio:';
$_['text_tax']          = 'Sin Impuesto:';
$_['text_reviews']      = 'Basado en %s revisiones.';
$_['text_compare']      = 'Productos Comparados (%s)';
$_['text_sort']         = 'Ordenar por:';
$_['text_default']      = 'Defecto';
$_['text_name_asc']     = 'Nombre (A - Z)';
$_['text_name_desc']    = 'Nombre (Z - A)';
$_['text_price_asc']    = 'Precio (Menor > Mayor)';
$_['text_price_desc']   = 'Precio (Mayor > Menor)';
$_['text_rating_asc']   = 'Calificación (Menor)';
$_['text_rating_desc']  = 'Calificación (Mayor)';
$_['text_model_asc']    = 'Modelo (A - Z)';
$_['text_model_desc']   = 'Modelo (Z - A)';
$_['text_limit']        = 'Mostrar:';

// Entry
$_['entry_search']      = 'Criterio de B&uacutesqueda';
$_['entry_description'] = 'Buscar en descripciones de productos';

