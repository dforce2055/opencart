<?php
// Heading
$_['heading_title'] = '¡Falló el Pago!';

// Texto
$_['text_basket']   = 'Carrito de Compras';
$_['text_checkout'] = 'Comprar';
$_['text_failure']  = 'Falló el Pago';
$_['text_message']  = '<p>Se produjo un problema al procesar su pago y el pedido no se completó.</p>

<p>Posibles razones son:</p>
<ul>
  <li>Saldos insuficientes</li>
  <li>Falló la verificación</li>
</ul>

<p>Por favor intente nuevamente, con un método de pago distinto.</p>

<p>Si el problema persiste, por favor <a href="%s">contáctenos</a> con los detalles del pedido que está intentando procesar.</p>
';