<?php
// Heading
$_['heading_title']        = '¡Su orden ha sido procesada!';

// Texto
$_['text_basket']          = 'Carrito de Compras';
$_['text_checkout']        = 'Comprar';
$_['text_success']         = 'Éxito';
$_['text_customer']        = '<p>¡Su pedido ha sido procesado correctamente!</p><p>Puede ver el historial de pedidos desde <a href="%s">mi cuenta</a> haciendo click en <a href="%s">historial</a>.</p><p>Si su compra tiene una descarga asociada, puede ir a la sección <a href="%s">descargas</a> para verlas.</p><p>Por favor dirija cualquier pregunta que tenga al <a href="%s">dueño de la tienda</a>.</p><p>¡Gracias por comprarnos online!</p>';
$_['text_guest']           = '<p>¡Su pedido ha sido procesado correctamente!</p><p>Por favor dirija cualquier pregunta que tenga al <a href="%s">dueño de la tienda</a>.</p><p>¡Gracias por comprarnos online!</p>';