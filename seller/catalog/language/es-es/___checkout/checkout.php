<?php
// Heading
$_['heading_title']                  = 'Comprar';//Checkout

// Texto
$_['text_cart']                      = 'Carrito de compras';
$_['text_checkout_option']           = 'Paso %s: Opciones de Compra'; //'Step %s: Proceso de compra en línea  Options'
$_['text_checkout_account']          = 'Paso %s: Datos &amp; de la Cuenta Facturación'; //'Step %s: Cuenta &amp; Billing Details'
$_['text_checkout_payment_address']  = 'Paso %s: Detalles de Facturación';
$_['text_checkout_shipping_address'] = 'Paso %s: Detalles de env&ioacute;o';
$_['text_checkout_shipping_method']  = 'Paso %s: Método de env&ioacute;o';
$_['text_checkout_payment_method']   = 'Paso %s: Método de Pago';
$_['text_checkout_confirm']          = 'Paso %s: Confirmar Pedido';
$_['text_modify']                    = 'Modificar &raquo;';
$_['text_new_customer']              = 'Nuevo Cliente';
$_['text_returning_customer']        = 'Soy Cliente';
$_['text_checkout']                  = 'Opciones de Compra:';
$_['text_i_am_returning_customer']   = 'Soy un cliente recurrente';
$_['text_register']                  = 'Registrar Cuenta';
$_['text_guest']                     = 'Comprar como invitado'; //'Guest Proceso de compra en línea '
$_['text_register_account']          = 'Al crear una cuenta se podrán realizar compras, revisar los pedidos, el estado, o realizar un seguimiento de las operaciones anteriores.';
$_['text_forgotten']                 = 'Olvidó su contraseña';
$_['text_your_details']              = 'Sus detalles personales';
$_['text_your_address']              = 'Su dirección';
$_['text_your_password']             = 'Su contraseña';
$_['text_agree']                     = 'He leído y estoy de acuerdo <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Quiero usar una nueva dirección';
$_['text_address_existing']          = 'Quiero usar una dirección existente';
$_['text_shipping_method']           = 'Por favor seleccione un método recurrente de pago para su pedido.';
$_['text_payment_method']            = 'Por favor seleccione el método preferido de pago para su pedido.';
$_['text_comments']                  = 'Agregue comentarios a su pedido';
$_['text_recurring_item']            = 'Artículos recurrentes';
$_['text_payment_recurring']         = 'Perfil de Pago';
$_['text_trial_description']         = '%s cada %d %s(s) para %d pago(s) entonces'; // '%s cada %d %s(s) para %d pago(s) entonces';
$_['text_payment_description']       = '%s cada %d %s(s) para %d pago(s)'; // '%s cada %d %s(s) para %d pago(s)';
$_['text_payment_cancel']            = '%s cada %d %s(s) hasta cancelar'; // '%s cada %d %s(s) hasta cancelación';
$_['text_day']                       = 'día';
$_['text_week']                      = 'semana';
$_['text_semi_month']                = 'mitad de mes';
$_['text_month']                     = 'mes';
$_['text_year']                      = 'año';

// Column
$_['column_name']                    = 'Nombre de Producto';
$_['column_model']                   = 'Modelo';
$_['column_quantity']                = 'Cantidad';
$_['column_price']                   = 'Precio unitario';
$_['column_total']                   = 'Total';

// Entry
$_['entry_email_address']            = 'Dirección de E-Mail';
$_['entry_email']                    = 'E-Mail';
$_['entry_password']                 = 'contraseña';
$_['entry_confirm']                  = 'Confirmar contraseña';
$_['entry_firstname']                = 'Nombre';
$_['entry_lastname']                 = 'Apellido';
$_['entry_telephone']                = 'Teléfono';
$_['entry_address']                  = 'Seleccionar Dirección ';
$_['entry_company']                  = 'Compañia';
$_['entry_customer_group']           = 'Grupo de Clientes';
$_['entry_address_1']                = 'Dirección 1';
$_['entry_address_2']                = 'Dirección 2';
$_['entry_postcode']                 = 'Código Postal';
$_['entry_city']                     = 'Ciudad';
$_['entry_country']                  = 'País';
$_['entry_zone']                     = 'Provincia/estado';
$_['entry_newsletter']               = 'Quiero suscribirme al %s newsletter.';
$_['entry_shipping']                 = 'Mi Dirección de envío y de pago son las mismas.';

// Error
$_['error_warning']                  = '¡Se produjo un error al intentar procesar su pago! Si el problema persiste, intente seleccionar un método de pago diferente o puede ponerse en contacto con el propietario de la tienda <a href="%s">haciendo click acá</a>.';
$_['error_login']                    = 'Advertencia: No coincide la Dirección de E-mail y / o la contraseña .';
$_['error_attempts']                 = 'Advertencia: Su cuenta ha excedido el número permitido de intentos de inicio de sesión. Inténte de nuevo en 1 hora.';
$_['error_approved']                 = 'Advertencia: Su cuenta requiere aprobación primero, antes de iniciar sesión.';
$_['error_exists']                   = 'Advertencia: ¡Su Dirección de E-Mail ya se encuentra registrada!';
$_['error_firstname']                = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']                 = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']                    = '¡Dirección de E-Mail invalida!';
$_['error_telephone']                = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_password']                 = '¡La contraseña debe tener entre 4 y 20 caracteres!';
$_['error_confirm']                  = '¡La confirmación de la contraseña no coincide con la contraseña!';
$_['error_address_1']                = '¡La Dirección 1 debe tener entre 3 y 128 caracteres!';
$_['error_city']                     = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_postcode']                 = '¡El código Postal debe tener entre 2 y 10 caracteres!';
$_['error_country']                  = '¡Por favor seleccione el País!';
$_['error_zone']                     = '¡Por favor seleccione Provincia / Estado!';
$_['error_agree']                    = 'Advertencia: Debe aceptar el %s!';
$_['error_address']                  = 'Advertencia: ¡Debe seleccionar la dirección!';
$_['error_shipping']                 = 'Advertencia: ¡Se requiere método de envio!';
$_['error_no_shipping']              = 'Advertencia: No hay opciones de env&iacuteo disponibles. Por favor <a href="%s">contáctenos</a> por asistencia!';
$_['error_payment']                  = 'Advertencia: ¡Se requiere método de Pago!';
$_['error_no_payment']               = 'Advertencia: No hay opciones de pago disponibles. Por favor <a href="%s">contáctenos</a> por asistencia!';
$_['error_custom_field']             = '%s requerido!';