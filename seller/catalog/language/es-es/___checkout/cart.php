<?php
// Heading
$_['heading_title']            = 'Carrito de Compras';

// Texto
$_['text_success']             = 'Éxito: ¡Has añadido <a href="%s">%s</a> a tu <a href="%s">carro de compras</a>!';
$_['text_remove']              = 'Éxito:: ¡Has modificado tu carro de compras!';
$_['text_login']               = 'Atención: ¡Debe iniciar <a href="%s">sesión</a> o <a href="%s">crear una cuenta</a> para ver los precios!';
$_['text_items']               = '%s articulo(s) - %s';
$_['text_points']              = 'Puntos de Recompensa: %s';
$_['text_next']                = '¿Qué te gustar&iactue;a hacer despu&eactue;s?';
$_['text_next_choice']         = 'Elija si tiene un código de descuento o puntos de recompensa que desea usar o si desea estimar el costo de envío.';
$_['text_empty']               = '¡Tu carro de compras esta vacío!';
$_['text_day']                 = 'día';
$_['text_week']                = 'semana';
$_['text_semi_month']          = 'mitad de mes';
$_['text_month']               = 'mes';
$_['text_year']                = 'año';
$_['text_trial']               = '%s cada %s %s para %s pagos entonces ';//'%s cada %s %s para %s payments then ';
$_['text_recurring']           = '%s cada %s %s';
$_['text_length']              = ' para %s pagos';
$_['text_until_cancelled']     = 'hasta cancelar';
$_['text_recurring_item']      = 'Art&iactue;culo recurrente';
$_['text_payment_recurring']   = 'Perfil de Pago';
$_['text_trial_description']   = '%s cada %d %s(s) para %d pago(s) entonces'; //'%s cada %d %s(s) para %d pago(s) entonces';
$_['text_payment_description'] = '%s cada %d %s(s) para %d pago(s)'; //'%s cada %d %s(s) para %d pago(s)';
$_['text_payment_cancel']      = '%s cada %d %s(s) hasta cancelar'; //'%s cada %d %s(s) hasta cancelación';

// Column
$_['column_image']             = 'Imagen';
$_['column_name']              = 'Nombre de Producto';
$_['column_model']             = 'Modelo';
$_['column_quantity']          = 'Cantidad';
$_['column_price']             = 'Precio Unitario';
$_['column_total']             = 'Total';

// Error
$_['error_stock']              = '¡Productos marcados con ***no está disponible en la cantidad deseada o no hay stock!';
$_['error_minimum']            = '¡La cantidad mínima del pedido %s es %s!';
$_['error_required']           = '¡%s requerido!';
$_['error_product']            = 'Advertencia: ¡No hay productos en su carro de compras!';
$_['error_recurring_required'] = '¡Por favor seleccione un pago recurrente!';