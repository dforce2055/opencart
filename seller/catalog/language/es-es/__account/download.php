<?php
// Heading
$_['heading_title']     = 'Cuenta de Descargas';

// Texto
$_['text_account']      = 'Cuenta';
$_['text_downloads']    = 'Descargas';
$_['text_empty']        = '¡No ha realizado ningún pedido anterior para descargar!';

// Column
$_['column_order_id']   = 'ID de Pedido';
$_['column_name']       = 'Nombre';
$_['column_size']       = 'Tamaño';
$_['column_date_added'] = 'Fecha añadido';