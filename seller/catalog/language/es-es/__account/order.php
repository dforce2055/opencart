<?php
// Heading
$_['heading_title']         = 'Historial de Pedidos';

// Texto
$_['text_account']          = 'Cuenta';
$_['text_order']            = 'Información de Pedido';
$_['text_order_detail']     = 'Detalles de Pedido';
$_['text_invoice_no']       = 'Nro. de Factura:';
$_['text_order_id']         = 'ID de pedido:';
$_['text_date_added']       = 'Fecha añadido:';
$_['text_shipping_address'] = 'Dirección de envío';
$_['text_shipping_method']  = 'Método de Envío:';
$_['text_payment_address']  = 'Dirección de Pago';
$_['text_payment_method']   = 'Método de Pago:';
$_['text_comment']          = 'Comentarios del Pedido';
$_['text_history']          = 'Historial del Pedido';
$_['text_success']          = 'Éxito: ha añadido  <a href="%s">%s</a> a su<a href="%s">Carrito de Compras</a>!';
$_['text_empty']            = '¡NO has hecho ningún pedido anteriormente!';
$_['text_error']            = '¡No se puedo encontrar el pedido solicitado!';
$_['text_seller']            = 'Producto de:';
$_['text_status']            = 'Estado:';
$_['text_products']            = 'Productos:';
$_['text_review']            = 'Opiniones';
$_['text_rate']             = 'Calificar Vendedor';
$_['button_Save']            = 'Guardar';


// Column
$_['column_order_id']       = 'ID de pedido';
$_['column_customer']       = 'Cliente';
$_['column_product']        = 'No. de Productos';
$_['column_name']           = 'Nombre Producto';
$_['column_model']          = 'Modelo';
$_['column_quantity']       = 'Cantidad';
$_['column_price']          = 'Precio';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';
$_['column_date_added']     = 'Fecha añadido';
$_['column_status']         = 'Estado';
$_['column_comment']        = 'Comentario';

// Error
$_['error_reorder']         = '%s actualmente no esta disponible para ser pedido nuevamente.';
