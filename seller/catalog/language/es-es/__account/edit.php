<?php
// Heading
$_['heading_title']      = 'Información de mi Cuenta';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_edit']          = 'Editar Información';
$_['text_your_details']  = 'Detalles personales';
$_['text_success']       = 'Éxito: Su cuenta ha sido actualizada.';

// Entry
$_['entry_firstname']    = 'Nombre';
$_['entry_lastname']     = 'Apellido';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Teléfono';

// Error
$_['error_exists']       = 'Advertencia:¡La dirección de E-Mail ya esta registrada!';
$_['error_firstname']    = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']     = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_email']        = '¡La dirección de E-Mail parece invalida!';
$_['error_telephone']    = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_custom_field'] = '¡%s necesario!';