<?php
// Heading
$_['heading_title']  = 'Reiniciar su contraseña';

// Texto
$_['text_account']   = 'Cuenta';
$_['text_password']  = 'Ingresar nueva contraseña.';
$_['text_success']   = 'Éxito: Su contraseña ha sido actualizada.';

// Entry
$_['entry_password'] = 'Contraseña';
$_['entry_confirm']  = 'Confirmar';

// Error
$_['error_password'] = '¡La Contraseña debe contener entre 4 y 20 caracteres!';
$_['error_confirm']  = '¡La Contraseña de confirmación y la contraseña no coinciden!';
$_['error_code']     = '¡El código para reiniciar su contraseña no funciona o ya fue utilizado!';


