<?php
// Heading
$_['heading_title']       = 'Mi cuenta';

// Texto
$_['text_account']        = 'Cuenta';
$_['text_my_account']     = 'Mi cuenta';
$_['text_my_orders']      = 'Mis Pedidos';
$_['text_my_affiliate']   = 'Mi cuenta de afiliado';
$_['text_my_newsletter']  = 'Boletín de Noticias';
$_['text_edit']           = 'Editar información de su cuenta';
$_['text_password']       = 'Cambiar contraseña';
$_['text_address']        = 'Modificar las entradas de la libreta de direcciones';
$_['text_credit_card']    = 'Gestionar tarjetas de crédito guardadas';
$_['text_wishlist']       = 'Modificar su lista de Deseados';
$_['text_order']          = 'Ver su historial de Pedidos';
$_['text_download']       = 'Descargas';
$_['text_reward']         = 'Sus puntos de recompensa';
$_['text_return']         = 'Ver sus solicitudes de devolución';
$_['text_transaction']    = 'Sus transacciones';
$_['text_newsletter']     = 'Suscribirse / Desuscribirse al newsletter';
$_['text_recurring']      = 'Pagos recurrentes';
$_['text_transactions']   = 'Transacciones';
$_['text_affiliate_add']  = 'Registrarse para cuenta de afiliado';
$_['text_affiliate_edit'] = 'Editar su información de afiliado';
$_['text_tracking']       = 'Código personalizado de seguimiento de afiliados';
$_['button_Save']            = 'Guardar';
$_['text_your_transaction']   = 'Sus Transacciones';
