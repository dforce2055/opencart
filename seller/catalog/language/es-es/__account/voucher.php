<?php
// Heading
$_['heading_title']    = 'Compre un cupón de regalo';

// Texto
$_['text_account']     = 'Cuenta';
$_['text_voucher']     = 'cupón de regalo';
$_['text_description'] = 'Este cupón de regalo, será enviado por e-mail al destinarario, después de que se procese el pago.';
$_['text_agree']       = 'Entiendo que los cupóns de regalo no son reembolsables.';
$_['text_message']     = '<p>¡Gracias por comprar un certificdo de regalo! Una vez que haya completado su pedido, el destinatario recibirá un e-mail con los detalles sobre como cambiar el cupón de regalo.</p>';
$_['text_for']         = '%s Certificado de regalo para %s';

// Entry
$_['entry_to_name']    = 'Nombre del destinarario';
$_['entry_to_email']   = 'E-mail del destinatario';
$_['entry_from_name']  = 'Su nombre';
$_['entry_from_email'] = 'Su E-mail';
$_['entry_theme']      = 'Tema del cupón de regalo';
$_['entry_message']    = 'Mensaje';
$_['entry_amount']     = 'Monto';

// Help
$_['help_message']     = 'Opcional';
$_['help_amount']      = 'El valor debe estar entre %s y %s';

// Error
$_['error_to_name']    = '¡El nombre del destinarario debe tener entre 1 y 64 caracteres!';
$_['error_from_name']  = '¡Su nombre debe tener entre 1 y 64 caracteres!';
$_['error_email']      = '¡La dirección de E-Mail parece inválida!';
$_['error_theme']      = '¡Debe seleccionar un tema!';
$_['error_amount']     = '¡El valor debe estar entre %s y %s!';
$_['error_agree']      = 'Advertencia: ¡Debe aceptar que los cupones de regalo no son reembolsables!';