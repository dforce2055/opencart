<?php
// Heading
$_['heading_title']                = 'Iniciar Sesi&óacute;n';

// Texto
$_['text_account']                 = 'Cuenta';
$_['text_login']                   = 'Iniciar Sesi&óacute;n';
$_['text_new_customer']            = 'Nuevo Cliente';
$_['text_register']                = 'Registrar Cuenta';
$_['text_register_account']        = 'Al crear una cuenta, va a poder comprar más rápido, ver el estado y hacer un seguimiento de sus Pedidos';
$_['text_returning_customer']      = 'Cliente recurrente';
$_['text_i_am_returning_customer'] = 'Soy Cliente Recurrente';
$_['text_forgotten']               = '¿Olvidó su contraseña?';

// Entry
$_['entry_email']                  = 'Direcci&óacute;n de E-Mail';
$_['entry_password']               = 'Contraseña';

// Error
$_['error_login']                  = 'Advertencia: ¡La Direcci&óacute;n de E-mail y/o la contraseña no coinciden!';
$_['error_attempts']               = 'Advertencia: ¡Su cuenta ha excedido el número de intentos máximos para iniciar sesi&óacute;n. Por favor intente de nuevo en 1 hora.';
$_['error_approved']               = 'Advertencia: ¡Su cuenta necesita aprobación antes de iniciar sesi&oacuten!';