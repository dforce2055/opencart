<?php
// Heading
$_['heading_title']  = 'Cambiar contraseña';

// Texto
$_['text_account']   = 'Cuenta';
$_['text_password']  = 'Su contraseña';
$_['text_success']   = 'Éxito: Su contraseña ha sido actualizada correctamente.';

// Entry
$_['entry_password'] = 'Contraseña';
$_['entry_confirm']  = 'Confirmar contraseña';

// Error
$_['error_password'] = 'La contraseña debe contener entre 4 y 20 caracteres!';
$_['error_confirm']  = '¡La contraseña y la contraseña de confirmación no coinciden!';