<?php
// Heading
$_['heading_title']      = 'Sus puntos de recompensa';

// Column
$_['column_date_added']  = 'Fecha añadido';
$_['column_description'] = 'Descripción';
$_['column_points']      = 'Puntos';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_reward']        = 'Puntos recompensa';
$_['text_total']         = 'El total de sus puntos es:';
$_['text_empty']         = '¡Usted no tiene puntos de recompensa!';