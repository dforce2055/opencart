<?php
// Heading
$_['heading_title']      = 'Devoluciones de Productos';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_return']        = 'Información de Devoluciones';
$_['text_return_detail'] = 'Detalles de Devoluciones';
$_['text_description']   = 'Por favor complete el siguiente formulario para solicitar el número de RMA.';
$_['text_order']         = 'Información de Pedidos';
$_['text_product']       = 'Información de Productos';
$_['text_reason']        = 'Razón de la devolución';
$_['text_message']       = '<p>Gracias por enviar su solicitud de devolución. La hemos enviado al departamento correspondiente para procesarla.</p><p> Se le notificara por E-mail el estado de su solicitud.</p>';
$_['text_return_id']     = 'ID Devolución:';
$_['text_order_id']      = 'ID de pedido:';
$_['text_date_ordered']  = 'Fecha Pedido:';
$_['text_status']        = 'Estado:';
$_['text_date_added']    = 'Fecha añadido:';
$_['text_comment']       = 'Comentarios de Devolución';
$_['text_history']       = 'Historial de Devolución';
$_['text_empty']         = '¡Previamente no ha hecho ninguna devolución!';
$_['text_agree']         = '¡He leído y estoy de acuerdo con <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'ID Devolución';
$_['column_order_id']    = 'ID de pedido';
$_['column_status']      = 'Estado';
$_['column_date_added']  = 'Fecha añadido';
$_['column_customer']    = 'Cliente';
$_['column_product']     = 'Nombre de Producto';
$_['column_model']       = 'Modelo';
$_['column_quantity']    = 'Cantidad';
$_['column_price']       = 'Precio';
$_['column_opened']      = 'Abierto';
$_['column_comment']     = 'Comentario';
$_['column_reason']      = 'Razón';
$_['column_action']      = 'Acción';

// Entry
$_['entry_order_id']     = 'ID de pedido';
$_['entry_date_ordered'] = 'Fecha Pedido';
$_['entry_firstname']    = 'Nombre';
$_['entry_lastname']     = 'Apellido';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Teléfono';
$_['entry_product']      = 'Nombre de Producto';
$_['entry_model']        = 'Código de Producto';
$_['entry_quantity']     = 'Cantidad';
$_['entry_reason']       = 'Razón de la devolución';
$_['entry_opened']       = 'El producto está abierto';
$_['entry_fault_detail'] = 'Falla u otros detalles';

// Error
$_['text_error']         = '¡El pedido de devolución no se encontro!';
$_['error_order_id']     = '¡El ID de pedido es necesario!';
$_['error_firstname']    = '¡El nombre debe contener entre 1 y 32 caracteres!';
$_['error_lastname']     = '¡El apellido debe contener entre 1 y 32 caracteres!';
$_['error_email']        = '¡La dirección de E-Mail parece invalida!';
$_['error_telephone']    = '¡El teléfono debe contener entre 3 y 32 caracteres!';
$_['error_product']      = '¡El nombre del producto debe tener entre 3 y 255 caracteres!';
$_['error_model']        = '¡El modelo del producto debe tener entre 3 y 64 caracteres!';
$_['error_reason']       = '¡Debe seleccionar una razón para devolver el producto!';
$_['error_agree']        = 'Advertencia: ¡debe acepta el %s!';