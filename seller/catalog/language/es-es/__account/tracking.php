<?php
// Heading
$_['heading_title']    = 'Seguimiento de Afiliados';

// Texto
$_['text_account']     = 'Cuenta';
$_['text_description'] = 'Para asegurarnos que usted recibe los pagos por referncia, necesitamos seguir la referencia, colocando un código de seguimiento en la URL que vincula con nosotros. Puede utilizar las siguientes herramientas para generar vinculos a %s sitio web.';

// Entry
$_['entry_code']       = 'Su código de seguimiento';
$_['entry_generator']  = 'Generar vinculo de seguimiento';
$_['entry_link']       = 'Enlace de seguimiento';

// Help
$_['help_generator']  = 'Escriba el nombre de un producto al que desea vincular';