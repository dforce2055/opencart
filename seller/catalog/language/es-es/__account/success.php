<?php
// Heading
$_['heading_title'] = '¡Su cuenta ha sido creada!';

// Texto
$_['text_message']  = '<p>Felicitaciones! ¡Su nueva cuenta ha sido creada satisfactoriamente!</p> <p>Ahora puede aprovechar las ventajas de ser miembro para mejorar su experiencia de compra en línea.</p> <p>Si tiene alguna duda o pregunta sobre el funcionamiento de la tienda en línea, por favor contactese con el dueño de la tienda</p> <p>Un e-mail de confirmación ha sido enviado a su cuenta, si no recibe dicho correo dentre de 1 hora, por favor <a href="%s">póngase en contacto con nosotros</a>.</p>';
$_['text_approval'] = '<p>Gracias por registrarse con nosotros %s!</p><p>Se le va a notificar por e-mail, una vez que el dueño de la tienda active su cuenta.</p><p>Si tiene alguna duda o pregunta sobre el funcionamiento de la tienda en línea, por favor <a href="%s">contáctese con el dueño de la tienda</a>.</p>';
$_['text_account']  = 'Cuenta';
$_['text_success']  = 'éxito';