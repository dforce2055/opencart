<?php
// Heading
$_['heading_title']      = 'Sus Transacciones';

// Column
$_['column_date_added']  = 'Fecha añadido';
$_['column_description'] = 'Descripción';
$_['column_amount']      = 'Monto (%s)';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_transaction']   = 'Sus Transacciones';
$_['text_total']         = 'Su balance actual es:';
$_['text_empty']         = '¡No tiene ninguna transacción!';
$_['text_txn_id'] 	= 'Txn Id';
?>
