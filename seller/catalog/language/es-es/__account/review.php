<?php
// Heading
$_['heading_title']       = 'Opinión';

// Texto
$_['text_success']      = 'Éxito: ¡Has actualizado tu reseña!';

// Column
$_['column_product']    = 'Producto';
$_['column_author']     = 'Autor';
$_['column_rating']     = 'Valoración';
$_['column_status']     = 'Estado';
$_['column_date_added'] = 'Fecha añadido';
$_['column_action']     = 'Acción';
$_['button_Save']            = 'Guardar';

// Entry
$_['entry_product']     = 'Producto:<br/><span class="help">(Autocompletar)</span>';
$_['entry_author']      = 'Autor:';
$_['entry_rating']      = 'Valoración:';
$_['entry_status']      = 'Estado:';
$_['entry_text']        = 'Texto:';
$_['entry_good']        = 'Bueno';
$_['entry_bad']         = 'Malo';

// Error
$_['error_permission']  = 'Adverencia: ¡No tiene permisos para modificar la reseña!';
$_['error_seller']     = '¡Es necesario un vendedor!';
$_['error_order']     = '¡Es necesario ID de Pedido!';
$_['error_author']      = 'El nombre debe tener entre 3 y 64 caracteres!';
$_['error_text']        = 'La reseña debe tener al menos 1 caracter!';
$_['error_rating']      = '¡Es necesaria la valoración de la reseña!';
?>
