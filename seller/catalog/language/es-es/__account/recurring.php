<?php
// Heading
$_['heading_title']                        = 'Pagos recurrentes';

// Texto
$_['text_account']                         = 'Cuenta';
$_['text_recurring']                       = 'Información de pagos recurrentes';
$_['text_recurring_detail']                = 'Detalles de pagos recurrentes';
$_['text_order_recurring_id']              = 'ID Reccurente:';
$_['text_date_added']                      = 'Fecha añadido:';
$_['text_status']                          = 'Estado:';
$_['text_payment_method']                  = 'Método de Pago:';
$_['text_order_id']                        = 'ID de pedido:';
$_['text_product']                         = 'Producto:';
$_['text_quantity']                        = 'Cantidad:';
$_['text_description']                     = 'Descripción';
$_['text_reference']                       = 'Referencia';
$_['text_transaction']                     = 'Transacciones';
$_['text_status_1']                        = 'Activo';
$_['text_status_2']                        = 'Inactivo';
$_['text_status_3']                        = 'Cancelado';
$_['text_status_4']                        = 'Suspendido';
$_['text_status_5']                        = 'Expirado';
$_['text_status_6']                        = 'Pendiente';
$_['text_transaction_date_added']          = 'Creado';
$_['text_transaction_payment']             = 'Pagado';
$_['text_transaction_outstanding_payment'] = 'Pago pendiente';
$_['text_transaction_skipped']             = 'Pago omitido';
$_['text_transaction_failed']              = 'Pago fallido';
$_['text_transaction_cancelled']           = 'Cancelado';
$_['text_transaction_suspended']           = 'Suspendido';
$_['text_transaction_suspended_failed']    = 'Suspendido por falla de pago';
$_['text_transaction_outstanding_failed']  = 'Falló el pago pendiente';
$_['text_transaction_expired']             = 'Expirado';
$_['text_empty']                           = '¡No se han encontrado pagos recurrentes!';
$_['text_error']                           = '¡No se pudo encontrar el pedido recurrente que solicitó!';
$_['text_cancelled']                       = '¡Se ha cancelado el pago recurrente!';

// Column
$_['column_date_added']                    = 'Fecha añadido';
$_['column_type']                          = 'Tipo';
$_['column_amount']                        = 'Monto';
$_['column_status']                        = 'Estado';
$_['column_product']                       = 'Producto';
$_['column_order_recurring_id']            = 'ID recurrente';

// Error
$_['error_not_cancelled']                  = 'Error: %s';
$_['error_not_found']                      = 'No se pudo cancelar recurrente';

// Button
$_['button_return']                        = 'Devolución';