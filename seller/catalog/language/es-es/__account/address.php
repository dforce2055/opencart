<?php
// Heading
$_['heading_title']      = 'Libreta de direcciones';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_address_book']  = 'Entradas en la Libreta de direcciones';
$_['text_address_add']   = 'Agregar dirección';
$_['text_address_edit']  = 'Editar dirección';
$_['text_add']           = 'Su dirección ha sido agregada';
$_['text_edit']          = 'Su dirección ha sido modificada';
$_['text_delete']        = 'Su dirección ha sido eliminada';
$_['text_empty']         = 'No tiene dirección en su cuenta.';

// Entry
$_['entry_firstname']    = 'Nombre';
$_['entry_lastname']     = 'Apellido';
$_['entry_company']      = 'Compañía';
$_['entry_address_1']    = 'dirección 1';
$_['entry_address_2']    = 'dirección 2';
$_['entry_postcode']     = 'Código postal';
$_['entry_city']         = 'Ciudad';
$_['entry_country']      = 'País';
$_['entry_zone']         = 'Provincia / Estado';
$_['entry_default']      = 'Dirección por defecto';

// Error
$_['error_delete']       = 'Advertencia: ¡Usted debe tener al menos una dirección!';
$_['error_default']      = 'Advertencia: ¡Ustede no puede borrar su dirección por defecto!';
$_['error_firstname']    = '¡El nombre debe tener entre 1 y 32 caracteres!';
$_['error_lastname']     = '¡El apellido debe tener entre 1 y 32 caracteres!';
$_['error_address_1']    = '¡La dirección debe tener entre 3 y 128 caracteres!';
$_['error_postcode']     = '¡El código postal debe tener entre 2 y 10 caracteres!';
$_['error_city']         = '¡La ciudad debe tener entre 2 y 128 caracteres!';
$_['error_country']      = '¡Por favor seleccione un País!';
$_['error_zone']         = '¡Por favor seleccione a provincia / estado!';
$_['error_custom_field'] = '¡%s necesario!';