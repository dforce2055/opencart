<?php
// Heading
$_['heading_title']   = '¿Olvidó su contraseña?';

// Texto
$_['text_account']    = 'Cuenta';
$_['text_forgotten']  = 'Contraseña olvidada';
$_['text_your_email'] = 'Su dirección de E-Mail';
$_['text_email']      = 'Ingrese la dirección de mail asociada a su cuenta. Haga click en enviar, para que le enviemos un e-mail para restaurar su contraseña.';
$_['text_success']    = 'Le enviamos un mail con el link de confirmación a su dirección de e-mail.';

// Entry
$_['entry_email']     = 'Dirección de E-Mail';
$_['entry_password']  = 'Nueva contraseña';
$_['entry_confirm']   = 'Confirmar';

// Error
$_['error_email']     = 'Advertencia: ¡La dirección de e-mail no se encuentra registrada! Inténtelo de nuevo.';
$_['error_approved']  = 'Advertencia: ¡Su cuenta debe ser aprobada antes de iniciar sesión por primera vez!';
$_['error_password']  = '¡Contraseña debe tener entre 4 y 20 caracteres!';
$_['error_confirm']   = '¡La contraseña y la contraseña de confirmación no coinciden!';