<?php
// Heading
$_['heading_title']        = 'Registrar Cuenta';

// Texto
$_['text_account']         = 'Cuenta';
$_['text_register']        = 'Registro';
$_['text_account_already'] = 'Si usted ya tiene cuentaa con nosotros, por favor inicie sesión desde la <a href="%s">pagina de inicio de sesión</a>.';
$_['text_your_details']    = 'Sus detalles personales';
$_['text_newsletter']      = 'Boletín informativo';
$_['text_your_password']   = 'Su contraseña';
$_['text_agree']           = 'Estoy de acuerdo con <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Grupo de Clientes';
$_['entry_firstname']      = 'Nombre';
$_['entry_lastname']       = 'Apellido';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Teléfono';
$_['entry_newsletter']     = 'Subscribir';
$_['entry_password']       = 'Contraseña';
$_['entry_confirm']        = 'Confirmar Contraseña';

// Error
$_['error_exists']         = 'Advertencia: ¡La dirección de E-Mail ya esta registrada!';
$_['error_firstname']      = '¡El nombre debe contener entre 1 y 32 caracteres!';
$_['error_lastname']       = '¡El apellido debe contener entre 1 y 32 caracteres!';
$_['error_email']          = '¡La dirección de E-Mail parece invalida!';
$_['error_telephone']      = '¡El teléfono debe contener entre 3 y 32 caracteres!';
$_['error_custom_field']   = '¡%s necesario!';
$_['error_password']       = '¡La Contraseña debe contener entre 4 y 20 caracteres!';
$_['error_confirm']        = '¡La Contraseña de confirmación y la contraseña no coinciden!';
$_['error_agree']          = 'Advertencia: ¡Usted debe aceptar %s!';