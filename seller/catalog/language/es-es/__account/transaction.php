<?php
// Heading
$_['heading_title']      = 'Sus transacciones';

// Column
$_['column_date_added']  = 'Fecha añadido';
$_['column_description'] = 'Descripción';
$_['column_amount']      = 'Monto (%s)';

// Texto
$_['text_account']       = 'Cuenta';
$_['text_transaction']   = 'Sus transacciones';
$_['text_total']         = 'Tu saldo actual es:';
$_['text_empty']         = '¡No tiene ninguna transacción!';
$_['text_txn_id'] 	= 'Txn Id';
$_['text_your_transaction']   = 'Sus Transacciones';
