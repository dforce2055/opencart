<?php
// Heading
$_['heading_title'] = 'Cerrar Sesión';

// Texto
$_['text_message']  = '<p>Usted cerro sesión correctamente. Ahora es seguro dejar su computadora</p><p>Su carro de compras se ha guardado, los elementos dentro de él se restaurarán cada vez que vuelva a iniciar sesión en su cuenta.</p>';
$_['text_account']  = 'Cuenta';
$_['text_logout']   = 'Cerrar Sesión';