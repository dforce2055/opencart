<?php
// Heading
$_['heading_title']    = 'Suscribite al Boletín Informativo';

// Texto
$_['text_account']     = 'Cuenta';
$_['text_newsletter']  = 'Boletín Informativo';
$_['text_success']     = 'Éxito: ¡La suscripción al Boletín Informativo ha sido actualizada!';

// Entry
$_['entry_newsletter'] = 'Suscribir';