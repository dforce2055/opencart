<?php
// Heading
$_['heading_title'] = 'Mi lista de deseados';

// Texto
$_['text_account']  = 'Cuenta';
$_['text_instock']  = 'En Stock';
$_['text_wishlist'] = 'Favoritos (%s)';
$_['text_login']    = 'Debe <a href="%s">iniciar sesión</a> o <a href="%s">crear una cuenta</a> para guardar <a href="%s">%s</a> en su <a href="%s">lista de deseados</a>!';
$_['text_success']  = 'éxito: ¡Has agregado <a href="%s">%s</a> a tu <a href="%s">lista de deseados</a>!';
$_['text_remove']   = 'éxito: ¡Has modificado lista de deseados!';
$_['text_empty']    = 'Su lista de deseados esta vacía.';

// Column
$_['column_image']  = 'Imagen';
$_['column_name']   = 'Nombre de Producto';
$_['column_model']  = 'Modelo';
$_['column_stock']  = 'Stock';
$_['column_price']  = 'Precio unitario';
$_['column_action'] = 'Acción';