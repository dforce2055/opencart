<?php
// Heading
$_['heading_title']         = 'Sagepay Direct Cards';

// Texto
$_['text_empty']		    = 'No tiene tarjetas almacenadas';
$_['text_account']          = 'Cuenta';
$_['text_card']			    = 'Manejo de SagePay Direct Card';
$_['text_fail_card']	    = 'Hubo inconvenientes al intentar remover su tarjeta SagePay, por favor contacte al administrador de la tienda para recibir ayuda.';
$_['text_success_card']     = 'Tarjeta SagePay removida exitosamente.';
$_['text_success_add_card'] = 'Tarjeta SagePay agregada exitosamente.';

// Column
$_['column_type']		    = 'Tipo de tarjeta';
$_['column_digits']	        = 'Últimos dígitos';
$_['column_expiry']		    = 'Expiración';

// Entry
$_['entry_cc_owner']        = 'Dueño de tarjeta';
$_['entry_cc_type']         = 'Tipo de tarjeta';
$_['entry_cc_number']       = 'Número de tarjeta';
$_['entry_cc_expire_date']  = 'Fecha de expiración de tarjeta';
$_['entry_cc_cvv2']         = 'Código de seguridad de tarjeta (CVV2)';
$_['entry_cc_choice']       = 'Seleccionar una tarjeta existente';

// Button
$_['button_add_card']       = 'Agregar tarjeta';
$_['button_new_card']       = 'Agregar nueva tarjeta';



