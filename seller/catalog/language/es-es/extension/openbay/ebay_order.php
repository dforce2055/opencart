<?php
// Texto
$_['text_total_shipping']		= 'Envío';
$_['text_total_discount']		= 'Descuentos';
$_['text_total_tax']			= 'Impuesto';
$_['text_total_sub']			= 'Sub-total';
$_['text_total']				= 'Total';
$_['text_smp_id']				= 'ID de venta Selling Manager : ';
$_['text_buyer']				= 'Nombre de usuario del comprador: ';