<?php
// Texto
$_['text_paid_amazon'] 			= 'Pagado en Amazon';
$_['text_total_shipping'] 		= 'Envío';
$_['text_total_shipping_tax'] 	= 'Impuesto d envío';
$_['text_total_giftwrap'] 		= 'Envuelto en regalo';
$_['text_total_giftwrap_tax'] 	= 'Impuesto de envoltura';
$_['text_total_sub'] 			= 'Sub-total';
$_['text_tax'] 					= 'Impuesto';
$_['text_total'] 				= 'Total';
$_['text_gift_message'] 		= 'Mensajes de regalo';
