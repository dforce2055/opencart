<?php
// Texto
$_['text_title']				= 'Tarjeta de Crédito o Débito';
$_['text_credit_card']			= 'Detalles de tarjeta de Crédito';
$_['text_wait']					= '¡Por favor espere!';

// Entry
$_['entry_cc_number']			= 'Número de tarjeta';
$_['entry_cc_name']				= 'Nombre del titular';
$_['entry_cc_expire_date']		= 'Fecha de expiración de tarjeta';
$_['entry_cc_cvv2']				= 'Código de seguridad de tarjeta(CVV2)';

// Help
$_['help_start_date']			= '(si está disponible)';
$_['help_issue']				= '(sólo para tarjetas Maestro y Solo)';

// Texto
$_['text_result']				= 'Resultado: ';
$_['text_approval_code']		= 'Código de aprobación: ';
$_['text_reference_number']		= 'Referencia: ';
$_['text_card_number_ref']		= 'Últimos 4 digitos de tarjeta: xxxx ';
$_['text_card_brand']			= 'Marca de tarjeta: ';
$_['text_response_code']		= 'Código de respuesta: ';
$_['text_fault']				= 'Mensaje de falla: ';
$_['text_error']				= 'Mensaje de error: ';
$_['text_avs']					= 'Verificación de dirección: ';
$_['text_address_ppx']			= 'Dirección no provista o Dirección no verificada por Emisor de tarjeta';
$_['text_address_yyy']			= 'Emisor de tarjeta confirmó que calle y código postal coinciden con sus registros';
$_['text_address_yna']			= 'Emisor de tarjeta confirmó que calle coincide con sus registros pero código postal no coincide';
$_['text_address_nyz']			= 'Emisor de tarjeta confirmó que código postal coincide con sus registros pero calle no coincide';
$_['text_address_nnn']			= 'Ambos calle y código postal no coinciden con los registros del Emisor de tarjeta';
$_['text_address_ypx']			= 'Emisor de tarjeta confirmó que calle coincide con sus registros. El emisor no verificó el código postal';
$_['text_address_pyx']			= 'Emisor de tarjeta confirmó que código postal coincide con sus registros. El emisor no verificó la dirección';
$_['text_address_xxu']			= 'Emisor de tarjeta no verificó la información AVS';
$_['text_card_code_verify']		= 'Código de seguridad: ';
$_['text_card_code_m']			= 'Código de seguridad de tarjeta coincide';
$_['text_card_code_n']			= 'Código de seguridad de tarjeta no coincide';
$_['text_card_code_p']			= 'No procesado';
$_['text_card_code_s']			= 'Vendedor ha indicado que el Código de seguridad de tarjeta no está presente en la misma';
$_['text_card_code_u']			= 'El emisor no está certificado y/o no proveyó claves de encripción.';
$_['text_card_code_x']			= 'No se recibió respuesta de asociación de tarjeta de crédito';
$_['text_card_code_blank']		= 'Una respuesta en blanco debería indicar que no se envió ningún código y que no hubo indicaciones de que el código no estaba presente en la tarjeta.';
$_['text_card_accepted']		= 'Tarjetas aceptadas: ';
$_['text_card_type_m']			= 'Mastercard';
$_['text_card_type_v']			= 'Visa (Crédito/Débito/Electron/Delta)';
$_['text_card_type_c']			= 'Diners';
$_['text_card_type_a']			= 'American Express';
$_['text_card_type_ma']			= 'Maestro';
$_['text_card_new']				= 'Nueva tarjeta';
$_['text_response_proc_code']	= 'Código Procesador: ';
$_['text_response_ref']			= 'Número de referencia: ';

// Error
$_['error_card_number']			= 'Por favor verifique que su Número de tarjeta es válido';
$_['error_card_name']			= 'Por favor verifique que el nombre del titular de tarjeta es válido';
$_['error_card_cvv']			= 'Por favor verifique que el CVV2 es válido';
$_['error_failed']				= 'No se pudo procesar su pago, por favor contacte al vendedor';