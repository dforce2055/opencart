<?php
// Texto
$_['text_title']				= 'Tarjeta de Crédito o Débito';
$_['text_secure_connection']	= 'Creando una conexión segura...';

// Error
$_['error_connection']			= 'No se pudo conectar con PayPal. Por favor contacte al administrador de la tienda por asistencia o elija otro método de pago.';