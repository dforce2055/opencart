<?php
// Texto
$_['text_title']            = 'Tarjeta de Crédito / Débito';
$_['text_card_details']     = 'Detalles de tarjeta';
$_['text_echeck_details']   = 'Detalles eCheque';
$_['text_card']             = 'Tarjeta';
$_['text_echeck']           = 'eCheque';
$_['text_wait']             = '¡Por favor espere!';
$_['text_confirm_delete']   = 'Está seguro de que desea eliminar la tarjeta?';
$_['text_no_cards']         = 'No hay tarjetas almacenadas';
$_['text_select_card']      = 'Por favor seleccione una tarjeta';

// Entry
$_['entry_method']          = 'Método';
$_['entry_card_new_or_old'] = 'Tarjeta nueva / existente';
$_['entry_card_new']        = 'Nueva';
$_['entry_card_old']        = 'Existente';
$_['entry_card_type']       = 'Tipo de tarjeta';
$_['entry_card_number']     = 'Número de tarjeta';
$_['entry_card_expiry']     = 'Expiración';
$_['entry_card_cvv2']       = 'CVV2';
$_['entry_card_save']       = 'Guardar tarjeta';
$_['entry_card_choice']     = 'Seleccione Su tarjeta';
$_['entry_account_number']  = 'Número de Cuenta';
$_['entry_routing_number']  = 'Número Routing';

// Button
$_['button_confirm']        = 'Confirmar Pedido';
$_['button_delete']         = 'Borrar Tarjeta Seleccionada';

// Error
$_['error_card_number']     = '¡Número de tarjeta debe tener entre 1 y 19 caracteres!';
$_['error_card_type']       = '¡Tipo de Tarjeta no es una selección válida!';
$_['error_card_cvv2']       = '¡CVV2 debe tener entre 1 y 4 caracteres!';
$_['error_data_missing']    = '¡Datos incompletos!';
$_['error_not_logged_in']   = '¡Debe iniciar sesión!';
$_['error_no_order']        = '¡Ningún pedido coincide!';
$_['error_no_post_data']    = '¡Sin datos $_POST!';
$_['error_select_card']     = '¡Por favor seleccione una tarjeta!';
$_['error_no_card']         = '¡No se encontró la tarjeta!';
$_['error_no_echeck']       = '¡eCheque no soportado!';
$_['error_account_number']  = 'Número de cuenta debe tener entre 1 y 19 caracteres!';
$_['error_routing_number']  = 'Número de routing debe tener entre 1 y 9 caracteres!';
$_['error_not_enabled']     = 'Módulo no activado';