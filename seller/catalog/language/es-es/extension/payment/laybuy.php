<?php
// Heading
$_['heading_title']			= 'Por favor Seleccionar Su plan de pago';

// Texto
$_['text_title']			= 'PUT IT ON LAY-BUY alimentado por PayPal';
$_['text_plan_preview']		= 'Vista previa de plan';
$_['text_payment']			= 'Pago';
$_['text_due_date']			= 'Fecha de vencimiento';
$_['text_amount']			= 'Monto';
$_['text_downpayment']		= 'Anticipo';
$_['texttoday']			= 'Hoy';
$_['text_delivery_msg']		= 'Sus bienes/servicios serán enviados una vez recibido su último pago.';
$_['text_fee_msg']			= 'Un gasto administrativo de 0.9% es pagable a Lay-Buys.com.';
$_['text_month']			= 'Mes';
$_['text_months']			= 'Meses';
$_['text_status_1']			= 'Pendiente';
$_['text_status_5']			= 'Completado';
$_['text_status_7']			= 'Cancelado';
$_['text_status_50']		= 'Corrección solicitada';
$_['text_status_51']		= 'Corregido';
$_['text_comment']			= 'Actualizado por Lay-Buy';

// Entry
$_['entry_initial']			= 'Pago inicial';
$_['entry_months']			= 'Meses';

// Button
$_['button_confirm']		= 'Confirmar Pedido';