<?php
// Texto
$_['text_title']				= 'Tarjeta de Crédito / Débito (SagePay)';
$_['text_credit_card']			= 'Detalles de tarjeta';
$_['text_card_type']			= 'Tipo de Tarjeta: ';
$_['text_card_name']			= 'Nombre de Tarjeta: ';
$_['text_card_digits']			= 'Últimos dígitos: ';
$_['text_card_expiry']			= 'Expiración: ';
$_['text_trial']				= '%s cada %s %s por %s pagos entonces ';
$_['text_recurring']			= '%s cada %s %s';
$_['text_length']				= ' por %s pagos';
$_['text_fail_card']			= 'Hubo un inconveniente al remover su tarjeta SagePay, Por favor contacte al administrador de la tienda por ayuda.';
$_['text_confirm_delete']		= 'Está seguro de que desea eliminar la tarjeta?';

// Entry
$_['entry_card']				= 'Tarjeta nueva o existente: ';
$_['entry_card_existing']		= 'Existente';
$_['entry_card_new']			= 'Nueva';
$_['entry_card_save']			= 'Recordar detalles de tarjeta';
$_['entry_cc_owner']			= 'Dueño de tarjeta';
$_['entry_cc_type']				= 'Tipo de tarjeta';
$_['entry_cc_number']			= 'Número de tarjeta';
$_['entry_cc_expire_date']		= 'Fecha de expiración de tarjeta';
$_['entry_cc_cvv2']				= 'Código de seguridad de tarjeta (CVV2)';
$_['entry_cc_choice']			= 'Seleccionar una tarjeta existente';

// Button
$_['button_delete_card']		= 'Borrar tarjeta seleccionada';