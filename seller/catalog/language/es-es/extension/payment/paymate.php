<?php
// Texto
$_['text_title']				= 'Tarjeta de Crédito / Débito (Paymate)';
$_['text_unable']				= 'No se pudo ubicar o actualizar el estado de su pedido';
$_['text_declined']				= 'El pago fue rechazado por Paymate';
$_['text_failed']				= 'Transacción Paymate fallida';
$_['text_failed_message']		= '<p>Desafortunadamente hubo un error al procesar su transacción Paymate.</p><p><b>Advertencia: </b>%s</p><p>Por favor verifique el balance de su cuenta Paymate antes de intentar re-procesar este pedido</p><p> Si cree que esta transacción fue completada exitosamente, o se esta listando como una deducción en su cuenta Paymate, por favor <a href="%s">contáctenos</a> con sus detalles de pedido.</p>';
$_['text_basket']				= 'Canasta';
$_['text_checkout']				= 'Proceso de compra en línea';
$_['text_success']				= 'Éxito';