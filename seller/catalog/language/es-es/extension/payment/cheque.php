<?php
// Texto
$_['text_title']       = 'Pedido Cheque / Dinero Efectivo';
$_['text_instruction'] = 'Pedido Instrucciones para Cheque / Dinero Efectivo';
$_['text_payable']     = 'Hacer el pago a nombre de: ';
$_['text_address']     = 'Enviar a: ';
$_['text_payment']     = 'Su pedido no será enviado, hasta recibir el pago.';