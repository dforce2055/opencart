<?php

// Texto
$_['text_title']												= 'Pagar con Tarjeta de Crédito (eWAY)';
$_['text_credit_card']											= 'Detalles de tarjeta de Crédito';
$_['text_testing']												= 'Este portal de pago esta siendo probado. Su tarjeta de crédito no será cargada.<br />Si éste es un pedido real, por favor utilice otro método de pago por el momento.';

$_['text_basket']												= 'Canasta';
$_['text_checkout']												= 'Comprar';
$_['text_success']												= 'Éxito';
$_['text_shipping']												= 'Envío';

// Entry
$_['entry_cc_number']											= 'Número de tarjeta';
$_['entry_cc_name']												= 'Nombre del titular';
$_['entry_cc_expire_date']										= 'Fecha de expiración de tarjeta';
$_['entry_cc_cvv2']												= 'Código de seguridad de tarjeta (CVV2)';

$_['button_pay']												= 'Pagar ahora';

$_['text_card_accepted']										= 'Tarjetas aceptadas: ';
$_['text_card_type_m']											= 'Mastercard';
$_['text_card_type_v']											= 'Visa (Crédito/Débito/Electron/Delta)';
$_['text_card_type_c']											= 'Diners';
$_['text_card_type_a']											= 'American Express';
$_['text_card_type_j']											= 'JCB';
$_['text_card_type_pp']											= 'Paypal';
$_['text_card_type_mp']											= 'Masterpass';
$_['text_card_type_vm']											= 'Proceso de compra en línea con Visa';
$_['text_type_help']											= 'Tras clickear "Confirmar Pedido" será redirigido a ';

$_['text_transaction_failed']									= 'Lo sentimos, su pago ha sido rechazado.';

// Help
$_['help_cvv']													= 'Para MasterCard o Visa, estos son los tres últimos dígitos en el área de firma en el reverso de su tarjeta.';
$_['help_cvv_amex']												= 'Para American Express, son los cuatro dígitos en el frente de la tarjeta.';

// Validation Error codes
$_['text_card_message_Por favor check the API Key y Contraseña']   = 'Por favor verifique su API Key y Contraseña';

$_['text_card_message_V6000']									= 'Error de validación indefinido';
$_['text_card_message_V6001']									= 'IP de cliente inválida';
$_['text_card_message_V6002']									= 'DeviceID inválido';
$_['text_card_message_V6011']									= 'Monto inválido';
$_['text_card_message_V6012']									= 'Descripción de factura inválida';
$_['text_card_message_V6013']									= 'Número de factura inválido';
$_['text_card_message_V6014']									= 'Referencia de factura inválida';
$_['text_card_message_V6015']									= 'Código de moneda inválido';
$_['text_card_message_V6016']									= 'Pago necesario';
$_['text_card_message_V6017']									= 'Código de moneda de pago necesario';
$_['text_card_message_V6018']									= 'Código de moneda de pago desconocido';
$_['text_card_message_V6021']									= 'Nombre de titular necesario';
$_['text_card_message_V6022']									= 'Número de tarjeta Necesario';
$_['text_card_message_V6023']									= 'CVN Necesario';
$_['text_card_message_V6031']									= 'Número de tarjeta inválido';
$_['text_card_message_V6032']									= 'CVN inválido';
$_['text_card_message_V6033']									= 'Fecha de expiración inválida';
$_['text_card_message_V6034']									= 'Número de emisión inválido';
$_['text_card_message_V6035']									= 'Fecha inicial inválida';
$_['text_card_message_V6036']									= 'Mes inválido';
$_['text_card_message_V6037']									= 'Año inválido';
$_['text_card_message_V6040']									= 'Token Id Cliente inválido';
$_['text_card_message_V6041']									= 'Cliente Necesario';
$_['text_card_message_V6042']									= 'Nombre de Cliente Necesario';
$_['text_card_message_V6043']									= 'Apellido de Cliente Necesario';
$_['text_card_message_V6044']									= 'Código de País de Cliente Necesario';
$_['text_card_message_V6045']									= 'Título de Cliente Necesario';
$_['text_card_message_V6046']									= 'Token ID Cliente Necesario';
$_['text_card_message_V6047']									= 'URL Redirección Necesaria';
$_['text_card_message_V6051']									= 'Nombre inválido';
$_['text_card_message_V6052']									= 'Apellido inválido';
$_['text_card_message_V6053']									= 'Código de País inválido';
$_['text_card_message_V6054']									= 'Email inválido';
$_['text_card_message_V6055']									= 'Teléfono inválido';
$_['text_card_message_V6056']									= 'Teléfono Celular inválido';
$_['text_card_message_V6057']									= 'Fax inválido';
$_['text_card_message_V6058']									= 'Título inválido';
$_['text_card_message_V6059']									= 'URL redirección inválida';
$_['text_card_message_V6060']									= 'URL redirección inválida';
$_['text_card_message_V6061']									= 'Referencia inválida';
$_['text_card_message_V6062']									= 'Nombre de Compañía inválido';
$_['text_card_message_V6063']									= 'Descripción de trabajo inválida';
$_['text_card_message_V6064']									= 'Calle1 inválida';
$_['text_card_message_V6065']									= 'Calle2 inválida';
$_['text_card_message_V6066']									= 'Ciudad inválida';
$_['text_card_message_V6067']									= 'Estado inválido';
$_['text_card_message_V6068']									= 'Código postal inválido';
$_['text_card_message_V6069']									= 'Email inválido';
$_['text_card_message_V6070']									= 'Teléfono inválido';
$_['text_card_message_V6071']									= 'Teléfono Celular inválido';
$_['text_card_message_V6072']									= 'Commentarios inválidos';
$_['text_card_message_V6073']									= 'Fax inválido';
$_['text_card_message_V6074']									= 'Url inválida';
$_['text_card_message_V6075']									= 'Nombre de dirección de envío inválido';
$_['text_card_message_V6076']									= 'Apellido de dirección de envío inválido';
$_['text_card_message_V6077']									= 'Calle1 de dirección de envío inválida';
$_['text_card_message_V6078']									= 'Calle2 de dirección de envío inválida';
$_['text_card_message_V6079']									= 'Ciudad de dirección de envío inválida';
$_['text_card_message_V6080']									= 'Estado de dirección de envío inválido';
$_['text_card_message_V6081']									= 'Código postal de dirección de envío inválido';
$_['text_card_message_V6082']									= 'Email de dirección de envío inválido';
$_['text_card_message_V6083']									= 'Teléfono de dirección de envío inválido';
$_['text_card_message_V6084']									= 'País de dirección de envío inválido';
$_['text_card_message_V6091']									= 'Código de país desconocido';
$_['text_card_message_V6100']									= 'Nombre de Tarjeta inválido';
$_['text_card_message_V6101']									= 'Mes de expiración de tarjeta inválido';
$_['text_card_message_V6102']									= 'Año de expiración de tarjeta inválido';
$_['text_card_message_V6103']									= 'Mes inicial de tarjeta inválido';
$_['text_card_message_V6104']									= 'Año inicial de tarjeta inválido';
$_['text_card_message_V6105']									= 'Número de emisión de tarjeta inválido';
$_['text_card_message_V6106']									= 'CVN De Tarjeta inválido';
$_['text_card_message_V6107']									= 'Código de acceso inválido';
$_['text_card_message_V6108']									= 'CustomerHostAddress inválido';
$_['text_card_message_V6109']									= 'UserAgent inválido';
$_['text_card_message_V6110']									= 'Número de tarjeta inválidoo';
$_['text_card_message_V6111']									= 'Acceso API no autorizado, Cuenta no certificada con PCI';
$_['text_card_message_V6112']									= 'Datos de tarjeta redundantes aparte de año y mes de expiración';
$_['text_card_message_V6113']									= 'Transacción inválida para reembolso';
$_['text_card_message_V6114']									= 'Error de validación de Gateway';
$_['text_card_message_V6115']									= 'DirectRefundRequest inválido, ID de Transacción';
$_['text_card_message_V6116']									= 'Datos de tarjeta inválidos en ID de Transacción original';
$_['text_card_message_V6124']									= 'Items de línea inválidos. Los ítems de línea fueron provistos pero los totales no coinciden con el campo MontoTotal.';
$_['text_card_message_V6125']									= 'Método de pago seleccionado no activado.';
$_['text_card_message_V6126']									= 'Número de tarjeta cifrado inválido, descifrado fallido.';
$_['text_card_message_V6127']									= 'cvn cifrado inválido, descifrado fallido.';
$_['text_card_message_V6128']									= 'Método inválido para el tipo de pago.';
$_['text_card_message_V6129']									= 'La transacción no fue autorizada para Captura/Cancelación.';
$_['text_card_message_V6130']									= 'Error de información de cliente genérico.';
$_['text_card_message_V6131']									= 'Error de información de envío genérico.';
$_['text_card_message_V6132']									= 'La Transacción ya fue completada o anulada, operación no permitida.';
$_['text_card_message_V6133']									= 'Proceso de compra no disponible para este tipo de pago.';
$_['text_card_message_V6134']									= ' ID de transacción Auth Inválido para Captura/Anulación';
$_['text_card_message_V6135']									= 'Error de PayPal al procesar reembolso';
$_['text_card_message_V6140']									= 'Cuenta Vendedor suspendida.';
$_['text_card_message_V6141']									= 'Detalles de cuenta Paypal o firma de API inválidos';
$_['text_card_message_V6142']									= 'Authorización no disponible para Banco/Sucursal';
$_['text_card_message_V6150']									= 'Monto de reembolso inválido';
$_['text_card_message_V6151']									= 'Monto de reembolso mayor a transacción original';

// Payment failure messages
$_['text_card_message_D4401']									= 'Referir a emisor';
$_['text_card_message_D4402']									= 'Referir a emisor, especial';
$_['text_card_message_D4403']									= 'No vendedor';
$_['text_card_message_D4404']									= 'Pick Up Card';
$_['text_card_message_D4405']									= 'No honrar';
$_['text_card_message_D4406']									= 'Error';
$_['text_card_message_D4407']									= 'Pick Up Card, Especial';
$_['text_card_message_D4409']									= 'Pedido en progreso';
$_['text_card_message_D4412']									= 'Transacción inválida';
$_['text_card_message_D4413']									= 'Monto inválido';
$_['text_card_message_D4414']									= 'Número de tarjeta inválido';
$_['text_card_message_D4415']									= 'Sin emisor';
$_['text_card_message_D4419']									= 'Re-ingresar última Transacción';
$_['text_card_message_D4421']									= 'No se tomó ningún método';
$_['text_card_message_D4422']									= 'Sospecha de falla';
$_['text_card_message_D4423']									= 'Tasa de transacción inaceptable';
$_['text_card_message_D4425']									= 'No se puede ubicar registro en archivo';
$_['text_card_message_D4430']									= 'Error de formato';
$_['text_card_message_D4431']									= 'Banco no soportado por Switch';
$_['text_card_message_D4433']									= 'Tarjeta expirada, Captura';
$_['text_card_message_D4434']									= 'Fraude sospechado, Retener Tarjeta';
$_['text_card_message_D4435']									= 'Tarjeta Acceptor, Contact Acquirer, Retain Card';
$_['text_card_message_D4436']									= 'Tarjeta restringida, Retener Tarjeta';
$_['text_card_message_D4437']									= 'Contact Acquirer Security Department, Retain Card';
$_['text_card_message_D4438']									= 'Exceso de intentos de PIN, Captura';
$_['text_card_message_D4439']									= 'Cuenta sin crédito';
$_['text_card_message_D4440']									= 'Función no soportada';
$_['text_card_message_D4441']									= 'Tarjeta perdida';
$_['text_card_message_D4442']									= 'Sin cuenta universal';
$_['text_card_message_D4443']									= 'Tarjeta robada';
$_['text_card_message_D4444']									= 'Sin cuenta de inversión';
$_['text_card_message_D4451']									= 'Fondos insuficientes';
$_['text_card_message_D4452']									= 'Sin cuenta de cheque';
$_['text_card_message_D4453']									= 'Sin cuenta de ahorro';
$_['text_card_message_D4454']									= 'Tarjeta expirada';
$_['text_card_message_D4455']									= 'PIN incorrecto';
$_['text_card_message_D4456']									= 'Sin registro de tarjeta';
$_['text_card_message_D4457']									= 'Función no permitida a titular';
$_['text_card_message_D4458']									= 'Función no permitida a terminal';
$_['text_card_message_D4460']									= 'Acceptor Contact Acquirer';
$_['text_card_message_D4461']									= 'Excede límite de retiro';
$_['text_card_message_D4462']									= 'Tarjeta restringida';
$_['text_card_message_D4463']									= 'Violación de seguridad';
$_['text_card_message_D4464']									= 'Monto Original Incorrecto';
$_['text_card_message_D4466']									= 'Acceptor Contact Acquirer, Seguridad';
$_['text_card_message_D4467']									= 'Capture Card';
$_['text_card_message_D4475']									= 'Intentos de PIN excedidos';
$_['text_card_message_D4482']									= 'Error de validación de CVV';
$_['text_card_message_D4490']									= 'Cutoff en progreso';
$_['text_card_message_D4491']									= 'Emisor de tarjeta no disponible';
$_['text_card_message_D4492']									= 'No se pudo rutear la transacción';
$_['text_card_message_D4493']									= 'No se puede completar, Violación de la ley';
$_['text_card_message_D4494']									= 'Transacción duplicada';
$_['text_card_message_D4496']									= 'Error de sistema';
$_['text_card_message_D4497']									= 'MasterPass Error Fallido';
$_['text_card_message_D4498']									= 'Crear Transacción de PayPal Error Fallido';
$_['text_card_message_D4499']									= 'Transaccióñ inválida para autorización/anulación';

$_['text_card_message_F7000']									= 'Error de fraude indefinido';
$_['text_card_message_F7001']									= 'Fraude disputado';
$_['text_card_message_F7002']									= 'Coincidencia de Fraude: País';
$_['text_card_message_F7003']									= 'País de Alto Riesgo de Fraude';
$_['text_card_message_F7004']									= 'Fraude de Proxy Anónimo';
$_['text_card_message_F7005']									= 'Fraude de Proxy Transparente';
$_['text_card_message_F7006']									= 'Fraude de Email Gratis';
$_['text_card_message_F7007']									= 'Fraude de transacción internacional';
$_['text_card_message_F7008']									= 'Fraude puntuación de riesgo';
$_['text_card_message_F7009']									= 'Fraude denegado';
$_['text_card_message_F7010']									= 'Denegado por reglas de fraude de PayPal';
$_['text_card_message_F9010']									= 'País de facturación de alto riesgo';
$_['text_card_message_F9011']									= 'Paíós de Tarjeta de Crédito de alto riesgo';
$_['text_card_message_F9012']									= 'Dirección IP de cliente de alto riesgo';
$_['text_card_message_F9013']									= 'Dirección EMail de alto riesgo';
$_['text_card_message_F9014']									= 'País de envío de alto riesgo';
$_['text_card_message_F9015']									= 'Múltiples números de tarjeta para un mismo email';
$_['text_card_message_F9016']									= 'Múltiples números de tarjeta para una misma ubicación';
$_['text_card_message_F9017']									= 'Múltiples emails para una misma tarjeta';
$_['text_card_message_F9018']									= 'Múltiples emails para una misma ubicación';
$_['text_card_message_F9019']									= 'Múltiples ubicaciones para una misma tarjeta';
$_['text_card_message_F9020']									= 'Múltiples ubicaciones para un mismo email';
$_['text_card_message_F9021']									= 'Nombre de cliente sospechoso';
$_['text_card_message_F9022']									= 'Apellido de Cliente sospechoso';
$_['text_card_message_F9023']									= 'Transacción rechazada';
$_['text_card_message_F9024']									= 'Multiples transacciones para misma dirección con tarjeta de crédito conocida';
$_['text_card_message_F9025']									= 'Multiples transacciones para misma dirección con tarjeta de crédito nueva';
$_['text_card_message_F9026']									= 'Multiples transacciones para mismo email con tarjeta de crédito nueva';
$_['text_card_message_F9027']									= 'Multiples transacciones para mismo email con tarjeta de crédito conocida';
$_['text_card_message_F9028']									= 'Multiples transacciones para nueva tarjeta de crédito';
$_['text_card_message_F9029']									= 'Multiples transacciones para tarjeta de crédito conocida ';
$_['text_card_message_F9030']									= 'Multiples transacciones para mismo email';
$_['text_card_message_F9031']									= 'Multiples transacciones para misma tarjeta de crédito';
$_['text_card_message_F9032']									= 'Apellido de Cliente inválido';
$_['text_card_message_F9033']									= 'Calle de facturación inválida';
$_['text_card_message_F9034']									= 'Calle de envío inválida';
$_['text_card_message_F9037']									= 'Dirección de Email de cliente sospechosa';