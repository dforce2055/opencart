<?php
$_['text_checkout_title']      = 'Pagar en cuotas';
$_['text_choose_plan']         = 'Seleccione su plan';
$_['text_choose_deposit']      = 'Seleccione su depósito';
$_['text_monthly_payments']    = 'pagos mensuales de';
$_['text_months']              = 'meses';
$_['text_term']                = 'plazo';
$_['text_deposit']             = 'Depósito';
$_['text_credit_amount']       = 'Costo de crédito';
$_['text_amount_payable']      = 'Total pagable';
$_['text_total_interest']      = 'Total interés APR';
$_['text_monthly_installment'] = 'Pago mensual';
$_['text_redirection']         = 'Será redirigido a Divido para completar esta aplicación de financiamiento cuando confirme su pedido.';
$_['divido_checkout']          = 'Confirmar y comprar con Divido';
$_['deposit_to_low']           = 'Depósito demasiado bajo';
$_['credit_amount_to_low']     = 'Monto de crédito demasiado bajo';
$_['no_country']               = 'País no aceptado';
