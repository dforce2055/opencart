<?php
// Texto
$_['text_title']				= 'Tarjeta de Crédito o Débito';
$_['text_credit_card']			= 'Detalles de tarjeta de Crédito';
$_['text_wait']					= '¡Por favor espere!';
$_['text_result']				= 'Resultado';
$_['text_message']				= 'Mensaje';
$_['text_cvn_result']			= 'Resultado CVN';
$_['text_avs_postcode']			= 'Código postal AVS';
$_['text_avs_address']			= 'Dirección AVS';
$_['text_eci']					= 'Resultado ECI (3D secure)';
$_['text_tss']					= 'Resultado TSS';
$_['text_card_bank']			= 'Banco emisor de tarjeta';
$_['text_card_country']			= 'País de tarjeta';
$_['text_card_region']			= 'Región de tarjeta';
$_['text_last_digits']			= 'Últimos 4 dígitos';
$_['text_order_ref']			= 'ref. pedido';
$_['text_timestamp']			= 'Timestamp';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';
$_['text_auth_code']			= 'Código autorización';
$_['text_3d_s1']				= 'Titular no inscripto, inversión de responsabilidades';
$_['text_3d_s2']				= 'No se pudo verificar inscripción, sin inversión de responsabilidades';
$_['text_3d_s3']				= 'Respuesta inválida de servidor de inscripción, sin inversión de responsabilidades';
$_['text_3d_s4']				= 'Inscripto, pero respuesta inválida de ACS (Servidor de Control de Acceso), sin inversión de responsabilidades';
$_['text_3d_s5']				= 'Autenticación exitosa, inversión de responsabilidades';
$_['text_3d_s6']				= 'Intento de autenticación reconocido, inversión de responsabilidades';
$_['text_3d_s7']				= 'Contraseña incorrecta ingresada, sin inversión de responsabilidades';
$_['text_3d_s8']				= 'Autenticación no disponible, sin inversión de responsabilidades';
$_['text_3d_s9']				=  'Respuesta inválida de ACS, sin inversión de responsabilidades';
$_['text_3d_s10']				= 'Error fatal RealMPI, sin inversión de responsabilidades';

// Entry
$_['entry_cc_type']				= 'Tipo de tarjeta';
$_['entry_cc_number']			= 'Número de tarjeta';
$_['entry_cc_name']				= 'Nombre del titular';
$_['entry_cc_expire_date']		= 'Fecha de expiración de tarjeta';
$_['entry_cc_cvv2']				= 'Código de seguridad de tarjeta(CVV2)';
$_['entry_cc_issue']			= 'Número de emisión de tarjeta';

// Help
$_['help_start_date']			= '(si está disponible)';
$_['help_issue']				= '(sólo para tarjetas Maestro y Solo)';

// Error
$_['error_card_number']			= 'Por favor verifique que su número de tarjeta es válido';
$_['error_card_name']			= 'Por favor verifique que el nombre del titular de tarjeta es válido';
$_['error_card_cvv']			= 'Por favor verifique que el CVV2 es válido';
$_['error_3d_unable']			= 'Vendedor requiere 3D secure pero no se pudo verificar con su banco, por favor intente de nuevo más tarde';
$_['error_3d_500_response_no_payment'] = 'Se recibió una respuesta inválida del Procesador de Tarjeta, no se realizó ningún pago';
$_['error_3d_unsuccessful']		= 'Falló la autorización 3D secure';