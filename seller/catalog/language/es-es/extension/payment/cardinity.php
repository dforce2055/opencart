<?php
// Texto
$_['text_title']						= 'Tarjeta de Crédito / Débito (Cardinity)';
$_['text_payment_success']				= 'Pago exitoso, detalles debajo';
$_['text_payment_failed']				= 'Pago fallido, detalles debajo';

// Entry
$_['entry_holder']						= 'Nombre de titular';
$_['entry_pan']							= 'Número de tarjeta';
$_['entry_expires']						= 'Expira';
$_['entry_exp_month']					= 'Mes';
$_['entry_exp_year']					= 'Año';
$_['entry_cvc']							= 'CVC';

// Error
$_['error_process_order']				= 'Ocurrió un error al procesar su pedido. Por favor contacte al administrador de la tienda por ayuda.';
$_['error_invalid_currency']			= 'Por favor use una moneda válida.';
$_['error_finalizing_payment']			= 'Error al finalizar pago.';
$_['error_unknown_order_id']			= 'No se encontró un pago de cardinity con éste order_id.';
$_['error_invalid_hash']				= 'Hash inválido.';
$_['error_payment_declined']			= 'Pago rechazado por banco emisor.';

// Button
$_['button_confirm']					= 'Pagar ahora';