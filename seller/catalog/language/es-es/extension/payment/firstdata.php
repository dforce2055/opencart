<?php
// Heading
$_['text_title']				= 'Tarjeta de Crédito / Débito (First Datos)';

// Button
$_['button_confirm']			= 'Continuar';

// Texto
$_['text_new_card']				= 'Nueva tarjeta';
$_['text_store_card']			= 'Recordar mis detalles de tarjeta';
$_['text_address_response']		= 'Verificación de Dirección: ';
$_['text_address_ppx']			= 'Dirección no provista o Dirección no verificada por Emisor de tarjeta';
$_['text_address_yyy']			= 'Emisor de tarjeta confirmó que calle y código postal coinciden con sus registros';
$_['text_address_yna']			= 'Emisor de tarjeta confirmó que calle coincide con sus registros pero código postal no coincide';
$_['text_address_nyz']			= 'Emisor de tarjeta confirmó que código postal coincide con sus registros pero calle no coincide';
$_['text_address_nnn']			= 'Ambos calle y código postal no coinciden con los registros del Emisor de tarjeta';
$_['text_address_ypx']			= 'Emisor de tarjeta confirmó que calle coincide con sus registros. El emisor no verificó el código postal';
$_['text_address_pyx']			= 'Emisor de tarjeta confirmó que código postal coincide con sus registros. El emisor no verificó la dirección';
$_['text_address_xxu']			= 'Emisor de tarjeta no verificó la información AVS';
$_['text_card_code_verify']		= 'Código de seguridad: ';
$_['text_card_code_m']			= 'Código de seguridad de tarjeta coincide';
$_['text_card_code_n']			= 'Código de seguridad de tarjeta no coincide';
$_['text_card_code_p']			= 'No procesado';
$_['text_card_code_s']			= 'Vendedor ha indicado que el Código de seguridad de tarjeta no está presente en la misma';
$_['text_card_code_u']			= 'El emisor no está certificado y/o no proveyó claves de encripción.';
$_['text_card_code_x']			= 'No se recibió respuesta de asociación de tarjeta de crédito';
$_['text_card_code_blank']		= 'Una respuesta en blanco debería indicar que no se envió ningún código y que no hubo indicaciones de que el código no estaba presente en la tarjeta.';
$_['text_card_type_m']			= 'Mastercard';
$_['text_card_type_v']			= 'Visa (Crédito/Débito/Electron/Delta)';
$_['text_card_type_c']			= 'Diners';
$_['text_card_type_a']			= 'American Express';
$_['text_card_type_ma']			= 'Maestro';
$_['text_card_type_mauk']		= 'Maestro UK/Solo';
$_['text_response_code_full']	= 'Código de aprobación: ';
$_['text_response_code']		= 'Código de respuesta completo: ';
$_['text_response_card']		= 'Tarjeta usada: ';
$_['text_response_card_type']	= 'Tipo de tarjeta: ';
$_['text_response_proc_code']	= 'Código Procesador: ';
$_['text_response_ref']			= 'Número de referencia: ';

// Error
$_['error_failed']				= 'No se pudo procesar su pago, por favor intente de nuevo';