<?php
// Texto
$_['text_title']	= 'PayPal';
$_['text_testmode']	= 'Advertencia: El portal de pago está en \'Modo Sandbox\'. Su cuenta no será cargada.';
$_['text_total']	= 'Envío, manejo, descuentos & impuestos';