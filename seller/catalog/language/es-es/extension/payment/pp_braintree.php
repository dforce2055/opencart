<?php
// Texto
$_['text_title']						= 'Tarjetas o PayPal';
$_['text_express_title']      			= 'Confirmar pedido';
$_['text_vaulted_payment_method_name']	= '%s terminando en %s, expira %s';
$_['text_remember']						= '¿Guardar para la próxima vez?';
$_['text_remove']						= 'Remover';
$_['text_remove_confirm']				= '¿Está seguro de que desea remover el método de pago guardado?';
$_['text_month']						= 'Mes';
$_['text_year']							= 'Año';
$_['text_loading']						= 'Cargando...';
$_['text_new_method']					= 'Nuevo método de pago';
$_['text_saved_method']					= 'Seleccionar un método de pago guardado';
$_['text_paypal']						= 'PayPal';
$_['text_pay_by_paypal']				= 'Pagar con PayPal';
$_['text_method_removed']				= '¡Método de pago ha sido removido';
$_['text_method_not_removed']			= 'No se pudo remover el método de pago';
$_['text_authentication']				= 'Autenticación';
$_['text_cart']               			= 'Carrito de compras';
$_['text_shipping_updated']   			= 'Servicio de envío actualizado';

// Entry
$_['entry_new']							= 'Nuevo método de pago';
$_['entry_saved_methods']				= 'Método de pago guardado';
$_['entry_card']						= 'Número de tarjeta';
$_['entry_expires']						= 'Expira el';
$_['entry_cvv']							= 'CVV';
$_['entry_remember_card_method']		= '¿Recordar tarjeta?';
$_['entry_remember_paypal_method']		= '¿Recordar cuenta de PayPal?';
$_['entry_card_placeholder']			= 'Número de tarjeta';
$_['entry_month_placeholder']			= 'Mes (MM)';
$_['entry_year_placeholder']			= 'Año (YYYY)';
$_['entry_cvv_placeholder']				= 'CVV2';

// Error
$_['error_process_order']				= 'Ocurrió un error al procesar su pedido. Por favor contacte al administrador de la tienda por ayuda.';
$_['error_alert_fields_empty']			= 'Todos los campos están vacíos! Por favor llene el formulario';
$_['error_alert_fields_invalid']		= 'Algunos campos son inválidos, por favor verifique su información';
$_['error_alert_failed_token']			= 'Nuestro proveedor de pagos no reconoció la tarjeta. ¿Es válida la tarjeta?';
$_['error_alert_failed_network']		= 'Ocurrió un error de red, por favor intente de nuevo';
$_['error_alert_unknown']				= 'Ocurrió un error desconocido, si el problema persiste por favor contacte al soporte';
$_['error_unavailable'] 	  			= 'Por favor use el Proceso de compra en línea completo con este pedido';
$_['error_no_shipping']    				= 'Advertencia: No hay opciones de envío disponibles.';

// Button
$_['button_confirm']					= 'Pagar Ahora';
$_['button_express_confirm']  			= 'Confirmar';
$_['button_express_shipping'] 			= 'Actualizar envío';
