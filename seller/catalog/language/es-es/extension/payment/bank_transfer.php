<?php
// Texto
$_['text_title']       = 'Transferencia Bancaria';
$_['text_instruction'] = 'Instrucciones de Transferencia Bancaria ';
$_['text_description'] = 'Por favor transfiera el total a la siguiente cuenta bancaria.';
$_['text_payment']     = 'Su pedido no sera enviado, hasta recibir el pago.';