<?php
// Heading
$_['text_title']				= 'Tarjeta de Crédito / Débito (Globalpay)';

// Button
$_['button_confirm']			= 'Confirmar';

// Entry
$_['entry_cc_type']				= 'Tipo de tarjeta';

// Texto
$_['text_success']				= 'Su pago ha sido autorizado.';
$_['text_decline']				= 'Su pago no fue exitoso.';
$_['text_bank_error']			= 'Ocurrió un error al procesar su pedido con el banco.';
$_['text_generic_error']		= 'Ocurrió un error al procesar su pedido.';
$_['text_hash_failed']			= 'Verificación de hash fallida. No intente pagar nuevamente puesto que el estado del pago es desconocido. Por favor contacte al vendedor.';
$_['text_link']					= 'Por favor haga click <a href="%s">aquí</a> para continuar';
$_['text_select_card']			= 'Por favor seleccione su Tipo de tarjeta';
$_['text_result']				= 'Resultado de auth';
$_['text_message']				= 'Mensaje';
$_['text_cvn_result']			= 'Resultado CVN';
$_['text_avs_postcode']			= 'Código postal AVS';
$_['text_avs_address']			= 'Dirección AVS';
$_['text_eci']					= 'Resultado ECI (3D secure)';
$_['text_tss']					= 'Resultado TSS';
$_['text_order_ref']			= 'ref. pedido';
$_['text_timestamp']			= 'Hora';
$_['text_card_type']			= 'Tipo de tarjeta';
$_['text_card_digits']			= 'Número de tarjeta';
$_['text_card_exp']				= 'Expiración de tarjeta';
$_['text_card_name']			= 'Nombre de tarjeta';
$_['text_3d_s1']				= 'Titular no inscripto, inversión de responsabilidades';
$_['text_3d_s2']				= 'No se pudo verificar inscripción, sin inversión de responsabilidades';
$_['text_3d_s3']				= 'Respuesta inválida de servidor de inscripción, sin inversión de responsabilidades';
$_['text_3d_s4']				= 'Inscripto, pero respuesta inválida de ACS (Servidor de Control de Acceso), sin inversión de responsabilidades';
$_['text_3d_s5']				= 'Autenticación exitosa, inversión de responsabilidades';
$_['text_3d_s6']				= 'Intento de autenticación reconocido, inversión de responsabilidades';
$_['text_3d_s7']				= 'Contraseña incorrecta ingresada, sin inversión de responsabilidades';
$_['text_3d_s8']				= 'Autenticación no disponible, sin inversión de responsabilidades';
$_['text_3d_s9']				= 'Respuesta inválida de ACS, sin inversión de responsabilidades';
$_['text_3d_s10']				= 'Error fatal RealMPI, sin inversión de responsabilidades';
$_['text_3d_liability']     	= 'Sin inversión de responsabilidades';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';