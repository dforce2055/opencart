<?php
// Heading
$_['express_text_title']      = 'Confirmar pedido';

// Texto
$_['text_title']              = 'Proceso de compra en línea PayPal Express ';
$_['text_cart']               = 'Carrito de compras';
$_['text_shipping_updated']   = 'Servicio de envío actualizado';
$_['text_trial']              = '%s cada %s %s por %s pagos entonces ';
$_['text_recurring']          = '%s cada %s %s';
$_['text_recurring_item']     = 'Item Recurrente';
$_['text_length']             = ' para %s pagos';

// Entry
$_['express_entry_coupon']    = 'Ingrese su cupón aquí:';

// Button
$_['button_express_coupon']   = 'Agregar';
$_['button_express_confirm']  = 'Confirmar';
$_['button_express_login']    = 'Continuar a PayPal';
$_['button_express_shipping'] = 'Actualizar envío';

// Error
$_['error_heading_title']	  = 'Ocurrió un error';
$_['error_too_many_failures'] = 'Su pago ha fallado demasiadas veces';
$_['error_unavailable'] 	  = 'Por favor use el Proceso de compra en línea completo con este pedido';
$_['error_no_shipping']    	  = 'Advertencia: ¡No hay opciones de envío disponibles. Por favor <a href="%s">contáctenos</a> por asistencia!';
