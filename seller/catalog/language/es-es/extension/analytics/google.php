<?php
$_['heading_title']    = 'Google Analytics';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']	   = 'Éxito: Has modificado Google Analytics!';
$_['text_edit']        = 'Edit Google Analytics';
$_['text_signup']      = 'Login to your <a href="http://www.google.com/analytics/" target="_blank"><u>Google Analytics</u></a> account and after creating your website profile copy and paste the analytics code into this field.';
$_['text_default']     = 'Defecto';

// Entry
$_['entry_code']       = 'Google Analytics Código';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar Google Analytics!';
$_['error_code']	   = 'Código required!';
