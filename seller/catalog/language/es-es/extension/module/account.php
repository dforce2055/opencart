<?php
// Heading
$_['heading_title']    = 'Cuenta';

// Texto
$_['text_register']    = 'Regístrese';
$_['text_login']       = 'Ingresar';
$_['text_logout']      = 'Salir';
$_['text_forgotten']   = 'Olvidó su Clave';
$_['text_account']     = 'Mi Cuenta';
$_['text_edit']        = 'Editar Cuenta';
$_['text_password']    = 'Clave';
$_['text_address']     = 'Direcciones';
$_['text_wishlist']    = 'Lista de Deseos';
$_['text_order']       = 'Historial de órdenes';
$_['text_download']    = 'Descargas';
$_['text_reward']      = 'Puntos de recompensa';
$_['text_return']      = 'Devoluciones';
$_['text_transaction'] = 'Transacciones';
$_['text_newsletter']  = 'Novedades';
$_['text_recurring']   = 'Pagos recurrentes';