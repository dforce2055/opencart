<?php
// Heading
$_['heading_title'] = 'Pagar con Amazon';

// Texto
$_['text_module'] = 'Modulos';
$_['text_success'] = 'Éxito: ¡Ha modificado el modulo Pagar con Amazon!';
$_['text_content_top'] = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left'] = 'Columna izquierda';
$_['text_column_right'] = 'Columna derecha';
$_['text_pwa_button'] = 'Pagar con Amazon';
$_['text_pay_button'] = 'Pagar';
$_['text_a_button'] = 'A';
$_['text_gold_button'] = 'Dorado';
$_['text_darkgray_button'] = 'Gris oscuro';
$_['text_lightgray_button'] = 'Gris claro';
$_['text_small_button'] = 'Pequeño';
$_['text_medium_button'] = 'Mediano';
$_['text_large_button'] = 'Grande';
$_['text_x_large_button'] = 'X-Grande';

//Entry
$_['entry_button_type'] = 'Tipo de botón';
$_['entry_button_colour'] = 'Color de botón';
$_['entry_button_size'] = 'Tamaño de botón';
$_['entry_layout'] = 'Disposición';
$_['entry_position'] = 'Posición';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Ordenar Pedidos';

//Error
$_['error_permission'] = 'Advertencia: ¡No tiene permiso para modifcar el modulo Pagar con Amazon!';