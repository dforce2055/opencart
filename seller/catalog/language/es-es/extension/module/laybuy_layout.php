<?php
// Heading
$_['heading_title']            = 'Lay-Buy Información';

// Texto
$_['text_reference_info']      = 'Información de referencia';
$_['text_laybuy_ref_no']       = 'ID Referencia Lay-Buy:';
$_['text_paypal_profile_id']   = 'ID Perfil PayPal:';
$_['text_payment_plan']        = 'Plan de pago';
$_['text_status']              = 'Estado:';
$_['text_amount']              = 'Monto:';
$_['text_downpayment_percent'] = 'Porcentaje anticipo:';
$_['text_months']              = 'Meses:';
$_['text_downpayment_amount']  = 'Monto anticipo:';
$_['text_payment_amounts']     = 'Monto de pagos:';
$_['text_first_payment_due']   = 'Vencimiento primer pago:';
$_['text_last_payment_due']    = 'Vencimiento último pago:';
$_['text_downpayment']         = 'Anticipo:';
$_['text_month']               = 'Mes';

// Column
$_['column_instalment']        = 'Cuota';
$_['column_amount']            = 'Monto';
$_['column_date']              = 'Fecha';
$_['column_pp_trans_id']       = 'ID Transacción PayPal';
$_['column_status']            = 'Estado';