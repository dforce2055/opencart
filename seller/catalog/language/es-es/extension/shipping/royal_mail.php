<?php
// Texto
$_['text_title']                        = 'Royal Mail';
$_['text_weight']                       = 'Peso:';
$_['text_insurance']                    = 'Asegurado hasta:';
$_['text_special_delivery']             = 'Entrega especial al día siguiente';
$_['text_1st_class_signed']             = 'First Clase Signed Post';
$_['text_2nd_class_signed']             = 'Second Clase Signed Post';
$_['text_1st_class_standard']           = 'First Clase Standard Post';
$_['text_2nd_class_standard']           = 'Second Clase Standard Post';
$_['text_international_standard']       = 'International Standard';
$_['text_international_tracked_signed'] = 'International Tracked & Signed';
$_['text_international_tracked']        = 'International Tracked';
$_['text_international_signed']         = 'International Signed';
$_['text_international_economy']        = 'International Economy';