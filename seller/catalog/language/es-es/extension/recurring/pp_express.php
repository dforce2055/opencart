<?php
// Texto
$_['text_title']          = 'Proceso de compra en línea PayPal Express ';
$_['text_canceled']       = 'Éxito: ¡Ha cancelado el pago exitosamente!';

// Button
$_['button_cancel']       = 'Cancelar pago recurrente';

// Error
$_['error_not_cancelled'] = 'Error: %s';
$_['error_not_found']     = 'No se pudo cancelar perfil recurrente';