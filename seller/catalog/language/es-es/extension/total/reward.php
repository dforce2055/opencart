<?php
// Heading
$_['heading_title'] = 'Use Puntos de Recompensa (Disponibles %s)';

// Texto
$_['text_reward']   = 'Puntos de Recompensa (%s)';
$_['text_order_id'] = 'ID de pedido: #%s';
$_['text_success']  = 'Éxito: ¡Sus puntos de recompensa fueron aplicados!';

// Entry
$_['entry_reward']  = 'Puntos a usar (Máximo %s)';

// Error
$_['error_reward']  = 'Advertencia: ¡Por favor ingrese la cantidad de puntos de recompensa a utilizar!';
$_['error_points']  = 'Advertencia: ¡No tiene %s puntos de recompensa!';
$_['error_maximum'] = 'Advertencia: ¡El máximo número de puntos de recompensa que puede aplicar son %s!';