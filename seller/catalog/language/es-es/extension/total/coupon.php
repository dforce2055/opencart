<?php
// Heading
$_['heading_title'] = 'Usar Código de Cupón';

// Texto
$_['text_coupon']   = 'Cupón (%s)';
$_['text_success']  = 'Éxito: ¡Su cupón de descuento fue aplicado!';

// Entry
$_['entry_coupon']  = 'Ingrese su cupón aquí';

// Error
$_['error_coupon']  = 'Advertencia: ¡El Cupón es invalido, expiró o alcanzó su límite de uso!';
$_['error_empty']   = 'Advertencia: ¡Por favor ingrese un código de cupón!';