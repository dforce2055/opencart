<?php
// Heading
$_['heading_title'] = 'Usar cupón de regalo';

// Texto
$_['text_voucher']  = 'Cupón de regalo (%s)';
$_['text_success']  = 'Éxito: ¡Su descuento por cupón de regalo fue aplicado!';

// Entry
$_['entry_voucher'] = 'Ingrese el código de su cupón de regalo aquí';

// Error
$_['error_voucher'] = 'Advertencia: ¡Cupón de regalo inválido o agotó su balance!';
$_['error_empty']   = 'Advertencia: ¡Por favor ingrese un código de cupón de regalo!';