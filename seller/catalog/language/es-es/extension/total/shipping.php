<?php
// Heading
$_['heading_title']        = 'Estimar envío &amp; Impuestos';

// Texto
$_['text_success']         = 'Éxito: ¡Su estimación de envíos fue aplicada!';
$_['text_shipping']        = 'Ingrese un destino para obtener una estimación de envío.';
$_['text_shipping_method'] = 'Por favor seleccione el método de envío preferido para utilizar con este pedido.';

// Entry
$_['entry_country']        = 'País';
$_['entry_zone']           = 'Provincia / Estado';
$_['entry_postcode']       = 'Código Postal';

// Error
$_['error_postcode']       = '¡El Código Postal debe tener entre 2 y 10 caracteres!';
$_['error_country']        = '¡Por favor seleccione un País!';
$_['error_zone']           = '¡Por favor seleccione Provincia / Estado!';
$_['error_shipping']       = 'Advertencia: ¡Método de envío necesario!';
$_['error_no_shipping']    = 'Advertencia: ¡No hay opciones de envío disponibles. Por favor <a href="%s">contáctenos</a> para ser asistido!';