<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/opencart/seller/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/opencart/seller/');

define('HTTP_SERVER1', 'http://localhost/opencart/');

define('HTTPS_SERVER1', 'http://localhost/opencart/');

// DIR
define('DIR_APPLICATION', '/var/www/html/opencart/seller/catalog/');
define('DIR_SYSTEM', '/var/www/html/opencart/seller/system/');
define('DIR_IMAGE', '/var/www/html/opencart/image/');
define('DIR_LANGUAGE', '/var/www/html/opencart/seller/catalog/language/');
define('DIR_TEMPLATE', '/var/www/html/opencart/seller/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/html/opencart/seller/system/config/');
define('DIR_CACHE', '/var/www/html/opencart/seller/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/html/opencart/seller/system/storage/download/');
define('DIR_LOGS', '/var/www/html/opencart/seller/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/html/opencart/seller/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/html/opencart/seller/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'lsfalv');
define('DB_DATABASE', 'opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
