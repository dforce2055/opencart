<?php
namespace Cart;
class Customer {
  private $customer_id;
  private $firstname;
  private $lastname;
  private $customer_group_id;
  private $email;
  private $telephone;
  private $newsletter;
  private $address_id;

  private $seller_id;
  private $username;
  private $sellerfirstname;
  private $sellerlastname;
  private $selleremail;
  private $sellertelephone;
  private $sellerfax;
  private $selleraddress_id;

  private $permissions = array();
  private $user_id;
  private $user_firstname;

  public function __construct($registry) {
    $this->config = $registry->get('config');
    $this->db = $registry->get('db');
    $this->request = $registry->get('request');
    $this->session = $registry->get('session');

    if (isset($this->session->data['customer_id'])) {
      $customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$this->session->data['customer_id'] . "' AND status = '1'");

      if ($customer_query->num_rows) {
        $this->customer_id = $customer_query->row['customer_id'];
        $this->firstname = $customer_query->row['firstname'];
        $this->lastname = $customer_query->row['lastname'];
        $this->customer_group_id = $customer_query->row['customer_group_id'];
        $this->email = $customer_query->row['email'];
        $this->telephone = $customer_query->row['telephone'];
        $this->newsletter = $customer_query->row['newsletter'];
        $this->address_id = $customer_query->row['address_id'];

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET language_id = '" . (int)$this->config->get('config_language_id') . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$this->session->data['customer_id'] . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

        if (!$query->num_rows) {
          $this->db->query("INSERT INTO " . DB_PREFIX . "customer_ip SET customer_id = '" . (int)$this->session->data['customer_id'] . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', date_added = NOW()");
        }
      } else {
        $this->logout();
      }
    }

    if (isset($this->session->data['seller_id'])) {
      $seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int)$this->session->data['seller_id'] . "' AND status = '1'");

      if ($seller_query->num_rows) {
        $this->seller_id = $seller_query->row['seller_id'];
        $this->sellerfirstname = $seller_query->row['firstname'];
        $this->sellerlastname = $seller_query->row['lastname'];
        $this->username = $seller_query->row['username'];
        $this->selleremail = $seller_query->row['email'];
        $this->sellertelephone = $seller_query->row['telephone'];
        $this->selleraddress_id = $seller_query->row['address_id'];

        $this->db->query("UPDATE " . DB_PREFIX . "sellers SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE seller_id = '" . (int)$this->seller_id . "'");

      } else {
        $this->sellerlogout();
      }
    }
  }

  public function sellerlogin($email, $password, $override = false) {
    if ($override) {
      $seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers where LOWER(email) = '" . $this->db->escape(strtolower($email)) . "' AND status = '1'");
    } else {
      $seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1' AND approved = '1'");
      //Si encuentro al vendedor, le asigno permisos de Vendedor_Administrador
      if ($seller_query)
        $this->session->data['permissions'] = $this->getSellerPermissions((int)$this->getSellerAdminId());

      //Si no encuentro el mail del vendedor, lo busco en la tabla de usuarios del vendedor
      //Y le asigno los permisos correspondientes
      if($seller_query->num_rows == 0)
      {
        $sellers_user = $this->db->query("SELECT * FROM oci_sellers_users WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "' AND password = '" . $this->db->escape(md5($password)) . "' AND status = '1'");
        if($sellers_user->num_rows)
        {
          $seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" .$sellers_user->row['seller_id'] . "'");
          //Al usuario del vendedor se le asigna permisos correspondientes segun
          //tabla oci_sellers_users.user_group_id
          $this->session->data['permissions'] = $this->getSellerPermissions($sellers_user->row['user_group_id']);
          $this->session->data['user_id'] = $sellers_user->row['oci_sellers_users_id'];
          $this->session->data['user_firstname'] = $sellers_user->row['firstname'];
          //$this->user_id = $sellers_user->row['oci_sellers_users_id'];
          //$this->user_firstname = $sellers_user->row['firstname'];
        }
      }
    }

    if ($seller_query->num_rows)
    {
      $this->session->data['seller_id'] = $seller_query->row['seller_id'];
      $this->seller_id = $seller_query->row['seller_id'];
      $this->username = $seller_query->row['username'];
      $this->sellerfirstname = $seller_query->row['firstname'];
      $this->sellerlastname = $seller_query->row['lastname'];
      $this->selleremail = $seller_query->row['email'];
      $this->sellertelephone = $seller_query->row['telephone'];
      $this->sellerfax = $seller_query->row['fax'];
      $this->selleraddress_id = $seller_query->row['address_id'];



      $this->db->query("UPDATE " . DB_PREFIX . "sellers SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE seller_id = '" . (int)$this->seller_id . "'");

        return true;
      } else {
          return false;
      }
    }


  public function login($email, $password, $override = false) {
    if ($override) {
      $customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND status = '1'");
    } else {
      $customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
    }

    if ($customer_query->num_rows) {
      $this->session->data['customer_id'] = $customer_query->row['customer_id'];

      $this->customer_id = $customer_query->row['customer_id'];
      $this->firstname = $customer_query->row['firstname'];
      $this->lastname = $customer_query->row['lastname'];
      $this->customer_group_id = $customer_query->row['customer_group_id'];
      $this->email = $customer_query->row['email'];
      $this->telephone = $customer_query->row['telephone'];
      $this->newsletter = $customer_query->row['newsletter'];
      $this->address_id = $customer_query->row['address_id'];

      $this->db->query("UPDATE " . DB_PREFIX . "customer SET language_id = '" . (int)$this->config->get('config_language_id') . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

      return true;
    } else {
      return false;
    }
  }

  public function logout() {
    unset($this->session->data['customer_id']);

    $this->customer_id = '';
    $this->firstname = '';
    $this->lastname = '';
    $this->customer_group_id = '';
    $this->email = '';
    $this->telephone = '';
    $this->newsletter = '';
    $this->address_id = '';
  }

  public function isLogged() {
    return $this->customer_id;
  }

  public function getUserId() {
    return $this->user_id;
  }

  public function getId() {
    return $this->customer_id;
  }

  public function getFirstName() {
    return $this->firstname;
  }

  public function getLastName() {
    return $this->lastname;
  }

  public function getGroupId() {
    return $this->customer_group_id;
  }

  public function getEmail() {
    return $this->email;
  }

  public function getTelephone() {
    return $this->telephone;
  }

  public function getNewsletter() {
    return $this->newsletter;
  }

  public function getAddressId() {
    return $this->address_id;
  }

  public function getBalance() {
    $query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$this->customer_id . "'");

    return $query->row['total'];
  }

  public function getRewardPoints() {
    $query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$this->customer_id . "'");

    return $query->row['total'];
  }



  public function sellerlogout() {
    unset($this->session->data['seller_id']);

    $this->seller_id = '';
    $this->username  = '';
    $this->sellerfirstname = '';
    $this->sellerlastname = '';
    $this->selleremail = '';
    $this->sellertelephone = '';
    $this->selleraddress_id = '';
    $this->user_firstname = '';
    $this->session->data['user_firstname'] = '';
    $this->session->data['permissions'] = '';
    }

    public function issellerLogged() {
      return $this->seller_id;
    }

    public function getsellerId() {
      return $this->seller_id;
    }

  public function getUserName() {
    return $this->username;
    }

    public function getsellerFirstName() {
    return $this->sellerfirstname;
    }

    public function getsellerLastName() {
    return $this->sellerlastname;
    }

    public function getsellerEmail() {
    return $this->selleremail;
    }

    public function getsellerTelephone() {
    return $this->sellertelephone;
    }


    public function getsellerAddressId() {
    return $this->selleraddress_id;
    }

    public function getsellerBalance() {
      $filter_eligible_status_id = $this->config->get('config_seller_payments');                if(!empty($filter_eligible_status_id))    {                    $filter_eligible_status_id = implode(",",$filter_eligible_status_id);                }else{                  $filter_eligible_status_id = 0;                }            $seller_id = $this->seller_id;
      $query = $this->db->query("SELECT SUM(st.amount) AS pamount
      FROM " . DB_PREFIX . "seller_transaction st
      LEFT JOIN " . DB_PREFIX . "order o ON (st.order_id=o.order_id)
      WHERE (st.seller_id = '" . (int)$seller_id. "' AND o.order_status_id IN (".$filter_eligible_status_id."))
      OR (st.seller_id = '" .(int)$seller_id. "' AND st.order_id=0)");

      return $query->row['pamount'];
    }

    //INDEPI
    public function getSellerPermissions($user_group_id)
    {
      //Al usuario del vendedor se le asigna permisos correspondientes segun
      //tabla oci_sellers_users.user_group_id
      //Vendedor_Administrador->user_group_id: 11
      $user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "'");

      $permissions = json_decode($user_group_query->row['permission'], true);

      if (is_array($permissions)) {
        foreach ($permissions as $key => $value) {
          $this->permission[$key] = $value;
        }
      }

      return $permissions;
    }

    public function getSellerAdminId()
    {
      //Retorna el id del usuario VENDEDOR administrador
      $sql = "select user_group_id as admin_id "
            ."from oc_user_group "
            ."where name like 'Vendedor_Administrador' " ;

      $query = $this->db->query($sql);

      return $query->row['admin_id'];
    }

    public function hasPermission($key, $value) {
      if (isset($this->session->data['permissions'][$key])) {
        return in_array($value, $this->session->data['permissions'][$key]);
      } else {
        return false;
      }
    }

    public function getUserFirstName() {
      return $this->user_firstname;
    }
}
