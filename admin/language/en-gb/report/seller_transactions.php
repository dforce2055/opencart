<?php
// Heading
$_['heading_title']     = 'Waiting for Payment';

// Text
$_['text_year']         = 'Years';
$_['text_month']        = 'Months';
$_['text_week']         = 'Weeks';
$_['text_day']          = 'Days';
$_['text_all_status']   = 'All Statuses';
$_['text_all_sellers']  = 'All Sellers';
$_['text_yes']          = 'Yes';
$_['text_no']          	= 'No';
$_['text_gross_incomes']	= 'Store Revenue : ';
$_['text_commision']    	= 'Store Commision : ';
$_['text_wait']             = 'Please Wait!';
$_['text_seller_earning'] 	= 'Seller Balance : ';
$_['text_payment_history'] 	= 'Payment History';
$_['text_seller_payment_history'] 	= 'Latest 10 Payout Received From Store';
$_['text_txn_id'] 	= 'Txn Id';



// Column
$_['column_date_added'] 		= 'Date Added';
$_['column_action'] 		= 'Action';
$_['column_order_id']   		= 'Order ID';
$_['column_product_name']   	= 'Product Name';
$_['column_unit_price']   		= 'Unit Price';
$_['column_quantity']       	= 'Quantity';
$_['column_commision']      	= 'Commision';
$_['column_amount']      		= 'Net Amount';
$_['column_total']      		= 'Total';
$_['column_transaction_status'] = 'Status';
$_['column_paid_status']      	= 'Paid';
$_['column_seller_id'] 			= 'Seller Id';
$_['column_seller_name'] 		= 'Seller Name';
$_['column_payment_amount'] 	= 'Payment Amount';
$_['column_payment_date'] 		= 'Payment Date';
$_['column_order_product'] 		= 'Order Products [Order ID - Product Name]';
$_['button_Paypal'] 			= 'Pay Seller';
$_['button_add_payment'] 		= 'Add Payment Manually';
$_['button_view_transactions'] 		= 'View Transaction';
$_['column_buyer_id']   		= 'BUYER ID';
$_['column_buyer_name']   		= 'BUYER NAME';
$_['column_buy_date']   		= 'DATE';

// Entry
$_['entry_date_start']   = 'Date Start :';
$_['entry_date_end']     = 'Date End:';
$_['entry_group']        = 'Group By :';
$_['entry_order_status'] = 'Order Status :';
$_['entry_status']       = 'Paid Status :';

// Seller Transactions
$_['text_txn_id'] 	= 'Txn Id';
$_['text_date_added'] 	= 'Date Added';
$_['text_product_name'] 	= 'Product Name';
$_['text_description'] 	= 'Description';
$_['text_unit_price'] 	= 'Unit Price';
$_['text_commission'] 	= 'Commission';
$_['text_amount'] 	= 'Amount';
$_['text_show'] 	= 'Show';
$_['text_item_details'] 	= 'Item Details';
$_['text_sale_details'] 	= 'Sale Detail';
$_['text_sale_amount'] 	= 'Sale Amount';
$_['text_order_item_value'] 	= 'Order Item Value';
$_['text_transaction_details'] 	= 'Transaction Details';
$_['text_market_place_free'] 	= 'Market place Fee';
$_['text_net_payable'] 	= 'Net Payable';
$_['text_no_transactions'] 	= 'You have no transactions';
$_['text_to'] 	= 'to';
$_['text_pay'] 	= 'pay';

$_['column_sale']   		= 'Sale';
$_['column_total']   		= 'Total';
$_['column_seller_cut']   		= 'Seller Cut';
$_['text_your_transaction']   = 'Your Transactions';
$_['text_seller_transactions']   = 'Seller Transactions';
$_['seller_bank_details_of']   = 'Bank Details of';
$_['seller_pay_to_seller']   = 'Pay to Seller';
$_['seller_payee_name']   = 'Payee Name';
$_['seller_bank_name']   = 'Bank Name';
$_['seller_account_number']   = 'Account Number';
$_['seller_branch']   = 'Branch';
$_['seller_ifsccode']   ='IFSC CODE';
$_['seller_telephone']   ='Telephone';
$_['seller_note']   ='Note';
$_['seller_amount_to_pay']   ='Amount to pay';
?>
