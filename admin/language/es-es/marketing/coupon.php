<?php
// Heading
$_['heading_title']       = 'Cupón';

// Text
$_['text_success'] = 'Se han modificado los Cupones.';
$_['text_list'] = 'Lista de Cupones';
$_['text_add'] = 'Agregar Cupón';
$_['text_edit'] = 'Editar Cupón';
$_['text_percent'] = 'Porcentaje';
$_['text_amount'] = 'Cantidad Fija';

// Column
$_['column_name'] = 'Nombre del Cupón';
$_['column_code'] = 'Código';
$_['column_discount'] = 'Descuento';
$_['column_date_start'] = 'Fecha de Inicio';
$_['column_date_end'] = 'Fecha Fin';
$_['column_status'] = 'Estado';
$_['column_order_id'] = 'Pedido ID';
$_['column_customer'] = 'Cliente';
$_['column_amount'] = 'Cantidad';
$_['column_date_added'] = 'Fecha Alta';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre del Cupón';
$_['entry_code'] = 'Código';
$_['entry_type'] = 'Tipo';
$_['entry_discount'] = 'Descuento';
$_['entry_logged'] = 'Acceso Cliente';
$_['entry_shipping'] = 'Envío Gratis';
$_['entry_total'] = 'Monto Total';
$_['entry_category'] = 'Rubro';
$_['entry_product'] = 'Productos' ;
$_['entry_date_start'] = 'Fecha Inicio';
$_['entry_date_end'] = 'Fecha Fin';
$_['entry_uses_total'] = 'Usos Por Cupón';
$_['entry_uses_customer'] = 'Usos por Cliente';
$_['entry_status'] = 'Estado';

// Ayuda
$_['help_code'] = 'Código del Cliente para obtener el descuento.';
$_['help_type'] = 'Porcentaje o Cantidad fija.';
$_['help_logged'] = 'El cliente debe estar registrado para utilizar el cupón.';
$_['help_total'] = 'La cantidad total que se debe alcanzar antes de que el cupón sea válido,';
$_['help_category'] = 'Elegir todos los productos bajo el rubro seleccionado.';
$_['help_product'] = 'Elegir productos específicos para que se aplique el cupón. Seleccionar productos para aplicar la Promoción. ';
$_['help_uses_total'] = 'El número máximo de veces que el cupón puede ser utilizado por cualquier Cliente. Dejar en blanco para un número ilimitado ';
$_['help_uses_customer'] = 'El número máximo de veces que el cupón puede ser utilizado por un Cliente. Dejar en blanco para un número ilimitado ';

// Error
$_['error_permission'] = 'Sin permiso para modificar Cupones.';
$_['error_exists'] = 'Error: El Código de descuento ya está en uso.';
$_['error_name'] = 'El Nombre del Cupón debe contener entre 3 y 128 caractéres.';
$_['error_code'] = 'El Código debe contener entre 3 y 10 caractéres.';