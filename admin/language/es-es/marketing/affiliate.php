<?php
// Heading
$_['heading_title']             = 'Afiliados';

// Text
$_['text_success'] = 'Se han modificado los  Afiliados.';
$_['text_approved'] = 'Se han aprobado las cuentas% s.';
$_['text_list'] = 'Lista de Afiliados';
$_['text_add'] = 'Agregar Afiliado';
$_['text_edit'] = 'Editar Afiliado';
$_['text_balance'] = 'Balance';
$_['text_cheque'] = 'Cheque';
$_['text_paypal'] = 'PayPal';
$_['text_bank'] = 'Transferencia Bancaria';

// Column
$_['column_name'] = 'Nombre de Afiliados';
$_['column_email'] = 'Email';
$_['column_code'] = 'Código de Seguimiento ';
$_['column_balance'] = 'Balance';
$_['column_status'] = 'Estado';
$_['column_approved'] = 'Aprobado';
$_['column_date_added'] = 'Fecha Alta';
$_['column_description'] = 'Descripción';
$_['column_amount'] = 'Cantidad';
$_['column_action'] = 'Acción';

// Entry
$_['entry_firstname'] = 'Nombre';
$_['entry_lastname'] = 'Apellido';
$_['entry_email'] = 'Email';
$_['entry_telephone'] = 'Teléfono';
$_['entry_fax'] = 'Fax';
$_['entry_status'] = 'Estado';
$_['entry_password'] = 'Contraseña';
$_['entry_confirm'] = 'Confirmar';
$_['entry_company'] = 'Empresa';
$_['entry_website'] = 'Sitio Web';
$_['entry_address_1'] = 'Dirección 1';
$_['entry_address_2'] = 'Dirección 2';
$_['entry_city'] = 'Ciudad';
$_['entry_postcode'] = 'Código Postal';
$_['entry_country'] = 'País';
$_['entry_zone'] = 'Provincia/Estado';
$_['entry_code'] = 'Código de Seguimiento';
$_['entry_commission'] = 'Comisión (%)';
$_['entry_tax'] = 'Impuesto ID';
$_['entry_payment'] = 'Forma de Pago ';
$_['entry_cheque'] = 'Nombre del Beneficiario del Cheque';
$_['entry_paypal'] = 'Email de Paypal';
$_['entry_bank_name'] = 'Nombre del Banco';
$_['entry_bank_branch_number'] = 'ABA/BSB número (Número Branch)';
$_['entry_bank_swift_code'] = 'Código SWIFT';
$_['entry_bank_account_name'] = 'Nombre de Cuenta';
$_['entry_bank_account_number'] = 'Número de Cuenta';
$_['entry_amount'] = 'Cantidad';
$_['entry_description'] = 'Descripción';
$_['entry_name'] = 'Nombre de Afiliados';
$_['entry_approved'] = 'Aprobado';
$_['entry_date_added'] = 'Fecha Alta';

// Ayuda
$_['help_code'] = 'El Código de seguimiento que se utilizará para realizar un seguimiento de referencias.';
$_['help_commission'] = 'Porcentaje que el afiliado recibe por cada Pedido.';

// Error
$_['error_permission'] = 'Sin permiso para modificar afiliados.';
$_['error_exists'] = 'Email ya registrado.';
$_['error_firstname'] = 'El Nombre debe contener entre 1 y 32 caractéres.';
$_['error_lastname'] = 'El Apellido debe contener entre 1 y 32 caractéres.';
$_['error_email'] = 'Email inválido.';
$_['error_cheque'] = 'Nombre del Beneficiario del Cheque Obligatorio.';
$_['error_paypal'] = 'La Dirección de Email Paypal es inválido.';
$_['error_bank_account_name'] = 'Nombre de Cuenta Obligatoria.';
$_['error_bank_account_number'] = 'Número de Cuenta Obligatorio.';
$_['error_telephone'] = 'El Teléfono debe contener entre 3 y 32 caractéres.';
$_['error_password'] = 'La Contraseña debe contener entre 4 y 20 caractéres.';
$_['error_confirm'] = 'La contraseña y su confirmación no coinciden.';
$_['error_address_1'] = 'La Dirección 1 debe contener entre 3 y 128 caractéres.';
$_['error_city'] = 'La Ciudad debe contener entre 2 y 128 caractéres.';
$_['error_postcode'] = 'El Código Postal debe contener entre 2 y 10 caractéres.';
$_['error_country'] = 'Seleccionar País.';
$_['error_zone'] = 'Seleccionar Provincia/Estado.';
$_['error_code'] = 'Código de Seguimiento Obligatorio.';