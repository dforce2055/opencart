<?php
// Heading
$_['heading_title']     = 'Marketing de Seguimiento';

// Text
$_['text_success'] = 'Se ha modificado el Marketing de Seguimiento.';
$_['text_list'] = 'Lista de Seguimiento';
$_['text_add'] = 'Agregar Seguimiento';
$_['text_edit'] = 'Editar Seguimiento';

// Column
$_['column_name'] = 'Nombre de la Campaña';
$_['column_code'] = 'Código';
$_['column_clicks'] = 'Clicks';
$_['column_orders'] = 'Pedidos';
$_['column_date_added'] = 'Fecha Alta';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre de la Campaña';
$_['entry_description'] = 'Campaña Descripción';
$_['entry_code'] = 'Código de Seguimiento';
$_['entry_example'] = 'Ejemplos';
$_['entry_date_added'] = 'Fecha Alta';

// Ayuda
$_['help_code'] = 'El Código de Seguimiento que se utilizará para las campañas de Marketing.';
$_['help_example'] = 'De modo que el sistema puede rastrear los referentes necesarios para agregar el código de seguimiento a la final de la URL con enlaces al sitio.';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Marketing de Seguimiento.';
$_['error_name'] = 'La Campaña debe contener entre 1 y 32 caractéres.';
$_['error_code'] = 'El Código de seguimiento es Obligatorio.';