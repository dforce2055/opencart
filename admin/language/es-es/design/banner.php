<?php
// Heading
$_['heading_title']      = 'Banners';

// Text
$_['text_success']       = 'Se han modificado los Banners.';
$_['text_list']          = 'Lista de Banners';
$_['text_add']           = 'Agregar Banner';
$_['text_edit']          = 'Editar Banner';
$_['text_default']       = 'Por Defecto';

// Column
$_['column_name']        = 'Nombre del Banner';
$_['column_status']      = 'Estado';
$_['column_action']      = 'Acción';

// Entry
$_['entry_name']         = 'Nombre del Banner';
$_['entry_title']        = 'Título';
$_['entry_link']         = 'Link';
$_['entry_image']        = 'Imagen';
$_['entry_status']       = 'Estado';
$_['entry_sort_order']   = 'Orden';

// Error
$_['error_permission']   = 'Sin permiso para modificar los Banners.';
$_['error_name']         = 'El nombre del Banner debe contener entre 3 y 64 caractéres.';
$_['error_title']        = 'El Título del Banner debe contener entre 2 y 64 caractéres.';