<?php
// Heading
$_['heading_title']       = 'Diseños';

// Text
$_['text_success']        = 'Se ha modificado un diseño.';
$_['text_list']           = 'Lista de Diseños';
$_['text_add']            = 'Agregar Diseño';
$_['text_edit']           = 'Editar Diseño';
$_['text_default']        = 'Por defecto';
$_['text_content_top']    = 'Contenido Top';
$_['text_content_bottom'] = 'Contenido Bottom';
$_['text_column_left']    = 'Columna Izquierda';
$_['text_column_right']   = 'Columna Derecha';

// Column
$_['column_name']         = 'Nombre del Diseño';
$_['column_action']       = 'Acción';

// Entry
$_['entry_name']          = 'Nombre del Diseño';
$_['entry_store']         = 'Comercio';
$_['entry_route']         = 'Ruta';
$_['entry_module']        = 'Módulo';
$_['entry_position']      = 'Posición';
$_['entry_sort_order']    = 'Ordenar';

// Error
$_['error_permission'] = 'Sin permiso para modificar los diseños.';
$_['error_name'] = 'El Nombre de presentación debe contener entre 3 y 64 caractéres.';
$_['error_default'] = 'Esta disposición no puede ser eliminada, ya que actualmente está asignadoa como la distribución del Comercio por Defecto.';
$_['error_store'] = 'Esta disposición no se puede eliminar ya que se asigna actualmente a los Comercios de% s.';
$_['error_product'] = 'Esta disposición no se puede eliminar ya que se asigna actualmente a los productos de% s.';
$_['error_category'] = 'Esta disposición no se puede eliminar ya que se asigna actualmente a% s rubros.';
$_['error_information'] = 'Esta disposición no se puede eliminar ya que se asigna actualmente a% s páginas de información.';