<?php
// Heading
$_['heading_title']     = 'Editor de Temas';

// Text
$_['text_success']      = 'Éxito: ¡Has modificado los temas!';
$_['text_edit']         = 'Editar Tema';
$_['text_store']        = 'Seleccione su Tienda';
$_['text_template']     = 'Seleccione su Plantilla';
$_['text_default']      = 'Defecto';
$_['text_history']      = 'Historial del Tema';
$_['text_twig']         = 'El editor de Temas, usa plantillas en lenguaje Twig. Puede leer acerca de <a href="http://twig.sensiolabs.org/documentation" target="_blank" class="alert-link">La sintaxis de Twig aquí</a>.';

// Column
$_['column_store']      = 'Tienda';
$_['column_route']      = 'Ruta';
$_['column_theme']      = 'Tema';
$_['column_date_added'] = 'Fecha añadido';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar el editor de Tema!';
$_['error_twig']        = 'Advertencia: ¡Solo puede guardar archivos con extensión .twig!';
