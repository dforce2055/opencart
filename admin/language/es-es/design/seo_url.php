<?php
// Heading
$_['heading_title']    = 'SEO URL';

// Text
$_['text_success']     = 'Éxito: !Has modificado SEO URL!';
$_['text_list']        = 'Lista SEO URL';
$_['text_add']         = 'Agregar SEO URL';
$_['text_edit']        = 'Editar SEO URL';
$_['text_filter']      = 'Filtrar';
$_['text_default']     = 'Defecto';

// Column
$_['column_query']     = 'Consulta';
$_['column_keyword']   = 'Palabra clave';
$_['column_store']     = 'Tienda';
$_['column_language']  = 'Lenguaje';
$_['column_action']    = 'Acción';

// Entry
$_['entry_query']      = 'Consulta';
$_['entry_keyword']    = 'Palabra Clave';
$_['entry_store']      = 'Tienda';
$_['entry_language']   = 'Lenguaje';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar SEO URL!';
$_['error_query']      = '¡La Consulta debe tener entre 3 y 64 caractéres!';
$_['error_keyword']    = '¡La Palabra Clave debe tener entre 3 y 64 caractéres!';
$_['error_exists']     = '¡La Palabra Clave Ya esta en uso!';
