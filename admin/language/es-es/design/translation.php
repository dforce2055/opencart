<?php
// Heading
$_['heading_title']    = 'Editor de Lenguaje';

// Text
$_['text_success']     = 'Éxito: Has modificado el editor de Lenguaje!';
$_['text_list']        = 'Lista de traducciones';
$_['text_edit']        = 'Editar Traducción';
$_['text_add']         = 'Agregar Traducción';
$_['text_default']     = 'Defecto';
$_['text_store']       = 'Tienda';
$_['text_language']    = 'Lenguaje';

// Column
$_['column_store']     = 'Tienda';
$_['column_language']  = 'Lenguaje';
$_['column_route']     = 'Ruta';
$_['column_key']       = 'Clave';
$_['column_value']     = 'Valor';
$_['column_action']    = 'Acción';

// Entry
$_['entry_store']      = 'Tienda';
$_['entry_language']   = 'Lenguaje';
$_['entry_route']      = 'Ruta';
$_['entry_key']        = 'Clave';
$_['entry_default']    = 'Defecto';
$_['entry_value']      = 'Valor';

// Error
$_['error_permission'] = 'Advertencia: !No tiene permisos para modificar el editor de lenguaje!';
$_['error_key']        = '¡La clave debe tener entre 3 and 64 caractéres!';
