<?php
// Heading
$_['heading_title']     = 'Contenido HTML';

// Text
$_['text_module'] = 'Módulos';
$_['text_success'] = 'Se ha modificado el Módulo Contenido HTML.';
$_['text_edit'] = 'Editar Módulo Contenido HTML ';

// Entry
$_['entry_name'] = 'Nombre del Módulo';
$_['entry_title'] = 'Título';
$_['entry_description'] = 'Descripción';
$_['entry_status'] = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Módulo Contenido HTML.';
$_['error_name'] = 'El Nombre del Módulo debe contener entre 3 y 64 caractéres.';