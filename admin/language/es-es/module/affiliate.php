<?php
// Heading
$_['heading_title']    = 'Afiliados';

$_['text_module']      = 'Módulo';
$_['text_success']     = 'Se ha modificado el Módulo de afiliados.';
$_['text_edit']        = 'Editar Módulo';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar el M&ocute;dulo Afiliados.';