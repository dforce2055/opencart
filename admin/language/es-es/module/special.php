<?php
// Heading
$_['heading_title']    = 'Ofertas';

// Text
$_['text_module'] = 'Módulos';
$_['text_success'] = 'Se ha modificado el Módulo Ofertas.';
$_['text_edit'] = 'Editar Módulo Ofertas';

// Entry
$_['entry_name'] = 'Nombre del Módulo';
$_['entry_limit'] = 'Límite';
$_['entry_width'] = 'Ancho';
$_['entry_height'] = 'Altura';
$_['entry_status'] = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Módulo Ofertas.';
$_['error_name'] = 'El Nombre del Módulo debe contener entre 3 y 64 caractéres.';
$_['error_width'] = 'Ancho Obligatorio.';
$_['error_height'] = 'Altura Obligatoria.';