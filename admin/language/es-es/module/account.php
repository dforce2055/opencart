<?php
// Heading
$_['heading_title']    = 'Cuenta';

$_['text_module'] = 'Módulos';
$_['text_success'] = 'Se ha modificado Módulo Cuenta';
$_['text_edit'] = 'Editar Módulo Cuenta';

// Entry
$_['entry_status'] = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Módulo Cuenta.';