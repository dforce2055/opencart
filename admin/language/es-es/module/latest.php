<?php
// Heading
$_['heading_title']    = 'Últimos Productos';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Se ha modificado el Módulo Últimos Productos.';
$_['text_edit']        = 'Editar Módulo';

// Entry
$_['entry_name']       = 'Nombre del Módulo';
$_['entry_limit']      = 'Límitee';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'No tiene permisos para modificar este módulo.';
$_['error_name']       = 'El Nombre del Módulo debe contener entre 3 y 64 caractéres.';
$_['error_width']      = 'Ancho Obligatorio.';
$_['error_height']     = 'Alto Obligatorio.';