<?php
// Heading
$_['heading_title']    = 'Google Hangouts';

// Text
$_['text_module'] = 'Módulos';
$_['text_success'] = 'Se ha modificado el Módulo Google Hangouts.';
$_['text_edit'] = 'Editar Módulo Google Hangouts';

// Entry
$_['entry_code'] = 'Código Talk Google';
$_['entry_status'] = 'Estado';

// Ayuda
$_['help_code'] = 'Ir a <a href="https://developers.google.com/+/hangouts/button" target="_blank"> Google Hangout </a> y copiar y pegar el código generado en el cuadro de texto.';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Módulo Google Hangouts.';
$_['error_code'] = 'Código Obligatorio.';