<?php
// Heading
$_['heading_title']    = 'Filtro';

// Text
$_['text_module']      = 'Módulo';
$_['text_success']     = 'Se han modificado los Filtros.';
$_['text_edit']        = 'Editar Módulo Filtros';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar los Filtros.';