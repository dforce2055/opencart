<?php
// Heading
$_['heading_title']    = 'Carrousel';

// Text
$_['text_module'] = 'Módulos';
$_['text_success'] = 'Se ha modificado el Módulo Carrousel.';
$_['text_edit'] = 'Editar Módulo Carrousel';

// Entry
$_['entry_name'] = 'Nombre del Módulo';
$_['entry_banner'] = 'Banner';
$_['entry_width'] = 'Ancho';
$_['entry_height'] = 'Alto';
$_['entry_status'] = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar Módulo Carrousel.';
$_['error_name'] = 'Nombre del Módulo debe contener entre 3 y 64 caractéres.';
$_['error_width'] = 'Ancho Obligatorio.';
$_['error_height'] = 'Alto Obligatorio.';