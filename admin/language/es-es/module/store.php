<?php
// Heading
$_['heading_title']    = 'Comercio';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Se ha modificado el Módulo Comercio.';
$_['text_edit']        = 'Editar Comercios';

// Entry
$_['entry_admin']      = 'Sólo para Administradores';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Sin permisos para modificar este Módulo.';