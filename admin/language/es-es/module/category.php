<?php
// Heading
$_['heading_title']    = 'Rubro';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Se ha modificado el Módulo Rubros.';
$_['text_edit']        = 'Editar Módulo';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Módulo Rubros.';