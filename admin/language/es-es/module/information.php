<?php
// Heading
$_['heading_title']    = 'Información';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Se ha modificado el Módulo Información.';
$_['text_edit']        = 'Editar Módulo';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Módulo de Información.';