<?php
// Heading
$_['heading_title']    = 'Destacados';

// Text
$_['text_module'] = 'Módulos';
$_['text_success'] = 'Se ha modificado el Módulo Destacados.';
$_['text_edit'] = 'Editar destacados';

// Entry
$_['entry_name'] = 'Nombre del Módulo';
$_['entry_product'] = 'Productos';
$_['entry_limit'] = 'Límite';
$_['entry_width'] = 'Ancho';
$_['entry_height'] = 'Altura';
$_['entry_status'] = 'Estado';

// Ayuda
$_['help_product'] = '(Autocompletar)';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Módulo Destacados.';
$_['error_name'] = 'El Nombre del Módulo debe contener entre 3 y 64 caractéres.';
$_['error_width'] = 'Ancho Obligatorio.';
$_['error_height'] = 'Altura Obligatoria.';