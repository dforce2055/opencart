<?php
// Heading
$_['heading_title']         = 'Estadísticas';

// Text
$_['text_success']          = 'Éxito: ¡Has modificado las Estadísticas!';
$_['text_list']             = 'Listado de Estadísticas';
$_['text_order_sale']       = 'Pedidos de Ventas';
$_['text_order_processing'] = 'Pedidos en Proceso';
$_['text_order_complete']   = 'Pedidos Completos';
$_['text_order_other']      = 'Otros Pedidos';
$_['text_returns']          = 'Devoluciones';
$_['text_customer']         = 'Clientes que esperan aprobación';
$_['text_affiliate']        = 'Afiliados que esperan aprobación';
$_['text_product']          = 'Productos fuera de Stock';
$_['text_review']           = 'Opiniones Pendientes';

// Column
$_['column_name']           = 'Nombre de Estadística';
$_['column_value']	        = 'Valor';
$_['column_action']         = 'Acción';

// Error
$_['error_permission']      = 'Advertencia: ¡No tiene permisos para modificar las estadísticas!';
