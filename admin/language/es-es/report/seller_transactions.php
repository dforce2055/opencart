<?php
// Heading
$_['heading_title']     = 'A la espera del Pago';

// Text
$_['text_year']         = 'Años';
$_['text_month']        = 'Meses';
$_['text_week']         = 'Semanas';
$_['text_day']          = 'Días';
$_['text_all_status']   = 'Todos los Estados';
$_['text_all_sellers']  = 'Todos los vendedores';
$_['text_yes']          = 'Si';
$_['text_no']          	= 'No';
$_['text_gross_incomes']	= 'Ingresos de Tienda : ';
$_['text_commision']    	= 'Comisión de Tienda : ';
$_['text_wait']             = '¡Por favor espere!';
$_['text_seller_earning'] 	= 'Saldo del Vendedor : ';
$_['text_payment_history'] 	= 'Historial de pago';
$_['text_seller_payment_history'] 	= 'Últimos 10 Pagos Recibidos de la Tienda';


// Column
$_['column_date_added'] 		= 'Fecha de agregación';
$_['column_action'] 		= 'Acción';
$_['column_order_id']   		= 'ID de Orden';
$_['column_product_name']   	= 'Nombre de Producto';
$_['column_unit_price']   		= 'Precio Unitario';
$_['column_quantity']       	= 'Cantidad';
$_['column_commision']      	= 'Comisión';
$_['column_amount']      		= 'Monto neto';
$_['column_total']      		= 'Total';
$_['column_transaction_status'] = 'Estado';
$_['column_paid_status']      	= 'Pagado';
$_['column_seller_id'] 			= 'ID Vendedor';
$_['column_seller_name'] 		= 'Nombre de Vendedor';
$_['column_payment_amount'] 	= 'Monto de Pago';
$_['column_payment_date'] 		= 'Fecha de Pago';
$_['column_order_product'] 		= 'Pedidos de Productos [ID Pedido - Nombre Producto]';
$_['button_Paypal'] 			= 'Pagar al Vendedor';
$_['button_addPago'] 		= 'Agregar Pago Manual';
$_['column_buyer_id']   		= 'ID COMPRADOR';
$_['column_buyer_name']   		= 'NOMBRE COMPRADOR';
$_['column_buy_date']   		= 'FECHA';

// Entry
$_['entry_date_start']   = 'Fecha de Inicio :';
$_['entry_date_end']     = 'Fecha de Fin:';
$_['entry_group']        = 'Agrupar por :';
$_['entry_order_status'] = 'Estado de Pedido :';
$_['entry_status']       = 'Estado de Pago :';

// Seller Transactions
$_['text_txn_id'] 	= 'Txn Id';
$_['text_date_added'] 	= 'Fecha Añadido';
$_['text_product_name'] 	= 'Nombre de Producto';
$_['text_description'] 	= 'Descripción';
$_['text_unit_price'] 	= 'Precio Unitario';
$_['text_commission'] 	= 'Comisión';
$_['text_amount'] 	= 'Monto';
$_['text_show'] 	= 'Mostrar';
$_['text_item_details'] 	= 'Detalle de Item';
$_['text_sale_details'] 	= 'Detalle de Venta';
$_['text_sale_amount'] 	= 'Monto de Venta';
$_['text_order_item_value'] 	= 'Ordenar por valor de Item';
$_['text_transaction_details'] 	= 'Detalle Transacción';
$_['text_market_place_free'] 	= 'Cuota del Mercado';
$_['text_net_payable'] 	= 'Neto a Pagar';
$_['text_no_transactions'] 	= 'No tiene Transacciones';
$_['text_to'] 	= 'a';
$_['text_pay'] 	= 'pagar';
$_['button_add_payment'] 		= 'Agregar Pago Manualmente';
$_['button_view_transactions'] 		= 'Ver Transaciones';

$_['column_sale']   		= 'Venta';
$_['column_total']   		= 'Total';
$_['column_seller_cut']   		= 'Ganancia Vendedor';
$_['text_your_transaction']   = 'Sus Transacciones';
$_['text_seller_transactions']   = 'Transacciones del Vendedor';
$_['text_transaction']   = 'Transacciones';
$_['pay_to_seller']   = 'Pagar al Vendedor';
$_['seller_bank_details_of']   = 'Detalles bancarios de';
$_['seller_payee_name']   = 'Nombre del Beneficiario';
$_['seller_bank_name']   = 'Nombre del Banco';
$_['seller_account_number']   = 'Número de Cuenta';
$_['seller_branch']   = 'Sucursal';
$_['seller_ifsccode']   ='CÓDIGO IFSC';
$_['seller_telephone']   ='Teléfono';
$_['seller_note']   ='Nota';
$_['seller_amount_to_pay']   ='Monto a Pagar';
?>
