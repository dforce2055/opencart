<?php
// Heading
$_['heading_title'] = 'Reportes';

// Text
$_['text_success']  = 'Éxito: Has modificado reportes!';
$_['text_list']     = 'Lista de reportes';
$_['text_type']     = 'Elija el tipo de reporte';
$_['text_filter']   = 'Filtro';