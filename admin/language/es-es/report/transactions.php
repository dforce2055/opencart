<?php
// Heading
$_['heading_title']     = 'A la espera del Pago';

// Text
$_['text_year']         = 'Años';
$_['text_month']        = 'Meses';
$_['text_week']         = 'Semanas';
$_['text_day']          = 'Días';
$_['text_all_status']   = 'Todos los Estados';
$_['text_all_sellers']  = 'Todos los vendedores';
$_['text_yes']          = 'Si';
$_['text_no']          	= 'No';
$_['text_gross_incomes']	= 'Ingresos de Tienda : ';
$_['text_commision']    	= 'Comisión de Tienda : ';
$_['text_wait']             = '¡Por favor espere!';
$_['text_seller_earning'] 	= 'Saldo del Vendedor : ';
$_['text_payment_history'] 	= 'Historial de pago';
$_['text_seller_payment_history'] 	= 'Últimos 10 Pagos Recibidos de la Tienda';
$_['text_txn_id'] 	= 'Txn Id';

// Column
$_['column_date_added'] 		= 'Fecha de agregación';
$_['column_action'] 		= 'Acción';
$_['column_order_id']   		= 'ID de Orden';
$_['column_product_name']   	= 'Nombre de Producto';
$_['column_unit_price']   		= 'Precio Unitario';
$_['column_quantity']       	= 'Cantidad';
$_['column_commision']      	= 'Comisión';
$_['column_amount']      		= 'Monto neto';
$_['column_total']      		= 'Total';
$_['column_transaction_status'] = 'Estado';
$_['column_paid_status']      	= 'Pagado';
$_['column_seller_id'] 			= 'ID Vendedor';
$_['column_seller_name'] 		= 'Nombre de Vendedor';
$_['column_payment_amount'] 	= 'Monto de Pago';
$_['column_payment_date'] 		= 'Fecha de Pago';
$_['column_order_product'] 		= 'Pedidos de Productos [ID Pedido - Nombre Producto]';
$_['button_Paypal'] 			= 'Pagar al Vendedor';
$_['button_addPago'] 		= 'Agregar Pago Manual';
$_['column_buyer_id']   		= 'ID COMPRADOR';
$_['column_buyer_name']   		= 'NOMBRE COMPRADOR';
$_['column_buy_date']   		= 'FECHA';

// Entry
$_['entry_date_start']   = 'Fecha de Inicio :';
$_['entry_date_end']     = 'Fecha de Fin:';
$_['entry_group']        = 'Agrupar por :';
$_['entry_order_status'] = 'Estado de Pedido :';
$_['entry_status']       = 'Estado de Pago :';

// Multiseller
$_['button_add_payment'] 		= 'Agregar Pago Manualmente';
$_['button_view_transactions'] 		= 'Ver Transaciones';
$_['column_sale']   		= 'Venta';
$_['column_total']   		= 'Total';
$_['column_seller_cut']   		= 'Ganancia Vendedor';
$_['text_your_transaction']   = 'Sus Transacciones';
$_['text_seller_transactions']   = 'Transacciones del Vendedor';
$_['text_transaction']   = 'Transacciones';
?>
