<?php
// Heading
$_['heading_title']         = 'Informe de Crédito de Clientes';

// Column
$_['text_list']             = 'Lista de Créditos';
$_['column_customer']       = 'Nombre del cliente';
$_['column_email']          = 'Email';
$_['column_customer_group'] = 'Grupo de Clientes';
$_['column_status']         = 'Estado';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';

// Entry
$_['entry_date_start']      = 'Fecha Inicio';
$_['entry_date_end']        = 'Fecha Fin';