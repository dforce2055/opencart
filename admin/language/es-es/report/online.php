<?php
// Heading
$_['heading_title']     = 'Online Report';

// Text
$_['text_extension']    = 'Extensiones';
$_['text_success']      = 'Éxito: ¡Has modificado el reporte de clientes online!';
$_['text_list']         = 'Online Lista';
$_['text_filter']       = 'Filtro';
$_['text_guest']        = 'Invitado';

// Column
$_['column_ip']         = 'IP';
$_['column_customer']   = 'Cliente';
$_['column_url']        = 'Última página visitada';
$_['column_referer']    = 'Referente';
$_['column_date_added'] = 'Último click';
$_['column_action']     = 'Acción';

// Entry
$_['entry_ip']          = 'IP';
$_['entry_customer']    = 'Cliente';
$_['entry_status']      = 'Estado';
$_['entry_sort_order']  = 'Orden de Clasificación';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar reporte! de clientes online!';