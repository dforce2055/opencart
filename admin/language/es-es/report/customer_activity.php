<?php
// Heading
$_['heading_title']       = 'Reporte de Actividad del Cliente';

// Text
$_['text_list'] = 'Lista de Actividades del Cliente';
$_['text_address_add'] = '<a href="customer_id=%d">% s </a> ha agregado una Nueva Dirección.';
$_['text_address_edit'] = '<a href="customer_id=%d">% s </a> ha actualizado su Dirección.';
$_['text_address_delete'] = '<a href="customer_id=%d">% s </a> ha eliminado una de sus Direcciones.';
$_['text_edit'] = '<a href="customer_id=%d">% s </a> ha actualizado sus Datos de la Cuenta.';
$_['text_forgotten'] = '<a href="customer_id=%d">% s </a> ha solicitado una Nueva Contraseña.';
$_['text_login'] = '<a href="customer_id=%d">% s </a> ha iniciado Sesión.';
$_['text_password'] = '<a href="customer_id=%d">% s </a> ha actualizado la Contraseña.';
$_['text_register'] = '<a href="customer_id=%d">% s </a> ha registrado una Cuenta.';
$_['text_return_account'] = '<a href="customer_id=%d">% s </a> ha presentado una Devolución de Producto.';
$_['text_return_guest'] = '% s ha presentado una Devolución de producto.';
$_['text_order_account'] = '<a href="customer_id=%d">% s </a> ha creado un <a href="order_id=%d"> nuevo Pedido </a>.';
$_['text_order_guest'] = '% s ha creado un <a href="order_id=%d"> nuevo Pedido </a>.';

// Column
$_['column_customer'] = 'Cliente';
$_['column_comment'] = 'Comentario';
$_['column_ip'] = 'IP';
$_['column_date_added'] = 'Fecha Alta';

// Entry
$_['entry_customer'] = 'Cliente';
$_['entry_ip'] = 'IP';
$_['entry_date_start'] = 'Fecha Inicio';
$_['entry_date_end'] = 'Fecha Fin';