<?php
// Heading
$_['heading_title']    = 'Error Log';

// Text
$_['text_success']     = 'Éxito: You have successfully cleared your error log!';
$_['text_list']        = 'Errors Lista';

// Error
$_['error_warning']    = 'Advertencia: Your error log archivo %s is %s!';
$_['error_permission'] = 'Advertencia: You do not have permission to clear error log!';