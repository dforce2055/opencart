<?php
// Heading
$_['heading_title']         = 'Aprobación de Clientes';

// Text
$_['text_success']          = 'Éxito: ¡Has modificado las aprobaciones de clientes!';
$_['text_list']             = 'Listado de Clientes Aprobados';
$_['text_default']          = 'Defecto';
$_['text_customer']         = 'Cliente';
$_['text_affiliate']        = 'Afiliado';

// Column
$_['column_name']           = 'Nombre de Cliente';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Grupo de Cliente';
$_['column_type']           = 'Tipo';
$_['column_date_added']     = 'Fecha añadido';
$_['column_action']         = 'Acción';

// Entry
$_['entry_name']            = 'Nombre de Cliente';
$_['entry_email']           = 'E-Mail';
$_['entry_customer_group']  = 'Grupo de Cliente';
$_['entry_type']            = 'Tipo';
$_['entry_date_added']      = 'Fecha añadido';

// Error
$_['error_permission']      = 'Advertencia: ¡No tiene permisos para modificar las aprobaciones de usuarios!';
