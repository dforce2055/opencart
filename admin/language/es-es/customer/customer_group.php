<?php
// Heading
$_['heading_title']     = 'Grupos de Clientes';

// Text
$_['text_success']      = 'Se han modificado los Grupos de Clientes.';
$_['text_list']         = 'Listado de Grupos de Clientes';
$_['text_add']          = 'Agregar Grupo de Clientes';
$_['text_edit']         = 'Editar Grupo de Clientes';

// Column
$_['column_name']       = 'Nombre del Grupo';
$_['column_sort_order'] = 'Orden';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Nombre del Grupo';
$_['entry_description'] = 'Descripción';
$_['entry_approval']    = 'Aprobar Nuevos Clientes';
$_['entry_sort_order']  = 'Orden';

// Ayuda
$_['help_approval']     = 'Los Clientes deberán ser Aprobados por el Administrador del Sistema antes de Acceder a su Cuenta.';

// Error
$_['error_permission']   = 'Sin Permiso para modificar el Grupo de Clientes.';
$_['error_name']         = 'El Nombre del Grupo de Clientes debe contener entre 3 y 32 caractéres.';
$_['error_default']      = 'Este Grupo no puede ser eliminado, ya que es el Grupo de Clientes por Defecto.';
$_['error_store']        = 'Este Grupo no puede ser eliminado, ya que actualmente está asignado a %s Comercios.';
$_['error_customer']     = 'Este Grupo no puede ser eliminado, ya que actualmente está asignado a %s Clientes.';
$_['error_seller']       = 'Este Grupo no puede ser eliminado, ya que actualmente está asignado a %s Vendedores.';
