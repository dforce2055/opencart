<?php
// Heading
$_['heading_title']         = 'Campos Personalizados';

// Text
$_['text_success']          = 'Se han modificado los Campos Personalizados.';
$_['text_list']             = 'Lista de Campos Personalizados';
$_['text_add']              = 'Agregar Campo Personalizado';
$_['text_edit']             = 'Editar Campo Personalizado';
$_['text_choose']           = 'Seleccionar';
$_['text_select']           = 'Selección';
$_['text_radio']            = 'Radio';
$_['text_checkbox']         = 'Checkbox';
$_['text_input']            = 'Entrada';
$_['text_text']             = 'Texto';
$_['text_textarea']         = 'Área de Texto';
$_['text_file']             = 'Archivo';
$_['text_date']             = 'Fecha';
$_['text_datetime']         = 'Fecha y Hora';
$_['text_time']             = 'Hora';
$_['text_account']          = 'Cuenta';
$_['text_address']          = 'Dirección';
$_['text_regex']            = 'Expresión Regular';
$_['text_custom_field']     = 'Campo Personalizado';
$_['text_value']            = 'Valores de los Campos Personalizados';

// Column
$_['column_name']           = 'Nombre del Campo';
$_['column_location']       = 'Ubicación';
$_['column_type']           = 'Tipo';
$_['column_sort_order']     = 'Orden';
$_['column_action']         = 'Acción';

// Entry
$_['entry_name']            = 'Nombre del Campo';
$_['entry_location']        = 'Ubicación';
$_['entry_type']            = 'Tipo';
$_['entry_value']           = 'Valor';
$_['entry_validation']      = 'Validación';
$_['entry_custom_value']    = 'Valor del Nombre del Campo';
$_['entry_customer_group']  = 'Grupo de Clientes';
$_['entry_necesario']        = 'Obligatorio';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Orden';

// Ayuda
$_['help_regex']            = 'Usuario Espresiones Regulares. Ej: /[a-zA-Z0-9_-]/';
$_['help_sort_order']       = 'Usar menos para contar hacia atrás.';

// Error
$_['error_permission']      = 'Sin permiso para modificar los Campos Personalizados.';
$_['error_name']            = 'El Nombre del Campo Personalizado debe contener entre 1 y 128 caractéres.';
$_['error_type']            = 'El Valor del Campo es Obligatorio.';
$_['error_custom_value']    = 'El Valor del Campo debe contener entre 1 y 128 caractéres.';
