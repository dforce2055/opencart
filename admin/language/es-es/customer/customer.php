<?php
// Heading
$_['heading_title']         = 'Clientes';

// Text
$_['text_success']          = 'Se han modificado los Clientes.';
$_['text_success_groups']   = 'Se han modificado los Grupos de pertenencia del Cliente.';
$_['text_list']             = 'Listado de Clientes';
$_['text_add']              = 'Agregar Cliente';
$_['text_edit']             = 'Editar Cliente';
$_['text_default']          = 'Por Defecto';
$_['text_balance']          = 'Balance';
$_['text_option']           = 'Opciones';
$_['text_unlock']           = 'Desbloquear Cuenta';
$_['text_login']            = 'Iniciar Sesión en la Tienda';
$_['text_account']          = 'Detalles del Cliente';
$_['text_password']         = 'Contraseña';
$_['entry_password']        = 'Contraseña';
$_['text_other']            = 'Otros';
$_['entry_affiliate']           = 'Afiliado';
$_['text_affiliate']            = 'Detalles de Afiliado';
$_['entry_website']             = 'Sitio Web';
$_['entry_tracking']            = 'Código de seguimiento';
$_['entry_commission']          = 'Comisión (%)';
$_['text_payment']              = 'Detalles de Pago';
$_['entry_tax']                 = 'ID Impuesto';
$_['entry_payment']             = 'Método de Pago';
$_['entry_cheque']              = 'Nombre del beneficiario del Cheque';
$_['entry_paypal']              = 'Email de la cuenta de PayPal';
$_['entry_bank_name']           = 'Nombre del Banco';
$_['text_history']              = 'Historia';
$_['text_history_add']          = 'Agregar Historia';
$_['text_transaction']          = 'Transacciones';
$_['text_transaction_add']      = 'Agregar Transacciones';
$_['text_reward']               = 'Puntos de Recompensa';
$_['text_reward_add']           = 'Agregar Puntos de Recompensa';

// Column
$_['column_name']           = 'Nombre del Cliente';
$_['column_email']          = 'Email';
$_['column_customer_group'] = 'Grupo de Clientes';
$_['column_status']         = 'Estado';
$_['column_date_added']     = 'Fecha de Alta';
$_['column_comment']        = 'Comentario';
$_['column_description']    = 'Descripción';
$_['column_amount']         = 'Monto';
$_['column_points']         = 'Puntos';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Total de Cuentas';
$_['column_action']         = 'Acción';

// Entry
$_['entry_customer_group']  = 'Grupo de Clientes';
$_['entry_firstname']       = 'Nombre';
$_['entry_lastname']        = 'Apellido';
$_['entry_email']           = 'Email';
$_['entry_telephone']       = 'Teléfono';
$_['entry_fax']             = 'Fax';
$_['entry_newsletter']      = 'Boletín de Noticias';
$_['entry_status']          = 'Estado';
$_['entry_approved']        = 'Aprobado';
$_['entry_safe']            = 'Seguridad';
$_['entry_password']        = 'Contraseña';
$_['entry_confirm']         = 'Confirmación';
$_['entry_company']         = 'Empresa';
$_['entry_address_1']       = 'Dirección 1';
$_['entry_address_2']       = 'Dirección 2';
$_['entry_city']            = 'Ciudad';
$_['entry_postcode']        = 'Código Postal';
$_['entry_country']         = 'País';
$_['entry_zone']            = 'Provincia/Estado';
$_['entry_default']         = 'Dirección por Defecto';
$_['entry_comment']         = 'Comentario';
$_['entry_description']     = 'Descripción';
$_['entry_amount']          = 'Monto';
$_['entry_points']          = 'Puntos';
$_['entry_name']            = 'Nombre del Cliente';
$_['entry_ip']              = 'IP';
$_['entry_date_added']      = 'Fecha de Alta';

// Ayuda
$_['help_safe']             = 'Establecer en Verdadero para evitar que el Cliente sea capturado por el Sistema AntiFraude.';
$_['help_points']           = 'Usar menos para quitar puntos.';

// Error
$_['error_warning']                = 'El Formulario contiene Errores.';
$_['error_permission']             = 'Sin Permiso para modificar los Clientes.';
$_['error_exists']                 = 'Email ya Registrado.';
$_['error_firstname']              = 'El Nombre debe contener entre 1 y 32 caractéres.';
$_['error_lastname']               = 'El Apellido debe contener entre 1 y 32 caractéres.';
$_['error_email']                  = 'Email inválido.';
$_['error_telephone']              = 'El Teléfono debe contener entre 3 y 32 caractéres.';
$_['error_password']               = 'La Contraseña debe contener entre 4 y 20 caractéres.';
$_['error_confirm']                = 'La Contraseña y su Confirmación no coinciden.';
$_['error_address_1']              = 'La Dirección debe contener entre 3 y 128 caractéres.';
$_['error_city']                   = 'La Ciudad debe contener entre 2 y 128 caractéres.';
$_['error_postcode']               = 'El Código postal debe contener entre 2 y 10 caractéres.';
$_['error_country']                = 'Seleccionar País';
$_['error_zone']                   = 'Seleccionar Provincia/Estado';
$_['error_custom_field']           = '%s Obligatorio.';
$_['error_custom_field_validate']  = '%s Inválido.';

// Grupos de pertenenecia
$_['text_membership_groups']       = 'Grupos de Pertenencia';
$_['text_customers_groups']        = 'Grupos de Clientes';
$_['text_is_a_group_member']       = 'Pertenece';
$_['text_seller_asigned']          = 'Vendedor Asignado';
