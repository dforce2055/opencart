<?php
// Heading
$_['heading_title'] = 'Panel de Control';

// Text
$_['text_order_total'] = 'Total de Pedidos';
$_['text_customer_total'] = 'Clientes totales';
$_['text_sale_total'] = 'Ventas Totales';
$_['text_online_total'] = 'Gente en l&iacutenea ';
$_['text_map'] = 'Mapa del Mundo';
$_['text_sale'] = 'Ventas Analytics';
$_['text_activity'] = 'Actividad Reciente';
$_['text_recent'] = 'Los Últimos Pedidos ';
$_['text_order'] = 'Pedidos' ;
$_['text_customer'] = 'Clientes' ;
$_['text_day'] = 'Hoy';
$_['text_week'] = 'Semana';
$_['text_month'] = 'Mes';
$_['text_year'] = 'Año';
$_['text_view'] = 'Ver más ...';

// Error
$_['error_install'] = 'Eliminar la carpeta de Instalación por razones de seguridad.';