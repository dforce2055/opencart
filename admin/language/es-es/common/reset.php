<?php
// header
$_['heading_title']  = 'Restablecer Contraseña';

// Text
$_['text_reset']     = 'Restablecer Contraseña.';
$_['text_password']  = 'Introducir la nueva Contraseña.';
$_['text_success']   = 'La Contraseña ha sido guardada.';

// Entry
$_['entry_password'] = 'Contraseña';
$_['entry_confirm']  = 'Confirmar';

// Error
$_['error_password'] = 'La Contraseña debe contener entre 5 y 20 caractéres.';
$_['error_confirm']  = 'La Contraseña y su confirmación no coinciden.';
