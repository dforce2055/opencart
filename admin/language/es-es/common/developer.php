<?php
// Heading
$_['heading_title']    = 'Opciones de Desarrollador';

// Text
$_['text_success']     = 'Éxito: ¡Has modificado las opciones de Desarrollador!';
$_['text_theme']       = 'theme';
$_['text_sass']        = 'SASS';
$_['text_cache']       = 'Éxito: ¡Se ha eliminado el %s cache!';

// Column
$_['column_component'] = 'Componente';
$_['column_action']    = 'Acción';

// Entry
$_['entry_theme']      = 'Tema';
$_['entry_sass']       = 'SASS';
$_['entry_cache']      = 'Cache';

// Button
$_['button_on']        = 'On';
$_['button_off']       = 'Off';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar las opciones de Desarrollador!';
