<?php
// Heading
$_['heading_title'] = 'Administrador de Imágenes';

// Text
$_['text_uploaded'] = 'El archivo se ha cargado.';
$_['text_directory'] = 'Directorio creado.';
$_['text_delete'] = 'Archivo o Directorio Eliminado.';

// Entry
$_['entry_search'] = 'Buscar ..';
$_['entry_folder'] = 'Nombre de la carpeta';

// Error
$_['error_permission'] = 'Permiso denegado.';
$_['error_filename'] = 'El Nombre del archivo debe contener entre 3 y 255 caractéres.';
$_['Error_folder'] = 'El Nombre de la carpeta debe contener entre 3 y 255 caractéres.';
$_['error_exists'] = 'Un archivo o directorio con el mismo nombre ya existe.';
$_['error_directory'] = 'El directorio no existe.';
$_['error_filetype'] = 'Tipo de archivo incorrecto.';
$_['error_upload'] = 'El archivo no puede ser cargado por una razón desconocida.';
$_['error_delete'] = 'No se puede eliminar este directorio.';