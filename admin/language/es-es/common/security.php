<?php
// Heading
$_['heading_title']    = '¡Importante Notificación de Seguridad!';

// Text
$_['text_success']     = 'Éxito: ¡Ha modificado su carpeta de almacenamiento!';
$_['text_admin']       = 'Editare admin/config.php y vuelva a intentar';
$_['text_security']    = 'Es muy importante que mueva el directorio "storage" fuera de la carpeta de web(ejemplo public_html, www or htdocs).';
$_['text_choose']      = 'Elija donde mover su directorio storage';
$_['text_automatic']   = 'Mover Automáticamente';
$_['text_manual']      = 'Mover Manualmente';
$_['text_move']        = 'Mover';
$_['text_to']          = 'a';
$_['text_config']      = 'Editare config.php y vuelva a intentar';
$_['text_admin']       = 'Editare admin/config.php y vuelva a intentar';

// Button
$_['button_move']      = 'Mover';
$_['button_manual']    = 'Manual';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar the storage directory!';
$_['error_path']       = 'Advertencia: ¡Ruta invalida!';
$_['error_directory']  = 'Advertencia: ¡Directorio Invalido!';
$_['error_exists']     = 'Advertencia: ¡El directorio ya existe!';
$_['error_writable']   = 'Advertencia: config.php y admin/config.php es necesario que sean reescribibles!';
