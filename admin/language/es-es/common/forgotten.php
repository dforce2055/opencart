<?php
// header
$_['heading_title']   = 'Contraseña Olvidada';

// Text
$_['text_forgotten']  = 'Contraseña Olvidada';
$_['text_your_email'] = 'Email';
$_['text_email']      = 'Introducir el Email asociado a la cuenta.';
$_['text_success']    = 'Un Email con el link de confirmación fue enviado a la Casilla de Correo Indicada.';

// Entry
$_['entry_email']     = 'Email';
$_['entry_password']  = 'Nueva Contraseña';
$_['entry_confirm']   = 'Confirmar';

// Error
$_['error_email']     = 'Email no encontrado, intentar nuevamente.';
$_['error_password']  = 'La contraseña debe contener entre 3 y 20 caractéres.';
$_['error_confirm']   = 'La contraseña y su confirmación no coinciden.';