<?php
// header
$_['heading_title']  = 'Administrador';

// Text
$_['text_heading']   = 'Administrador';
$_['text_login']     = 'Ingresar Datos de Acceso.';
$_['text_forgotten'] = 'Contraseña Olvidada';

// Entry
$_['entry_username'] = 'Nombre de Usuario';
$_['entry_password'] = 'Contraseña';

// Button
$_['button_login']   = 'Acceder';

// Error
$_['error_login']    = 'Datos Incorrectos.';
$_['error_token']    = 'Token Inválido. Ingresar Nuevamente.';