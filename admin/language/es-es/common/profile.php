<?php
// Heading
$_['heading_title']         = 'Perfil';

// Text
$_['text_success']          = 'Éxito: ¡Has modificado tu Perfil!';
$_['text_edit']             = 'Editar tu Perfil';

// Column
$_['column_username']       = 'Nombre de Usuario';
$_['column_status']         = 'Estado';
$_['column_date_added']     = 'Fecha añadido';
$_['column_action']         = 'Acción';

// Entry
$_['entry_username']        = 'Nombre de Usuario';
$_['entry_password']        = 'Contraseña';
$_['entry_confirm']         = 'Confirmar';
$_['entry_firstname']       = 'Nombre';
$_['entry_lastname']        = 'Apellido';
$_['entry_email']           = 'E-Mail';
$_['entry_image']           = 'Imagen';

// Error
$_['error_permission']      = 'Advertencia: ¡No tiene permisos para modificar tu perfil!';
$_['error_exists_username'] = 'Advertencia: ¡Nombre de Usuario ya esta en uso!';
$_['error_username']        = '¡Nombre de Usuario debe tener entre 3 y 20 caractéres!';
$_['error_password']        = '¡Contraseña debe tener entre 4 y 20 caractéres!';
$_['error_confirm']         = '¡Contraseña y la contraseña de confirmación no coinciden!';
$_['error_firstname']       = '¡Nombre debe tener entre 1 y 32 caractéres!';
$_['error_lastname']        = '¡Apellido debe tener entre 1 y 32 caractéres!';
$_['error_email']           = '¡La dirección de E-Mail no parece valida!';
$_['error_exists_email']    = 'Advertencia: ¡La dirección de E-Mail ya fue registrada!';
