<?php
// Heading
$_['heading_title'] = 'Nombre Del Cliente';

// Text
$_['text_order'] = 'Pedidos';
$_['text_order_status'] = 'En espera';
$_['text_complete_status'] = 'Completado';
$_['text_customer'] = 'Clientes';
$_['text_customer_online'] = 'En linea';
$_['text_approval'] = 'En espera de Aprobación';
$_['text_product'] = 'Productos' ;
$_['text_stock'] = 'Agotado';
$_['text_review'] = 'Comentarios';
$_['text_return'] = 'Devoluciones';
$_['text_affiliate'] = 'Afiliados';
$_['text_store'] = 'Comercios';
$_['text_front'] = 'Web';
$_['text_help'] = 'Ayuda';
$_['text_homepage'] = 'Página de inicio';
$_['text_support'] = 'Foro de Apoyo';
$_['text_documentation'] = 'Documentación';
$_['text_logout'] = 'Salir';
