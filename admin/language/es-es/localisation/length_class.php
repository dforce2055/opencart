<?php
// Heading
$_['heading_title']    = 'Clases de Medidas';

// Text
$_['text_success']     = 'Se han modificado las Clases de Medidas.';
$_['text_list']        = 'Lista de Clases de Medidas';
$_['text_add']         = 'Agregar Clase de Medidas';
$_['text_edit']        = 'Editar Clase de Medidas';

// Column
$_['column_title']     = 'Clase';
$_['column_unit']      = 'Unidad';
$_['column_value']     = 'Valor';
$_['column_action']    = 'Acción';

// Entry
$_['entry_title']      = 'Clase';
$_['entry_unit']       = 'Unidad';
$_['entry_value']      = 'Valor';

// Ayuda
$_['help_value'] = 'Se establece en 1,00 si es la Clase de Medida por Defecto.';

// Error
$_['error_permission'] = 'Sin permiso para modificar Clases de Medida.';
$_['error_title'] = 'El Nombre de la Clase deben contener entre 3 y 32 caractéres.';
$_['error_unit'] = 'La Unidad debe contener entre 1 y 4 caractéres.';
$_['error_default'] = 'Esta clase no puede ser eliminada, ya que actualmente está asignada como la Clase por Defecto.';
$_['error_product'] = 'Esta clase no se puede eliminar ya que se asigna actualmente a los productos de% s.';