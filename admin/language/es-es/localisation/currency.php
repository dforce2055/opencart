<?php
// Heading
$_['heading_title']        = 'Monedas';

// Text
$_['text_success']         = 'Se ha modificado la Moneda.';
$_['text_list']            = 'Lista de Monedas';
$_['text_add']             = 'Agregar Moneda';
$_['text_edit']            = 'Editar Moneda';

// Column
$_['column_title']         = 'Nombre de la Moneda';
$_['column_code']          = 'Código';
$_['column_value']         = 'Valor';
$_['column_date_modified'] = 'La Última Actulacización';
$_['column_action']        = 'Acción';

// Entry
$_['entry_title']          = 'Nombre de la Moneda';
$_['entry_code']           = 'Código';
$_['entry_value']          = 'Valor';
$_['entry_symbol_left']    = 'Símbolo Izquierdo';
$_['entry_symbol_right']   = 'Símbolo Derecho';
$_['entry_decimal_place']  = 'Número de Decimales';
$_['entry_status']         = 'Estado';

// Ayuda
$_['help_code'] = 'No cambiar si ésta es la Moneda por Defecto. Debe ser válido el<a href="http://www.xe.com/iso4217.php"  target="_blank"> Código ISO </a>. ';
$_['help_value'] = 'Establecer en 1,00 si es la Moneda por Defecto.';

// Error
$_['error_permission'] = 'Sin permiso para modificar monedas.';
$_['error_title'] = 'El Nombre de la Moneda debe contener entre 3 y 32 caractéres.';
$_['error_code'] = 'Código Monedas debe contener 3 caractéres.';
$_['error_default'] = 'Esta Moneda no se puede eliminar, ya que está actualmente asignada como Moneda por Defecto.';
$_['error_store'] = 'Esta Moneda no se puede eliminar ya que se asigna actualmente a los Comercios% s.';
$_['error_order'] = 'Esta Moneda no se puede eliminar ya que se asigna actualmente a los Pedidos% s.';