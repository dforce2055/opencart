<?php
// Heading
$_['heading_title'] = 'Acciones de Devolución';

// Texto
$_['text_success'] = 'Se han modificado las Acciones de Devolución.';
$_['text_list'] = 'Lista de Acciones de Devoluciones';
$_['text_add'] = 'Agregar Acción de Devolución';
$_['text_edit'] = 'Editar Acción de Devolución';

// columna
$_['column_name'] = 'Nombre del Tipo de devolución';
$_['column_action'] = 'Acción';

// Entrada
$_['entry_name'] = 'Nombre del Tipo de Devolución';

// Error
$_['error_permission'] = 'Sin permiso para modificar las Acciones de Devolución.';
$_['error_name'] = 'El nombre del Tipo de devolución debe contener entre 3 y 64 caractéres.';
$_['error_return'] = 'Este Tipo de Devolución no se puede eliminar ya que se asigna actualmente a% s productos devueltos.';