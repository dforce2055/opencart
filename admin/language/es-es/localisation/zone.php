<?php
// Heading
$_['heading_title']          = 'Zonas';

// Text
$_['text_success'] = 'Se han modificado las Zonas.';
$_['text_list'] = 'Lista de Zonas';
$_['text_add'] = 'Agregar Zona';
$_['text_edit'] = 'Editar';

// Columna
$_['column_name'] = 'Nombre de la Zona';
$_['column_code'] = 'Código de la Zona';
$_['column_country'] = 'País';
$_['column_action'] = 'Acción';

// Entrada
$_['entry_name'] = 'Nombre de la Zona';
$_['entry_code'] = 'Código de la Zona';
$_['entry_country'] = 'País';
$_['entry_status'] = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar las Zonas.';
$_['error_name'] = 'El Nombre de la Zona debe contener entre 3 y 128 caractéres.';
$_['error_default'] = 'Esta Zona no puede eliminarse, ya que actualmente está asignada como la Zona por Defecto.';
$_['error_store'] = 'Esta Zona no se puede eliminar ya que se asigna actualmente a los Comercios de% s.';
$_['error_address'] = 'Esta Zona no puede ser eliminada, ya que está actualmente asignado a las entradas de la Libreta de Direcciones% s.';
$_['error_affiliate'] = 'Esta Zona no se puede eliminar ya que se asigna actualmente a los afiliados% s.';
$_['error_zone_to_geo_zone'] = 'Esta Zona no puede ser eliminada, ya que está actualmente asignada a las Zonas de% s para GeoZonas.';