<?php
// Heading
$_['heading_title'] = 'GeoZonas';

// Text
$_['text_success'] = 'Se han modificado las GeoZonas.';
$_['text_list'] = 'Lista de GeoZonas';
$_['text_add'] = 'Agregar GeoZona';
$_['text_edit'] = 'Editar GeoZona';

// column
$_['column_name'] = 'Nombre de la GeoZona';
$_['column_description'] = 'Descripción';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre de la GeoZona';
$_['entry_description'] = 'Descripción';
$_['entry_country'] = 'País';
$_['entry_zone'] = 'Zona';

// Error
$_['error_permission'] = 'Sin permiso para modificar las GeoZonas.';
$_['error_name'] = 'El Nombre de la GeoZona debe contener entre 3 y 32 caractéres.';
$_['error_description'] = 'El Nombre de la Descripción debe contener entre 3 y 255 caractéres.';
$_['error_tax_rate'] = 'Esta GeoZona no puede ser eliminada, ya que está actualmente asignado a una o más tasas de impuestos.';