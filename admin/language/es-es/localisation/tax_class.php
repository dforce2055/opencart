<?php
// Heading
$_['heading_title'] = 'Clases de Impuestos';

// Text
$_['text_success'] = 'Se han modificado las Clases de Impuestos.';
$_['text_list'] = 'Clases de Impuestos';
$_['text_add'] = 'Agregar clase de Impuestos ';
$_['text_edit'] = 'Editar clase de Impuestos';
$_['text_shipping'] = 'Dirección de Envío ';
$_['text_payment'] = 'Dirección de Pago ';
$_['text_store'] = 'Dirección del Comercio';

// Column
$_['column_title'] = 'Nombre';
$_['column_action'] = 'Acción';

// Entry
$_['entry_title'] = 'Nombre de la Clase';
$_['entry_description'] = 'Descripción';
$_['entry_rate'] = 'Tasa de Impuesto ';
$_['entry_based'] = 'Basado en la ';
$_['entry_geo_zone'] = 'GeoZona';
$_['entry_priority'] = 'Prioridad';

// Error
$_['error_permission'] = 'Sin permiso para modificar las Clases de Impuestos.';
$_['error_title'] = 'El Nombre debe contener entre 3 y 32 caractéres.';
$_['error_description'] = 'La Descripción debe contener entre 3 y 255 caractéres.';
$_['error_product'] = 'Esta clase de impuestos no puede ser eliminada, ya que se asigna actualmente a los productos de% s.';