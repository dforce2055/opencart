<?php
// Heading
$_['heading_title'] = 'Razones de Devolución';

// Text
$_['text_success'] = 'Se han modificado las Razones de Devolución';
$_['text_list'] = 'Lista de Razones de Devolución';
$_['text_add'] = 'Agregar Razón de Devolución';
$_['text_edit'] = 'Editar Razón de Devolución';

// column
$_['column_name'] = 'Nombre de la Razón';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Razón';

// Error
$_['error_permission'] = 'Sin permiso para modificar las Razones de Devolución.';
$_['error_name'] = 'El Nombre de la Razón debe contener entre 3 y 128 caractéres.';
$_['error_return'] = 'Esta razón no se puede eliminar ya que está asignada actualmente a% s productos devueltos.';