<?php
// Heading
$_['heading_title'] = 'Estado de Devoluciones';

// Text
$_['text_success'] = 'Se ha modificado el Estado de Devolución.';
$_['text_list'] = 'Estados de Devoluciones';
$_['text_add'] = 'Agregar Estado';
$_['text_edit'] = 'Editar Estado';

// column
$_['column_name'] = 'Estado';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Estados de Devoluciones';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Estado de Devolución.';
$_['error_name'] = 'El nombre del Estado de Devolución debe contener entre 3 y 32 caractéres.';
$_['error_default'] = 'Este estado de Devolución no se puede eliminar, ya que está actualmente asignado como el Estado de Devolución por Defecto.';
$_['error_return'] = 'Este estado de Devolución no se puede eliminar ya que se asigna actualmente a las devoluciones% s.';