<?php
// Heading
$_['heading_title'] = 'Ubicación del Comercio';

// Text
$_['text_success'] = 'Se ha modificado la ubicación del Comercio.';
$_['text_list'] = 'Listado Ubicación del Comercio';
$_['text_add'] = 'Agregar Ubicación del Comercio';
$_['text_edit'] = 'Editar Ubicación del Comercio';
$_['text_default'] = 'Por defecto';
$_['text_time'] = 'Horario';
$_['text_geocode'] = 'Geocode no se cargó por la siguiente razón:';

// Column
$_['column_name'] = 'Nombre del Comercio';
$_['column_address'] = 'Dirección';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre del Comercio';
$_['entry_address'] = 'Dirección';
$_['entry_geocode'] = 'Geocode';
$_['entry_telephone'] = 'Teléfono';
$_['entry_fax'] = 'Fax';
$_['entry_image'] = 'Imagen';
$_['entry_open'] = 'Horario';
$_['entry_comment'] = 'Comentario';

// Ayuda
$_['help_geocode'] = 'Introducir la Ubicación codificada geográfica manualmente del Comercio.';
$_['help_open'] = 'Horario de Atención.';
$_['help_comment'] = 'Comentarios Especiales.';

// Error
$_['error_permission'] = 'Sin permiso para modificar Ubicaciones de los Comercios.';
$_['error_name'] = 'Nombre del Comercio debe contener al menos 1 carácter.';
$_['error_address'] = 'La Dirección debe contener entre 3 y 128 caractéres.';
$_['error_telephone'] = 'El Teléfono debe contener entre 3 y 32 caractéres.';