<?php
// Heading
$_['heading_title'] = 'Idiomas';

// Text
$_['text_success'] = 'Se han modificado los Idiomas.';
$_['text_list'] = 'Lista de Idiomas';
$_['text_add'] = 'Agregar Idioma ';
$_['text_edit'] = 'Editar Idioma';

// Column
$_['column_name'] = 'Nombre de Idioma';
$_['column_code'] = 'Código';
$_['column_sort_order'] = 'Orden';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre de Idioma';
$_['entry_code'] = 'Código';
$_['entry_locale'] = 'Local';
$_['entry_image'] = 'Imagen';
$_['entry_directory'] = 'Directorio';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Orden';

// help
$_['help_code'] = 'Ejemplo: en.';
$_['help_locale'] =  'Ejemplo: en_US.UTF-8, es_ES, es-es, es_ES, Inglés';
$_['help_image'] = 'Ejemplo: gb.png';
$_['help_directory'] = 'Nombre del directorio de idioma (mayúsculas y minúsculas)';
$_['help_status'] = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar los Idiomas.';
$_['error_name'] = 'El Nombre de Idioma debe contener entre 3 y 32 caractéres.';
$_['error_code'] = 'El Código del Lenguaje debe contener al menos tener 2 caractéres.';
$_['error_locale'] = 'Configuración Regiónal Obligatoria.';
$_['error_image'] = 'El Nombre del Archivo de Imagen debe contener entre 3 y 64 caractéres.';
$_['error_directory'] = 'Directorio Obligatorio.';
$_['error_default'] = 'Este lenguaje no se puede eliminar, ya que está actualmente asignado como el idioma por Defecto.';
$_['error_admin'] = 'Este lenguaje no se puede eliminar, ya que está actualmente asignado como el idioma del Administrador.';
$_['error_store'] = 'Este lenguaje no se puede eliminar ya que se asigna actualmente a los Comercios% s.';
$_['error_order'] = 'Este lenguaje no se puede eliminar ya que se asigna actualmente a los Pedidos% s.';