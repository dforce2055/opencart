<?php
// Heading
$_['heading_title']           = 'Países';

// Text
$_['text_success']            = 'Se han modificado los Países.';
$_['text_list']               = 'Lista de Países';
$_['text_add']                = 'Agregar país';
$_['text_edit']               = 'Editar país';

// Column
$_['column_name']             = 'Nombre del País';
$_['column_iso_code_2']       = 'ISO Código (2)';
$_['column_iso_code_3']       = 'ISO Código (3)';
$_['column_action']           = 'Acción';

// Entry
$_['entry_name']              = 'Nombre del País';
$_['entry_iso_code_2']        = 'ISO Código (2)';
$_['entry_iso_code_3']        = 'ISO Código (3)';
$_['entry_address_format']    = 'Formato de Dirección';
$_['entry_postcode_necesario'] = 'Código Obligatorio';
$_['entry_status']            = 'Estado';

// Ayuda
$_['help_address_format']     = 'Nombre = {nombre}<br />Apellidos = {apellido}<br />Empresa = {empresa}<br />Dirección 1 = {dirección_1}<br />Dirección 2 = {dirección_2}<br />Ciudad = {ciudad}<br />Código Postal = {cp}<br />Provincia = {zona}<br />Código Provincia = {code_prov}<br />país = {país}';

// Error
$_['error_permission']        = 'Sin permiso para modificar el países.';
$_['error_name']              = 'El nombre del paí debe contener entre 3 y 128 caractéres.';
$_['error_default']           = 'Este país no se puede eliminar, ya que está asignado a la Moneda por Defecto del Comercio.';
$_['error_store']             = 'Este país no se puede eliminar, ya que está asignado a %s Comercios.';
$_['error_address'] 		  = 'Este país no puede ser eliminado, ya que está actualmente asignado a las entradas de la libreta de direcciones% s.';
$_['error_affiliate']         = 'Este país no puede ser eliminado, ya que se asigna actualmente a los afiliados% s.';
$_['error_zone'] 			  = 'Este país no puede ser eliminado, ya que está actualmente asignado a las Zonas de% s.';
$_['error_zone_to_geo_zone']  = 'Este país no puede ser eliminado, ya que está actualmente asignado a las Zonas de% s para GeoZonas.';