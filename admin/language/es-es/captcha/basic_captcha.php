<?php
$_['heading_title']    = 'Captcha Básico';

// Text
$_['text_captcha']     = 'Captcha';
$_['text_success']	   = 'Se ha modificado el Captcha Básico.';
$_['text_edit']        = 'Editar Captcha Básico';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar Captcha Básico.';
