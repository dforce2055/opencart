<?php
// Heading
$_['heading_title']                    = 'Comercios';

// Text
$_['text_settings'] = 'Ajustes';
$_['text_success'] = 'Se han modificado los Comercios.';
$_['text_list'] = 'Lista de Comercios';
$_['text_add'] = 'Agregar Comercio';
$_['text_edit'] = 'Editar Comercio';
$_['text_items'] = 'Artículos';
$_['text_tax'] = 'Impuestos';
$_['text_account'] = 'Cuenta';
$_['text_checkout'] = 'Pagar';
$_['text_stock'] = 'Stock';
$_['text_shipping'] = 'Dirección de Envío';
$_['text_payment'] = 'Dirección de Pago ';

// Column
$_['column_name'] = 'Nombre del Comercio';
$_['column_url'] = 'URL del Comercio';
$_['column_action'] = 'Acción';

// Entry
$_['entry_url'] = 'URL del Comercio';
$_['entry_ssl'] = 'URL SSL';
$_['entry_name'] = 'Nombre del Comercio';
$_['entry_owner'] = 'Propietario del Comercio';
$_['entry_address'] = 'Dirección';
$_['entry_geocode'] = 'Geocode';
$_['entry_email'] = 'Email';
$_['entry_telephone'] = 'Teléfono';
$_['entry_fax'] = 'Fax';
$_['entry_image'] = 'Imagen';
$_['entry_open'] = 'Horario';
$_['entry_comment'] = 'Comentario';
$_['entry_location'] = 'Ubicación del Comercio';
$_['entry_meta_title'] = 'Meta Título';
$_['entry_meta_description'] = 'Meta Descripción';
$_['entry_meta_keyword'] = 'Meta Palabras Clave';
$_['entry_layout'] = 'Diseño predeterminado';
$_['entry_template'] = 'Plantilla';
$_['entry_country'] = 'País';
$_['entry_zone'] = 'Provincia/Estado';
$_['entry_language'] = 'Idioma';
$_['entry_currency'] = 'Moneda';
$_['entry_product_limit'] = 'Cantidad por Defecto de Productos por página (Catálogo)';
$_['entry_product_description_length'] = 'Descripción Límite (Catálogo)';
$_['entry_tax'] = 'Mostrar precios con impuestos';
$_['entry_tax_default'] = 'Usar Dirección del Comercio para Impuestos';
$_['entry_tax_customer'] = 'Usar la Dirección del Cliente para Impuestos';
$_['entry_customer_group'] = 'Grupo de Clientes';
$_['entry_customer_group_display'] = 'Grupos de clientes';
$_['entry_customer_price'] = 'Mostrar precios a clientes Registrados';
$_['entry_account'] = 'Términos de la Cuenta';
$_['entry_cart_weight'] = 'Mostrar Peso del Carro de Compras';
$_['entry_checkout_guest'] = 'Permitir Compras sin Registro';
$_['entry_checkout'] = 'Condiciones del Pedido';
$_['entry_order_status'] = 'Estado del Pedido';
$_['entry_stock_display'] = 'Mostrar stock en el Producto';
$_['entry_stock_checkout'] = 'Comprar sin stock';
$_['entry_logo'] = 'Logo del Comercio';
$_['entry_icon'] = 'Icono';
$_['entry_image_category'] = 'Tamaño de la Imagen del Rubro';
$_['entry_image_thumb'] = 'Tamaño de la Imagen del Producto';
$_['entry_image_popup'] = 'Tamaño de la Imagen Ventana Emergente del Producto';
$_['entry_image_product'] = 'Tamaño de las Imagenes en la Lista de Productos';
$_['entry_image_additional'] = 'Tamaño de la Imagen adicional del Producto';
$_['entry_image_related'] = 'Tamaño de la Imagen de Productos Relacionados';
$_['entry_image_compare'] = 'Tamaño de la Imagen en el Comparador';
$_['entry_image_wishlist'] = 'Tamaño de la Imagen el la Lista de deseos';
$_['entry_image_cart'] = 'Tamaño de la imagen en el Carro de Compras';
$_['entry_image_location'] = 'Tamaño de la imagen del Comercio';
$_['entry_width'] = 'Ancho';
$_['entry_height'] = 'Altura';
$_['entry_secure'] = 'Usar SSL';

// Ayuda
$_['HELP_URL'] = 'Incluir la URL completa al Comercio. Asegurarse de Agregar \ al final. Ejemplo: http://www.yourdomain.com/path/ <br /> <br /> no utilizar directorios para crear un nuevo Comercios. Siempre se debe introducir otro dominio o subdominio al hosting. ';
$_['help_ssl'] = 'URL SSL al Comercio. Asegurarse de Agregar \ al final. Ejemplo: http://www.yourdomain.com/path/ <br /> <br /> no utilizar directorios para crear un nuevo Comercio. Siempre se debe introducir otro dominio o subdominio al hosting. ';
$_['help_geocode'] = 'Ingresar la Ubicación Geográfica Codificada manualmente del Comercio.';
$_['help_open'] = 'Horario de Atención.';
$_['help_comment'] = 'Mensajes Especiales.';
$_['help_location'] = ' Las diferentes ubicaciones de los Comercios para mostrar en el formulario de contacto';
$_['help_currency'] = 'Cambiar la moneda por defecto. Borrar la caché del navegador para ver el cambio y restablecer la cookie existente'.
$_['help_product_limit'] = 'Determina cuántos se muestran artículos del catálogo por página (productos, rubros, etc.)';
$_['help_product_description_length'] = 'En la vista de lista, límite descripción corta de caractéres (rubros, etc especial) ';
$_['help_tax_default'] = 'Utilizar la dirección del Comercio para el cálculo de los impuestos si no hay nadie conectado se puede optar por utilizar la dirección del Comercio para el envío de los clientes o la dirección de pago.';
$_['help_tax_customer'] = 'Use la dirección de los clientes de forma predeterminada cuando se inicia sesión para calcular los impuestos. Se puede optar por utilizar la dirección por Defecto para el envío de los clientes o la dirección de pago.';
$_['help_customer_group'] = 'grupo de clientes por defecto.';
$_['help_customer_group_display'] = 'Grupos de Clientes de visualización que los nuevos clientes pueden seleccionar para utilizar como negocio al por mayor y al registrarse.';
$_['help_customer_price'] = 'Sólo los precios muestran cuando un cliente se registra en.';
$_['help_account'] = 'obliga a la gente a concedes a las condiciones antes de una cuenta se puede crear.';
$_['help_checkout_guest'] = 'Permitir a los clientes a la comprobación sin crear una cuenta. Esto no va a estar disponible cuando un producto descargable está en el Carro de Compra.';
$_['help_checkout'] = 'Obliga al Cliente a aceptar las  condiciones antes de pagar el pedido.';
$_['help_order_status'] = 'Establecer el estado de la orden por defecto cuando una orden es procesada.';
$_['help_stock_display'] = 'Mostrar cantidad de stock en la página del producto.';
$_['help_stock_checkout'] = 'Permitir a los clientes comprar aunque los productos no tengan stock.';
$_['help_icon'] = 'El icono debe ser un PNG que es 16px x 16px.';
$_['help_secure'] = 'Para utilizar SSL verificar si se ha instalado un certificado SSL.';

// Error
$_['error_warning'] = 'El Formulario contiene Errores.';
$_['error_permission'] = 'Sin permiso para modificar los Comercios.';
$_['error_name'] = 'Nombre del Comercio debe contener entre 3 y 32 caractéres.';
$_['error_owner'] = 'El Nombre del Propietario del Comercio debe contener entre 3 y 64 caractéres.';
$_['error_address'] = 'La Dirección del Comercio debe contener entre 10 y 256 caractéres.';
$_['error_email'] = 'Email inválido.';
$_['error_telephone'] = 'El Teléfono debe contener entre 3 y 32 caractéres.';
$_['error_url'] = 'URL Obligatoria.';
$_['error_meta_title'] = 'El Título debe contener entre 3 y 32 caractéres.';
$_['error_limit'] = 'Límite Obligatorio.';
$_['error_customer_group_display'] = 'Incluir el Grupo de Clientes por defecto si se va a utilizar esta función.';
$_['error_image_thumb'] = 'Las dimensiones de la Imagen en miniatura son Obligatorias.';
$_['error_image_popup'] = 'Las dimensiones de la Imagen del Ventana Emergente son Obligatorias.';
$_['error_image_product'] = 'Las dimensiones de la Imagen del Producto son Obligatorias.';
$_['error_image_category'] = 'Las dimensiones de la Imagen de los Rubros son Obligatorias.';
$_['error_image_additional'] = 'Las dimensiones de las Imágenes Adicionales del Producto son Obligatorias.';
$_['error_image_related'] = 'Las dimensiones de la Imagen de Productos Relacionados son Obligatorias.';
$_['error_image_compare'] = 'Las dimensiones de la Imagen en en el Comparador son Obligatorias.';
$_['error_image_wishlist'] = 'Las dimensiones de la Imagen de la Lista de Deseos son Obligatorias.';
$_['error_image_cart'] = 'Las dimensiones de la Imagen en el Carro de Compras son Obligatorias.';
$_['error_image_location'] = 'Las dimensiones de la Imagen del Comercio son Obligatorias.';
$_['error_default'] = 'No se puede eliminar el Comercio predeterminado.';
$_['error_store'] = 'Este Comercio no se puede eliminar ya que está asignado actualmente a los Pedidos% s';