<?php
// Heading
$_['heading_title']		 = 'Sin Cargo';

// Text
$_['text_payment']		 = 'Pago';
$_['text_success']		 = 'Se ha modificado el Módulo de Pago Sin Cargo.';
$_['text_edit']          = 'Editar Módulo Sin Cargo';

// Entry
$_['entry_order_status'] = 'Estado del Pedido';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden';

// Error
$_['error_permission']	  = 'Sin permiso para modificar el Método de Pago Sin Cargo.';