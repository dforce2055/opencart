<?php
// Heading
$_['heading_title']					= 'Pago contra Entrega';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= 'Se ha modificado el Módulo Pago contra Entrega.';
$_['text_edit']                     = 'Editar Pago contra Entrega';

// Entry
$_['entry_total']					= 'Total';
$_['entry_order_status']			= 'Estado del Pedido';
$_['entry_geo_zone']				= 'GeoZona';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Orden';

// Ayuda
$_['help_total']					= 'Total del Pedido para que este Método de Pago sea Válido.';

// Error
$_['error_permission']				= 'Sin permiso para Modificar el Pago contra Entrega.';