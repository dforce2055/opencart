<?php
// Heading
$_['heading_title'] = 'Extensión del Instalador';

// Text
$_['text_success'] = 'Se ha instalado una Extensión.';
$_['text_unzip'] = 'Extracción de Archivos.';
$_['text_ftp'] = 'Copia de Archivos.';
$_['text_sql'] = 'Ejecutar SQL.';
$_['text_xml'] = 'Aplicar Modificaciones.';
$_['text_php'] = 'Ejecutar PHP.';
$_['text_remove'] = 'La eliminación de archivos temporales.';
$_['text_clear'] = 'Se han desactivado todos los archivos temporales.';

// Entry
$_['entry_upload'] = 'Subir archivo';
$_['entry_overwrite'] = 'Archivos a Sobrescribir' ;
$_['entry_progress'] = 'Progreso';

// Ayuda
$_['help_upload'] = 'Se requiere un archivo de modificación con extensión ".ocmod.zip" o ".ocmod.xml".';

// Error
$_['error_permission'] = 'Sin permiso para modificar las extensiones.';
$_['error_temporary'] = 'Hay algunos archivos temporales que requieren la eliminación. Hacer click en el botón de borrado para eliminarlos.';
$_['error_upload'] = 'El archivo no se pudo cargar.';
$_['error_filetype'] = 'Tipo de archivo inválido.';
$_['error_file'] = 'El archivo no se pudo encontrar.';
$_['error_unzip'] = 'archivo Zip no pudo ser abierto.';
$_['error_code'] = 'La modificación requiere un código de identificación único.';
$_['error_exists'] = 'Modificación% s es utilizando el mismo código de identificación como el que se está tratando de subir.';
$_['error_directory'] = 'El Directorio que contiene los archivos a subir no se pudo encontrar.';
$_['error_ftp_status'] = 'El FTP debe estar habilitado en la configuración.';
$_['error_ftp_connection'] = 'No se pudo conectar en% s:% s.';
$_['error_ftp_login'] = 'No se pudo iniciar sesión en% s.';
$_['error_ftp_root'] = 'No se pudo configurar el directorio raíz% s.';
$_['error_ftp_directory'] = 'No se ha podido cambiar al directorio% s.';
$_['error_ftp_file'] = 'No se pudo cargar el archivo% s.';