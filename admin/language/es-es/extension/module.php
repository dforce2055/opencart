<?php
// Heading
$_['heading_title']    = 'Módulos';

// Text
$_['text_success']     = 'Se ha modificado un módulo.';
$_['text_layout']      = 'Después de haber instalado y configurado un módulo se puede agregar a un diseño desde <a href="%s" class="alert-link">aquí</a>.';
$_['text_add']         = 'Agregar Módulo';
$_['text_list']        = 'Lista de Módulos';

// Column
$_['column_name']      = 'Nombre del Módulo';
$_['column_action']    = 'Acción';

// Entry
$_['entry_code']       = 'Módulo';
$_['entry_name']       = 'Nombre del Módulo';

// Error
$_['error_permission'] = 'Sin permisos para modificar Módulos.';
$_['error_name']       = 'El nombre del Módulo debe contener entre 3 y 64 caracteres.';
$_['error_code']       = 'Extensión Obligatoria.';