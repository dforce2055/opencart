<?php
// Heading
$_['heading_title']      = 'Anti-Fraude IP';

// Text
$_['text_extension']     = 'Extensiones';
$_['text_success']       = 'Éxito: ¡Has modificado IP Anti-Fraude!';
$_['text_edit']          = 'Editar IP Anti-Fraude';
$_['text_ip_add']        = 'Agregar Dirección de IP';
$_['text_ip_list']       = 'Fraude: Lista de direcciones de IP';

// Column
$_['column_ip']          = 'IP';
$_['column_total']       = 'Total de Cuentas';
$_['column_date_added']  = 'Fecha añadido';
$_['column_action']      = 'Acción';

// Entry
$_['entry_ip']           = 'IP';
$_['entry_status']       = 'Estado';
$_['entry_order_status'] = 'Estado de pedido';

// Ayuda
$_['help_order_status']  = 'Este estado de pedido será asignado a los clientes que tengan una IP baneada en sus cuentas y no podrán alcanzar el estado completado automáticamente.';

// Error
$_['error_permission']   = 'Advertencia: ¡No tiene permisos para modificar IP Anti-Fraude!';