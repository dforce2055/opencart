<?php
// Heading
$_['heading_title']              = 'FraudLabs Pro';

// Text
$_['text_extension']             = 'Extensiones';
$_['text_success']               = 'Éxito: ¡Has modificado configuración de FraudLabs Pro!';
$_['text_edit']                  = 'Configuraciones';
$_['text_signup']                = 'FraudLabs Pro is es un servicio de detección de fraude. Puede <a href="http://www.fraudlabspro.com/plan?ref=1730" target="_blank"><u>registrarse aquí</u></a> para recibir una Clave de API gratuita.';
$_['text_id']                    = 'ID FraudLabs Pro';
$_['text_ip_address']            = 'Dirección de IP';
$_['text_ip_net_speed']          = 'Velocidad de la Red de IP';
$_['text_ip_isp_name']           = 'Nombre del ISP de IP';
$_['text_ip_usage_type']         = 'Tipo de Uso de IP';
$_['text_ip_domain']             = 'Dominio de IP';
$_['text_ip_time_zone']          = 'Zona Horaria de IP';
$_['text_ip_location']           = 'Localización de IP';
$_['text_ip_distance']           = 'Distancia de IP';
$_['text_ip_latitude']           = 'Latitud de IP';
$_['text_ip_longitude']          = 'Longitud de IP';
$_['text_risk_country']          = 'País Alto Riesgo';
$_['text_free_email']            = 'Email gratuito';
$_['text_ship_forward']          = 'Reenvío';
$_['text_using_proxy']           = 'Usando Proxy';
$_['text_bin_found']             = 'BIN Encontrado';
$_['text_email_blacklist']       = 'Lista Negra de Correos Electrónicos';
$_['text_credit_card_blacklist'] = 'Lista Negra de Tarjetas de Crédito';
$_['text_score']                 = 'Puntaje FraudLabs Pro';
$_['text_status']                = 'Estado FraudLabs Pro';
$_['text_message']               = 'Mensaje';
$_['text_transaction_id']        = 'Transaction ID';
$_['text_credits']               = 'Balance';
$_['text_error']                 = 'Error:';
$_['text_flp_upgrade']           = '<a href="http://www.fraudlabspro.com/plan" target="_blank">[Mejorar las Prestaciones]</a>';
$_['text_flp_merchant_area']     = 'Por favor inicie sesión en <a href="http://www.fraudlabspro.com/merchant/login" target="_blank">FraudLabs Pro Zona Comercial</a> para más información sobre de este pedido.';

// Entry
$_['entry_status']               = 'Estado';
$_['entry_key']                  = 'Clave de API';
$_['entry_score']                = 'Puntuación de Riesgo';
$_['entry_order_status']         = 'Estado de pedido';
$_['entry_review_status']        = 'Estado de revisión';
$_['entry_approve_status']       = 'Estado de aprobación';
$_['entry_reject_status']        = 'Estado de rechazo';
$_['entry_simulate_ip']          = 'Simular IP';

// Ayuda
$_['help_order_status']          = 'Este estado de pedido será asignado a pedidos con una puntuación por encima de la puntuación de riesgo que haya elegido';
$_['help_review_status']         = 'Este estado de pedido será asignado a pedidos marcados como "revisar" por FraudLabs Pro.';
$_['help_approve_status']        = 'Este estado de pedido será asignado a pedidos marcados como aprobados por FraudLabs Pro.';
$_['help_reject_status']         = 'Este estado de pedido será asignado a pedidos marcados como rechazados por FraudLabs Pro.';
$_['help_simulate_ip']           = 'Simular IP del visitante para pruebas. Dejar en blanco para desactivar.';
$_['help_fraudlabspro_id']       = 'Identificador único para una transacción examinada por el sistema de FraudLabs Pro.';
$_['help_ip_address']            = 'Dirección de IP.';
$_['help_ip_net_speed']          = 'Velocidad de conexión.';
$_['help_ip_isp_name']           = 'ISP de la dirección de IP.';
$_['help_ip_usage_type']         = 'Tipo de uso de la dirección de IP. Por ejemplo, ISP, Comercial, Residencial.';
$_['help_ip_domain']             = 'Nombre de dominio de la dirección de IP.';
$_['help_ip_time_zone']          = 'Zona horaria de la dirección de IP.';
$_['help_ip_location']           = 'Localización de la dirección de IP.';
$_['help_ip_distance']           = 'Distancia desde dirección IP a ubicación de facturación.';
$_['help_ip_latitude']           = 'Latitud de la dirección de IP.';
$_['help_ip_longitude']          = 'Longitud de la dirección de IP.';
$_['help_risk_country']          = 'Si el país de la dirección IP esta en la más reciente lista de países de alto riesgo.';
$_['help_free_email']            = 'Si el e-mail es de un proveedor gratuito.';
$_['help_ship_forward']          = 'Si la dirección de envío es la dirección de un flete tercerizado.';
$_['help_using_proxy']           = 'Si la IP es de un Servidor Proxy Anónimo.';
$_['help_bin_found']             = 'Si la información BIN coincide con nuestra lista BIN.';
$_['help_email_blacklist']       = 'Si la dirección email está en nuestra base de datos de lista negra.';
$_['help_credit_card_blacklist'] = 'Si la tarjeta de crédito está en nuestra base de datos de lista negra.';
$_['help_score']                 = 'Puntiación de riesgo, 0 (riesgo bajo) - 100 (riesgo alto).';
$_['help_status']                = 'Estado FraudLabs Pro.';
$_['help_message']               = 'Descripción de mensaje de error de FraudLabs Pro.';
$_['help_transaction_id']        = 'Identificador único para una transacción examinada por el sistema de FraudLabs Pro.';
$_['help_credits']               = 'Balance de créditos disponibles luego de esta transacción.';

// Error
$_['error_permission']           = 'Advertencia: ¡No tiene permisos para modificar configuración de FraudLabs Pro!';
$_['error_key']                  = '¡Clave de API necesaria!';