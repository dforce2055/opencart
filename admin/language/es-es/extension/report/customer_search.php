<?php
// Heading
$_['heading_title']     = 'Reporte Búsquedas del Cliente';

// Text
$_['text_extension']    = 'Extensiones';
$_['text_edit']         = 'Editar Reporte Búsquedas del Cliente';
$_['text_success']      = 'Éxito: ¡Has modificado reporte de búsquedas del cliente!';
$_['text_filter']       = 'Filtro';
$_['text_guest']        = 'Invitado';
$_['text_customer']     = '<a href="%s">%s</a>';

// Column
$_['column_keyword']    = 'Palabra Clave';
$_['column_products']   = 'Encontrado Productos';
$_['column_category']   = 'Categoría';
$_['column_customer']   = 'Cliente';
$_['column_ip']         = 'IP';
$_['column_date_added'] = 'Fecha añadido';

// Entry
$_['entry_date_start']  = 'Fecha de Inicio';
$_['entry_date_end']    = 'Fecha de Finalización';
$_['entry_keyword']     = 'Palabra Clave';
$_['entry_customer']    = 'Cliente';
$_['entry_ip']          = 'IP';
$_['entry_status']      = 'Estado';
$_['entry_sort_order']  = 'Orden de Clasificación';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar reporte de búsquedas del cliente!';