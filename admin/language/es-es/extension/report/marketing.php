<?php
// Heading
$_['heading_title']    = 'Reporte de Marketing';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_edit']        = 'Editar Reporte de Marketing';
$_['text_success']     = 'Éxito: Has modificado reporte de marketing!';
$_['text_filter']      = 'Filtro';
$_['text_all_status']  = 'Todos los Estados';

// Column
$_['column_campaign']  = 'Nombre de campaña';
$_['column_code']      = 'Código';
$_['column_clicks']    = 'Clicks';
$_['column_orders']    = 'Nro. de Pedidos';
$_['column_total']     = 'Total';

// Entry
$_['entry_date_start'] = 'Fecha de Inicio';
$_['entry_date_end']   = 'Fecha de Finalización';
$_['entry_status']     = 'Estado de pedido';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar reporte de marketing!';