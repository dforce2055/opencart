<?php
// Heading
$_['heading_title']         = 'Reporte Puntos de Recompensa del Cliente';

// Text
$_['text_extension']        = 'Extensiones';
$_['text_edit']             = 'Editar Reporte Puntos de Recompensa del Cliente';
$_['text_success']          = 'Éxito: Has modificado reporte de puntos de recompensa de clientes!';
$_['text_filter']           = 'Filtro';

// Column
$_['column_customer']       = 'Nombre del cliente';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Grupo de clientes';
$_['column_status']         = 'Estado';
$_['column_points']         = ' Puntos de Recompensa';
$_['column_orders']         = 'Nro. de Pedidos';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';

// Entry
$_['entry_date_start']      = 'Fecha de Inicio';
$_['entry_date_end']        = 'Fecha de Finalización';
$_['entry_customer']        = 'Cliente';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Orden de Clasificación';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar reporte de puntos de recompensa de clientes!';
