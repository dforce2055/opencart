<?php
// Heading
$_['heading_title']    = 'Reporte de Cupones';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_edit']        = 'Editar Reporte de Cupones';
$_['text_success']     = 'Éxito: ¡Has modificado reporte de cupones!';
$_['text_filter']      = 'Filtro';

// Column
$_['column_name']      = 'Nombre de Cupón';
$_['column_code']      = 'Código';
$_['column_orders']    = 'Pedidos';
$_['column_total']     = 'Total';
$_['column_action']    = 'Acción';

// Entry
$_['entry_date_start'] = 'Fecha de Inicio';
$_['entry_date_end']   = 'Fecha de Finalización';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar reporte de cupones!';