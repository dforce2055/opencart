<?php
// Heading
$_['heading_title']         = 'Reporte Transacción del Cliente';

// Column
$_['text_extension']        = 'Extensiones';
$_['text_edit']             = 'Editar Reporte Transacción del Cliente';
$_['text_success']          = 'Éxito: ¡Has modificado reporte de crédito de cliente!';
$_['text_filter']           = 'Filtro';

// Column
$_['column_customer']       = 'Nombre de Cliente';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Grupo de Cliente';
$_['column_status']         = 'Estado';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';

// Entry
$_['entry_date_start']      = 'Fecha de Inicio';
$_['entry_date_end']        = 'Fecha de Finalización';
$_['entry_customer']        = 'Cliente';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Orden de Clasificación';

// Error
$_['error_permission']      = 'Advertencia: ¡No tiene permisos para modificar reporte de crédito de cliente!';