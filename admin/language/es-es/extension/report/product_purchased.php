<?php
// Heading
$_['heading_title']     = 'Reporte de Productos Comprados';

// Text
$_['text_extension']    = 'Extensiones';
$_['text_edit']         = 'Editar Reporte de Productos Comprados';
$_['text_success']      = 'Éxito: ¡Has modificado reporte de productos comprados!';
$_['text_filter']       = 'Filtro';
$_['text_all_status']   = 'Todos los Estados';

// Column
$_['column_date_start'] = 'Fecha de Inicio';
$_['column_date_end']   = 'Fecha de Finalización';
$_['column_name']       = 'Nombre de Producto';
$_['column_model']      = 'Modelo';
$_['column_quantity']   = 'Cantidad';
$_['column_total']      = 'Total';

// Entry
$_['entry_date_start']  = 'Fecha de Inicio';
$_['entry_date_end']    = 'Fecha de Finalización';
$_['entry_status']      = 'Order Estado';
$_['entry_status']      = 'Estado';
$_['entry_sort_order']  = 'Orden de Clasificación';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar reporte de productos comprados!';