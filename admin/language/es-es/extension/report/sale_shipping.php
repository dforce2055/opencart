<?php
// Heading
$_['heading_title']     = 'Reporte de Envíos';

// Text
$_['text_extension']    = 'Extensiones';
$_['text_edit']         = 'Editar Reporte de Envíos';
$_['text_success']      = 'Éxito: ¡Has modificado reporte de envíos!';
$_['text_filter']       = 'Filtro';
$_['text_year']         = 'Años';
$_['text_month']        = 'Meses';
$_['text_week']         = 'Semanas';
$_['text_day']          = 'Días';
$_['text_all_status']   = 'Todos los Estados';

// Column
$_['column_date_start'] = 'Fecha de Inicio';
$_['column_date_end']   = 'Fecha de Finalización';
$_['column_title']      = 'Título de Envío';
$_['column_orders']     = 'Nro. de Pedidos';
$_['column_total']      = 'Total';

// Entry
$_['entry_date_start']  = 'Fecha de Inicio';
$_['entry_date_end']    = 'Fecha de Finalización';
$_['entry_group']       = 'Agrupar Por';
$_['entry_status']      = 'Order Estado';
$_['entry_status']      = 'Estado';
$_['entry_sort_order']  = 'Orden de Clasificación';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar reporte de envíos!';