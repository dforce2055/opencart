<?php
// Heading
$_['heading_title']         = 'Reporte Pedidos del Cliente';

// Text
$_['text_extension']        = 'Extensiones';
$_['text_edit']             = 'Editar Reporte Pedidos del Cliente';
$_['text_success']          = 'Éxito: Has modificado Reporte Pedidos del Cliente!';
$_['text_filter']           = 'Filtro';
$_['text_all_status']       = 'Todos los Estados';

// Column
$_['column_customer']       = 'Nombre de cliente';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Grupo de cliente';
$_['column_status']         = 'Estado';
$_['column_orders']         = 'Nro. de Pedidos';
$_['column_products']       = 'Nro. de Productos';
$_['column_total']          = 'Total';
$_['column_action']         = 'Acción';

// Entry
$_['entry_date_start']      = 'Fecha de Inicio';
$_['entry_date_end']        = 'Fecha de Finalización';
$_['entry_customer']        = 'Cliente';
$_['entry_status']          = 'Estado de pedido';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Orden de Clasificación';

// Error
$_['error_permission']  = 'Advertencia: No tiene permisos para modificar Reporte Pedidos del Cliente!';