<?php
// Heading
$_['heading_title']                = 'Reporte Actividad del Cliente';

// Text
$_['text_extension']               = 'Extensiones';
$_['text_edit']                    = 'Editar Reporte Actividad del Cliente';
$_['text_success']                 = 'Éxito: ¡Has modificado reporte de actividad del cliente!';
$_['text_filter']                  = 'Filtro';
$_['text_activity_register']       = '<a href="customer_id=%d">%s</a> se registró.';
$_['text_activity_edit']           = '<a href="customer_id=%d">%s</a> actualizó sus detalles de cuenta.';
$_['text_activity_password']       = '<a href="customer_id=%d">%s</a> actualizó su contraseña de cuenta.';
$_['text_activity_reset']          = '<a href="customer_id=%d">%s</a> reinició su contraseña de cuenta.';
$_['text_activity_login']          = '<a href="customer_id=%d">%s</a> inició sesión.';
$_['text_activity_forgotten']      = '<a href="customer_id=%d">%s</a> solicitó un reinicio de contraseña.';
$_['text_activity_address_add']    = '<a href="customer_id=%d">%s</a> añadió una dirección.';
$_['text_activity_address_edit']   = '<a href="customer_id=%d">%s</a> actualizó su dirección.';
$_['text_activity_address_delete'] = '<a href="customer_id=%d">%s</a> borró una de sus direcciones.';
$_['text_activity_return_account'] = '<a href="customer_id=%d">%s</a> envío una devolución de pr oducto.';
$_['text_activity_return_guest']   = '%s envió una devolución dep r oducto.';
$_['text_activity_order_account']  = '<a href="customer_id=%d">%s</a> creó un <a href="order_id=%d">nuevo pedido</a>.';
$_['text_activity_order_guest']    = '%s creó un <a href="order_id=%d">nuevo pedido</a>.';
$_['text_activity_affiliate_add']  = '<a href="customer_id=%d">%s</a> se registró para una cuenta de afiliado.';
$_['text_activity_affiliate_edit'] = '<a href="customer_id=%d">%s</a> actualizó sus detalles de afiliado.';
$_['text_activity_transaction']    = '<a href="customer_id=%d">%s</a> recibió una comisión por un nuevo <a href="order_id=%d">pedido</a>.';

// Column
$_['column_customer']              = 'Cliente';
$_['column_comment']               = 'Comentario';
$_['column_ip']                    = 'IP';
$_['column_date_added']            = 'Fecha añadido';

// Entry
$_['entry_customer']               = 'Cliente';
$_['entry_ip']                     = 'IP';
$_['entry_date_start']             = 'Fecha de Inicio';
$_['entry_date_end']               = 'Fecha de Finalización';
$_['entry_status']                 = 'Estado';
$_['entry_sort_order']             = 'Orden de Clasificación';

// Error
$_['error_permission']             = 'Advertencia: ¡No tiene permisos para modificar reporte de actividad del cliente!';