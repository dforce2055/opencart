<?php
// Heading
$_['heading_title']    = 'Reporte de Productos Vistos';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_edit']        = 'Editar Reporte de Productos';
$_['text_success']     = 'Éxito: ¡Ha reiniciado el reporte de productos vistos!';

// Column
$_['column_name']      = 'Nombre de Producto';
$_['column_model']     = 'Modelo';
$_['column_viewed']    = 'Visto';
$_['column_percent']   = 'Porcentaje';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar reporte de productos vistos!';