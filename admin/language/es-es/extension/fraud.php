<?php
// Heading
$_['heading_title']    = 'Anti-Fraude';

// Text
$_['text_success']     = 'Se ha modificado la Extensión Anti-Fraude.';
$_['text_list']        = 'Lista Anti-Fraude';

// Column
$_['column_name']      = 'Nombre del Anti-Fraude';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Sin permiso para modificar Anti-Fraude.';