<?php
$_['heading_title']    = 'Google Analytics';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']	   = 'Éxito: ¡Has modificado Google Analytics!';
$_['text_edit']        = 'Editar Google Analytics';
$_['text_signup']      = 'Inicie sesión en su cuenta <a href="http://www.google.com/analytics/" target="_blank"><u>Google Analytics</u></a> y, luego de crear su perfil de sitio web, copie el coódigo de Analytics en este campo.';
$_['text_default']     = 'Defecto';

// Entry
$_['entry_code']       = 'Código de Google Analytics';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar Google Analytics!';
$_['error_code']	   = '¡Código necesario!';
