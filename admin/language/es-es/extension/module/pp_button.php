<?php
// Heading
$_['heading_title']    = 'Botón de proceso de pago con PayPal Express';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado módulo de Botón de proceso de pago con PayPal Express!';
$_['text_edit']        = 'Editar módulo de Botón de proceso de pago con PayPal Express';
$_['text_info']        = 'El botón <u>no</u> aparecerá bajo ciertas condiciones:';
$_['text_info_li1']    = 'El carro de compras está vacío y no se aplicaron cupones';
$_['text_info_li2']    = 'El carro de compras tiene descargas o pagos recurrentes y el usuario no inició sesión';
$_['text_info_li3']    = 'El proceso de pago de items sin stock está desactivado y el carro de compras contiene un item fuera de stock';
$_['text_layouts']     = 'Luego de activar el módulo, use el gestor de diseños para agregar el botón a sectores de la tienda.';
$_['text_layout_link'] = 'Haga click aquí para acceder a la página de diseños';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar módulo de Botón de proceso de pago con PayPal Express!';