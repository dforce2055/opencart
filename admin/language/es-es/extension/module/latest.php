<?php
// Heading
$_['heading_title']    = 'Lo más reciente';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo Lo más reciente!';
$_['text_edit']        = 'Editar el módulo Lo más reciente';

// Entry
$_['entry_name']       = 'Nombre del módulo';
$_['entry_limit']      = 'Límite';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo Lo más reciente!';
$_['error_name']       = '¡Nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = '¡Ancho necesario!';
$_['error_height']     = '¡Alto necesario!';