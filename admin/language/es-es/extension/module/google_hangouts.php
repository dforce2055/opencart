<?php
// Heading
$_['heading_title']    = 'Google Hangouts';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo de Google Hangouts!';
$_['text_edit']        = 'Editar módulo de Google Hangouts';

// Entry
$_['entry_code']       = 'Código Google Talk';
$_['entry_status']     = 'Estado';

// Ayuda
$_['help_code']        = 'Vaya a <a href="https://developers.google.com/+/hangouts/button" target="_blank">crear Google Hangout chatback badge</a> y copie y pegue el código generado dentro de la caja.';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo de Google Hangouts!';
$_['error_code']       = 'Código necesario.';