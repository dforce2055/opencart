<?php
// Heading
$_['heading_title']    = 'Filtrado';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado el módulo de filtrado!';
$_['text_edit']        = 'Editar módulo de filtrado';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo de filtrado!';