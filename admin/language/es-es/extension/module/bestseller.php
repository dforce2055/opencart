<?php
// Heading
$_['heading_title']    = 'Más vendiddos';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo de más vendidos!';
$_['text_edit']        = 'Editar módulo de más vendidos';

// Entry
$_['entry_name']       = 'Nombre del módulo';
$_['entry_limit']      = 'Límite';
$_['entry_image']      = 'Imagen (A x A) y tipo de redimensión';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo de más vendidos!';
$_['error_name']       = 'Nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = 'Ancho necesario!';
$_['error_height']     = 'Alto necesario!';