<?php
// Heading
$_['heading_title']    = 'Diseño Lay-Buy';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo Diseño Lay-Buy!';
$_['text_edit']        = 'Editar el módulo Diseño Lay-Buy';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo Diseño Lay-Buy!';