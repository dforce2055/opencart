<?php
// Heading
$_['heading_title']    = 'Carrusel';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado el módulo carrusel!';
$_['text_edit']        = 'Editar Carousel Module';

// Entry
$_['entry_name']       = 'Nombre del módulo';
$_['entry_banner']     = 'Banner';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar el módulo carrusel!';
$_['error_name']       = 'Nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = 'Ancho necesario!';
$_['error_height']     = 'Alto necesario!';