<?php
// Heading
$_['heading_title']         = 'Iniciar sesión con Amazon';

// Text
$_['text_extension']        = 'Extensiones';
$_['text_success']          = 'Éxito: ¡Has modificado el módulo Iniciar sesióñ con Amazon!';
$_['text_content_top']      = 'Contenido superior';
$_['text_content_bottom']   = 'Contenido inferior';
$_['text_column_left']      = 'Columna izquierda';
$_['text_column_right']     = 'Columna derecha';
$_['text_lwa_button']       = 'Iniciar sesióñ con Amazon';
$_['text_login_button']     = 'Iniciar sesión';
$_['text_a_button']         = 'A';
$_['text_gold_button']      = 'Dorado';
$_['text_darkgray_button']  = 'Gris oscuro';
$_['text_lightgray_button'] = 'Gris claro';
$_['text_small_button']     = 'Pequeño';
$_['text_medium_button']    = 'Mediano';
$_['text_large_button']     = 'Grande';
$_['text_x_large_button']   = 'X-Grande';

//Entry
$_['entry_button_type']     = 'Tipo de botón';
$_['entry_button_colour']   = 'Color de botón';
$_['entry_button_size']     = 'Tamaño de botón';
$_['entry_layout']          = 'Diseño';
$_['entry_position']        = 'Posición';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Orden de Clasificación';

//Error
$_['error_permission']      = 'Advertencia: ¡No tiene permisos para modificar el módulo Iniciar sesióñ con Amazon!';