<?php
// Heading
$_['heading_title']     = 'Listado de eBay';

// Text
$_['text_extension']    = 'Extensiones';
$_['text_success']      = 'Éxito: ¡Has modificado módulo destacados eBay!';
$_['text_edit']        	= 'Editar módulo de eBay';
$_['text_list']         = 'Lista de diseños';
$_['text_register']     = '¡Debe registrarse y activar OpenBay Pro para eBay!';
$_['text_about'] 		= 'El módulo de listado de eBay display módulo permite listar productos en su cuenta de eBay directamente en su sitio wweb.';
$_['text_latest']       = 'Lo más reciente';
$_['text_random']       = 'Azar';

// Entry
$_['entry_name']        = 'Nombre del módulo';
$_['entry_username']    = 'Nombre de usuario de eBay';
$_['entry_keywords']    = 'Palabras Claves de búsqueda';
$_['entry_description'] = 'Incluir búsqueda en descripción';
$_['entry_limit']       = 'Límite';
$_['entry_length']      = 'Longitud';
$_['entry_width']       = 'Ancho';
$_['entry_height']      = 'Alto';
$_['entry_site']   		= 'Sitio de eBay';
$_['entry_sort']   		= 'Ordenar por';
$_['entry_status']   	= 'Estado';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar módulo eBay!';
$_['error_name']        = 'Nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']       = '¡Ancho necesario!';
$_['error_height']      = '¡Alto necesario!';