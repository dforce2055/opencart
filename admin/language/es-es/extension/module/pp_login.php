<?php
// Heading
$_['heading_title']        = 'Iniciar sesión con PayPal';

// Text
$_['text_extension']       = 'Extensiones';
$_['text_success']         = 'Éxito: ¡Has modificado el módulo Iniciar sesión con PayPal!';
$_['text_edit']            = 'Editar el módulo Iniciar sesión con PayPal';
$_['text_button_grey']     = 'Gris';
$_['text_button_blue']     = 'Azul (Recomendado)';

// Entry
$_['entry_client_id']      = 'ID de cliente';
$_['entry_secret']         = 'Secreto';
$_['entry_sandbox']        = 'Modo Sandbox';
$_['entry_debug']          = 'Registro Debug';
$_['entry_customer_group'] = 'Grupo de Clientes';
$_['entry_button']         = 'Color de botón';
$_['entry_seamless']       = 'Permitir "Seamless Checkout"';
$_['entry_locale']         = 'Locación';
$_['entry_return_url']     = 'URL de retorno';
$_['entry_status']         = 'Estado';

// Ayuda
$_['help_sandbox']         = 'Usar ambiente sandbox (de prueba)?';
$_['help_customer_group']  = '¿Con qué grupo de cliente deberían crearse los nuevos clientes?';
$_['help_debug_logging']   = 'Activar esta opción permitirá que se agreguen datos a su registro de errores para ayudar a debuggear pboelemas.';
$_['help_seamless']        = 'Permitir auto-inicio-de-sesión cuando los clientes elijan Proceso de pago PayPal Express. Para usar esto, la opción debe estar activada en su cuenta de Iniciar sesión con PayPal. También debe usar la misma cuenta que utiliza con Express Checkout.';
$_['help_locale']          = 'Este es el parámetro de locación de PayPal para los idiomas de su tienda';
$_['help_return_url']      = 'Esto debe ser agregado en la configuración de app de PayPal bajo "URLs de redirección de app".';

// Error
$_['error_permission']     = 'Advertencia: ¡No tiene permisos para modificar el módulo Iniciar sesión con PayPal!';
$_['error_client_id']      = '¡ID de cliente necesario!';
$_['error_secret']         = '¡Secreto necesario!';