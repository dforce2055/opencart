<?php
// Heading
$_['heading_title']    = 'Banner';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo de banner!';
$_['text_edit']        = 'Editar módulo de banner';

// Entry
$_['entry_name']       = 'Nombre de módulo';
$_['entry_banner']     = 'Banner';
$_['entry_dimension']  = 'Dimension (A x A) y tipo de redimensión';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo de banner!';
$_['error_name']       = 'Nombre de módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = 'Ancho necesario!';
$_['error_height']     = 'Alto necesario!';