<?php
// Heading
$_['heading_title']    = 'Procesar pago con Pilibaba';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado el módulo Procesar pago con Pilibaba!';
$_['text_edit']        = 'Editar el módulo Procesar pago con Pilibaba';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar el módulo Procesar pago con Pilibaba!';