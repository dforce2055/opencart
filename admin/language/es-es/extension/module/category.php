<?php
// Heading
$_['heading_title']    = 'Categorías';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo de categorías!';
$_['text_edit']        = 'Editar módulo de Categorías';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar category module!';