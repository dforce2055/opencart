<?php
// Heading
$_['heading_title']    = 'Calculadora de página de productos Divido';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado la Calculadora de página de productos Divido!';
$_['text_edit']        = 'Editar Calculadora de página de productos Divido';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo Calculadora de página de productos Divido!';