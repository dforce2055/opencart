<?php
// Heading
$_['heading_title']    = 'Administración de Tarjeta Sagepay Server';

$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo Administración de Tarjeta Sagepay Server!';
$_['text_edit']        = 'Editar el módulo Administración de Tarjeta Sagepay Server';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo Administración de Tarjeta Sagepay Server!';