<?php
// Heading
$_['heading_title']    = 'Información';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo de informacion!';
$_['text_edit']        = 'Editar el módulo de informacion';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo de informacion!';