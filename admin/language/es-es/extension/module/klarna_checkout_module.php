<?php
// Heading
$_['heading_title']	   = 'Procesar pago con Klarna';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']	   = 'Éxito: ¡Has modificado el módulo Procesar pago con Klarna!';

//Entry
$_['entry_status']	   = 'Estado';

//Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo Procesar pago con Klarna!';