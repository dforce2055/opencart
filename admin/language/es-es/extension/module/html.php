<?php
// Heading
$_['heading_title']     = 'contenido HTML';

// Text
$_['text_extension']    = 'Extensiones';
$_['text_success']      = 'Éxito: Has modificado el módulo de contenido HTML!';
$_['text_edit']         = 'Editar el módulo de contenido HTML';

// Entry
$_['entry_name']        = 'Nombre del módulo';
$_['entry_title']       = 'Título de cabecera';
$_['entry_description'] = 'Descripción';
$_['entry_status']      = 'Estado';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar el módulo de contenido HTML!';
$_['error_name']        = 'Nombre del módulo debe tener entre 3 y 64 caracteres!';