<?php
// Heading
$_['heading_title']    = 'Tienda';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el módulo Tienda!';
$_['text_edit']        = 'Editar módulo Tienda';

// Entry
$_['entry_admin']      = 'Sólo usuarios Admin';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo Tienda!';