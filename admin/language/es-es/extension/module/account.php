<?php
// Heading
$_['heading_title']    = 'Cuenta';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Ha modificado el módulo de cuentas!';
$_['text_edit']        = 'Editar módulo de cuentas';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el módulo de cuentas!';