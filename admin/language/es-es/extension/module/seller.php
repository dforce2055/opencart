<?php
// Heading
$_['heading_title']    = 'Vendedor';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: ¡Pudo modificar el Módulo de Vendedor!';
$_['text_edit']        = 'Editar Módulo de Vendedor';

// Entry
$_['entry_product']    = 'Vendedores';
$_['entry_name']    = 'Nombre';
$_['entry_limit']      = 'Límite';
$_['entry_image']      = 'Imagen (Ancho x Alto) y Tipo de redimensionamiento';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Ayuda
$_['help_product']     = '(Autocompletar)';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permiso para modificar el Módulo de Vendedor!';
$_['error_image']      = '¡Se necesitan ancho y alto de imagán!';