<?php
// Heading
$_['heading_title']     = 'Métodos de Envío';

// Text
$_['text_success']      = 'Se ha modificado el Método de envío.';
$_['text_list']         = 'Lista de Métodos de Envío';

// Column
$_['column_name']       = 'Método de Envío';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Ordenar';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Sin permiso para modificar el Método de Envío.';