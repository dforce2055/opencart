<?php
// Heading
$_['heading_title']    = 'Captcha básico';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el Captcha básico!';
$_['text_edit']        = 'Editar Captcha básico';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el Captcha básico!';