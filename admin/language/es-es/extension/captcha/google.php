<?php
// Heading
$_['heading_title']    = 'Google reCAPTCHA';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado Google reCAPTCHA!';
$_['text_edit']        = 'Editar Google reCAPTCHA';
$_['text_signup']      = 'Vaya a la <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank"><u>página de Google reCAPTCHA</u></a> y registre su sitio web.';

// Entry
$_['entry_key']        = 'Clave de sitio';
$_['entry_secret']     = 'Clave secreta';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar Google reCAPTCHA!';
$_['error_key']        = '¡Clave necesaria!';
$_['error_secret']     = '¡Clave secreta necesaria!';