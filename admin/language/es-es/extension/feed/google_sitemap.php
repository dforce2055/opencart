<?php
// Heading
$_['heading_title']    = 'Google SiteMaps';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado el feed de Google SiteMaps!';
$_['text_edit']        = 'Editar Google SiteMaps';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_data_feed']  = 'Url de feed de datos';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar feed de Google SiteMaps!';