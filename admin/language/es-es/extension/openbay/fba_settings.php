<?php
// Headings
$_['heading_title']        	   = 'Configuraciones';
$_['text_openbay']             = 'OpenBay Pro';
$_['text_fba']                 = 'Cumplimiento por Amazon';

// Text
$_['text_success']     		   = 'Ajustes guardados';
$_['text_status']         	   = 'Estado';
$_['text_account_ok']  		   = 'Connexión con Cumplimiento por Amazon OK';
$_['text_api_ok']       	   = 'Conexión con API OK';
$_['text_api_status']          = 'Conexión con API';
$_['text_edit']           	   = 'Editar ajustes de Cumplimiento con Amazon';
$_['text_standard']            = 'Estándar';
$_['text_expedited']           = 'Expeditado';
$_['text_priority']            = 'Prioridad';
$_['text_fillorkill']          = 'Cumplir or matar';
$_['text_fillall']             = 'Cumplir Todos';
$_['text_fillallavailable']    = 'Cumplir Todos disponibles';
$_['text_prefix_warning']      = 'No cambie este ajuste luego de que se hayan enviado pedidos a Amazon, sólo fije esto cuando instala por primera vez.';
$_['text_disabled_cancel']     = 'Desactivado - no cancelar cumplimientos automáticamente';
$_['text_validate_success']    = '¡Sus Detalles de API están funcionando correctamente! Presione guadar para asegurarse de guardar los ajustes.';
$_['text_register_banner']     = 'Haga click aquí si necesita registrar una cuenta';

// Entry
$_['entry_api_key']            = 'API token';
$_['entry_encryption_key']     = 'Clave de encripción 1';
$_['entry_encryption_iv']      = 'Clave de encripción 2';
$_['entry_account_id']         = 'ID de cuenta';
$_['entry_send_orders']        = 'Enviar pedidos automáticamente';
$_['entry_fulfill_policy']     = 'Política de cumplimiento';
$_['entry_shipping_speed']     = 'Velocidad de envío por defecto';
$_['entry_debug_log']          = 'Activar registro para debug';
$_['entry_new_order_status']   = 'Disparador de nuevo cumplimiento';
$_['entry_order_id_prefix']    = 'Prefijo de ID de Pedido';
$_['entry_only_fill_complete'] = 'Todos los items deben ser FBA';

// Ayuda
$_['help_api_key']             = 'Esta es su API token, obténgala de su área de cuenta OpenBay Pro';
$_['help_encryption_key']      = 'Esta es your Clave de encripción #1, obténgala de su área de cuenta OpenBay Pro';
$_['help_encryption_iv']       = 'Esta es your Clave de encripción #2, obténgala de su área de cuenta OpenBay Pro';
$_['help_account_id']          = 'Esta es la ID de cuenta que coincide con la cuenta Amazon registrada por OpenBayPro, obténgala de su área de cuenta OpenBay Pro';
$_['help_send_orders']  	   = 'Pedidos que contengan productos coincidentes con Cumplimiento por Amazon se enviarán a Amazon automáticamente.';
$_['help_fulfill_policy']  	   = 'La política de cumplimiento por defecto (CumplirTodas - Todos los items cumplibles en el pedido de cumplimiento se envían. El pedido de cumplimiento queda en estado de procesamiento hasta que todos los items son enviados por Amazon o cancelados por el vendedor. CumplirTodosDisponibles - Todos los items cumplibles en el pedido de cumplimiento son enviadas. Todos los items incumplibles en el pedido son cancelados por Amazon.CumplirOMatar - Si un item en un pedido de cumplimiento se determina incumplible antes de que cualquier envíoo en el pedido se cambia al estado Pendiente (el proceso de recolectar items del inventario ya comenzó), entonces todo el pedido se considera incumplible. Sin embargo, si un item en un pedido de cumplimiento se determina incumplible luego de que un envío cambia al estado Pendiente, Amazon cancela tanto del pedido de cumplimiento como sea posible.';
$_['help_shipping_speed']  	   = 'Esta es la categoría de velocidad de envío por defecto a aplicar a nuevos pedidos, distintos niveles d servicio pueden incurrir en distintos costos.';
$_['help_debug_log']  		   = 'El registro de debug guardará información a un archivo de registro con las acciones llevadas a cabo por el módulo. Esto debería dejarse activado para encontrar la causa de problemas eventuales.';
$_['help_new_order_status']    = 'Este es el estado de pedido que disparará que se cree un pedido de cumplimiento. Asegúrese de que esto utiliza un estado posterior a recibir el pago.';
$_['help_order_id_prefix']     = 'Tener un prefijo de pedido ayudará a identificar pedidos que vienen de su tienda y no de otras intregraciones. Esto es muyútil cuando los vendedores venden en muchos mercados y usan FBA.';
$_['help_only_fill_complete']  = 'Esto sólo permitirá que se envíen pedidos a cumplimientos si TODOS los items del pedido coinciden con items de Cumplimiento por Amazon. Si algún item no coincide, el pedido entero quedará sin cumplir.';

// Error
$_['error_api_connect']        = 'Falló la conexión con API';
$_['error_account_info']       = 'No se puede verificar conexión con API de Cumplimiento por Amazon ';
$_['error_api_key']    		   = 'La API token es inválida';
$_['error_api_account_id']     = 'La ID de cuenta es inválida';
$_['error_encryption_key']     = 'La Clave de encripción #1 es inválida';
$_['error_encryption_iv']      = 'La Clave de encripción #2 es inválida';
$_['error_validation']    	   = 'Ocurrió un error al validar sus detalles';

// Tabs
$_['tab_api_info']             = 'Detalles de API';

// Buttons
$_['button_verify']            = 'Verificar detalles';
