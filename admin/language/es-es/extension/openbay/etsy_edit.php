<?php
// Headings
$_['heading_title']       = 'Editar listado';
$_['text_openbay']        = 'OpenBay Pro';
$_['text_etsy']           = 'Etsy';

// Tabs

// Text
$_['text_option']      	  = 'Seleccione opción';
$_['text_listing_id']  	  = 'ID de listado';
$_['text_updated']  	  = 'Se actualizó su listado en Etsy ';
$_['text_edit']  		  = 'Actualizar su listado en Etsy';

// Entry
$_['entry_title']      	  = 'Título de producto';
$_['entry_description']   = 'Descripción';
$_['entry_price']      	  = 'Precio';
$_['entry_state']      	  = 'Estado';

// Errors
$_['error_price_missing'] = 'Falta el producto o está vacío';
$_['error_title_length']  = 'Título demasiado largo';
$_['error_title_missing'] = 'Título faltante';
$_['error_desc_missing']  = 'Falta la descripción o está vacía';
$_['error_state_missing'] = 'Falta el estado o está vacío';