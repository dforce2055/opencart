<?php
// Heading
$_['heading_title']             = 'Subscripción';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Buttons
$_['button_plan_change']  		= 'Change to plan';

// Columns
$_['column_plan']  				= 'Nombre del plan';
$_['column_call_limit']  		= 'Límite de llamadas';
$_['column_price']  			= 'Precio (p/mes)';
$_['column_description']  		= 'Descripción';
$_['column_current']  			= 'Plan actual';

// Text
$_['text_subscription_current'] = 'Plan actual';
$_['text_subscription_avail']   = 'Planes disponibles';
$_['text_subscription_avail1']  = 'Cambiar de planes será inmediato y las llamadas sin utilizar no serán acreditadas.';
$_['text_ajax_acc_load_plan']   = 'ID de subscripciOn de PayPal: ';
$_['text_ajax_acc_load_plan2']  = ', debería cancelar TODAS las demás subscripciones con nosotros';
$_['text_load_my_plan']         = 'Cargando su plan';
$_['text_load_plans']           = 'Cargando planes disponibles';
$_['text_subscription']         = 'Cambiar su subscripción de OpenBay';

// Errors
$_['error_ajax_load']      		= 'Lo sentimos, no se pudo obtener una respuesta. Intente de nuevo más tarde.';