<?php
// Heading
$_['heading_title']				   = 'Revisar listado eBay';
$_['text_openbay']				   = 'OpenBay Pro';
$_['text_ebay']					   = 'eBay';

// Tab
$_['tab_recommendations']		   = 'Recomendaciones';

// Text
$_['text_revise']                  = 'Revisar listado';
$_['text_loading']                 = 'Obteniendo información de eBay';
$_['text_error_loading']           = 'Ocurrió un error al obtener información de eBay';
$_['text_saved']                   = 'Se ha guardado el listado';
$_['text_alert_removed']           = 'El listado se ha desvinculado';
$_['text_alert_ended']             = 'Se ha terminado con el listado en eBay';
$_['text_listing_info']            = 'Información de listado';
$_['text_check_recommendations']   = 'Verificando recomendaciones de listados en eBay';
$_['text_success_recommendations'] = '¡No hay recomendaciones para mejorar el listado de este item!';

// Buttons
$_['button_view']				   = 'Ver listado';
$_['button_remove']				   = 'Quitar vínculo';
$_['button_end']                   = 'Terminar listado';
$_['button_retry']				   = 'Reintentar';

// Entry
$_['entry_title']				   = 'Título';
$_['entry_price']				   = 'Precio de venta (incluye cualquier impuesto)';
$_['entry_stock_store']			   = 'Stock local';
$_['entry_stock_listed']		   = 'Stock eBay';
$_['entry_stock_reserve']		   = 'Nivel de reserva';
$_['entry_stock_matrix_active']	   = 'Matriz de Stock (activa)';
$_['entry_stock_matrix_inactive']  = 'Matriz de Stock (inactiva)';

// Column
$_['column_sku']				   = 'SKU';
$_['column_stock_listed']		   = 'Listado';
$_['column_stock_reserve']		   = 'Reserva';
$_['column_stock_total']		   = 'En stock';
$_['column_price']				   = 'Precio';
$_['column_status']				   = 'Activo';
$_['column_add']				   = 'Agregar';
$_['column_combination']		   = 'Combinación';

// Ayuda
$_['help_stock_store']			   = 'Este es el nivel de stock en OpenCart';
$_['help_stock_listed']			   = 'Este es el nivel de stock en eBay';
$_['help_stock_reserve']		   = 'Este es el nivel máximo de stock en eBay (0 = sin límito)';

// Error
$_['error_ended']				   = 'El listado vinculado ya no existe, no puede editarlo. Debería quitar el vínculo.';
$_['error_reserve']				   = 'La reserva no puede ser mayor que el stock local.';
$_['error_no_sku']          	   = '¡No se encontró ningún SKU!';
$_['error_no_item_id']             = 'Falta la ID de item en la solicitud.';
$_['error_recommendations_load']   = 'No se pudieron cargar recomendaciones de item';