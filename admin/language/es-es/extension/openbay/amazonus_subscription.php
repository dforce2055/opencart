<?php
// Heading
$_['heading_title']        	  = 'Subscripción';
$_['text_openbay']			  = 'OpenBay Pro';
$_['text_amazon']			  = 'Amazon US';

// Text
$_['text_current_plan']       = 'Plan actual';
$_['text_register_invite']    = '¿No tiene sus detalles de API aún?';
$_['text_available_plans']    = 'Planes disponibles';
$_['text_listings_remaining'] = 'Listados restantes';
$_['text_listings_reserved']  = 'Productos siendo procesados';
$_['text_account_status']     = 'Estado de cuenta';
$_['text_merchantid']         = 'ID de Vendedor';
$_['text_change_merchantid']  = 'Cambiar';
$_['text_allowed']            = 'Permitido';
$_['text_not_allowed']        = 'No Permitido';
$_['text_price']              = 'Precio';
$_['text_name']               = 'Nombre';
$_['text_description']        = 'Descripción';
$_['text_order_frequency']    = 'Frecuencia de importación de pedidos';
$_['text_bulk_listing']       = 'Listado por lotes';
$_['text_product_listings']   = 'Listados por mes';

// Columns
$_['column_name']             = 'Nombre';
$_['column_description']      = 'Descripción';
$_['column_order_frequency']  = 'Frecuencia de importación de pedidos';
$_['column_bulk_listing']     = 'Listado por lotes';
$_['column_product_listings'] = 'Listados por mes';
$_['column_price']            = 'Precio';

// Buttons
$_['button_change_plan']      = 'Cambiar plan';
$_['button_register']         = 'Registrar';