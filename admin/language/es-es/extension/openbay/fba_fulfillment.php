<?php
// Heading
$_['heading_title']						= 'Cumplimientos';
$_['text_openbay']						= 'OpenBay Pro';
$_['text_fba']							= 'Cumplimiento By Amazon';
$_['heading_order_info']				= 'Info de pedido';
$_['heading_products']					= 'Productos';

// Entry
$_['entry_seller_fulfillment_order_id'] = 'ID de pedido de cumplimiento de vendedor';
$_['entry_displayable_order_id']     	= 'ID de Pedido para mostrar';
$_['entry_displayable_date']     		= 'Fecha/hora para mostrar';
$_['entry_displayable_comment']     	= 'Comentario para mostrar';
$_['entry_shipping_speed_category']     = 'Categoría de velocidad de envío';
$_['entry_fulfillment_policy']     		= 'Política de Cumplimiento';
$_['entry_fulfillment_order_status']    = 'Estado';
$_['entry_notification_email_list']     = 'Lista de emails de notificación';
$_['entry_button_cancel']     			= 'Cancelar cumplimiento';
$_['entry_button_ship']     			= 'Enviar cumplimiento';

// Text
$_['text_no_results'] 					= 'No se encontraron cumplimientos en Amazon';
$_['text_fulfillment_list'] 			= 'Lista de cumplimientos Amazon';
$_['text_form'] 						= 'Cuplimiento Amazon';
$_['text_ship_success'] 				= 'Envío de Cumplimiento confirmado, puede tardar unos minutos en actualizarse en Amazon';
$_['text_cancel_success'] 				= 'Cumplimiento cancelado,  puede tardar unos minutos en actualizarse en Amazon';
$_['text_cancel_confirm'] 				= '¿Está seguro de que desea cancelar este cumplimiento?';
$_['text_ship_confirm'] 				= '¿Está seguro de que desea enviar este cumplimiento?';
$_['text_status'] 						= 'Estado Cumplimiento';
$_['text_fulfillment_sent'] 			= '¡Cumplimiento confirmado!';
$_['text_fulfillment_shipped'] 			= '¡Cumplimiento enviado!';
$_['text_fulfillment_cancelled'] 		= '¡Cumplimiento cancelado!';

// Columns
$_['column_sku'] 						= 'SKU';
$_['column_order_item_id'] 				= 'ID de item de pedido';
$_['column_quantity'] 					= 'Cantidad';
$_['column_cancelled_quantity'] 		= 'Cantidad cancelada';
$_['column_unfulfillable_quantity'] 	= 'Cantidad incumplible';
$_['column_estimated_ship'] 			= 'Envío estimado';
$_['column_estimated_arrive'] 			= 'Arribo estimado';

// Errors
$_['error_loading_fulfillment']         = 'Ocurrió un problema al obtener la información del pedido de cumplimiento de Amazon';
$_['error_ship']             			= 'Ocurrió un problema al confirmar el envío con Amazon';
$_['error_cancel']             			= 'Ocurrió un problema al cancelar el cumplimiento con Amazon';
$_['error_missing_id']             		= 'ID faltante en solicitud';
$_['error_no_shipping']             	= 'No se encontró un método de envío';
$_['error_no_items']             		= 'No se encontraron items para este pedido';
$_['error_amazon_request']             	= 'Se recibió una respuesta de error de Amazon, por favor verifique los errores para la solicitud';