<?php
// Heading
$_['heading_title']           = 'Vínculos de items';
$_['text_openbay']			  = 'OpenBay Pro';
$_['text_ebay']				  = 'eBay';

// Buttons
$_['button_resync']           = 'Resincronizar';
$_['button_check_unlinked']   = 'Verificar items desvinculados';
$_['button_remove_link']      = 'Quitar vínculo';

// Errors
$_['error_ajax_load']         = 'Lo sentimos, no se pudo obtener una respuesta. Intente de nuevo más tarde.';
$_['error_validation']        = 'Debe registrarse para recibir su API Token y activar el módulo.';
$_['error_no_listings']       = 'No se encontraron productos vinculados';
$_['error_link_value']        = 'El vínculo del producto no es valor';
$_['error_link_no_stock']     = 'No se puede crear un vínculo para un item sin stock. Termine el listado manualmente en eBay.';
$_['error_subtract_setting']  = 'Este producto está configurado para no quitar stock en OpenCart.';

// Text
$_['text_linked_items']       = 'Items vinculados';
$_['text_unlinked_items']     = 'Items desvinculados';
$_['text_alert_stock_local']  = '¡Su listado de eBay será actualizado con sus niveles de stock locales!';
$_['text_link_desc1']         = 'Vincular sus items le permitirá ejercer control de stock en eBay.';
$_['text_link_desc2']         = 'Por cada item que se actualiza en su stock local (el stock disponible en su tienda OpenStore) se actualizará su listado en eBay.';
$_['text_link_desc3']         = 'Su stock local es el stock disponible a la venta. Su stock de eBay debería coincidir.';
$_['text_link_desc4']         = 'Su stock asignado son items que fueron vendidos pero no se pagaron aún. Estos items deberían separarse y no contarse como parte de su stock.';
$_['text_text_linked_desc']   = 'Items vinculados son items de OpenCart que tienen un vínculo con un listado de eBay.';
$_['text_text_unlinked_desc'] = 'Items desvinculados son items en su Cuenta eBay que no se vinculan con ninguno de sus productos en OpenCart.';
$_['text_text_unlinked_info'] = 'Haga click en el boton de verificar items desvinculados para buscar items desvinculados sus listados de eBay activos. Esto puede llevar mucho tiempo si tiene muchos listados en eBay.';
$_['text_text_loading_items'] = 'Cargando items';
$_['text_failed']       	  = 'Falló la carga';
$_['text_limit_reached']      = 'Se alcanzó el número máximo de verificaciones por solicitud, haga click en el botón para seguir buscando.';
$_['text_stock_error']        = 'Error de stock';
$_['text_listing_ended']      = 'Listdo terminado';
$_['text_filter']             = 'Filtrar resultados';
$_['text_filter_title']       = 'Título';
$_['text_filter_range']       = 'Rango de stock';
$_['text_filter_range_from']  = 'Min';
$_['text_filter_range_to']    = 'Max';
$_['text_filter_var']         = 'Includir variantes';

// Tables
$_['column_action']           = 'Acción';
$_['column_status']           = 'Estado';
$_['column_variants']         = 'Variantes';
$_['column_item_id']          = 'Item ID de eBay';
$_['column_product']          = 'Producto';
$_['column_product_auto']     = 'Nombre de producto (auto completar)';
$_['column_listing_title']    = 'Título de listado en eBay';
$_['column_allocated']        = 'Stock asignado';
$_['column_ebay_stock']       = 'Stock en eBay';
$_['column_stock_available']  = 'Stock en tienda';
$_['column_stock_reserve']    = 'Nivel de reserva';