<?php
// Headings
$_['heading_title']        		= 'Ajustes de Tienda';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_success']     			= 'Se guardaron los cambios';
$_['text_status']         		= 'Estado';
$_['text_account_ok']  			= 'Connexión con Etsy OK';
$_['text_token_register']       = 'Registrar';
$_['text_api_ok']       		= 'Conexión API OK';
$_['text_pull_orders']    		= 'Traer pedidos';
$_['text_sync_settings']    	= 'Ajustes de sincronización';
$_['text_complete']    			= 'Completar';
$_['text_failed']    			= 'Fallido';
$_['text_orders_imported']    	= 'Se solicitó traer pedidos';
$_['text_api_status']           = 'Conexióón con API';
$_['text_edit']           		= 'Editar ajustes de Etsy';
$_['text_register_banner']      = 'Haga click aquí si necesita registrar una cuenta';

// Entry
$_['entry_import_def_id']       = 'Estado por defecto de importación (impago):';
$_['entry_import_paid_id']      = 'Estado pagado:';
$_['entry_import_shipped_id']   = 'Estado enviado:';
$_['entry_encryption_key']      = 'Clave de encripción 1';
$_['entry_encryption_iv']       = 'Clave de encripción 2';
$_['entry_token']            	= 'API token';
$_['entry_address_format']      = 'Formato de dirección por defecto';
$_['entry_debug']				= 'Activar registro';

// Error
$_['error_api_connect']         = 'Falló la conexión con API';
$_['error_account_info']    	= 'No se puede verificar conexión con API de Etsy ';

// Tabs
$_['tab_api_info']            	= 'Detalles de API';

// Ayuda
$_['help_address_format']  		= 'Solo utilizado si el país vinculado no tiene un formato de dirección configurado.';
$_['help_sync_settings']  		= 'Esto actualizará su base de datos con las últimas opciones de ajustes como tipos de condición, fechas y más.';
$_['help_pull_orders']  		= 'Esto disparará una importación manual de pedidos nuevos y actualizados.';
