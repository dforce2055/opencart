<?php
// Text
$_['text_openbay_extension']           = 'OpenBay Pro';
$_['text_openbay_dashboard']           = 'Panel de Control';
$_['text_openbay_orders']              = 'Actualizar pedidos en masa';
$_['text_openbay_items']               = 'Administrar items';
$_['text_openbay_ebay']                = 'eBay';
$_['text_openbay_amazon']              = 'Amazon (EU)';
$_['text_openbay_amazonus']            = 'Amazon (US)';
$_['text_openbay_etsy']            	   = 'Etsy';
$_['text_openbay_settings']            = 'Configuraciones';
$_['text_openbay_links']               = 'Vínculos de items';
$_['text_openbay_report_price']        = 'Reporte de precios';
$_['text_openbay_order_import']        = 'Reporte de pedidos';
$_['text_openbay_fba']                 = 'Cumplimiento por Amazon';
$_['text_openbay_fulfillmentlist']     = 'Cumplimientos';
$_['text_openbay_orderlist']           = 'Pedidos';