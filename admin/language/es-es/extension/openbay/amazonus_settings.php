<?php
// Heading
$_['heading_title']        		  = 'Ajustes de Tienda';
$_['text_openbay']				  = 'OpenBay Pro';
$_['text_amazon']				  = 'Amazon US';
$_['text_edit']				      = 'Editar ajustes de Amazon US';

// Text
$_['text_api_status']             = 'Estado de conexión con API';
$_['text_api_ok']                 = 'Conexión OK, Auth OK';
$_['text_api_auth_error']         = 'Conexión OK, Auth fallido';
$_['text_api_error']              = 'Error de conexión';
$_['text_order_statuses']         = 'Estados de pedido';
$_['text_unshipped']              = 'No enviado';
$_['text_partially_shipped']      = 'Parcialmente enviado';
$_['text_shipped']                = 'Enviado';
$_['text_canceled']               = 'Cancelado';
$_['text_other']                  = 'Otros';
$_['text_marketplaces']           = 'Tiendas';
$_['text_settings_updated']       = 'Ajustes actualizados exitosamente.';
$_['text_new'] 					  = 'Nuevo';
$_['text_used_like_new'] 		  = 'Usado - Como nuevo';
$_['text_used_very_good'] 		  = 'Usado - Muy Bueno';
$_['text_used_good'] 			  = 'Usado - Bueno';
$_['text_used_acceptable'] 		  = 'Usado - Aceptable';
$_['text_collectible_like_new']   = 'Artículo de colección - Como Nuevo';
$_['text_collectible_very_good']  = 'Artículo de colección - Muy Bueno';
$_['text_collectible_good'] 	  = 'Artículo de colección - Bueno';
$_['text_collectible_acceptable'] = 'Artículo de colección - Aceptable';
$_['text_refurbished'] 			  = 'Renovado';
$_['text_register_banner']        = 'Haga click aquí si necesita registrar una cuenta';

// Error
$_['error_permission']         	  = 'No tiene acceso a este modulo';

// Entry
$_['entry_status']                = 'Estado';
$_['entry_token']				  = 'API token';
$_['entry_encryption_key']        = 'Clave de encripción 1';
$_['entry_encryption_iv']         = 'Clave de encripción 2';
$_['entry_import_tax']            = 'Impuesto para productos importados';
$_['entry_customer_group']        = 'Grupo de clientes';
$_['entry_tax_percentage']        = 'Percentage added to default product\'s price';
$_['entry_default_condition']     = 'Tipo de condición de producto por defecto';
$_['entry_notify_admin']          = 'Noificar admin de nuevo pedido';
$_['entry_default_shipping']      = 'Envío por defecto';

// Tabs
$_['tab_settings']            	  = 'Detalles de API';
$_['tab_listing']                 = 'Listados';
$_['tab_orders']                  = 'Pedidos';

// Ayuda
$_['help_import_tax']          	  = 'Utilizado si Amazon no devuelve información de impuestos';
$_['help_customer_group']      	  = 'Seleccione el grupo de clientes asignado a pedidos importados';
$_['help_default_shipping']    	  = 'Utilizado como la opción preseleccionada en actualización de pedidos por lotes';
$_['help_tax_percentage']         = 'Porcentaje añadido al precio de producto por defecto';
