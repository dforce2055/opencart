<?php
// Heading
$_['heading_title'] 			 = 'Vinculación por lotes';
$_['text_openbay'] 				 = 'OpenBay Pro';
$_['text_amazon'] 				 = 'Amazon US';

// Button
$_['button_load'] 				 = 'Cargar';
$_['button_link'] 				 = 'Vincular';

// Text
$_['text_local'] 				 = 'Local';
$_['text_load_listings'] 		 = "Cargar todos sus listados desde Amazon puede llevar un tiempo (hasta 2 horas en algunos casos). Si vincula sus items, los niveles de stock en Amazon se actualizarán con sus niveles de stock de sus tiendas.";
$_['text_report_requested'] 	 = 'Reporte de listado solicitado a Amazon exitosamente.';
$_['text_report_request_failed'] = 'No se pudo solicitar reporte de listado.';
$_['text_loading'] 				 = 'Cargando items';

// Column
$_['column_asin'] 				 = 'ASIN';
$_['column_price'] 				 = 'Precio';
$_['column_name'] 				 = 'Nombre';
$_['column_sku'] 				 = 'SKU';
$_['column_quantity'] 			 = 'Cantidad';
$_['column_combination'] 		 = 'Combinación';

// Error
$_['error_bulk_link_permission'] = 'Vinculación por lotes no está disponible en su plan, por favor mejórelo para utilizar esta característica.';