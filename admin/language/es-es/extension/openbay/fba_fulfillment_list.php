<?php
// Heading
$_['heading_title']					     = 'Cumplimientos';
$_['text_openbay']					     = 'OpenBay Pro';
$_['text_fba']						     = 'Cumplimiento por Amazon';

// Buttons
//$_['button_pull_orders']        	     = 'Start';

// Entry
$_['entry_start_date']             		 = 'Fecha inicial (formato AAAA-MM-DD)';

// Text
$_['text_no_results'] 					 = 'No se encontraron cumplimientos en Amazon';
$_['text_fulfillment_list'] 			 = 'Lista de cumplimientos por Amazon';

// Columns
$_['column_seller_fulfillment_order_id'] = 'ID de Pedido de cumplimiento de vendedor';
$_['column_displayable_order_id'] 		 = 'ID de Pedido a mostrar';
$_['column_displayable_order_date'] 	 = 'Fecha/hora a mostrar';
$_['column_shipping_speed_category'] 	 = 'Velocidad de envío';
$_['column_fulfillment_order_status'] 	 = 'Estado de pedido';
$_['column_action'] 					 = 'Acción';

// Errors
//$_['error_validation']                 = 'Debe registrarse para recibir su API token y activar el módulo.';