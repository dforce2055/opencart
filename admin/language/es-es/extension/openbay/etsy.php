<?php
// Heading
$_['heading_title']         = 'Etsy';
$_['text_openbay']			= 'OpenBay Pro';
$_['text_dashboard']		= 'Etsy Panel de Control';

// Mensajes
$_['text_success']         	= 'Ha guardado los cambios de la extension Etsy';
$_['text_heading_settings'] = 'Configuraciones';
$_['text_heading_sync']     = 'Sincronizar';
$_['text_heading_register'] = 'Regístrese aquí';
$_['text_heading_products'] = 'Vínculos de items';
$_['text_heading_listings'] = 'Listados Etsy';

// Errors
$_['error_generic_fail']	= '¡Ocurrió un error desconocido!';
$_['error_permission']		= 'No tiene permisos para modificar ajustes de Etsy';