<?php
// Headings
$_['heading_title']   = 'Uso';
$_['text_openbay']    = 'OpenBay Pro';
$_['text_ebay']       = 'eBay';

// Text
$_['text_usage']      = 'Su uso de cuenta';

// Errors
$_['error_ajax_load'] = 'Lo sentimos, no se obtuvo una respuesta. Intente de nuevo más tarde.';