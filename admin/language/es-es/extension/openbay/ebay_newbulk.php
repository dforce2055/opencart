<?php
//Heading
$_['heading_title']                = 'Nuevos listados por lotes';
$_['text_ebay']               	   = 'eBay';
$_['text_openbay']                 = 'Openbay Pro';

// Buttons
$_['text_none']                    = 'Ninguno';
$_['text_preview']                 = 'Vista previa';
$_['text_add']                     = 'Agregar';
$_['text_preview_all']             = 'Verificar todos';
$_['text_submit']                  = 'Enviar';
$_['text_features']                = 'Características';
$_['text_catalog']                 = 'Seleccionar catálogo';
$_['text_catalog_search']          = 'Buscar en catálogo';
$_['text_search_term']             = 'Buscar término';
$_['text_close']           		   = 'Cerrar';
$_['text_bulk']           		   = 'Crear nuevos listados por lotes';

//Form options / text
$_['text_pixels']                  = 'Pixeles';
$_['text_other']                   = 'Otros';

//Profile names
$_['text_profile']            	   = 'Perfiles';
$_['text_profile_theme']           = 'Perfil de tema';
$_['text_profile_shipping']        = 'Perfil de envío';
$_['text_profile_returns']         = 'Perfil de devoluciones';
$_['text_profile_generic']         = 'Perfil genérico';

// Text
$_['text_title']                   = 'Título';
$_['text_price']                   = 'Precio';
$_['text_stock']                   = 'Stock';
$_['text_search']                  = 'Búsqueda';
$_['text_loading']                 = 'Cargando detalles';
$_['text_preparing0']              = 'Preparando';
$_['text_preparing1']              = 'de';
$_['text_preparing2']              = 'elementos';
$_['entry_condition']              = 'Condición';
$_['text_duration']                = 'Duración';
$_['text_category']                = 'Categoría';
$_['text_exists']                  = 'Algunos items ya están listado en eBay y han sido removidos';
$_['text_error_count']             = 'Ha seleccionado %s items, puede llevar un rato procesar los datos';
$_['text_verifying']               = 'Verificando items';
$_['text_processing']              = 'En Proceso <span id="activeArtículos"></span> items';
$_['text_listed']                  = 'Item listado! ID: ';
$_['text_ajax_confirm_listing']    = '&iquot;Está seguro de que desea listar estos items en masa?';
$_['text_bulk_plan_error']         = 'Su plan actual no permite subir por lotes, mejore su plan <a href="%s">aquí</a>';
$_['text_item_limit']              = 'No puede listar estos items porque excedería el límite de su plan, mejore su plan <a href="%s">aquí</a>';
$_['text_search_text']             = 'Ingrese algún texto de búsqueda';
$_['text_catalog_no_products']     = 'No se encontraron items de catálogo';
$_['text_search_failed']           = 'Falló la búsqueda';
$_['text_esc_key']                 = 'La página de presentación ha estado oculta pero puede no haber terminado de cargar';
$_['text_loading_categories']      = 'Cargando categories';
$_['text_loading_condition']       = 'Cargando condiciones de productos';
$_['text_loading_duration']        = 'Cargando duraciones de listados';
$_['text_total_fee']         	   = 'Gastos totales';
$_['text_category_choose']         = 'Encontrar categoría';
$_['text_suggested']         	   = 'Categorías recomendadas';
$_['text_product_identifiers']     = 'Identificadores de producto';
$_['text_ean']    				   = 'EAN';
$_['text_upc']    				   = 'UPC';
$_['text_isbn']    				   = 'ISBN';
$_['text_identifier_not_required'] = 'No necesario';

//Errors
$_['text_error_ship_profile']      = 'Debe tener un perfil de envío por defecto configuado';
$_['text_error_generic_profile']   = 'Debe tener un perfil genérico por defecto configuado';
$_['text_error_return_profile']    = 'Debe tener un perfil de devolución por defecto configuado';
$_['text_error_theme_profile']     = 'Debe tener un perfil de tema por defecto configuado';
$_['text_error_variants']          = 'Artículos con variantes no pueden ser listados y han sido deseleccionados';
$_['text_error_stock']             = 'Algunos items no tienen stock y han sido removidos';
$_['text_error_no_product']        = 'No hay produtos elegibles para utilizar la funcionalidad de subir por lotes';
$_['text_error_reverify']          = 'Ocurrió un error, debería editar y volver averificar los items';
$_['error_missing_settings']       = 'No puede listar items por lotes hasta que sincronice sus ajustes de eBay';
$_['text_error_no_selection']      = 'Debe seleccionar al menos 1 item para listar';