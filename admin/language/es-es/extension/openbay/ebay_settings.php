<?php
// Heading
$_['heading_title']        		  = 'Ajustes de Tienda';
$_['text_openbay']				  = 'OpenBay Pro';
$_['text_ebay']					  = 'eBay';

// Text
$_['text_developer']				= 'Desarrollador  / soporte';
$_['text_app_settings']				= 'Ajustes de aplicación';
$_['text_default_import']			= 'Ajustes de importación por defecto';
$_['text_payments']					= 'Pagos';
$_['text_notify_settings']			= 'Ajustes de Notificación';
$_['text_listing']					= 'Defectos de listados';
$_['text_token_renew']				= 'Renovar eBay token';
$_['text_application_settings']		= 'Sus ajustes de aplicacióñ le permiten configurar como traba y se integra OpenBay Pro con su sistema.';
$_['text_import_description']		= 'Personalizar el estado de un pedido durante distintas etapas. No puede esar un estado en un pedido de eBay que no exista en esta lista. ';
$_['text_payments_description']		= 'Pre-completar sus opciones de pago para nuevo listados, esto ahorrará tiempo al no tene que ingresarloos para cada listado que publique.';
$_['text_allocate_1']				= 'Cuando el cliente compra';
$_['text_allocate_2']				= 'Cuando el cliente ha pagado';
$_['text_developer_description']	= 'No debería usar este área salvo que se le instruya';
$_['text_payment_paypal']			= 'PayPal aceptado';
$_['text_payment_paypal_add']		= 'Dirección de email de PayPal';
$_['text_payment_cheque']			= 'Cheque aceptado';
$_['text_payment_card']				= 'Tarjetas aceptadas';
$_['text_payment_desc']				= 'Ver descripción (por ej. transferencia bancaria)';
$_['text_tax_use_listing'] 			= 'Usar tasa de impuesto fijada en listado de eBay';
$_['text_tax_use_value']			= 'Usar un valor fijado para todo';
$_['text_tax_use_product']			= 'Calcular impuestos basado en país del comprador y grupo de impuestos del producto';
$_['text_notifications']			= 'Controlar cuándoo los clientes reciben notificaciones de la aplicación. Activar emails puede mejorar su puntaje DSR ya que el usuario podrá recibir novedades sobre su pedido.';
$_['text_listing_1day']             = '1 día';
$_['text_listing_3day']             = '3 días';
$_['text_listing_5day']             = '5 días';
$_['text_listing_7day']             = '7 días';
$_['text_listing_10day']            = '10 días';
$_['text_listing_30day']            = '30 días';
$_['text_listing_gtc']              = 'GTC- Bueno hasta cancelado';
$_['text_api_status']               = 'Estado de conexión con API';
$_['text_api_ok']                   = 'Conexión OK, token expira';
$_['text_api_failed']               = 'Validación fallida';
$_['text_api_other']        		= 'Otras acciones';
$_['text_create_date_0']            = 'Agregado a OpenCart en fecha';
$_['text_create_date_1']            = 'Creado en eBay en fecha';
$_['text_obp_detail_update']        = 'Actualizar URL de tienda &amp; email de contacto';
$_['text_success']					= 'Se guardaron sus ajustes';
$_['text_edit']						= 'Editar ajustes de eBay';
$_['text_checking_details'] 		= 'Detalles de checking';
$_['text_register_banner']          = 'Haga click aquí si necesita registrar una cuenta';

// Entry
$_['entry_status']				  = 'Estado';
$_['entry_token']				  = 'API token';
$_['entry_secret']				  = 'Secreto';
$_['entry_encryption_key']        = 'Clave de encripción 1';
$_['entry_encryption_iv']         = 'Clave de encripción 2';
$_['entry_end_items']			  = '¿Terminar items?';
$_['entry_relist_items']		  = '¿Volver a listar cuando haya stock?';
$_['entry_disable_soldout']		  = '¿Desactivar producto cuando no hay stock?';
$_['entry_debug']				  = 'Activar registro';
$_['entry_currency']			  = 'Moneda por defecto';
$_['entry_stock_allocate']		  = 'Asignar stock';
$_['entry_created_hours']		  = 'Límite de tiempo de nuevo pedido';
$_['entry_developer_locks']		  = '¿Quitar bloqueo de pedidos?';
$_['entry_payment_instruction']	  = 'Instrucciones de pago';
$_['entry_payment_immediate']	  = 'Pago inmediato necesario';
$_['entry_payment_types']		  = 'Tipos de pago';
$_['entry_brand_disable']		  = 'Desactivar vínculo de marca';
$_['entry_duration']			  = 'Duración de listadoo por defecto';
$_['entry_measurement']			  = 'Sistema de medidas';
$_['entry_address_format']		  = 'Formato de dirección por defecto';
$_['entry_timezone_offset']		  = 'Desplazamiento de zona horaria';
$_['entry_tax_listing']			  = 'Impuesto de producto';
$_['entry_tax']					  = '% impositivo usado para todo';
$_['entry_create_date']			  = 'Fecha de creación para nuevos pedidos';
$_['entry_notify_order_update']	  = 'Novedades de pedidos';
$_['entry_notify_buyer']		  = 'Nuevo pedido - comprador';
$_['entry_notify_admin']		  = 'Nuevo pedido - admin';
$_['entry_import_pending']		  = 'Importar pedidos sin pagar:';
$_['entry_import_def_id']		  = 'Estado por defecto de importación:';
$_['entry_import_paid_id']		  = 'Estado pagado:';
$_['entry_import_shipped_id']	  = 'Estado enviado:';
$_['entry_import_cancelled_id']	  = 'Estado cancelado:';
$_['entry_import_refund_id']	  = 'Estado reintegrado:';
$_['entry_import_part_refund_id'] = 'Estado parcialmente reintegrado:';

// Tabs
$_['tab_api_info']				  = 'Detalles de API';
$_['tab_setup']					  = 'Configuraciones';
$_['tab_defaults']				  = 'Valores por defecto para listados';

// Ayuda
$_['help_disable_soldout']		  = 'Cuando se agota un item, se desactiva el producto en OpenCart';
$_['help_relist_items'] 		  = 'Si un vínculo de item existió antes, se volverá a listar el item previo si vuelve a estar en stock';
$_['help_end_items']    		  = 'Si se debería terminar el listado en eBay cuando se agota un item';
$_['help_currency']     		  = 'Basado en monedas de su tienda';
$_['help_created_hours']   		  = 'Los Pedidos son nuevos cuando tienen menos horas que este límite. 72 por defecto';
$_['help_stock_allocate'] 		  = '¿Cuándo deberíaasignarse stock desde la tienda?';
$_['help_payment_instruction']    = 'Sea tan descriptivo como pueda. ¿Requiere un pago dentro de cierto tiempo? ¿Deben llamar para pagar con tarjeta? ¿Tiene términos dep ago especiales?';
$_['help_payment_immediate'] 	  = 'Pago inmediato evita compradores sin pago, ya que un item no está vendido hasta que pagan.';
$_['help_listing_tax']     		  = 'Si usa la tasa de listado asegúrese de que es correcta en eBay';
$_['help_tax']             		  = 'Cuando importe pedidos o items';
$_['help_duration']    			  = 'GTC sólo está disponible si posee una tienda eBay.';
$_['help_address_format']      	  = 'Sólo utilizado si el país no tiene un formato de dirección configurado.';
$_['help_create_date']         	  = 'Elija el tiempo de creación que se mostrará en un pedido cuando es importado';
$_['help_timezone_offset']     	  = 'Basado en horas. 0 es huso horario GMT. Sólo funciona si se utiliza el tiempo de eBay para la creación de pedidos.';
$_['help_notify_admin']   		  = 'Notificar al administador de tienda con el email de nuevo pedido por defecto.';
$_['help_notify_order_update']	  = 'Esto es para novedades automáticas, por ejemplo si actualiza un pedido en eBay y el nuevo estado es actualizado en su tienda automáticamente.';
$_['help_notify_buyer']        	  = 'Notificar al usuario con el email de nuevo pedido por defecto.';
$_['help_measurement']        	  = 'Elija el sistema de medición a utilizar en sus listados';

// Buttons
$_['button_update']               = 'Actualizar';
$_['button_repair_links']    	  = 'Reparar vínculos de items';

// Error
$_['error_api_connect']           = 'No se pudo conectar con la API';
