<?php
// Headings
$_['heading_title']      	 = 'Nuevo listado';
$_['text_openbay']           = 'OpenBay Pro';
$_['text_etsy']              = 'Etsy';

// Tabs
$_['tab_additional']      	 = 'Información adicional';

// Text
$_['text_option']      		 = 'Seleccione una opción';
$_['text_category_selected'] = 'Categoría seleccionada';
$_['text_material_add']  	 = 'Agregar Material';
$_['text_material_remove']   = 'Quitar Material';
$_['text_tag_add']  		 = 'Agregar etiqueta';
$_['text_tag_remove']  		 = 'Quitar etiqueta';
$_['text_created']  		 = 'Listado creado';
$_['text_listing_id']  		 = 'ID de listado';
$_['text_img_upload']  		 = 'Subiendo imagen';
$_['text_img_upload_done']   = 'Imagen subida';
$_['text_create']  			 = 'Crear nuevo listado en Etsy';

// Entry
$_['entry_title']      		 = 'Título de producto';
$_['entry_description']      = 'Descripción';
$_['entry_price']      		 = 'Precio';
$_['entry_non_taxable']      = 'No imponible';
$_['entry_category']     	 = 'Selección de Categoría';
$_['entry_who_made']		 = 'Quíen lo hizo';
$_['entry_when_made']		 = 'Cuándo se hizo';
$_['entry_recipient']		 = 'Para quién es';
$_['entry_occasion']		 = 'Para qué ocasión es';
$_['entry_is_supply']		 = '¿Es un insumo?';
$_['entry_state']      		 = 'Estado de listado';
$_['entry_style']      		 = 'Etiqueta de estilo 1';
$_['entry_style_2']      	 = 'Etiqueta de estilo 2';
$_['entry_processing_min']   = 'En Proceso tiempo min';
$_['entry_processing_max']   = 'En Proceso tiempo max';
$_['entry_materials']  		 = 'Materiales';
$_['entry_tags']  			 = 'Etiquetas';
$_['entry_shipping']  		 = 'Perfil de envío';
$_['entry_shop']  			 = 'Sección de tienda';
$_['entry_is_custom']  		 = '¿Es personalizable?';
$_['entry_image']  			 = 'Imagen principal';
$_['entry_image_other']		 = 'Otras imágenes';

// Ayuda
$_['help_description']		 = 'Se quitó todo el HTML de descripción puesto que no es soportado en Etsy';

// Errors
$_['error_no_shipping']  	 = '¡No ha configurado perfiles de envío!';
$_['error_no_shop_secton']   = '¡No ha configurado secciones de tienda!';
$_['error_no_img_url']  	 = 'No se seleccionó una imagen para subir';
$_['error_no_listing_id']  	 = 'No se suministró un ID de listado';
$_['error_title_length']  	 = 'Título es demasiado largo';
$_['error_title_missing']  	 = 'Falta Título';
$_['error_desc_missing']  	 = 'Falta descripción o está vacía';
$_['error_price_missing']  	 = 'Falta precio o está vacío';
$_['error_category']  		 = 'Categoría no seleccionada';
$_['error_style_1_tag']  	 = 'Etiqueta de estilo 1 inválida';
$_['error_style_2_tag']  	 = 'Etiqueta de estilo 2 inválida';
$_['error_materials']  		 = 'Sólo puede agregar 13 materiales';
$_['error_tags']  			 = 'Sólo puede agregar 13 etiquetas';
$_['error_stock_max']  		 = 'El stock máximo que puede listar en Etsy es 999, usted tiene %s en stock';
$_['error_image_max']  		 = 'El máximo de imagenes que puede utilizar en Etsy son 5, usted ha elegido %s';
$_['error_variant']			 = 'Items variantes en Etsy.com no están soportados aún en OpenBay Pro';