<?php
// Heading
$_['heading_title']        	= 'Actualizaciones de stock';
$_['text_openbay']			= 'OpenBay Pro';
$_['text_amazon']			= 'Amazon EU';

// Text
$_['text_empty']            = '¡Sin resultados!';

// Entry
$_['entry_date_start']      = 'Fecha de Inicio';
$_['entry_date_end']        = 'Fecha de Finalización';

// Column
$_['column_ref']            = 'Ref';
$_['column_date_requested'] = 'Fecha solicitado';
$_['column_date_updated']   = 'Fecha actualizado';
$_['column_status']         = 'Estado';
$_['column_sku']            = 'Amazon SKU';
$_['column_stock']          = 'Stock';

// Error
$_['error_api_connection']  = 'No se pudo conectar a la API';