<?php
// Heading
$_['heading_title']         	  = 'Amazon USA';
$_['text_openbay']				  = 'OpenBay Pro';
$_['text_dashboard']			  = 'Amazon US Panel de Control';

// Text
$_['text_heading_settings'] 	  = 'Configuraciones';
$_['text_heading_account'] 		  = 'Cambiar plan';
$_['text_heading_links'] 		  = 'Vínculos de items';
$_['text_heading_register'] 	  = 'Regístrese aquí';
$_['text_heading_bulk_listing']   = 'Listado por lotes';
$_['text_heading_stock_updates']  = 'Actualizaciones de stock';
$_['text_heading_saved_listings'] = 'Listados guardados';
$_['text_heading_bulk_linking']   = 'Vinculación por lotes';
