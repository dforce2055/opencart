<?php
// Heading
$_['heading_title']   		   = 'Actualización de pedidos en masa';
$_['text_openbay']             = 'OpenBay Pro';
$_['text_confirm_title']       = 'Revisar actualización de estados en masa';

// Button
$_['button_status']            = 'Cambiar estado';
$_['button_update']            = 'Actualizar';
$_['button_ship']              = 'Enviar pedido';

// Column
$_['column_channel']           = 'Canal de pedido';
$_['column_additional']        = 'Info adicional';
$_['column_comments']      	   = 'Comentarios';
$_['column_notify']        	   = 'Notificar';

// Text
$_['text_confirmed']           = '%s pedidos actualizados';
$_['text_no_orders']           = 'No se seleccionaron pedidos para actualizar';
$_['text_confirm_change_text'] = 'Cambiando estado de pedido a';
$_['text_other']               = 'Otros';
$_['text_error_carrier_other'] = '¡Falta un pedido en la entrada "Otro transporte"!';
$_['text_web']                 = 'Web';
$_['text_ebay']                = 'eBay';
$_['text_amazon']              = 'Amazon EU';
$_['text_amazonus']            = 'Amazon US';
$_['text_etsy']                = 'Etsy';
$_['text_list']				   = 'Lista de pedidos';

// Entry
$_['entry_carrier']            = 'Transporte';
$_['entry_tracking_no']        = 'Rastreo(Tracking)';
$_['entry_other']              = 'Otros';
$_['entry_date_added']         = 'Fecha añadido';
$_['entry_order_channel']      = 'Canal de pedido';
