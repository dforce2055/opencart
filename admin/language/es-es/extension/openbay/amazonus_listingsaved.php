<?php
// Heading
$_['heading_title'] 	  = 'Listados guardados';
$_['text_openbay'] 		  = 'OpenBay Pro';
$_['text_amazon'] 		  = 'Amazon US';

// Text
$_['text_description']    = 'Esta es la lista de listados de productos que están guardados y listos para subir a Amazon.';
$_['text_uploaded_alert'] = 'Listado(s) guardado(s) subido!';
$_['text_delete_confirm'] = '¿Está seguro?';
$_['text_complete']       = 'Listados subidos';

// Column
$_['column_name']         = 'Nombre';
$_['column_model']        = 'Modelo';
$_['column_sku']          = 'SKU';
$_['column_amazon_sku']   = 'SKU de Amazon de item';
$_['column_action']       = 'Acción';