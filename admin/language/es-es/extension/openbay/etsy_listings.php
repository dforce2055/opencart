<?php
// Headings
$_['heading_title']        	  = 'Listados Etsy';
$_['text_openbay']            = 'OpenBay Pro';
$_['text_etsy']               = 'Etsy';

// Text
$_['text_link_saved']         = 'Item vinculado';
$_['text_activate']           = 'Activar';
$_['text_deactivate']         = 'Deactivar';
$_['text_add_link']           = 'Agregar vínculo';
$_['text_delete_link']        = 'Eliminar vínculo';
$_['text_delete']         	  = 'Eliminar listado';
$_['text_status_stock']       = 'Stock no sincronizado';
$_['text_status_ok']          = 'OK';
$_['text_status_nolink']      = 'No vinculado';
$_['text_link_added']         = 'Se vinculó un producto al listado';
$_['text_link_deleted']       = 'Se desvinculó un producto del listado';
$_['text_item_ended']         = 'Se eliminó el item de Etsy';
$_['text_item_deactivated']   = 'Se desactivó el item en Etsy';
$_['text_item_activated']     = 'Se activó el item en Etsy';
$_['text_confirm_end']        = '¿Está seguro de que desea eliminar el listado?';
$_['text_confirm_deactivate'] = '¿Está seguro de que desea desactivar el listado?';
$_['text_confirm_activate']   = '¿Está seguro de que desea activar el listado?';
$_['text_listings']     	  = 'Administrar sus Listados Etsy';
$_['text_active']     	      = 'Activo';
$_['text_inactive']     	  = 'Inactivo';
$_['text_draft']     	      = 'Borrador';
$_['text_expired']     	      = 'Expirado';

// Columns
$_['column_listing_id']		  = 'ID de Etsy';
$_['column_title']			  = 'Título';
$_['column_listing_qty']	  = 'Cantidad de listado';
$_['column_store_qty']		  = 'Cantidad de tienda';
$_['column_status']			  = 'Mensaje de estado';
$_['column_link_status']	  = 'Estado de vínculo';
$_['column_action']			  = 'Acción';

// Entry
$_['entry_limit']			  = 'Límite de página';
$_['entry_status']			  = 'Estado';
$_['entry_keywords']		  = 'Palabras Clave';
$_['entry_name']			  = 'Nombre de Producto';
$_['entry_etsy_id']			  = 'ID de item de Etsy';

// Ayuda
$_['help_keywords']			  = 'Palabras Clave sólo se aplican con listados activos';

// Error
$_['error_etsy']			  = '¡Error! Respuesta de API de Etsy ';
$_['error_product_id']		  = 'ID de Producto necesario';
$_['error_etsy_id']			  = 'ID de item de Etsy necesario';
