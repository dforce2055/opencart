<?php
// Heading
$_['heading_title']				= 'Vínculos de items';
$_['text_openbay']				= 'OpenBay Pro';
$_['text_amazon']				= 'Amazon US';

// Text
$_['text_desc1']                = 'Vincular sus items le permitirá controlar el stock en sus listados de Amazon.';
$_['text_desc2'] 				= 'Por cada item que se actualiza en el stock local (el stock disponible en su tienda OpenCart) se actualizará su listado en Amazon.';
$_['text_desc3']                = 'Puede vincular items manualmente ingresando el SKU de Amazon y el nombre de producto o cargar todos los productos sin vincular y luego ingresar los SKUs de Amazon. (Subir productos de OpenCart a Amazon añadirá vínculos automáticamente)';
$_['text_new_link']             = 'Nuevo vínculo';
$_['text_autocomplete_product'] = 'Producto (Auto completar desde nombre)';
$_['text_amazon_sku']           = 'SKU de Amazon de item';
$_['text_action']               = 'Acción';
$_['text_linked_items']         = 'Items vinculados';
$_['text_unlinked_items']       = 'Items desvinculados';
$_['text_name']                 = 'Nombre';
$_['text_model']                = 'Modelo';
$_['text_combination']          = 'Combinación de variante';
$_['text_sku']                  = 'SKU de producto';
$_['text_sku_variant']          = 'SKU de variante';

// Button
$_['button_load']               = 'Cargar';

// Error
$_['error_empty_sku']        	= '¡El SKU de Amazon no puede estar vacío!';
$_['error_empty_name']       	= '¡El nombre de producto no puede estar vacío!';
$_['error_no_product_exists']   = 'El producto no existe. Por favor utilice valores auto completados.';