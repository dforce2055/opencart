<?php
// Heading
$_['heading_title'] 				  = 'Administrar listings';

// Text
$_['text_markets']                    = 'Mercados';
$_['text_openbay']                    = 'OpenBay Pro';
$_['text_ebay'] 					  = 'eBay';
$_['text_amazon'] 					  = 'Amazon EU';
$_['text_amazonus'] 				  = 'Amazon US';
$_['text_etsy'] 					  = 'Etsy';
$_['text_status_all'] 				  = 'Todos';
$_['text_status_ebay_active'] 		  = 'eBay activo';
$_['text_status_ebay_inactive'] 	  = 'eBay inactivo';
$_['text_status_amazoneu_saved'] 	  = 'Amazon EU guardado';
$_['text_status_amazoneu_processing'] = 'Amazon EU en proceso';
$_['text_status_amazoneu_active'] 	  = 'Amazon EU activo';
$_['text_status_amazoneu_notlisted']  = 'Amazon EU no listado';
$_['text_status_amazoneu_failed'] 	  = 'Amazon EU falló';
$_['text_status_amazoneu_linked'] 	  = 'Amazon EU vinculado';
$_['text_status_amazoneu_notlinked']  = 'Amazon EU no vinculado';
$_['text_status_amazonus_saved'] 	  = 'Amazon US guardado';
$_['text_status_amazonus_processing'] = 'Amazon US en proceso';
$_['text_status_amazonus_active'] 	  = 'Amazon US activo';
$_['text_status_amazonus_notlisted']  = 'Amazon US no listado';
$_['text_status_amazonus_failed'] 	  = 'Amazon US falló';
$_['text_status_amazonus_linked'] 	  = 'Amazon US vinculado';
$_['text_status_amazonus_notlinked']  = 'Amazon US no vinculado';
$_['text_processing']       		  = 'En Proceso';
$_['text_category_missing'] 		  = 'Categoría faltante';
$_['text_variations'] 				  = 'variaciones';
$_['text_variations_stock'] 		  = 'stock';
$_['text_min']                        = 'Min';
$_['text_max']                        = 'Max';
$_['text_option']                     = 'Opción';
$_['text_list']              		  = 'Lista de productos';

// Entry
$_['entry_title'] 					  = 'Título';
$_['entry_model'] 					  = 'Modelo';
$_['entry_manufacturer'] 			  = 'Fabricante';
$_['entry_status'] 					  = 'Estado';
$_['entry_status_marketplace'] 		  = 'Estado de tienda';
$_['entry_stock_range'] 			  = 'Rango de stock';
$_['entry_category'] 				  = 'Categoría';
$_['entry_populated'] 				  = 'Poblado';
$_['entry_sku'] 					  = 'SKU';
$_['entry_description'] 			  = 'Descripción';

// Button
$_['button_error_fix']                = 'Corregir errores';
$_['button_amazon_eu_bulk']           = 'Subir en masa a Amazon EU';
$_['button_amazon_us_bulk']           = 'Subir en masa a Amazon US';
$_['button_ebay_bulk']                = 'Subir en masa a eBay';

// Error
$_['error_select_items']              = 'Debe seleccionar al menos 1 item para listar en masa';