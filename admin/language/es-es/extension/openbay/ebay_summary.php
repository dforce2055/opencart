<?php
// Heading
$_['heading_title']        = 'Resumen';
$_['text_openbay']         = 'OpenBay Pro';
$_['text_ebay']            = 'eBay';

// Text
$_['text_use_desc']        = 'Esta es su página de resumen de su cuenta eBay. Es una vista rápida de cualquieer límite en su cuenta así como también su rendimiento de ventas DSR.';
$_['text_ebay_limit_head'] = 'Su Cuenta eBay tiene límites de venta!';
$_['text_ebay_limit_t1']   = 'Puede vender';
$_['text_ebay_limit_t2']   = 'más items (esta es la cantidad total de items, no de listados individuales) al valor de';
$_['text_ebay_limit_t3']   = 'Cuando intente crear un listado, fallará si excede los valores anteriores.';
$_['text_as_described']    = 'Item como descrito';
$_['text_communication']   = 'Comunicación';
$_['text_shippingtime']    = 'Tiempo de envío';
$_['text_shipping_charge'] = 'Cargos de envío';
$_['text_score']           = 'Puntaje';
$_['text_count']           = 'Cuenta';
$_['text_report_30']       = '30 días';
$_['text_report_52']       = '52 semanas';
$_['text_title_dsr']       = 'Reportes DSR';
$_['text_failed']          = 'Falló la carga';
$_['text_summary']         = 'Ver su Resumen de eBay';

// Error
$_['error_validation']     = 'Debe registrarse para recibir su API token y activar el módulo.';
$_['error_ajax_load']      = 'Lo sentimos, falló la conexión al servidor.';