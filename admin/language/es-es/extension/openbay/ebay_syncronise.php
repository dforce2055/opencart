<?php
// Heading
$_['heading_title']         = 'Sincronizar';
$_['text_openbay']          = 'OpenBay Pro';
$_['text_ebay']             = 'eBay';

// Buttons
$_['button_update']			= 'Actualizar';

// Entry
$_['entry_sync_categories'] = 'Obtener categorías de eBay';
$_['entry_sync_shop']       = 'Obtener categorías de tienda';
$_['entry_sync_setting']    = 'Obtener ajustes';

// Text
$_['text_complete']         = 'Completar';
$_['text_sync_desc']        = 'Sincronice su tienda con las más recientes opciones de envío y categorías de eBay, estos datos son sólo para las opcinoes cuando se lista un iiem a eBaay - no importará categorías a su tienda, etc.';
$_['text_ebay_categories']  = 'Esto puede demorar un rato, espere 5 minutos antes de realizar cualquier otra operación.';
$_['text_category_import']  = 'Sus categorías de negocio de eBay han sido importadas.';
$_['text_setting_import']   = 'Sus ajustes han sido importados.';
$_['text_sync']  			= 'Actualizar ajustes desde eBay';

// Ayuda
$_['help_sync_categories']  = '¡Esto no importa ninguna categoría a su tienda!';
$_['help_sync_shop']   		= '¡Esto no importa ninguna categoría a su tienda!';
$_['help_sync_setting']		= 'Esto importa los métodos de pago, envío, ubicaciones y más que están disponibles.';

// Errors
$_['error_settings']		= 'Ocurrió un error al cargar los ajustes.';
$_['error_failed']          = 'Falló la carga';