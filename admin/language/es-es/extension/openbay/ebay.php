<?php
// Heading
$_['heading_title']         	   = 'eBay';
$_['text_openbay']				   = 'OpenBay Pro';
$_['text_dashboard']			   = 'Panel de Control eBay';

// Text
$_['text_success']         		   = 'Ha cambiado sus cambios a la extensión de eBay';
$_['text_heading_settings']        = 'Configuraciones';
$_['text_heading_sync']            = 'Sincronizar';
$_['text_heading_subscription']    = 'Cambiar plan';
$_['text_heading_usage']           = 'Uso';
$_['text_heading_links']           = 'Vínculos de items';
$_['text_heading_item_import']     = 'Importar items';
$_['text_heading_order_import']    = 'Importar pedidos';
$_['text_heading_adds']            = 'Complementos instalados';
$_['text_heading_summary']         = 'Resumen de eBay';
$_['text_heading_profile']         = 'Perfiles';
$_['text_heading_template']        = 'Plantillas';
$_['text_heading_ebayacc']         = 'Cuenta eBay';
$_['text_heading_register']        = 'Regístrese aquí';

// Error
$_['error_category_nosuggestions'] = 'No se pudo cargar ninguna categoría sugerida';
$_['error_loading_catalog']        = 'Falló la búsqueda en el catálogo de eBay';
$_['error_generic_fail']           = '¡Ocurrió un error desconocido!';
$_['error_no_products']         	= 'No se encontraron productos';