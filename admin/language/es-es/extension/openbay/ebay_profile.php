<?php
// Heading
$_['heading_title']                                   = 'Perfiles';
$_['text_openbay']                                    = 'OpenBay Pro';
$_['text_ebay']                                       = 'eBay';

//Tabs
$_['tab_returns']          				              = 'Devoluciones';
$_['tab_template']         				              = 'Plantilla';
$_['tab_gallery']          				              = 'Galería';
$_['tab_settings']         				              = 'Configuraciones';

//Envío Profile
$_['text_shipping_dispatch_country']                  = 'Envío desde país';
$_['text_shipping_postcode']                          = 'Código posta/zip de ubicación';
$_['text_shipping_location']                          = 'Ciudad o Estado de ubicación';
$_['text_shipping_despatch']                          = 'Tiempo de despacho';
$_['text_shipping_despatch_help']                     = 'Este es el número máximo de díaas que le tomará realizar el envío';
$_['text_shipping_nat']                               = 'Servicios de envío nacional';
$_['text_shipping_intnat']                            = 'Servicios de envío internacional';
$_['text_shipping_first']                             = 'Primer item';
$_['text_shipping_add']                               = 'Items adicionales';
$_['text_shipping_service']                           = 'Servicio';
$_['text_shipping_in_desc']                           = 'Info de flete en descripción';
$_['text_shipping_getitfast']                         = '¡Obtener rápido!';
$_['text_shipping_zones']                             = 'Envío a zonas';
$_['text_shipping_worldwide']                         = 'Mundial';
$_['text_shipping_type_nat']           	              = 'Envío nacional';
$_['text_shipping_type_int']           	              = 'Envío internacional';
$_['text_shipping_flat']           		              = 'Tarifa plana';
$_['text_shipping_calculated']                        = 'Calculado';
$_['text_shipping_freight']          	              = 'Flete';
$_['text_shipping_handling']          	              = 'Gasto administrativo';
$_['text_shipping_cod']           		              = 'Gasto por efectivo en entrega';
$_['text_shipping_handling_nat']    	              = 'Gasto administrativo (nacional)';
$_['entry_shipping_handling_int']    	              = 'Gasto administrativo (internacional)';
$_['entry_shipping_pickupdropoff']  	              = 'Clickear y Recolectar';
$_['entry_shipping_pickupinstore']  	              = 'Disponible para recoger en tienda';
$_['entry_shipping_global_shipping']  	              = 'Usar servicio de envíoo global de eBay';
$_['entry_shipping_promotion_discount']               = 'Descuentos de envío combinados (nacional)';
$_['entry_shipping_promotion_discount_international'] = 'Descuentos de envío combinados (internacional)';

//Returns profile
$_['text_returns_accept']       		              = 'Se aceptan devoluciones';
$_['text_returns_inst']         		              = 'Políticas de devolución';
$_['text_returns_days']         		              = 'Días de devolución';
$_['text_returns_days10']       		              = '10 Días';
$_['text_returns_days14']       		              = '14 Días';
$_['text_returns_days30']       		              = '30 Días';
$_['text_returns_days60']       		              = '60 Días';
$_['text_returns_type']         		              = 'Tipo de devolución';
$_['text_returns_type_money']   		              = 'Devolución de dinero';
$_['text_returns_type_exch']    		              = 'Cambio o Devolución de dinero';
$_['text_returns_costs']        		              = 'Devolución de costos de envío';
$_['text_returns_costs_b']      		              = 'El comprador paga';
$_['text_returns_costs_s']      		              = 'El vendedor paga';
$_['text_returns_restock']      		              = 'Gasto de reabastecimiento';
$_['text_list']           				              = 'Lista de perfiles';

//Plantilla profile
$_['text_template_choose']      		              = 'Plantilla por defecto';
$_['text_template_choose_help'] 		              = 'Se cargará una plantilla por defecto automáticamente cuando se hace un listado para ahorrar tiempo';
$_['text_image_gallery']        		              = 'Tamaño de imagen de galería';
$_['text_image_gallery_help']   		              = 'Tamaño en pixeles de imágenes de galerías que son agregadas a su plantilla.';
$_['text_image_thumb']          		              = 'Tamaño de imagen miniatura';
$_['text_image_thumb_help']     		              = 'Tamaño en pixeles de imágenes miniatura que son agregadas a su plantilla.';
$_['text_image_super']          		              = 'Imágenes supersize';
$_['text_image_gallery_plus']   		              = 'Galería plus';
$_['text_image_all_ebay']       		              = 'Agregar todas las imágenes a eBay';
$_['text_image_all_template']   		              = 'Agregar todas las imágenes a template';
$_['text_image_exclude_default']		              = 'Excluir imagen por defecto';
$_['text_image_exclude_default_help']	              = '¡Sólo para listado por lotes! No incluirá la imagen del producto por defecto en la lista de imágenes de tema';
$_['text_confirm_delete']       		              = '¿Está seguro de que desea eliminar el perfil?';
$_['text_width']      					              = 'Ancho';
$_['text_height']      					              = 'Alto';
$_['text_px']      						              = 'px';
$_['text_add']      					              = 'Agregar perfil';
$_['text_edit']      					              = 'Editar perfil';

//General profile
$_['text_general_private']      		              = 'Listar items como subasta privada';
$_['text_general_price']        		              = '% modificación de precio';
$_['text_general_price_help']   		              = '0 por defecto, -10 lo reduce en 10%, 10 lo incrementa en 10% (sólo utilizado en listados por lotes)';

//General profile options
$_['text_profile_name']         		              = 'Nombre';
$_['text_profile_default']      		              = 'Defecto';
$_['text_profile_type']         		              = 'Tipo';
$_['text_profile_desc']         		              = 'Descripción';
$_['text_profile_action']       		              = 'Acción';

// Profile types
$_['text_type_shipping']       			              = 'Envío';
$_['text_type_returns']       			              = 'Devoluciones';
$_['text_type_template']       			              = 'Plantilla &amp; gallery';
$_['text_type_general']       			              = 'Ajustes generales';

//Éxito messages
$_['text_added']                		              = 'Nuevo perfil creado';
$_['text_updated']              		              = 'Perfil ha sido actualizado';

//Errors
$_['error_permission']        			              = 'No tiene permisos para editar perfiles';
$_['error_name']           				              = 'Debe ingresar un nombre de perfil';
$_['error_no_template']          		              = 'ID de Plantilla no existe';
$_['error_missing_settings'] 			              = 'No puede agregar, editar o quitar perfiles hasta que sincronice los ajustes con eBay';

//Ayuda
$_['help_shipping_promotion_discount']                = 'Ofrecer descuentos de envío a compradores nacionales si compran varios items. Los descuentos deben estar configurados en eBay para tener efecto.';
$_['help_shipping_promotion_discount_international']  = 'Ofrecer descuentos de envío a compradores internacionales si compran varios items. Los descuentos deben estar configurados en eBay para tener efecto.';