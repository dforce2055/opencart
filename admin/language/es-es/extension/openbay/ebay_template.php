<?php
// Headings
$_['heading_title']       = 'Plantillas de listados';
$_['text_ebay']           = 'eBay';
$_['text_openbay']        = 'OpenBay Pro';

// Columns
$_['column_name']         = 'Nombre de Plantilla';
$_['column_action']       = 'Acción';

// Entry
$_['entry_template_name'] = 'Nombre';
$_['entry_template_html'] = 'HTML';

// Text
$_['text_added']          = 'Se agregó una nueva plantilla';
$_['text_updated']        = 'Plantilla actualizada';
$_['text_deleted']        = 'Plantilla eliminada';
$_['text_confirm_delete'] = '¿Está seguro de que desea eliminar la plantilla?';
$_['text_list']           = 'Lista de Plantillas';
$_['text_add']      	  = 'Agregar perfil';
$_['text_edit']      	  = 'Editar perfil';

// Error
$_['error_name']          = 'Debe ingresar un nombre de plantilla';
$_['error_permission']    = 'No tiene permisos para editar plantillas';
$_['error_no_template']   = 'ID de plantilla no existe';