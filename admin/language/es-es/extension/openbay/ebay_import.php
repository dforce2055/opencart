<?php
// Heading
$_['heading_title']              = 'Importar items';
$_['text_openbay']               = 'OpenBay Pro';
$_['text_ebay']                  = 'eBay';

// Text
$_['text_sync_import_line1']     = '<strong>Advertencia!</strong> Esto importará todos sus productos de eBay y creará una estructura de categorías en su tienda. Se recomienda que borre todas sus categorías y productos antes de ejecutar esta opción. <br />La estructura de categorías es la de las categorías normales de eBay, no de las categorías de su tienda (si tiene una tienda en eBay). Puede renombrar, quitar, y editar las categorías importadas sin afectar sus productos en eBay.';
$_['text_sync_import_line3']     = 'Debe asegurarse de que su servidor puede aceptar y procesar grandes tamaños de datos POST. 1000 items de eBay son alrededor de 40Mb en tamaño, deberá calcular cuánto requiere. Si su llamada falla es probable que su configuración es demasiado pequeña. Su límite de memoria de PHP debe ser de aproximadamente 128Mb.';
$_['text_sync_server_size']      = 'Actualmente su servidor puede aceptar: ';
$_['text_sync_memory_size']      = 'Su limite de memoria de PHP: ';
$_['text_import_confirm']		 = '¿Está seguro de que desea importar todos sus items de eBay como nuevos productos? ¡ Esto NO se puede deshacer. ASEGÚRESE de tener un backup primero!';
$_['text_import_notify']		 = 'Su solicitud de importación ha sido enviada para su procesamiento. La importación lleva alrededor de 1 hora por cada 1000 items.';
$_['text_import_images_msg1']    = 'imágenes están pendientes de copiar/importar de eBay. Actualice esta página, si el número no disminuye entonces';
$_['text_import_images_msg2']    = 'haga click aquí';
$_['text_import_images_msg3']    = 'y espere. Más información de por qué sucedió esto se puede encontrar <a href="http://shop.openbaypro.com/index.php?route=information/faq&topic=8_45" target="_blank">aquí</a>';

// Entry
$_['entry_import_item_advanced'] = 'Obtener datos avanzados';
$_['entry_import_categories']    = 'Importar categorías';
$_['entry_import_description']	 = 'Importar descripciones de items';
$_['entry_import']				 = 'Importar items de eBay';

// Buttons
$_['button_import']				 = 'Importar';
$_['button_complete']			 = 'Completar';

// Ayuda
$_['help_import_item_advanced']  = 'Llevará hasta 10 veces más tiempo importar items. Importa pesos, tamaños, ISBN y más si están disponibles';
$_['help_import_categories']     = 'Crea una estructura de categorías en su tienda en base a las categorías de eBay';
$_['help_import_description']    = 'Esto importará todo incluyendo HTML, contadores de visitas, etc';

// Error
$_['error_import']               = 'Falló la carga';
$_['error_maintenance']			 = 'Su tienda está en modo de mantenimiento. ¡Las importaciones fallarán!';
$_['error_ajax_load']			 = 'No se pudo conectar al servidor';
$_['error_validation']			 = 'Debe registrarse para recibir su API token y activar el módulo.';