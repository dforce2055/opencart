<?php
// Heading
$_['heading_title'] 			  = 'Nuevo listado en Amazon';
$_['text_title_advanced'] 		  = 'Listado avanzado';
$_['text_openbay'] 				  = 'OpenBay Pro';
$_['text_amazon'] 				  = 'Amazon US';

// Buttons
$_['button_new'] 				  = 'Crear producto nuevo';
$_['button_amazon_price'] 		  = 'Obtener precio de Amazon';
$_['button_list'] 				  = 'Listar en Amazon';
$_['button_remove_error'] 		  = 'Quitar mensajes de error';
$_['button_save_upload'] 		  = 'Guardar y subir';
$_['button_browse'] 			  = 'Explorar';
$_['button_saved_listings'] 	  = 'Ver listados guardados';
$_['button_remove_links'] 		  = 'Quitar vínculos';
$_['button_create_new_listing']   = 'Crear nuevo listado';

// Ayuda
$_['help_sku'] 					  = 'ID de producto único asignado por vendedor';
$_['help_restock_date'] 		  = 'Esta es la fecha en la que podrá enviar cualquier pedido de items fuera de stock a cliente. Esta fecha no debería ser mayor a 30 días de la fecha listado o los pedidos recibidos serán cancelados automaticamente.';
$_['help_sale_price'] 			  = 'Precio de venta debe tener una fecha inicial y una fecha final';

// Text
$_['text_products_sent'] 		  = 'Productos fueron enviados para ser procesados';
$_['button_view_on_amazon'] 	  = 'Ver en Amazon';
$_['text_list'] 				  = 'Listar en Amazon';
$_['text_new'] 					  = 'Nuevo';
$_['text_used_like_new'] 		  = 'Usado - Como nuevo';
$_['text_used_very_good'] 		  = 'Usado - Muy Bueno';
$_['text_used_good'] 			  = 'Usado - Bueno';
$_['text_used_acceptable'] 		  = 'Usado - Aceptable';
$_['text_collectible_like_new']   = 'Artículo de colección - Como Nuevo';
$_['text_collectible_very_good']  = 'Artículo de colección - Muy Bueno';
$_['text_collectible_good'] 	  = 'Artículo de colección - Bueno';
$_['text_collectible_acceptable'] = 'Artículo de colección - Aceptable';
$_['text_refurbished'] 			  = 'Renovado';
$_['text_product_not_sent'] 	  = 'Producto no fue enviado a Amazon. Razón: %s';
$_['text_not_in_catalog'] 		  = 'O, si no está en el catálogo&nbsp;&nbsp;&nbsp;';
$_['text_placeholder_search'] 	  = 'Ingrese nombre de producto, UPC, EAN, ISBN o ASIN';
$_['text_placeholder_condition']  = 'Describa aquí la condición de sus productos.';
$_['text_characters'] 			  = 'caracteres';
$_['text_uploaded'] 			  = 'Listado(s) guardado(s) subido!';
$_['text_saved_local'] 			  = 'Listado guardado pero no subido aún';
$_['text_product_sent'] 		  = 'Producto fue enviaddo a Amazon exitosamente.';
$_['text_links_removed'] 		  = 'Vínculos de producto de Amazon eliminados';
$_['text_product_links'] 		  = 'Vínculos de producto';
$_['text_has_saved_listings'] 	  = 'Este producto tiene uno o más listados guardados que no están subidos';
$_['text_edit_heading'] 		  = 'Editar listado';

// Columns
$_['column_image'] 				  = 'Imagen';
$_['column_asin'] 				  = 'ASIN';
$_['column_price'] 				  = 'Precio';
$_['column_action'] 			  = 'Acción';
$_['column_name'] 				  = 'Nombre de Producto';
$_['column_model'] 				  = 'Modelo';
$_['column_combination'] 		  = 'Combinación de variante';
$_['column_sku_variant'] 		  = 'SKU de variante';
$_['column_sku'] 				  = 'SKU de producto';
$_['column_amazon_sku'] 		  = 'SKU de Amazon de item';

// Entry
$_['entry_sku'] 				  = 'SKU';
$_['entry_condition'] 			  = 'Condición';
$_['entry_condition_note'] 		  = 'Nota de Condición';
$_['entry_price'] 				  = 'Precio';
$_['entry_sale_price'] 			  = 'Precio de oferta';
$_['entry_sale_date'] 			  = 'Rango de fechas de oferta';
$_['entry_quantity'] 			  = 'Cantidad';
$_['entry_start_selling'] 		  = 'Disponible desde fecha';
$_['entry_restock_date'] 		  = 'Fecha de reabastecimiento';
$_['entry_country_of_origin'] 	  = 'País de Origen';
$_['entry_release_date'] 		  = 'Fecha de lanzamiento';
$_['entry_from'] 				  = 'Fecha desde';
$_['entry_to'] 					  = 'Fecha hasta';
$_['entry_product'] 			  = 'Listado para producto';
$_['entry_category'] 			  = 'Categoría de Amazon';

//Tabs
$_['tab_main'] 					  = 'Principal';
$_['tab_necesario'] 				  = 'Información necesaria';
$_['tab_additional'] 			  = 'Adicional options';

//Errors
$_['error_text_missing'] 		  = 'Debe ingresar algunos detalles de búsqueda';
$_['error_data_missing'] 		  = 'Required data is missing';
$_['error_missing_asin'] 		  = 'ASIN faltante';
$_['error_marketplace_missing']   = 'Por favor seleccione un mercado';
$_['error_condition_missing'] 	  = 'Por favor seleccione una condición';
$_['error_fetch'] 				  = 'Could not get the data';
$_['error_amazonus_price'] 		  = 'No se pudo obtener el precio de Amazon US';
$_['error_stock'] 				  = 'No puede listar un item con menos de 1 item en stock';
$_['error_sku'] 				  = 'Debe ingresar un SKU para el item';
$_['error_price'] 				  = 'Debe ingresar un precio para el item';
$_['error_connecting'] 			  = 'Advertencia: Hubo un problema al conectar con la API. Por favor verifique su configuración de OpenBay Pro Amazon. Si el problema persiste, por favor contacte a soporte.';
$_['error_required'] 			  = '¡Este campo es necesario!';
$_['error_not_saved'] 			  = 'Listaing was not saved. Check your input.';
$_['error_char_limit'] 			  = 'caracteres over limit.';
$_['error_length'] 				  = 'Longitud mínima es';
$_['error_upload_failed'] 		  = 'Error al subir producto con SKU: "%s". Razón: "%s" Proceso de subida cancelado.';
$_['error_load_nodes'] 			  = 'Error al cargar nodos de exploración';
$_['error_not_searched'] 		  = 'Busque items coincidentes antes de intentar listar. Los artículos deben coincidir con un item del catálogo de Amazon';