<?php
// Heading
$_['heading_title']					= 'Pedidos';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_fba']						= 'Cumplimiento por Amazon';

// Buttons
$_['button_ship']        			= 'Enviar cumplimiento';
$_['button_resend']					= 'Solicitar reenvío de cumplimiento';

// Tab
$_['tab_history']        			= 'Historial de solicitudes';

// Entry
$_['entry_start_date']             	= 'Fecha inicial(formato AAAA-MM-DD)';
$_['entry_end_date']             	= 'Fecha final (formato AAAA-MM-DD)';
$_['entry_status']             		= 'Estado';

// Text
$_['text_no_results'] 				= 'No se encontraron pedidos';
$_['text_fulfillment_list'] 		= 'Lista de cumplimientos por Amazon';
$_['text_option_all'] 				= 'Todas';
$_['text_option_new'] 				= 'Nuevo / pendiente';
$_['text_option_error'] 			= 'Error';
$_['text_option_held'] 				= 'Espera';
$_['text_option_shipped'] 			= 'Enviado';
$_['text_option_cancelled'] 		= 'Cancelado';
$_['text_order'] 					= 'Información de pedido';
$_['text_shipping_address'] 		= 'Dirección de Envío';
$_['text_history'] 					= 'Historial de cumplimientos';
$_['text_opencart_order'] 			= 'ID de Pedido de OpenCart';
$_['text_order_info'] 				= 'Info de pedido';
$_['text_status'] 					= 'Estado de cumplimiento';
$_['text_errors'] 					= 'Errores de respuesta';
$_['text_show_errors'] 				= 'Mostrar errors';
$_['text_no_errors'] 				= 'No hay errores para esta solicitud de cumplimiento';
$_['text_no_sku'] 					= 'No se encontró SKU';
$_['text_show_request'] 			= 'Mostrar cuerpo de solicitd';
$_['text_show_response'] 			= 'Mostrar cuerpo de respuesta';
$_['text_fulfillment_id'] 			= 'ID de cumplimiento';
$_['text_type_new'] 				= 'Crear';
$_['text_type_ship'] 				= 'Envío';
$_['text_type_cancel'] 				= 'Cancelación';
$_['text_no_requests'] 				= '¡No se hicieron solicitudes aún!';
$_['text_order_list']				= 'Listado de pedidos Amazon';


// Columns
$_['column_order_id'] 				= 'ID de Pedido';
$_['column_created'] 				= 'Creado';
$_['column_status'] 				= 'Estado';
$_['column_action'] 				= 'Acción';
$_['column_sku'] 					= 'SKU';
$_['column_product'] 				= 'Producto';
$_['column_quantity'] 				= 'Cantidad';
$_['column_fba'] 					= 'Amazon Cumplimiento';
$_['column_fulfillment_id'] 		= 'Referencia de Solicitud de Cumplimiento';
$_['column_response_code'] 			= 'Código de respuesta';
$_['column_actions'] 				= 'Accións';
$_['column_type'] 					= 'Tipo de solicitud';
$_['column_fba_item_count'] 		= 'Cantidad de items FBA';
