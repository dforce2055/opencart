<?php
// Heading
$_['heading_title']			  = 'Importar pedidos';
$_['text_openbay']			  = 'OpenBay Pro';
$_['text_ebay']				  = 'eBay';

// Buttons
$_['button_pull_orders']      = 'Comenzar';

// Entry
$_['entry_pull_orders']       = 'Traer nuevos pedidos';

// Text
$_['text_sync_pull_notice']   = 'Esto traerá nuevos pedidos desde la última verificación automática. Si acaba de instalar, se utilizarán las últimas 24 horas.';
$_['text_ajax_orders_import'] = 'Cualquier pedido nuevo debería aparecer dentro de unos minutos';
$_['text_complete']           = 'Importación solicitada';
$_['text_failed']             = 'Falló la carga';
$_['text_pull']               = 'Solicitar importación manual';

// Errors
$_['error_validation']        = 'Debe registrarse para recibir su API token y activar el módulo.';