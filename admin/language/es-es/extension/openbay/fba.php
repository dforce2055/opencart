<?php
// Heading
$_['heading_title']         	= 'Cumplimiento por Amazon';
$_['text_openbay']				= 'OpenBay Pro';
$_['text_dashboard']			= 'Cumplimiento por Amazon Panel de Control';

// Text
$_['text_heading_settings'] 	= 'Configuraciones';
$_['text_heading_account'] 		= 'Cuenta/subscripción';
$_['text_heading_fulfillments'] = 'Cumplimientos';
$_['text_heading_register'] 	= 'Regístrese aquí';
$_['text_heading_orders'] 		= 'Pedidos';
