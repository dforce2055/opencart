<?php
// Heading
$_['heading_title'] 				= 'Listado por lotes';
$_['text_openbay'] 					= 'OpenBay Pro';
$_['text_amazon'] 					= 'Amazon EU';

// Text
$_['text_searching'] 				= 'Buscando';
$_['text_finished'] 				= 'Finalizado';
$_['text_marketplace'] 				= 'Tienda';
$_['text_de'] 						= 'Alemania';
$_['text_fr'] 						= 'Francia';
$_['text_es'] 						= 'España';
$_['text_it'] 						= 'Italia';
$_['text_uk'] 						= 'Reino Unido';
$_['text_dont_list'] 				= 'No listar';
$_['text_listing_values'] 			= 'Valores de listado';
$_['text_new'] 						= 'Nuevo';
$_['text_used_like_new'] 			= 'Usado - Como nuevo';
$_['text_used_very_good'] 			= 'Usado - Muy Bueno';
$_['text_used_good'] 				= 'Usado - Bueno';
$_['text_used_acceptable'] 			= 'Usado - Aceptable';
$_['text_collectible_like_new'] 	= 'Artículo de colección - Como Nuevo';
$_['text_collectible_very_good'] 	= 'Artículo de colección - Muy Bueno';
$_['text_collectible_good'] 		= 'Artículo de colección - Bueno';
$_['text_collectible_acceptable'] 	= 'Artículo de colección - Aceptable';
$_['text_refurbished'] 				= 'Renovado';

// Entry
$_['entry_condition'] 				= 'Condición';
$_['entry_condition_note'] 			= 'Nota de Condición';
$_['entry_start_selling'] 			= 'Comenzar a vender';

// Column
$_['column_name'] 					= 'Nombre';
$_['column_image'] 					= 'Imagen';
$_['column_model'] 					= 'Modelo';
$_['column_status'] 				= 'Estado';
$_['column_matches']				= 'Coincidencias';
$_['column_result'] 				= 'Resultado';

// Button
$_['button_list'] 					= 'Lista';

// Error
$_['error_product_sku'] 			= 'El producto debe tener un SKU';
$_['error_searchable_fields'] 		= 'El producto debe tener un campo ISBN, EAN< UP o JAN poblado';
$_['error_bulk_listing_permission'] = 'Listado por lotes no está disponible en su plan, por favor mejórelo para utilizar esta característica.';
$_['error_select_items'] 			= 'Debe seleccionar al menos 1 item para buscar';