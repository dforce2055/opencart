<?php
// Headings
$_['heading_title']      = 'Vínculos Etsy';
$_['text_openbay']       = 'OpenBay Pro';
$_['text_etsy']          = 'Etsy';

// Text
$_['text_loading']       = 'Cargando items';
$_['text_new_link']      = 'Crear nuevo vínculo';
$_['text_current_links'] = 'Vínculos actuales';
$_['text_link_saved']    = 'Item vinculado';
$_['text_no_links']      = 'No tiene items vinculados a items Etsy';

// Columns
$_['column_product']	 = 'Nombre de producto';
$_['column_item_id']	 = 'ID de Etsy';
$_['column_store_stock'] = 'Stock';
$_['column_etsy_stock']	 = 'Stock Etsy';
$_['column_status']		 = 'Estado de vínculo';
$_['column_action']		 = 'Acción';

// Entry
$_['entry_name']		 = 'Nombre de producto';
$_['entry_etsy_id']		 = 'ID de item de Etsy';

// Error
$_['error_product']		 = 'Producto no existe en su tienda';
$_['error_stock']		 = 'No puede vincular un item sin stock';
$_['error_product_id']	 = 'ID de producto necesario';
$_['error_etsy_id']		 = 'ID de item de Etsy necesario';
$_['error_link_id']		 = 'ID de vínculo necesario';
$_['error_link_exists']	 = 'Ya existe un vínculo activo para este item';
$_['error_etsy']		 = 'No se pudo vincular item, Respuesta de API de Etsy: ';
$_['error_status']		 = 'Filtro de estado necesario';