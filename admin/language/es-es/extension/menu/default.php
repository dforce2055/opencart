<?php
// Heading
$_['heading_title']          = 'Menu por defecto';

// Text
$_['text_extension']         = 'Extensiones';
$_['text_success']           = 'Éxito: ¡Has modificado el feed de Google Base!';
$_['text_edit']              = 'Editar Google Base';
$_['text_import']            = 'Para bajar la más reciente lista de categorias de Google <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">haga click aquí</a> y elija taxonomía con IDs numéricas en texto plano (.txt). Suba mediante el botón verde de importar.';

// Column
$_['column_google_category'] = 'Categoría Google';
$_['column_category']        = 'Categoría';
$_['column_action']          = 'Acción';

// Entry
$_['entry_google_category']  = 'Categoría Google';
$_['entry_category']         = 'Categoría';
$_['entry_data_feed']        = 'Url de Feed de datos';
$_['entry_status']           = 'Estado';

// Error
$_['error_permission']       = 'Advertencia: ¡No tiene permisos para modificar feed de Google Base!';
$_['error_upload']           = '¡Archivo no se puede subir!';
$_['error_filetype']         = '¡Tipo de archivo inválido!';