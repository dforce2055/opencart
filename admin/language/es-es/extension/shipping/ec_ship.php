<?php
// Heading
$_['heading_title']                            = 'EC-Ship';

// Text
$_['text_extension']                           = 'Extensión';
$_['text_success']                             = 'Éxito: Has modificado envío EC-Ship!';
$_['text_edit']                                = 'Editar envío EC-Ship';
$_['text_air_registered_mail']                 = 'Air Registered Mail';
$_['text_air_parcel']                          = 'Air Parcel(Peso máximo de paquete 20KG)';
$_['text_e_express_service_to_us']             = 'e-Express Service to US(Peso máximo de paquete 2KG)';
$_['text_e_express_service_to_canada']         = 'e-Express Service to Canada(Peso máximo de paquete 2KG)';
$_['text_e_express_service_to_united_kingdom'] = 'e-Express Service to Reino Unido(Peso máximo de paquete 2KG)';
$_['text_e_express_service_to_russia']         = 'e-Express Service to Russia(Peso máximo de paquete 2KG)';
$_['text_e_express_service_one']               = 'e-Express service (incluyendo Alemania, France y Noruega)(Peso máximo de paquete 2KG)';
$_['text_e_express_service_two']               = 'e-Express service (incluyendo Australia, New Zealand, Korea, Singapore y Vietnam)(Peso máximo de paquete 2KG)';
$_['text_speed_post']                          = 'SpeedPost (Standard Service)(Peso máximo de paquete 30KG)';
$_['text_smart_post']                          = 'Smart Post(Peso máximo de paquete 2KG)';
$_['text_local_courier_post']                  = 'Local Servicio de Mensajería Post (Counter Collection)(Peso máximo de paquete 2KG)';
$_['text_local_parcel']                        = 'Local Parcel (Peso máximo de paquete 20KG)';

// Entry
$_['entry_api_key']                            = 'Clave de API';
$_['entry_username']                           = 'Nombre de Usuario Integrator';
$_['entry_api_username']                       = 'Nombre de Usuario de API';
$_['entry_test']                               = 'Modo de prueba';
$_['entry_service']                            = 'Servicios';
$_['entry_weight_class']                       = 'Clase de peso';
$_['entry_tax_class']                          = 'Clase de Impuesto';
$_['entry_geo_zone']                           = 'Geo Zona';
$_['entry_status']                             = 'Estado';
$_['entry_sort_order']                         = 'Orden de Clasificación';

// Ayuda
$_['help_api_key']                             = 'Ingrese la Clave de API que le fue asignada por EC-SHIP.';
$_['help_username']                            = 'Ingrese su nombre de usuario para Integrator de EC-SHIP.';
$_['help_api_username']                        = 'Ingrese su nombre de usuario para la API de EC-SHIP.';
$_['help_test']                                = '¿Usar este módulo en modo de prueba (SI) o modo de producción (NO)?';
$_['help_service']                             = 'Seleccione los servicios EC-SHIP a ofrecer.';
$_['help_weight_class']                        = 'Fijar a sólo kilogramos..';

// Error
$_['error_permission']                         = 'Advertencia: ¡No tiene permisos para modificar envío EC-SHIP!';
$_['error_api_key']                            = '¡Clave de API necesaria!';
$_['error_username']                           = '¡Nombre de Usuario necesario!';
$_['error_api_username']                       = '¡Nombre de Usuario de API necesario!';
