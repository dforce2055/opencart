<?php
// Heading
$_['heading_title']    = 'Envío gratuito';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado envío gratuito!';
$_['text_edit']        = 'Editar envío gratuito';

// Entry
$_['entry_total']      = 'Total';
$_['entry_geo_zone']   = 'Geo Zona';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Ayuda
$_['help_total']       = 'Sub-Total necesario antes de que el módulo de envío gratuito esté disponible.';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar envío gratuito!';