<?php
// Heading
$_['heading_title']    = 'Tasa fija';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado envío con tasa fija!';
$_['text_edit']        = 'Editar Envío con tasa fija';

// Entry
$_['entry_cost']       = 'Costo';
$_['entry_tax_class']  = 'Clase de Impuesto';
$_['entry_geo_zone']   = 'Geo Zona';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar Envío con tasa fija!';