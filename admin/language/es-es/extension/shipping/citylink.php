<?php
// Heading
$_['heading_title']    = 'Citylink';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado envío Citylink!';
$_['text_edit']        = 'Editar envío Citylink';

// Entry
$_['entry_rate']       = 'Tasas Citylink';
$_['entry_tax_class']  = 'Clase de Impuesto';
$_['entry_geo_zone']   = 'Geo Zona';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Ayuda
$_['help_rate']        = 'Ingrese valores hasta 5, 2 posiciones decimales. (12345.67) Por ejemplo: .1:1,.25:1.27 - Pesos menores o iguales a 0.1Kg costarían &pound;1.00. Pesos menores o iguales a 0.25g pero mayores a 0.1Kg costarían 1.27. No ingrese KG ni símbolos.';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar envío Citylink!';