<?php
// Heading
$_['heading_title']                    = 'Royal Mail';

// Text
$_['text_extension']                   = 'Extensiones';
$_['text_success']                     = 'Éxito: ¡Has modificado envío Royal Mail!';
$_['text_edit']                        = 'Editar envío Royal Mail';

// Entry
$_['entry_rate']                       = 'Tarifas';
$_['entry_rate_eu']                    = 'Tarifas Europa';
$_['entry_rate_non_eu']                = 'Tarifas No-Europa ';
$_['entry_rate_zone_1']                = 'Tarifas Zona 1 del Mundo';
$_['entry_rate_zone_2']                = 'Tarifas Zona 2 del Mundo';
$_['entry_insurance']                  = 'Tarifas de compensación';
$_['entry_display_weight']             = 'Mostrar Peso de Entrega';
$_['entry_display_insurance']          = 'Mostrar Seguro';
$_['entry_weight_class']               = 'Clase de peso';
$_['entry_tax_class']                  = 'Clase de Impuesto';
$_['entry_geo_zone']                   = 'Geo Zona';
$_['entry_status']                     = 'Estado';
$_['entry_sort_order']                 = 'Orden de Clasificación';

// Ayuda
$_['help_rate']                        = 'Ejemplo: 5:10.00,7:12.00 Peso:Costo,Peso:Costo, etc..';
$_['help_insurance']                   = 'Ingrese valores hasta 5, 2 posiciones decimales. (12345.67) ejemplo: 34:0,100:1,250:2.25 - Cobertura de seguro paravalores de carro hasta 34 costarían 0.00 adicionales, mayores que 100 y hasta 250 costarían 2.25 adicionales. No ingrese símbolos de moneda.';
$_['help_display_weight']              = '¿Desea mostrar el peso de entrega? (por ejemplo peso de entrega: 2.7674 kg)';
$_['help_display_insurance']           = '¿Desea mostrar el seguro?(por ejemplo asegurado hasta &pound;500))';
$_['help_international']               = '<p>Guía de precios y servicios aquí:</p><p><a href="http://www.royalmail.com/international-zones" target="_blank">http://www.royalmail.com/international-zones</a></p><p><a href="http://www.royalmail.com/sites/default/files/RM_OurPrecios_Mar2014a.pdf" target="_blank">http://www.royalmail.com/sites/default/files/RM_OurPrices_Mar2014a.pdf</a></p><p><a href="http://www.royalmail.com/sites/default/files/RoyalMail_International_TrackedCoverage_Jan2014.pdf" target="_blank">http://www.royalmail.com/sites/default/files/RoyalMail_International_TrackedCoverage_Jan2014.pdf</a></p>';

// Tab
$_['tab_special_delivery_500']         = 'Special Delivery Next Day (&pound;500)';
$_['tab_special_delivery_1000']        = 'Special Delivery Next Day (&pound;1000)';
$_['tab_special_delivery_2500']        = 'Special Delivery Next Day (&pound;2500)';
$_['tab_1st_class_signed']             = '1st Class Signed';
$_['tab_2nd_class_signed']             = '2nd Class Signed';
$_['tab_1st_class_standard']           = '1st Class Standard';
$_['tab_2nd_class_standard']           = '2nd Class Standard';
$_['tab_international_standard']       = 'International Standard';
$_['tab_international_tracked_signed'] = 'International Tracked & Signed';
$_['tab_international_tracked']        = 'International Tracked';
$_['tab_international_signed']         = 'International Signed';
$_['tab_international_economy']        = 'International Economy';

// Error
$_['error_permission']                 = 'Advertencia: ¡No tiene permisos para modificar envíos Royal Mail!';