<?php
// Heading
$_['heading_title']    = 'Reecoger en Tienda';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado recoger en tienda!';
$_['text_edit']        = 'Editar envío Recoger en tienda';

// Entry
$_['entry_geo_zone']   = 'Geo Zona';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar Recoger en tienda!';