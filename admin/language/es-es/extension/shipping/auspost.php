<?php
// Heading
$_['heading_title']      = 'Australia Post';

// Text
$_['text_extension']     = 'Extensiones';
$_['text_success']       = 'Éxito: ¡Has modificado envío Australia Post!';
$_['text_edit']          = 'Editar envío Australia Post';

// Entry
$_['entry_api']          = 'Clave de API';
$_['entry_postcode']     = 'Código Postal';
$_['entry_weight_class'] = 'Clase de peso';
$_['entry_tax_class']    = 'Clase de Impuesto';
$_['entry_geo_zone']     = 'Geo Zona';
$_['entry_status']       = 'Estado';
$_['entry_sort_order']   = 'Orden de Clasificación';

// Ayuda
$_['help_weight_class']  = 'Fijar a kilogramos.';

// Error
$_['error_permission']   = 'Advertencia: ¡No tiene permisos para modificar envío Australia Post!';
$_['error_postcode']     = '¡El código postal debe tener 4 dígitos!';