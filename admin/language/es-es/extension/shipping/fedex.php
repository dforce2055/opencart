<?php
// Heading
$_['heading_title']                            = 'Fedex';

// Text
$_['text_shipping']                            = 'Envío';
$_['text_success']                             = 'Éxito: Has modificado envío Fedex!';
$_['text_edit']                                = 'Editar Fedex Envío';
$_['text_europe_first_international_priority'] = 'Europe First International Priority';
$_['text_fedex_1_day_freight']                 = 'Fedex Flete 1 Día';
$_['text_fedex_2_day']                         = 'Fedex Flete 2 Día';
$_['text_fedex_2_day_am']                      = 'Fedex 2 Día AM';
$_['text_fedex_2_day_freight']                 = 'Fedex Flete 2 Día Flete';
$_['text_fedex_3_day_freight']                 = 'Fedex Flete 3 Día Flete';
$_['text_fedex_express_saver']                 = 'Fedex Express Saver';
$_['text_fedex_first_freight']                 = 'Fedex Primer Flete';
$_['text_fedex_freight_economy']               = 'Fedex Flete Económico';
$_['text_fedex_freight_priority']              = 'Fedex Flete Prioritario';
$_['text_fedex_ground']                        = 'Fedex Tierra';
$_['text_first_overnight']                     = 'First Overnight';
$_['text_ground_home_delivery']                = 'Tierra A Domicilio';
$_['text_international_economy']               = 'International Economy';
$_['text_international_economy_freight']       = 'International Flete Económico';
$_['text_international_first']                 = 'International First';
$_['text_international_priority']              = 'International Prioritario';
$_['text_international_priority_freight']      = 'International Flete Prioritario';
$_['text_priority_overnight']                  = 'Priority Overnight';
$_['text_smart_post']                          = 'Smart Post';
$_['text_standard_overnight']                  = 'Standard Overnight';
$_['text_regular_pickup']                      = 'Regular Pickup';
$_['text_request_courier']                     = 'Request Servicio de Mensajería';
$_['text_drop_box']                            = 'Drop Box';
$_['text_business_service_center']             = 'Business Service Center';
$_['text_station']                             = 'Station';
$_['text_fedex_envelope']                      = 'FedEx Envelope';
$_['text_fedex_pak']                           = 'FedEx Pak';
$_['text_fedex_box']                           = 'FedEx Box';
$_['text_fedex_tube']                          = 'FedEx Tube';
$_['text_fedex_10kg_box']                      = 'FedEx 10kg Box';
$_['text_fedex_25kg_box']                      = 'FedEx 25kg Box';
$_['text_your_packaging']                      = 'Su embalaje';
$_['text_list_rate']                           = 'Tarifa de Lista';
$_['text_account_rate']                        = 'Tarifa de Cuenta';

// Entry
$_['entry_key']                                = 'Clave';
$_['entry_password']                           = 'Contraseña';
$_['entry_account']                            = 'Número de cuenta';
$_['entry_meter']                              = 'Número de metro';
$_['entry_postcode']                           = 'Código Postal';
$_['entry_test']                               = 'Modo de Prueba';
$_['entry_service']                            = 'Servicios';
$_['entry_dimension']                          = 'Dimensiones de caja (Largo x Ancho x Alto)';
$_['entry_length_class']                       = 'Clase de largo';
$_['entry_length']                             = 'Largo';
$_['entry_width']                              = 'Ancho';
$_['entry_height']                             = 'Alto';
$_['entry_dropoff_type']                       = 'Tipo de entrega';
$_['entry_packaging_type']                     = 'Tipo de embalaje';
$_['entry_rate_type']                          = 'Tipo de tarifa';
$_['entry_display_time']                       = 'Mostrar tiempo de entrega';
$_['entry_display_weight']                     = 'Mostrar peso de entrega';
$_['entry_weight_class']                       = 'Clase de peso';
$_['entry_tax_class']                          = 'Clase de Impuesto';
$_['entry_geo_zone']                           = 'Geo Zona';
$_['entry_status']                             = 'Estado';
$_['entry_sort_order']                         = 'Orden de Clasificación';

// Ayuda
$_['help_length_class']                        = 'Fijar a pulgadas o centímetros.';
$_['help_display_time']                        = '¿Desea mostrar el tiempo de entrega? (por ejemplo entrega en 3 a 5 días)';
$_['help_display_weight']                      = '¿Desea mostrar el peso de entrega? (por ejemplo peso de entrega: 2.7674 kg)';
$_['help_weight_class']                        = 'Fijar a kilogramos o libras.';

// Error
$_['error_permission']                         = 'Advertencia: ¡No tiene permisos para modificar envío Fedex!';
$_['error_key']                                = '¡Clave necesaria!';
$_['error_password']                           = '¡Contraseña necesaria!';
$_['error_account']                            = '¡Cuenta necesaria!';
$_['error_meter']                              = '¡Metro necesario!';
$_['error_postcode']                           = '¡Código Postal necesario!';
$_['error_dimension']                          = '¡Ancho &amp; Alto necesario!';