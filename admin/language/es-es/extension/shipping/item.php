<?php
// Heading
$_['heading_title']    = 'Por Item';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado tasas de envío por item!';
$_['text_edit']        = 'Editar envío por item';

// Entry
$_['entry_cost']       = 'Costo';
$_['entry_tax_class']  = 'Clase de Impuesto';
$_['entry_geo_zone']   = 'Geo Zona';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar tasas de envío por item!';