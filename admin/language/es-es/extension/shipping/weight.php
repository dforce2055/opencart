<?php
// Heading
$_['heading_title']    = 'Envío basado en Peso';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado envío basado en peso!';
$_['text_edit']        = 'Editar Envío basado en Peso';

// Entry
$_['entry_rate']       = 'Tarifas';
$_['entry_tax_class']  = 'Clase de Impuesto';
$_['entry_geo_zone']   = 'Geo Zona';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Ayuda
$_['help_rate']        = 'Ejemplo: 5:10.00,7:12.00 Peso:Costo,Peso:Costo, etc..';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar envío basado en peso!';