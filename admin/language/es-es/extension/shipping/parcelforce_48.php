<?php
// Heading
$_['heading_title']           = 'Parcelforce 48';

// Text
$_['text_extension']          = 'Extensiones';
$_['text_success']            = 'Éxito: ¡Has modificado envío Parcelforce 48!';
$_['text_edit']               = 'Editar envío Parcelforce 48';

// Entry
$_['entry_rate']              = 'Tarifas de Parcelforce 48';
$_['entry_insurance']         = 'Tarifas de compensación de Parcelforce48';
$_['entry_display_weight']    = 'Mostrar peso de envío';
$_['entry_display_insurance'] = 'Mostrar seguro';
$_['entry_display_time']      = 'Mostrar tiempo de envío';
$_['entry_tax_class']         = 'Clase de Impuesto';
$_['entry_geo_zone']          = 'Geo Zona';
$_['entry_status']            = 'Estado';
$_['entry_sort_order']        = 'Orden de Clasificación';

// Ayuda
$_['help_rate']               = 'Ingrese valores hasta 5, 2 posiciones decimales. (12345.67) Por ejemplo: .1:1,.25:1.27 - Pesos menores o iguales a 0.1Kg costarían &pound;1.00. Pesos menores o iguales a 0.25g pero mayores a 0.1Kg costarían 1.27. No ingrese KG ni símbolos.';
$_['help_insurance']          = 'Ingrese valores hasta 5, 2 posiciones decimales. (12345.67) ejemplo: 34:0,100:1,250:2.25 - Cobertura de seguro paravalores de carro hasta 34 costarían 0.00 adicionales, mayores que 100 y hasta 250 costarían 2.25 adicionales. No ingrese símbolos de moneda.';
$_['help_display_weight']     = '¿Desea mostrar el peso de entrega? (por ejemplo peso de entrega: 2.7674 kg)';
$_['help_display_insurance']  = '¿Desea mostrar el seguro?(por ejemplo asegurado hasta &pound;500)';
$_['help_display_time']       = '¿Desea mostrar el tiempo de entrega? (por ejemplo entrega en 3 a 5 días)';

// Error
$_['error_permission']        = 'Advertencia: ¡No tiene permisos para modificar envío Parcelforce 48!';