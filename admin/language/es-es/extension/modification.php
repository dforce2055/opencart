<?php
// Heading
$_['heading_title']     = 'Modificaciónes';

// Text
$_['text_success']      = 'Se ha modificado una Modificación.';
$_['text_refresh']      = 'Siempre que se Habilite/Deshabilite o se elimine una modificación hacer click en el Botón de Actualización para reconstruir la caché.';
$_['text_list']         = 'Lista de Modificaciones';

// Column
$_['column_name']       = 'Nombre de la Modificación';
$_['column_author']     = 'Autor';
$_['column_version']    = 'Versión';
$_['column_status']     = 'Estado';
$_['column_date_added'] = 'Fecha';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Sin permiso para modificar las modificaciones.';