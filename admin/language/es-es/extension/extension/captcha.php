<?php
// Heading
$_['heading_title']    = 'Captchas';

// Text
$_['text_success']     = 'Éxito: ¡Has modificado captchas!';
$_['text_list']        = 'Lista de Captchas';

// Column
$_['column_name']      = 'Nombre de Captcha';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar captchas!';