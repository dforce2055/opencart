<?php
// Heading
$_['heading_title']    = 'Anti-Fraude';

// Text
$_['text_success']     = 'Éxito: ¡Has modificado anti-fraude!';
$_['text_list']        = 'Lista Anti-Fraude';

// Column
$_['column_name']      = 'Nombre de Anti-Fraude';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar anti-fraude!';