<?php
// Heading
$_['heading_title']    = 'Feeds';

// Text
$_['text_success']     = 'Éxito: Has modificado feeds!';
$_['text_list']        = 'Lista de feeds';

// Column
$_['column_name']      = 'Nombre de feed de producto';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar feeds!';