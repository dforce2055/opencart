<?php
// Heading
$_['heading_title']     = 'Total de Pedidos';

// Text
$_['text_success']      = 'Éxito: Has modificado totales!';
$_['text_list']         = 'Lista de Totales de Pedidos';

// Column
$_['column_name']       = 'Total de Pedidos';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Orden de Clasificación';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar totales!';