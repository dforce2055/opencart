<?php
// Heading
$_['heading_title']    = 'Menú';

// Text
$_['text_success']     = 'Éxito: ¡Has modificado menu!';
$_['text_list']        = 'Listade menúes';

// Column
$_['column_name']      = 'Nombre de menú';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar menú!';