<?php
// Heading
$_['heading_title']     = 'Envío';

// Text
$_['text_success']      = 'Éxito: ¡Has modificado envío!';
$_['text_list']         = 'Lista de envíos';

// Column
$_['column_name']       = 'Método de Envío';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Orden de Clasificación';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar envíos!';