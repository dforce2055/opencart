<?php
// Heading
$_['heading_title']     = 'Pagos';

// Text
$_['text_success']      = 'Éxito: Has modificado pagos!';
$_['text_list']         = 'Lista de pagos';
$_['text_recommended']  = 'Pagos - Soluciones recomendadas';


// Column
$_['column_name']       = 'Método de pago';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Orden de Clasificación';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar pagos!';