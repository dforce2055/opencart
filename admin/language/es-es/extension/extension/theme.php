<?php
// Heading
$_['heading_title']    = 'Temas';

// Text
$_['text_success']     = 'Éxito: ¡Has modificado themes!';
$_['text_list']        = 'Lista de temas';

// Column
$_['column_name']      = 'Nombre de tema';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar temas!';