<?php
// Heading
$_['heading_title']    = 'Analíticas';

// Text
$_['text_success']     = 'Éxito: ¡Has modificado analíticas!';
$_['text_list']        = 'Lista de analíticas';

// Column
$_['column_name']      = 'Nombre de analítica';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar analíticas!';