<?php
// Heading
$_['heading_title']     = 'Reportes';

// Text
$_['text_success']      = 'Éxito: ¡Has modificado reportes!';
$_['text_list']         = 'Lista de Reportes';

// Column
$_['column_name']       = 'Nombre de Reporte';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Orden de Clasificación';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar reports!';