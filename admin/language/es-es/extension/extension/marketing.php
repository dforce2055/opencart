<?php
// Heading
$_['heading_title']    = 'Marketing';

// Text
$_['text_success']     = 'Éxito: ¡Has modificado marketing!';
$_['text_list']        = 'Lista de analítica';

// Column
$_['column_name']      = 'Nombre de Marketing';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar marketing!';