<?php
// Heading
$_['heading_title']    = 'Módulos';

// Text
$_['text_success']     = 'Éxito: Has modificado modulos!';
$_['text_layout']      = 'Una vez instalado y configurado el módulo puede agregaro al diseño <a href="%s" class="alert-link">aquí</a>!';
$_['text_add']         = 'Agregar Módulo';
$_['text_list']        = 'Lista de módulos';

// Column
$_['column_name']      = 'Nombre de módulo';
$_['column_status']    = 'Estado';
$_['column_action']    = 'Acción';

// Entry
$_['entry_code']       = 'Módulo';
$_['entry_name']       = 'Nombre de módulo';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar módulos!';
$_['error_name']       = '¡El nombre de módulo debe tener entre 3 y 64 caracteres!';
$_['error_code']       = '¡Extensión necesaria!';