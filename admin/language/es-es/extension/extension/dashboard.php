<?php
// Heading
$_['heading_title']     = 'Panel de Control';

// Text
$_['text_success']      = 'Éxito: ¡Has modificado panel de control!';
$_['text_list']         = 'Lista de Paneles de Control';

// Column
$_['column_name']       = 'Nombre de Panel de Control';
$_['column_width']      = 'Ancho';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Orden de Clasificación';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar paneles de control!';