<?php
// Heading
$_['heading_title']                    = 'Tema por defecto de la Tienda';

// Text
$_['text_extension']                   = 'Extensiones';
$_['text_success']                     = 'Éxito: ¡Has modificado el Tema por defecto de la Tienda!';
$_['text_edit']                        = 'Editar Tema por defecto de la Tienda';
$_['text_general']                     = 'General';
$_['text_product']                     = 'Productos';
$_['text_image']                       = 'Imagenes';

// Entry
$_['entry_directory']                  = 'Directorio del Tema';
$_['entry_status']                     = 'Estado';
$_['entry_product_limit']              = 'Artículos Por Pagina por defecto';
$_['entry_product_description_length'] = 'Límite Lista Descripción';
$_['entry_image_category']             = 'Tamaño de Imágen de categoría (Ancho x Alto)';
$_['entry_image_thumb']                = 'Tamaño de Imágen Producto Miniatura (Ancho x Alto)';
$_['entry_image_popup']                = 'Tamaño de Imágen Producto Ventana Emergente (Ancho x Alto)';
$_['entry_image_product']              = 'Tamaño de Imágen Producto Lista (Ancho x Alto)';
$_['entry_image_additional']           = 'Tamaño de Imágen Producto Adicional (Ancho x Alto)';
$_['entry_image_related']              = 'Tamaño de Imágen Producto Relacionado (Ancho x Alto)';
$_['entry_image_compare']              = 'Tamaño de Imágen Comparación de Productos (Ancho x Alto)';
$_['entry_image_wishlist']             = 'Tamaño de Imágen Favoritos (Ancho x Alto)';
$_['entry_image_cart']                 = 'Tamaño de Imágen Carro de compras (Ancho x Alto)';
$_['entry_image_location']             = 'Tamaño de Imágen Tienda (Ancho x Alto)';
$_['entry_width']                      = 'Ancho';
$_['entry_height']                     = 'Alto';

// Ayuda
$_['help_directory']                   = 'Este campo es solo para permitir que los temas anteriores sean compatibles con el nuevo sistema de temas. Puede configurar el directorio de temas para usar en las configuraciones de tamaño de imagen definidas aquí.';
$_['help_product_limit']               = 'Determina cuántos artículos de catálogo se muestran por página (productos, categorías, etc.)';
$_['help_product_description_length']  = 'En la vista de lista, el límite de caracteres de descripción breve (categorías, especial, etc.)';

// Error
$_['error_permission']                 = 'Advertencia: No tiene permisos para modificar Tema por defecto de la Tienda!';
$_['error_limit']                      = '¡Límite de Productos necesario!';
$_['error_image_thumb']                = '¡Tamaño de Imágen Producto Miniatura necesario!';
$_['error_image_popup']                = '¡Tamaño de Imágen Producto Ventana Emergente necesario!';
$_['error_image_product']              = '¡Tamaño de Imágen Producto Lista necesario!';
$_['error_image_category']             = '¡Tamaño de Imágen de categoría necesario!';
$_['error_image_additional']           = '¡Tamaño de Imágen Producto Adicional necesario!';
$_['error_image_related']              = '¡Tamaño de Imágen Producto Relacionado necesario!';
$_['error_image_compare']              = '¡Tamaño de Imágen Comparación de Productos necesario!';
$_['error_image_wishlist']             = '¡Tamaño de Imágen Favoritos necesario!';
$_['error_image_cart']                 = '¡amaño de Imágen Carro de compras (Ancho x Alto)!';
$_['error_image_location']             = '¡Tamaño de Imágen Tienda necesario!';
