<?php
// Heading
$_['heading_title']     = 'Ultimos Pedidos';

// Text
$_['text_extension']    = 'Extensiones';
$_['text_success']      = 'Éxito: Has modificado Pedidos Recientes de panel de control!';
$_['text_edit']         = 'Editar Pedidos Recientes de Panel de Control';

// Column
$_['column_order_id']   = 'ID de Pedido';
$_['column_customer']   = 'Cliente';
$_['column_status']     = 'Estado';
$_['column_total']      = 'Total';
$_['column_date_added'] = 'Fecha añadido';
$_['column_action']     = 'Acción';


// Entry
$_['entry_status']      = 'Estado';
$_['entry_sort_order']  = 'Orden de Clasificación';
$_['entry_width']       = 'Ancho';

// Error
$_['error_permission']  = 'Advertencia: No tiene permisos para modificar Pedidos Recientes de panel de control!';