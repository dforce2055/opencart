<?php
// Heading
$_['heading_title']    = 'Analítica de ventas';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado la gráfica del panel de control!';
$_['text_edit']        = 'Editar gráfica del panel de control';
$_['text_order']       = 'Pedidos';
$_['text_customer']    = 'Clientes';
$_['text_day']         = 'Hoy';
$_['text_week']        = 'Semana';
$_['text_month']       = 'Mes';
$_['text_year']        = 'Año';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar la gráfica del panel de control!';