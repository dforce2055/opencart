<?php
// Heading
$_['heading_title']    = 'Clientes Totales';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado clientes de panel de control!';
$_['text_edit']        = 'Editar Clientes de Panel de Control';
$_['text_view']        = 'Ver más...';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar clientes de panel de control!';