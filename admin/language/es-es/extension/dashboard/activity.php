<?php
// Heading
$_['heading_title']                = 'Actividad';

// Text
$_['text_extension']               = 'Extensiones';
$_['text_success']                 = 'Éxito: ¡Has modificado actividad del panel de control!';
$_['text_edit']                    = 'Editar actividad reciente del panel de control';
$_['text_activity_register']       = '<a href="customer_id=%d">%s</a> registró una nueva cuenta.';
$_['text_activity_edit']           = '<a href="customer_id=%d">%s</a> actualizó sus detalles de cuenta.';
$_['text_activity_password']       = '<a href="customer_id=%d">%s</a> actualizó su contraseña.';
$_['text_activity_reset']          = '<a href="customer_id=%d">%s</a> reinició su contraseña.';
$_['text_activity_login']          = '<a href="customer_id=%d">%s</a> inició sesión.';
$_['text_activity_forgotten']      = '<a href="customer_id=%d">%s</a> ha solicitado una nueva contraseña.';
$_['text_activity_address_add']    = '<a href="customer_id=%d">%s</a> agregó una nueva dirección.';
$_['text_activity_address_edit']   = '<a href="customer_id=%d">%s</a> actualizó su dirección.';
$_['text_activity_address_delete'] = '<a href="customer_id=%d">%s</a> eliminó una de sus direcciones.';
$_['text_activity_return_account'] = '<a href="customer_id=%d">%s</a> envió una <a href="return_id=%d">devolución</a> de producto.';
$_['text_activity_return_guest']   = '%s envió una <a href="return_id=%d">devolución</a> de producto.';
$_['text_activity_order_account']  = '<a href="customer_id=%d">%s</a> agregó un <a href="order_id=%d">nuevo pedido</a>.';
$_['text_activity_order_guest']    = '%s creó un <a href="order_id=%d">nuevo pedido</a>.';
$_['text_activity_affiliate_add']  = '<a href="customer_id=%d">%s</a> se registró para una cuenta de afiliado.';
$_['text_activity_affiliate_edit'] = '<a href="customer_id=%d">%s</a> actualizó sus detalles de afiliado.';
$_['text_activity_transaction']    = '<a href="customer_id=%d">%s</a> recibió una comisión por un <a href="order_id=%d">pedido</a>.';

// Entry
$_['entry_status']                 = 'Estado';
$_['entry_sort_order']             = 'Orden de Clasificación';
$_['entry_width']                  = 'Ancho';

// Error
$_['error_permission']             = 'Advertencia: ¡No tiene permisos para modificar actividad del panel del control!';