<?php
// Heading
$_['heading_title']    = 'Total Pedidos';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado Pedidos de panel de control!';
$_['text_edit']        = 'Editar Pedidos de panel de control';
$_['text_view']        = 'Ver más...';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar Pedidos de panel de control!';