<?php
// Heading
$_['heading_title']    = 'Mapa del Mundo';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado mapa del panel de control!';
$_['text_edit']        = 'Editar Mapa de Panel de Control';
$_['text_order']       = 'Pedidos';
$_['text_sale']        = 'Ventas';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar mapa del panel de control!';