<?php
// Heading
$_['heading_title']    = 'Gente Online';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado Gente Online de panel de control!';
$_['text_edit']        = 'Editar Gente Online de Panel de Control';
$_['text_view']        = 'Ver más...';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';
$_['entry_width']      = 'Ancho';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar Gente Online de panel de control!';