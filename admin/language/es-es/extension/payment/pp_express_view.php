<?php
// Heading
$_['heading_title']			  = 'Ver Transacción';

// Text
$_['text_pp_express']		  = 'Proceso de compra con PayPal Express';
$_['text_product_lines']	  = 'Líneas de producto';
$_['text_ebay_txn_id']		  = 'ID de transacción de eBay';
$_['text_name']				  = 'Nombre';
$_['text_qty']				  = 'Cantidad';
$_['text_price']			  = 'Precio';
$_['text_number']			  = 'Number';
$_['text_coupon_id']		  = 'ID de cupón';
$_['text_coupon_amount']	  = 'Monto de cupón';
$_['text_coupon_currency']	  = 'Moneda de cupón';
$_['text_loyalty_disc_amt']	  = 'Monto de descuento de tarjeta de lealtad';
$_['text_loyalty_currency']	  = 'Moneda de tarjeta de lealtad';
$_['text_options_name']		  = 'Nombre de opciones';
$_['text_tax_amt']			  = 'Monto de impuestos';
$_['text_currency_code']	  = 'Código de moneda';
$_['text_amount']			  = 'Monto';
$_['text_gift_msg']			  = 'Mensaje de regalo';
$_['text_gift_receipt']		  = 'Recibo de regalo';
$_['text_gift_wrap_name']	  = 'Nombre de envoltura de regalo';
$_['text_gift_wrap_amt']	  = 'Monto de envoltura de regalo';
$_['text_buyer_email_market'] = 'Email de marketing de comprador';
$_['text_survey_question']	  = 'Pregunta de encuesta';
$_['text_survey_chosen']	  = 'Elección de encuesta';
$_['text_receiver_business']  = 'Negocio destinatario';
$_['text_receiver_email']	  = 'Email destinatario';
$_['text_receiver_id']		  = 'ID destinatario';
$_['text_buyer_email']		  = 'Email de comprador';
$_['text_payer_id']			  = 'ID de pagador';
$_['text_payer_status']		  = 'Estado de pagador';
$_['text_country_code']		  = 'Código de país';
$_['text_payer_business']	  = 'Negocio de pagador';
$_['text_payer_salute']		  = 'Saludo de pagador';
$_['text_payer_firstname']	  = 'Nombre de pagador';
$_['text_payer_middlename']   = 'Segundo nombre de pagador';
$_['text_payer_lastname']	  = 'Apellido de pagador';
$_['text_payer_suffix']		  = 'Sufijo de pagador';
$_['text_address_owner']	  = 'Dueño de dirección';
$_['text_address_status']	  = 'Estado de dirección';
$_['text_ship_sec_name']	  = 'Enviar a nombre secundario';
$_['text_ship_name']		  = 'Enviar a nombre';
$_['text_ship_street1']		  = 'Enviar a dirección 1';
$_['text_ship_street2']		  = 'Enviar a dirección 2';
$_['text_ship_city']		  = 'Enviar a ciudad';
$_['text_ship_state']		  = 'Enviar a estado';
$_['text_ship_zip']			  = 'Enviar a ZIP';
$_['text_ship_country']		  = 'Enviar a código de país';
$_['text_ship_phone']		  = 'Enviar a teléfono number';
$_['text_ship_sec_add1']	  = 'Enviar a secondary address 1';
$_['text_ship_sec_add2']	  = 'Enviar a secondary address 2';
$_['text_ship_sec_city']	  = 'Enviar a secondary city';
$_['text_ship_sec_state']	  = 'Enviar a secondary state';
$_['text_ship_sec_zip']		  = 'Enviar a secondary ZIP';
$_['text_ship_sec_country']	  = 'Enviar a secondary código de país';
$_['text_ship_sec_phone']	  = 'Enviar a secondary teléfono';
$_['text_trans_id']			  = 'ID de transacción';
$_['text_receipt_id']		  = 'ID de recibo';
$_['text_parent_trans_id']	  = 'ID de transacción madre';
$_['text_trans_type']		  = 'Tipo de transacción';
$_['text_payment_type']		  = 'Tipo de pago';
$_['text_order_time']		  = 'Tiempo de pedido';
$_['text_fee_amount']		  = 'Monto de gasto';
$_['text_settle_amount']	  = 'Monto de establecimiento';
$_['text_tax_amount']		  = 'Monto de impuestos';
$_['text_exchange']			  = 'Tipo de cambio';
$_['text_payment_status']	  = 'Estado de pago';
$_['text_pending_reason']	  = 'Pendiente por motivo';
$_['text_reason_code']		  = 'Código de motivo';
$_['text_protect_elig']		  = 'Eligibilidad de protección';
$_['text_protect_elig_type']  = 'Tipo de Eligibilidad de protección';
$_['text_store_id']			  = 'ID de tienda';
$_['text_terminal_id']		  = 'ID de terminal';
$_['text_invoice_number']	  = 'Número de Factura';
$_['text_custom']			  = 'Personalizado';
$_['text_note']				  = 'Nota';
$_['text_sales_tax']		  = 'Impuesto de ventas';
$_['text_buyer_id']			  = 'ID de comprador';
$_['text_close_date']		  = 'Fecha de cierre';
$_['text_multi_item']		  = 'Multi item';
$_['text_sub_amt']			  = 'Monto de subscripción';
$_['text_sub_period']		  = 'Período de subscripción';