<?php
// Heading
$_['heading_title']         = 'Pilibaba for Chinese Checkout';

// Tab
$_['tab_register']          = 'Registrar';
$_['tab_settings']          = 'Configuraciones';

// Text
$_['text_extension']        = 'Extensiones';
$_['text_success']          = 'Éxito: ¡¡Ha modificado el módulo de pago Pilibaba!';
$_['text_edit']             = 'Editar Pilibaba';
$_['text_pilibaba']         = '<a href="http://www.pilibaba.com" target="_blank"><img src="view/image/payment/pilibaba.png" alt="Pilibaba" title="Pilibaba"></a>';
$_['text_live']             = 'Funcionando';
$_['text_test']             = 'Prueba';
$_['text_payment_info']     = 'Info de pago';
$_['text_order_id']         = 'ID de Pedido';
$_['text_amount']           = 'Monto';
$_['text_fee']              = 'Gastos';
$_['text_date_added']       = 'Fecha añadido';
$_['text_tracking']         = 'Rastreo(Tracking)';
$_['text_barcode']          = 'Código de barras';
$_['text_barcode_info']     = '(Imprima este código de barras único y péguelo en la superficie de la parcela)';
$_['text_confirm']          = '¿Está seguro de que desea actualizar el número de rastreo?';
$_['text_register_success'] = 'Se ha registrado exitosamente. Recibirá un email a la brevedad.';
$_['text_tracking_success'] = 'Se actualizo el número de rastreo exitosamente';
$_['text_other']            = 'Otros';
$_['text_email']            = 'La dirección de email registrada para su cuenta Pilibaba es %s';

// Entry
$_['entry_email_address']   = 'Dirección de email';
$_['entry_password']        = 'Contraseña';
$_['entry_currency']        = 'Moneda';
$_['entry_warehouse']       = 'Almacén';
$_['entry_country']         = 'País';
$_['entry_merchant_number'] = 'Número de vendedor';
$_['entry_secret_key']      = 'Clave secreta';
$_['entry_environment']     = 'Entorno';
$_['entry_shipping_fee']    = 'Gasto de envío';
$_['entry_order_status']    = 'Estado de pedido';
$_['entry_status']          = 'Estado';
$_['entry_logging']         = 'Registro de debug';
$_['entry_sort_order']      = 'Orden de Clasificación';

// Ayuda
$_['help_email_address']    = 'Por favor ingrese la dirección de email para el dueño de este negocio.';
$_['help_password']         = 'Por favor ingrese una contraseña de entre 8 y 30 caracteres.';
$_['help_currency']         = 'Seleccione la moneda a usar en su sitio web y para retirar a su cuenta bancaria.';
$_['help_warehouse']        = 'Seleccione el almacén más cercano al que hará envíos. Cuando reciba pedidos de clientes chinos (vía el portal Pilibaba) podrá enviar parcelas a este almacén.';
$_['help_country']          = 'Díganos su país, y le informaremos cuando se abra un almacén en este país.';
$_['help_merchant_number']  = 'Su número de vendedor personal de cuenta Pilibaba.';
$_['help_secret_key']       = 'Su clave secreta de acceso a la API Pilibaba.';
$_['help_shipping_fee']     = 'El costo de envío desde su almacén al almacén de Pilibaba. Use dos dígitos decimales.';
$_['help_order_status']     = 'El estado de pedido luego de que el cliente realiza el pedido.';
$_['help_total']            = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago. Must be a value with no currency sign.';
$_['help_logging']          = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario.';

// Error
$_['error_warning']         = 'Advertencia: ¡Por favor compruebe el formulario cuidadosamente en busca de errores!';
$_['error_permission']      = 'Advertencia: ¡No tiene permisos para modificar pago con Pilibaba!';
$_['error_merchant_number'] = '¡Número de vendedor necesario!';
$_['error_secret_key']      = '¡Clave secreta necesaria!';
$_['error_shipping_fee']    = '¡Gasto de envío debe ser un número decimal!';
$_['error_not_enabled']     = '¡Módulo no activado!';
$_['error_data_missing']    = '¡Datos faltantes!';
$_['error_tracking_length'] = '¡Número de rastreo debe tener entre 1 y 50 caracteres!';
$_['error_email_address']   = '¡Por favor ingrese su dirección de email!';
$_['error_email_invalid']   = '¡Dirección de email inválida!';
$_['error_password']        = '¡La contraseña debe tener al menos 8 caracteres';
$_['error_currency']        = '¡Por favor seleccione una moneda!';
$_['error_warehouse']       = '¡Por favor seleccione un almacén!';
$_['error_country']         = '¡Por favor seleccione un país!';
$_['error_weight']          = 'Por favor cambie su configuración de <a href="%s">clase de peso</a> a gramos. Está en \'Sistema -> Configuraciones\' en la solapa \'Local\'.';
$_['error_bad_response']    = 'Respuesta inválida recibida. Por favor uelva a intentar más tarde.';

// Button
$_['button_register']       = 'Registrar';
$_['button_tracking']       = 'Actualizar número de rastreo';
$_['button_barcode']        = 'Generar código de barras';