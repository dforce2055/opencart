<?php
// Text
$_['text_extension']		 = 'Información de pago';
$_['text_capture_status']	 = 'Estado de captura';
$_['text_amount_authorised'] = 'Monto autorizado';
$_['text_amount_captured']	 = 'Monto capturado';
$_['text_amount_refunded']	 = 'Monto reintegrado';
$_['text_transaction']		 = 'Transacciones';
$_['text_complete']			 = 'Completar';
$_['text_confirm_void']		 = 'Si anula no podrá capturar más fondos';
$_['text_view']				 = 'Ver';
$_['text_refund']			 = 'Reintegro';
$_['text_resend']			 = 'Reenviar';
$_['text_success']           = 'Transaction was successfully sent';
$_['text_full_refund']		 = 'Reintegro total';
$_['text_partial_refund']	 = 'Reintegro parcial';
$_['text_payment']		 	 = 'Pago';
$_['text_current_refunds']   = 'Ya se realizaron reintregros para esta transacción. El reintegro máximo es';

// Column
$_['column_transaction']	 = 'ID de transacción';
$_['column_amount']			 = 'Monto';
$_['column_type']			 = 'Tipo de pago';
$_['column_status']			 = 'Estado';
$_['column_pending_reason']	 = 'Motivo de pendiente';
$_['column_date_added']		 = 'Fecha añadido';
$_['column_action']			 = 'Acción';

// Entry
$_['entry_capture_amount']	 = 'Monto capturado';
$_['entry_capture_complete'] = 'Captura completa';
$_['entry_full_refund']		 = 'Reintegro total';
$_['entry_amount']			 = 'Monto';
$_['entry_note']             = 'Nota';

// Ayuda
$_['help_capture_complete']  = 'Si esta es la última captura.';

// Tab
$_['tab_capture']		     = 'Captura';
$_['tab_refund']             = 'Reintegro';

// Button
$_['button_void']			 = 'Anular';
$_['button_capture']		 = 'Capturar';
$_['button_refund']		     = 'Emitir reintegro';

// Error
$_['error_capture']		     = 'Ingrese monto a capturar';
$_['error_transaction']	     = '¡No se pudo llevar a cabo la transacción!';
$_['error_not_found']	     = '¡No se pudo encontrar la transacción!';
$_['error_partial_amt']		 = 'Debe ingresar un monto de reintegro parcial';