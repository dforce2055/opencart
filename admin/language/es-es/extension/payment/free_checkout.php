<?php
// Heading
$_['heading_title']      = 'Proceso de compra en línea gratuito';

// Text
$_['text_extension']     = 'Extensiones';
$_['text_success']       = 'Éxito: ¡Ha modificado el módulo de pago Proceso de compra Gratis!';
$_['text_edit']          = 'Editar Proceso de compra Gratis';

// Entry
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_status']       = 'Estado';
$_['entry_sort_order']   = 'Orden de Clasificación';

// Error
$_['error_permission']   = 'Advertencia: ¡No tiene permisos para modificar Proceso de compra Gratis!';