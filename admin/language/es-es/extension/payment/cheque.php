<?php
// Heading
$_['heading_title']      = 'Cheque / Giro Postal';

// Text
$_['text_extension']     = 'Extensiones';
$_['text_success']       = 'Éxito: ¡Ha modificado sus detalles de cuenta cheque / giro postal!';
$_['text_edit']          = 'Editar Cheque / Giro Postal';

// Entry
$_['entry_payable']      = 'Beneficiario';
$_['entry_total']        = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']     = 'Geo Zona';
$_['entry_status']       = 'Estado';
$_['entry_sort_order']   = 'Orden de Clasificación';

// Ayuda
$_['help_total']         = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']   = 'Advertencia: ¡No tiene permisos para modificar pagos con cheque / giro postal!';
$_['error_payable']      = '¡Beneficiario necesario!';