<?php
/**
 * @package		OpenCart
 * @author		Meng Wenbin
 * @copyright	Copyright (c) 2010 - 2017, Chengdu Guangda Network Technology Co. Ltd. (https://www.opencart.cn/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.cn
 */

// Heading
$_['heading_title']                  = 'Wechat Pay';

// Text
$_['text_extension']                 = 'Extensiones';
$_['text_success']                   = 'Éxito: ¡Ha modificado sus detalles de cuenta Wechat!';
$_['text_edit']                      = 'Editar Wechat Pay';
$_['text_wechat_pay']                = '<a target="_BLANK" href="https://pay.weixin.qq.com"><img src="view/image/payment/Wechat.png" alt="Sitioo web Wechat Pay" title="Sitio web Wechat Pay" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_app_id']                   = 'ID de App';
$_['entry_app_secret']               = 'Secreto de App';
$_['entry_mch_id']                   = 'ID de Vendedor';
$_['entry_api_secret']               = 'API Secreto';
$_['entry_debug']                    = 'Modo debug';
$_['entry_total']                    = 'Total';
$_['entry_currency']                 = 'Moneda';
$_['entry_completed_status']         = 'Estado completado';
$_['entry_geo_zone']                 = 'Geo Zona';
$_['entry_status']                   = 'Estado';
$_['entry_sort_order']               = 'Orden de Clasificación';

// Ayuda
$_['help_total']                     = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';
$_['help_currency']                  = '¡La moneda con la cual el cliente le pago al vendedor!';
$_['help_wechat_pay_setup']          = '<a target="_blank" href="http://www.opencart.cn/docs/wechat-pay">Haga click aquí</a> para aprender como establecer una cuenta de Wechat Pay.';

// Error
$_['error_permission']               = 'Advertencia: ¡No tiene permisos para modificar pago con Wechat!';
$_['error_app_id']                   = '¡ID de App necesaria!';
$_['error_app_secret']               = '¡Secreto de App necesario!';
$_['error_mch_id']                   = '¡ID de Vendedor necesaria!';
$_['error_api_secret']               = '¡Secreto de API necesario!';
