<?php
// Heading
$_['heading_title']					 = 'PayPal Payflow Pro iFrame';
$_['heading_refund']				 = 'Reintegrar';

// Text
$_['text_extension']				 = 'Extensiones';
$_['text_success']					 = 'Éxito: ¡Ha modificado sus detalles de cuenta PayPal Payflow Pro iFrame!';
$_['text_edit']                      = 'Editar PayPal Payflow Pro iFrame';
$_['text_pp_payflow_iframe']		 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Autorización';
$_['text_sale']						 = 'Venta';
$_['text_authorise']				 = 'Authorizar';
$_['text_capture']					 = 'Captura retrasada';
$_['text_void']						 = 'Anular';
$_['text_payment_info']				 = 'Información de pago';
$_['text_complete']					 = 'Completo';
$_['text_incomplete']				 = 'Incompleto';
$_['text_transaction']				 = 'Transacción';
$_['text_confirm_void']				 = 'Si anula no podrá capturar más fondos';
$_['text_refund']					 = 'Reintegrar';
$_['text_refund_issued']			 = 'Reintegro emitido exitosamente';
$_['text_redirect']					 = 'Redirect';
$_['text_iframe']					 = 'Iframe';
$_['help_checkout_method']			 = 'Por favor utilice el método Redirect si no tiene SSL instalado o si no tiene la opción Pagar con PayPal desactivada en su página de pago hosteada.';

// Column
$_['column_transaction_id']			 = 'ID de transacción';
$_['column_transaction_type']		 = 'Tipo de transacción';
$_['column_amount']					 = 'Monto';
$_['column_time']					 = 'Tiempo';
$_['column_actions']				 = 'Acciones';

// Tab
$_['tab_settings']					 = 'Configuraciones';
$_['tab_order_status']				 = 'Estado de pedido';
$_['tab_checkout_customisation']	 = 'Personalizacion de proceso de pago';

// Entry
$_['entry_vendor']					 = 'Vendedor';
$_['entry_user']					 = 'Usuario';
$_['entry_password']				 = 'Contraseña';
$_['entry_partner']					 = 'Afiliado';
$_['entry_test']					 = 'Modo de Prueba';
$_['entry_transaction']				 = 'Método de transacción';
$_['entry_total']					 = 'Total';
$_['entry_order_status']			 = 'Estado de pedido';
$_['entry_geo_zone']				 = 'Geo Zona';
$_['entry_status']					 = 'Estado';
$_['entry_sort_order']				 = 'Orden de Clasificación';
$_['entry_transaction_id']			 = 'ID de transacción';
$_['entry_full_refund']				 = 'Reintegro total';
$_['entry_amount']					 = 'Monto';
$_['entry_message']					 = 'Mensaje';
$_['entry_ipn_url']					 = 'URL IPN';
$_['entry_checkout_method']			 = 'Método de proceso de pago';
$_['entry_debug']					 = 'Modo debug';
$_['entry_transaction_reference']	 = 'Referencia de transacción';
$_['entry_transaction_amount']		 = 'Monto de transacción';
$_['entry_refund_amount']			 = 'Monto de reintegro';
$_['entry_capture_status']			 = 'Estado de captura';
$_['entry_void']					 = 'Anular';
$_['entry_capture']					 = 'Captura';
$_['entry_transactions']			 = 'Transacciones';
$_['entry_complete_capture']		 = 'Completar Captura';
$_['entry_canceled_reversal_status'] = 'Estado revocación cancelada:';
$_['entry_completed_status']		 = 'Estado completado:';
$_['entry_denied_status']			 = 'Estado denegado:';
$_['entry_expired_status']			 = 'Estado expirado:';
$_['entry_failed_status']			 = 'Estado fallado:';
$_['entry_pending_status']			 = 'Estado pendiente:';
$_['entry_processed_status']		 = 'Estado procesado:';
$_['entry_refunded_status']			 = 'Estado reintegrado:';
$_['entry_reversed_status']			 = 'Estado devuelto:';
$_['entry_voided_status']			 = 'Estado anulado:';
$_['entry_cancel_url']				 = 'URL de cancelación:';
$_['entry_error_url']				 = 'URL de error:';
$_['entry_return_url']				 = 'URL de retorno:';
$_['entry_post_url']				 = 'URL de POST silencioso:';

// Ayuda
$_['help_vendor']					 = 'Su ID de inicio de sesión que creó cuando registró una cuenta de Website Payments Pro';
$_['help_user']						 = 'Si configura uno o más usuarios adicionales en la cuenta, este valorp es el ID de usuario autorizado a procesar transacciones. Si no ha configuradoo usuarios adicionales en la cuenta, USUARIO y VENDEDOR tienen los mismos valores';
$_['help_password']					 = 'La contraseña de entre 6 y 32 caracteres que definió cuando registró su cuenta';
$_['help_partner']					 = 'La ID provista a usted por el Revendedor PayPal que lo registró para el SDK Payflow. Si compró su cuenta directamente de PayPal, utilice PayPal Pro en su lugar';
$_['help_test']						 = '¿Utilizar el servidor de producción o de prueba (sandbox) para transacciones?';
$_['help_total']					 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';
$_['help_debug']					 = 'Registra información adicional';

// Button
$_['button_refund']					 = 'Reintegrar';
$_['button_void']					 = 'Anular';
$_['button_capture']				 = 'Captura';

// Error
$_['error_permission']				 = 'Advertencia: ¡No tiene permisos para modificar pago con PayPal Website Payment Pro iFrame (UK)!';
$_['error_vendor']					 = '¡Vendedor necesario!';
$_['error_user']					 = '¡Usuario necesario!';
$_['error_password']				 = '¡Contraseña necesario!';
$_['error_partner']					 = '¡Afiliado necesario!';
$_['error_missing_data']			 = 'Datos faltantes';
$_['error_missing_order']			 = 'No se encontró el pedido';
$_['error_general']					 = 'Ocurrió un error';
$_['error_capture']				     = 'Ingrese monto a capturar';