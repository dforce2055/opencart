<?php
// Heading
$_['heading_title']		   = 'Reintegro de transacción';

// Text
$_['text_pp_express']	   = 'Proceso de compra con PayPal Express';
$_['text_current_refunds'] = 'Ya se realizaron reintregros para esta transacción. El reintegro máximo es';
$_['text_refund']		   = 'Reintegro';

// Entry
$_['entry_transaction_id'] = 'ID de transacción';
$_['entry_full_refund']	   = 'Reintegro total';
$_['entry_amount']		   = 'Monto';
$_['entry_message']		   = 'Mensaje';

// Button
$_['button_refund']		   = 'Reintegrar';

// Error
$_['error_partial_amt']	   = 'Debe ingresar un monto de reintegro parcial';
$_['error_data']		   = 'Faltan datos en la solicitud';