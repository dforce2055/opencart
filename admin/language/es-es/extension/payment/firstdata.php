<?php
// Heading
$_['heading_title']					 = 'First Data EMEA Connect (Con 3DSecure)';

// Text
$_['text_extension']				 = 'Extensiones';
$_['text_success']					 = 'Éxito: ¡Ha modificado sus detalles de cuenta First Data!';
$_['text_edit']                      = 'Editar First Data EMEA Connect (Con 3DSecure)';
$_['text_notification_url']			 = 'URL de notificación';
$_['text_live']						 = 'Funcionando';
$_['text_demo']						 = 'Demo';
$_['text_enabled']					 = 'Activado';
$_['text_merchant_id']				 = 'ID de tienda';
$_['text_secret']					 = 'Secreto compartido';
$_['text_capture_ok']				 = 'Captura exitosa';
$_['text_capture_ok_order']			 = 'Captura exitosa, estado de pedido actualizado a exitoso - establecido';
$_['text_void_ok']					 = 'Anulación exitosa, estado de pedido actualizado a anulado';
$_['text_settle_auto']				 = 'Venta';
$_['text_settle_delayed']			 = 'Pre auth';
$_['text_success_void']				 = 'Transacción anulada';
$_['text_success_capture']			 = 'Transacción capturada';
$_['text_firstdata']				 = '<img src="view/image/payment/firstdata.png" alt="First Data" title="First Data" style="border: 1px solid #EEEEEE;" />';
$_['text_payment_info']				 = 'Información de pago';
$_['text_capture_status']			 = 'Pago capturado';
$_['text_void_status']				 = 'Pago anulado';
$_['text_order_ref']				 = 'Referencia de pedido';
$_['text_order_total']				 = 'Total autorizado';
$_['text_total_captured']			 = 'Total capturado';
$_['text_transactions']				 = 'Transacciones';
$_['text_column_amount']			 = 'Monto';
$_['text_column_type']				 = 'Tipo';
$_['text_column_date_added']		 = 'Creado';
$_['text_confirm_void']				 = '¿Está seguro de que desea anular el pago?';
$_['text_confirm_capture']			 = '¿Está seguro de que desea capture el pago?';

// Entry
$_['entry_merchant_id']				 = 'ID de tienda';
$_['entry_secret']					 = 'Secreto compartido';
$_['entry_total']					 = 'Total';
$_['entry_sort_order']				 = 'Orden de clasificación';
$_['entry_geo_zone']				 = 'Geo Zona';
$_['entry_status']					 = 'Estado';
$_['entry_debug']					 = 'Registro de debug';
$_['entry_live_demo']				 = 'Producción / Demo';
$_['entry_auto_settle']			  	 = 'Tipo de establecimiento';
$_['entry_card_select']				 = 'Selección de tarjeta';
$_['entry_tss_check']				 = 'Comprobaciones TSS';
$_['entry_live_url']				 = 'Conexión URL de producción';
$_['entry_demo_url']				 = 'Conexión URL de demo';
$_['entry_status_success_settled']	 = 'Éxito - establecido';
$_['entry_status_success_unsettled'] = 'Éxito - no establecido';
$_['entry_status_decline']		 	 = 'Declinar';
$_['entry_status_void']				 = 'Anulado';
$_['entry_enable_card_store']		 = 'Activar tokens de almacenamiento de tarjetas';

// Ayuda
$_['help_total']					 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';
$_['help_notification']				 = 'Debe proveer esta URL a First Data para obtener notificaciones de pago';
$_['help_debug']					 = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario';
$_['help_settle']					 = 'Si utiliza pre-auth debe completar una acción post-auth dentro de 3-5 días o su transacción será descartada'; 

// Tab
$_['tab_account']					 = 'Info de API';
$_['tab_order_status']				 = 'Estado de pedido';
$_['tab_payment']					 = 'Ajustes de pago';
$_['tab_advanced']					 = 'Avanzado';

// Button
$_['button_capture']				 = 'Captura';
$_['button_void']					 = 'Anular';

// Error
$_['error_merchant_id']				 = 'ID de tienda necesario';
$_['error_secret']					 = 'Shared secret necesario';
$_['error_live_url']				 = 'URL de producción necesaria';
$_['error_demo_url']				 = 'URL de Demo necesaria';
$_['error_data_missing']			 = 'Datos faltantes';
$_['error_void_error']				 = 'No se pudo anular la transacción';
$_['error_capture_error']			 = 'No se pudo capturar la transacción';