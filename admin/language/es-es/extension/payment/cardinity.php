<?php
// Heading
$_['heading_title']			= 'Cardinity';

// Text
$_['text_extension']		= 'Extensiones';
$_['text_success']			= 'Éxito: ¡¡Ha modificado el módulo de pago Cardinity!';
$_['text_edit']             = 'Editar Cardinity';
$_['text_cardinity']		= '<a href="http://cardinity.com/?crdp=opencart" target="_blank"><img src="view/image/payment/cardinity.png" alt="Cardinity" title="Cardinity" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_production']		= 'Producción';
$_['text_sandbox']			= 'Sandbox';
$_['text_payment_info']		= 'Información de reintegro';
$_['text_no_refund']		= 'Sin historia de reintegros';
$_['text_confirm_refund']	= 'Está seguro de que desea reintegrar';
$_['text_na']				= 'N/A';
$_['text_success_action']	= 'Éxito';
$_['text_error_generic']	= 'Error: Ocurrió un error con su solicitud. Por favor revise el registro.';

// Column
$_['column_refund']			= 'Reintegro';
$_['column_date']			= 'Fecha';
$_['column_refund_history'] = 'Historial de reintegros';
$_['column_action']			= 'Acción';
$_['column_status']			= 'Estado';
$_['column_amount']			= 'Monto';
$_['column_description']	= 'Descripción';

// Entry
$_['entry_total']			= 'Total';
$_['entry_order_status']	= 'Estado de pedido';
$_['entry_geo_zone']		= 'Geo Zona';
$_['entry_status']			= 'Estado';
$_['entry_sort_order']		= 'Orden de Clasificación';
$_['entry_key']				= 'Clave';
$_['entry_secret']			= 'Secreto';
$_['entry_debug']			= 'Debug';

// Ayuda
$_['help_debug']			= 'Activar debug escribirá datos sensiblse a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario.';
$_['help_total']			= 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Button
$_['button_refund']			= 'Reintegrar';

// Error
$_['error_key']				= '¡Clave necesaria!';
$_['error_secret']			= '¡Secreto necesario!';
$_['error_composer']		= 'No se pudo cargar el SDK Cardinity. Por favor descargue una carpeta de vendedor compilada o corra el compositor.';
$_['error_php_version']		= '¡Versión mínima de PHP 5.4.0 es necesaria!';
$_['error_permission']		= 'Advertencia: ¡No tiene permisos para modificar pago con Cardinity!';
$_['error_connection']		= 'Ocurrió un problema al establecer la conexióón con la API de Cardinity. Por favor verifique su configuración de Clave y Secreto.';
$_['error_warning']			= 'Advertencia: ¡Por favor revise el formulario cuidadosamente en busca de errores!';