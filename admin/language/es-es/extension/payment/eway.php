<?php
// Heading
$_['heading_title']				= 'eWAY Payments';

// Text
$_['text_extension']			= 'Extensiones';
$_['text_success']				= 'Éxito: ¡Ha modificado sus detalles de eWAY!';
$_['text_edit']					= 'Editar eWAY';
$_['text_eway']					= '<a target="_BLANK" href="http://www.eway.com.au/"><img src="view/image/payment/eway.png" alt="eWAY" title="eWAY" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorisation']		= 'Autorización';
$_['text_sale']					= 'Venta';
$_['text_transparent']			= 'Redirección transparente (formulario de pago en sitio)';
$_['text_iframe']				= 'IFrame (formulario de pago en ventana)';

// Entry
$_['entry_paymode']				= 'Modo de pago';
$_['entry_test']				= 'Modo de prueba';
$_['entry_order_status']		= 'Estado de pedido';
$_['entry_order_status_refund'] = 'Estado de pedido reintegrado';
$_['entry_order_status_auth']	= 'Estado de pedido autorizado';
$_['entry_order_status_fraud']	= 'Estado de pedido sospechado de fraude';
$_['entry_status']				= 'Estado';
$_['entry_username']			= 'Clave de API eWay';
$_['entry_password']			= 'Contraseña eWay';
$_['entry_payment_type']		= 'Tipo de pago';
$_['entry_geo_zone']			= 'Geo Zona';
$_['entry_sort_order']			= 'Orden de clasificación';
$_['entry_transaction_method']	= 'Método de transacción';

// Error
$_['error_permission']			= 'Advertencia: ¡No tiene permisos para modificar el módulo de pago eWAY!';
$_['error_username']			= '¡Clave de API eWay necesaria!';
$_['error_password']			= '¡Contraseña eWAY necesaria!';
$_['error_payment_type']		= '¡Debe haber al menos un método de pago!';

// Ayuda hints
$_['help_testmode']				= 'Seleccione SI para activar el Sandbox eWay.';
$_['help_username']				= 'Su clave de API eWay de su cuenta MYeWAY.';
$_['help_password']				= 'Su contraseña de API eWay de su cuenta MYeWAY.';
$_['help_transaction_method']	= 'Autorización sólo disponible para bancos australianos.';

// Order page - payment tab
$_['text_payment_info']			= 'Información de pago';
$_['text_order_total']			= 'Total autorizado';
$_['text_transactions']			= 'Transacciones';
$_['text_column_transactionid'] = 'ID de transacción eWay';
$_['text_column_amount']		= 'Monto';
$_['text_column_type']			= 'Tipo';
$_['text_column_created']		= 'Creado';
$_['text_total_captured']		= 'Total capturado';
$_['text_capture_status']		= 'Pago capturado';
$_['text_void_status']			= 'Pago anulado';
$_['text_refund_status']		= 'Pago reintegrado';
$_['text_total_refunded']		= 'Total reintegrado';
$_['text_refund_success']		= '¡Reintegro exitoso!';
$_['text_capture_success']		= '¡Captura exitosa!';
$_['text_refund_failed']		= 'Reintegro fallido: ';
$_['text_capture_failed']		= 'Captura fallida: ';
$_['text_unknown_failure']		= 'Pedido o monto inválido: ';

$_['text_confirm_capture']		= '¿Está seguro de que desea capturar el pago?';
$_['text_confirm_release']		= '¿Está seguro de que desea liberar el pago?';
$_['text_confirm_refund']		= 'Está seguro de que desea reintegrar el pago?';

$_['text_empty_refund']			= 'Por favor ingrese un monto a reintegrar';
$_['text_empty_capture']		= 'Por favor ingrese un monto a capturar';

$_['btn_refund']				= 'Reintegrar';
$_['btn_capture']				= 'Capturar';

// Validation Código de errors
$_['text_card_message_V6000']	= 'Error de validación indefinido';
$_['text_card_message_V6001'] 	= 'IP de cliente inválida';
$_['text_card_message_V6002'] 	= 'DeviceID inválido';
$_['text_card_message_V6011'] 	= 'Monto inválido';
$_['text_card_message_V6012'] 	= 'Descripción de factura inválida';
$_['text_card_message_V6013'] 	= 'Número de factura inválido';
$_['text_card_message_V6014'] 	= 'Referencia de factura inválida';
$_['text_card_message_V6015']   = 'Código de moneda inválido';
$_['text_card_message_V6016']   = 'Pago necesario';
$_['text_card_message_V6017']   = 'Código de moneda de pago necesario';
$_['text_card_message_V6018']   = 'Código de moneda de pago desconocido';
$_['text_card_message_V6021']   = 'Nombre de titular necesario';
$_['text_card_message_V6022']   = 'Número de tarjeta Necesario';
$_['text_card_message_V6023']   = 'CVN Necesario';
$_['text_card_message_V6031']   = 'Número de tarjeta inválido';
$_['text_card_message_V6032']   = 'CVN inválido';
$_['text_card_message_V6033']   = 'Fecha de expiración inválida';
$_['text_card_message_V6034']   = 'Número de emisión inválido';
$_['text_card_message_V6035']   = 'Fecha inicial inválida';
$_['text_card_message_V6036']   = 'Mes inválido';
$_['text_card_message_V6037']   = 'Año inválido';
$_['text_card_message_V6040']   = 'Token Id Cliente inválido';
$_['text_card_message_V6041']   = 'Cliente Necesario';
$_['text_card_message_V6042']   = 'Nombre de Cliente Necesario';
$_['text_card_message_V6043']   = 'Apellido de Cliente Necesario';
$_['text_card_message_V6044']   = 'Código de País de Cliente Necesario';
$_['text_card_message_V6045']   = 'Título de Cliente Necesario';
$_['text_card_message_V6046']   = 'Token ID Cliente Necesario';
$_['text_card_message_V6047']   = 'URL Redirección Necesaria';
$_['text_card_message_V6051']   = 'Nombre inválido';
$_['text_card_message_V6052']   = 'Apellido inválido';
$_['text_card_message_V6053']   = 'Código de País inválido';
$_['text_card_message_V6054']   = 'Email inválido';
$_['text_card_message_V6055']   = 'Teléfono inválido';
$_['text_card_message_V6056']   = 'Teléfono Celular inválido';
$_['text_card_message_V6057']   = 'Fax inválido';
$_['text_card_message_V6058']   = 'Título inválido';
$_['text_card_message_V6059']   = 'URL redirección inválida';
$_['text_card_message_V6060']   = 'URL redirección inválida';
$_['text_card_message_V6061']   = 'Referencia inválida';
$_['text_card_message_V6062']   = 'Nombre de Compañía inválido';
$_['text_card_message_V6063']   = 'Descripción de trabajo inválida';
$_['text_card_message_V6064']   = 'Calle1 inválida';
$_['text_card_message_V6065']   = 'Calle2 inválida';
$_['text_card_message_V6066']   = 'Ciudad inválida';
$_['text_card_message_V6067']   = 'Estado inválido';
$_['text_card_message_V6068']   = 'Código postal inválido';
$_['text_card_message_V6069']   = 'Email inválido';
$_['text_card_message_V6070']   = 'Teléfono inválido';
$_['text_card_message_V6071']   = 'Teléfono Celular inválido';
$_['text_card_message_V6072']   = 'Commentarios inválidos';
$_['text_card_message_V6073']   = 'Fax inválido';
$_['text_card_message_V6074']   = 'Url inválida';
$_['text_card_message_V6075']   = 'Nombre de dirección de envío inválido';
$_['text_card_message_V6076']   = 'Apellido de dirección de envío inválido';
$_['text_card_message_V6077']   = 'Calle1 de dirección de envío inválida';
$_['text_card_message_V6078']   = 'Calle2 de dirección de envío inválida';
$_['text_card_message_V6079']   = 'Ciudad de dirección de envío inválida';
$_['text_card_message_V6080']   = 'Estado de dirección de envío inválido';
$_['text_card_message_V6081']   = 'Código postal de dirección de envío inválido';
$_['text_card_message_V6082']   = 'Email de dirección de envío inválido';
$_['text_card_message_V6083']   = 'Teléfono de dirección de envío inválido';
$_['text_card_message_V6084']   = 'País de dirección de envío inválido';
$_['text_card_message_V6091']   = 'Código de país desconocido';
$_['text_card_message_V6100']   = 'Nombre de Tarjeta inválido';
$_['text_card_message_V6101']   = 'Mes de expiración de tarjeta inválido';
$_['text_card_message_V6102']   = 'Año de expiración de tarjeta inválido';
$_['text_card_message_V6103']   = 'Mes inicial de tarjeta inválido';
$_['text_card_message_V6104']   = 'Año inicial de tarjeta inválido';
$_['text_card_message_V6105']   = 'Número de emisión de tarjeta inválido';
$_['text_card_message_V6106']   = 'CVN De Tarjeta inválido';
$_['text_card_message_V6107']   = 'Código de acceso inválido';
$_['text_card_message_V6108']   = 'CustomerHostAddress inválido';
$_['text_card_message_V6109']   = 'UserAgent inválido';
$_['text_card_message_V6110']   = 'Número de tarjeta inválidoo';
$_['text_card_message_V6111'] 	= 'Acceso API no autorizado, Cuenta no certificada con PCI';
$_['text_card_message_V6112'] 	= 'Datos de tarjeta redundantes aparte de año y mes de expiración';
$_['text_card_message_V6113'] 	= 'Transacción inválida para reembolso';
$_['text_card_message_V6114'] 	= 'Error de validación de Gateway';
$_['text_card_message_V6115'] 	= 'DirectRefundRequest inválido, ID de Transacción';
$_['text_card_message_V6116'] 	= 'Datos de tarjeta inválidos en ID de Transacción original';
$_['text_card_message_V6124'] 	= 'Items de línea inválidos. Los ítems de línea fueron provistos pero los totales no coinciden con el campo MontoTotal.';
$_['text_card_message_V6125'] 	= 'Método de pago seleccionado no activado.';
$_['text_card_message_V6126'] 	= 'Número de tarjeta cifrado inválido, descifrado fallido.';
$_['text_card_message_V6127'] 	= 'cvn cifrado inválido, descifrado fallido.';
$_['text_card_message_V6128'] 	= 'Método inválido para el tipo de pago.';
$_['text_card_message_V6129'] 	= 'La transacción no fue autorizada para Captura/Cancelación.';
$_['text_card_message_V6130'] 	= 'Error de información de cliente genérico.';
$_['text_card_message_V6131'] 	= 'Error de información de envío genérico.';
$_['text_card_message_V6132'] 	= 'La Transacción ya fue completada o anulada, operación no permitida.';
$_['text_card_message_V6133'] 	= 'Proceso de compra no disponible para este tipo de pago.';
$_['text_card_message_V6134'] 	= 'ID de transacción Auth Inválido para Captura/Anulación';
$_['text_card_message_V6135'] 	= 'Error de PayPal al procesar reembolso';
$_['text_card_message_V6140'] 	= 'Cuenta Vendedor suspendida.';
$_['text_card_message_V6141'] 	= 'Detalles de cuenta Paypal o firma de API inválidos';
$_['text_card_message_V6142'] 	= 'Autorización no disponible para Banco/Sucursal';
$_['text_card_message_V6150'] 	= 'Invalid Refund Amount';
$_['text_card_message_V6151'] 	= 'Monto de reembolso inválido';
$_['text_card_message_D4406'] 	= 'Error desconocido';
$_['text_card_message_S5010'] 	= 'Error desconocido';