<?php
// Heading
$_['heading_title']				= 'Web Payment Software';

// Text
$_['text_extension']			= 'Extensiones';
$_['text_success']				= 'Éxito: ¡Ha modificado sus detalles de cuenta Web Payment Software!';
$_['text_edit']                 = 'Editar Web Payment Software';
$_['text_web_payment_software']	= '<a href="http://www.web-payment-software.com/" target="_blank"><img src="view/image/payment/wps-logo.jpg" alt="Web Payment Software" title="Web Payment Software" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']					= 'Prueba';
$_['text_live']					= 'Funcionando';
$_['text_authorization']		= 'Autorización';
$_['text_capture']				= 'Captura';

// Entry
$_['entry_login']				= 'ID de Vendedor';
$_['entry_key']					= 'Clave de vendedor';
$_['entry_mode']				= 'Modo de transacción';
$_['entry_method']				= 'Método de transacción';
$_['entry_total']				= 'Total';
$_['entry_order_status']		= 'Estado de pedido';
$_['entry_geo_zone']			= 'Geo Zona';
$_['entry_status']				= 'Estado';
$_['entry_sort_order']			= 'Orden de Clasificación';

// Ayuda
$_['help_total']				= 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']			= 'Advertencia: ¡No tiene permisos para modificar pago con Web Payment Software!';
$_['error_login']				= '¡ID de inicio de sesión necesaria!';
$_['error_key']					= '¡Clave de transacción necesaria!';