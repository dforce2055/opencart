<?php
// Heading
$_['heading_title']		 = 'PayPoint';

// Text
$_['text_extension']	 = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta PayPoint!';
$_['text_edit']          = 'Editar PayPoint';
$_['text_paypoint']		 = '<a href="https://www.paypoint.net/partners/opencart" target="_blank"><img src="view/image/payment/paypoint.png" alt="PayPoint" title="PayPoint" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']			 = 'Producción';
$_['text_successful']	 = 'Siempre exitoso';
$_['text_fail']			 = 'Siempre fallido';

// Entry
$_['entry_merchant']	 = 'ID de Vendedor';
$_['entry_password']	 = 'Contraseña remota';
$_['entry_test']		 = 'Modo de Prueba';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']	 = 'Geo Zona';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden de Clasificación';

// Ayuda
$_['help_password']		 = 'Deje vacío si no tiene "Autenticación con Clave Digest" activado en su cuenta.';
$_['help_total']		 = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']   = 'Advertencia: ¡No tiene permisos para modificar pago con PayPoint!';
$_['error_merchant']	 = '¡ID de Vendedor necesaria!';