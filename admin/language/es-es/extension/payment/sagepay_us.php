<?php
// Heading
$_['heading_title']		 = 'Sage Pay Solutions (US)';

// Text
$_['text_extension']	 = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta SagePay!';
$_['text_edit']          = 'Editar Sage Pay Solutions (US)';

// Entry
$_['entry_merchant_id']	 = 'ID de Vendedor';
$_['entry_merchant_key'] = 'Clave de vendedor';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']	 = 'Geo Zona';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden de Clasificación';

// Ayuda
$_['help_total']		 = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']	 = 'Advertencia: ¡No tiene permisos para modificar pago con SagePay!';
$_['error_merchant_id']	 = '¡ID de Vendedor necesaria!';
$_['error_merchant_key'] = '¡Clave de vendedor necesaria!';