<?php
// Heading
$_['heading_title']                 = 'CardConnect';

// Tab
$_['tab_settings']                  = 'Configuraciones';
$_['tab_order_status']              = 'Estado de pedido';

// Text
$_['text_extension']                = 'Extensiones';
$_['text_success']                  = 'Éxito: ¡¡Ha modificado el módulo de pago CardConnect!';
$_['text_edit']                     = 'Editar CardConnect';
$_['text_cardconnect']              = '<a href="http://www.cardconnect.com" target="_blank"><img src="view/image/payment/cardconnect.png" alt="CardConnect" title="CardConnect"></a>';
$_['text_payment']                  = 'Pago';
$_['text_authorize']                = 'Autorizar';
$_['text_live']                     = 'Funcionando';
$_['text_test']                     = 'Prueba';
$_['text_no_cron_time']             = 'El cron no se ha ejecutado aún';
$_['text_payment_info']             = 'Información de pago';
$_['text_payment_method']           = 'Método de pago';
$_['text_card']                     = 'Tarjeta';
$_['text_echeck']                   = 'eCheque';
$_['text_reference']                = 'Referencia';
$_['text_update']                   = 'Actualizar';
$_['text_order_total']              = 'Total de Pedidos';
$_['text_total_captured']           = 'Total Captured';
$_['text_capture_payment']          = 'Capturar Pago';
$_['text_refund_payment']           = 'Reintegrar Pago';
$_['text_void']                     = 'Anular';
$_['text_transactions']             = 'Transacciones';
$_['text_column_type']              = 'Tipo';
$_['text_column_reference']         = 'Referencia';
$_['text_column_amount']            = 'Monto';
$_['text_column_status']            = 'Estado';
$_['text_column_date_modified']     = 'Fecha Modificación';
$_['text_column_date_added']        = 'Fecha añadido';
$_['text_column_update']            = 'Actualizar';
$_['text_column_void']              = 'Anular';
$_['text_confirm_capture']          = '¿Está seguro de que desea capturar el pago?';
$_['text_confirm_refund']           = 'Está seguro de que desea reintegrar el pago?';
$_['text_inquire_success']          = 'Consulta exitosa';
$_['text_capture_success']          = 'Solicitud de captura exitosa';
$_['text_refund_success']           = 'Solicitud de reintegro exitosa';
$_['text_void_success']             = 'Solicitud de anulación exitosa';

// Entry
$_['entry_merchant_id']             = 'ID de Vendedor';
$_['entry_api_username']            = 'Nombre de Usuario de API';
$_['entry_api_password']            = 'Contraseña de API';
$_['entry_token']                   = 'Token Secreto';
$_['entry_transaction']             = 'Transacción';
$_['entry_environment']             = 'Entorno';
$_['entry_site']                    = 'Sitio';
$_['entry_store_cards']             = 'Tarjetas de tienda';
$_['entry_echeck']                  = 'eCheque';
$_['entry_total']                   = 'Total';
$_['entry_geo_zone']                = 'Geo Zona';
$_['entry_status']                  = 'Estado';
$_['entry_logging']                 = 'Registro de debug';
$_['entry_sort_order']              = 'Orden de Clasificación';
$_['entry_cron_url']                = 'URL de Cron Job';
$_['entry_cron_time']               = 'Última ejecución de Cron Job';
$_['entry_order_status_pending']    = 'Pendiente';
$_['entry_order_status_processing'] = 'En Proceso';

// Ayuda
$_['help_merchant_id']              = 'Su ID personal de vendedor de cuenta de CardConnect';
$_['help_api_username']             = 'Su nombre de usuario para acceder a la API de CardConnect.';
$_['help_api_password']             = 'Su contraseña para acceder a la API de CardConnect.';
$_['help_token']                    = 'Ingrese un token aleatorio para seguridad, o utilice el generado.';
$_['help_transaction']              = 'Elija \'Pago\' para capturar el pagoo inmediatamente o \'Autorizar\' para tener que aprobarlo.';
$_['help_site']                     = 'Esto determina la primera parte de la URL de API. Sólo cambiar si se le instruye/';
$_['help_store_cards']              = 'Si quiere almacenar tarjetas usando tokenización.';
$_['help_echeck']                   = 'Si quiere ofrecer la habilidad de pagar con un eCheque.';
$_['help_total']                    = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago. Must be a value with no currency sign.';
$_['help_logging']                  = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario.';
$_['help_cron_url']                 = 'Setear un cron job que llame a esta URL para que los pedidos seann actualizados automáticamente. Está diseñado para ser ejecutado unas horas despues de la última captura del horario de atención.';
$_['help_cron_time']                = 'La última vez que se ejecutó la URL del cron job.';
$_['help_order_status_pending']     = 'El estado de pedido cuando el pedido debe ser autorizado por el vendedor.';
$_['help_order_status_processing']  = 'El estado de pedido cuando el pedido es capturado automáticamente.';

// Button
$_['button_inquire_all']            = 'Consultar Todas';
$_['button_capture']                = 'Captura';
$_['button_refund']                 = 'Reintegrar';
$_['button_void_all']               = 'Anular Todas';
$_['button_inquire']                = 'Consultar';
$_['button_void']                   = 'Anular';

// Error
$_['error_permission']              = 'Advertencia: ¡No tiene permisos para modificar pago con CardConnect!';
$_['error_merchant_id']             = '¡ID de Vendedor necesaria!';
$_['error_api_username']            = '¡Nombre de Usuario de API necesario!';
$_['error_api_password']            = '¡Contraseña de API necesaria!';
$_['error_token']                   = '¡Token Secreto necesario!';
$_['error_site']                    = '¡Sitio necesario!';
$_['error_amount_zero']             = '¡Monto debe ser mayor a cero!';
$_['error_no_order']                = '¡No hay información de pedido coincidente!';
$_['error_invalid_response']        = '¡Respuesta inválida recibida!';
$_['error_data_missing']            = '¡Datos faltantes!';
$_['error_not_enabled']             = '¡Module no activado!';