<?php
$_['heading_title']                              = 'Servicio Web Secure Trading';

$_['tab_settings']                               = 'Configuraciones';
$_['tab_myst']                                   = 'MiST';

$_['text_securetrading_ws']                      = '<a href="http://www.securetradingfs.com/partner/open-cart/" target="_blank"><img src="view/image/payment/secure_trading.png" alt="Secure Trading" title="Secure Trading" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_extension']                             = 'Extensiones';
$_['text_all_geo_zones']                         = 'Todas Geo Zonas';
$_['text_process_immediately']                   = 'Procesar inmediatamente';
$_['text_wait_x_days']                           = 'Espere %d días';
$_['text_wait']                                  = 'Por favor espere.';
$_['text_authorisation_reversed']                = 'Autorizacion revertida exitosamente';
$_['text_refund_issued']                         = 'Reintegro emitido exitosamente';
$_['text_success']                               = 'Éxito: ¡Ha modificado el módulo de pago Servicio Web Secure Trading!';
$_['text_pending_settlement']                    = 'Establecimiento pendiente';
$_['text_manual_settlement']                     = 'Establecimiento manual';
$_['text_suspended']                             = 'Suspendido';
$_['text_cancelled']                             = 'Cancelado';
$_['text_settling']                              = 'Estableciendo';
$_['text_settled']                               = 'Establecido';
$_['text_no_transactions']                       = 'No hay transacciones para mostrar';
$_['text_ok']                                    = 'Ok';
$_['text_denied']                                = 'Denegado';
$_['text_pending_settlement_manually_overriden'] = 'Establecimiento pendiente, control manual';
$_['text_pending_suspended']                     = 'Suspendido';
$_['text_pending_settled']                       = 'Establecido';
$_['text_payment_info']                          = 'Información de pago';
$_['text_release_status']                        = 'Pago liberado';
$_['text_void_status']                           = 'Autorización revertida';
$_['text_rebate_status']                         = 'Pago reembolsado';
$_['text_order_ref']                             = 'Referencia de pedido';
$_['text_order_total']                           = 'Total autorizado';
$_['text_total_released']                        = 'Total liberado';
$_['text_transactions']                          = 'Transacciones';
$_['text_column_amount']                         = 'Monto';
$_['text_column_type']                           = 'Tipo';
$_['text_column_created']                        = 'Creado';
$_['text_release_ok']                            = 'Liberación exitosa';
$_['text_release_ok_order']                      = 'Liberación exitosa, estado de pedido actualizado a exitoso - establecido';
$_['text_rebate_ok']                             = 'Reembolso exitoso';
$_['text_rebate_ok_order']                       = 'Reembolso exitoso, estado de pedido actualizado a reembolsado';
$_['text_void_ok']                               = 'Anulación exitosa, estado de pedido actualizado a anulado';
$_['text_confirm_void']                          = '¿Está seguro de que desea reverse the authorisation?';
$_['text_confirm_release']                       = '¿Está seguro de que desea liberar el pago?';
$_['text_confirm_rebate']                        = '¿Está seguro de que desea reembolsar el pago?';

$_['entry_site_reference']                       = 'Referencia de sitio';
$_['entry_username']                             = 'Nombre de Usuario';
$_['entry_password']                             = 'Contraseña';
$_['entry_csv_username']                         = 'Nombre de usuario CSV';
$_['entry_csv_password']                         = 'Contraseña CSV';
$_['entry_3d_secure']                            = 'Usar 3D Secure';
$_['entry_cards_accepted']                       = 'Tarjetas aceptadas';
$_['entry_order_status']                         = 'Estado de pedido';
$_['entry_failed_order_status']                  = 'Estado de pedido fallido';
$_['entry_declined_order_status']                = 'Estado de pedido rechazado';
$_['entry_refunded_order_status']                = 'Estado de pedido reintegrado';
 
$_['entry_authorisation_reversed_order_status']  = 'Estado de pedido de autorización revertida';
$_['entry_settle_status']                        = 'Estado de establecimiento';
$_['entry_settle_due_date']                      = 'Fecha de vencimiento de establecimiento'; 
$_['entry_geo_zone']                             = 'Geo Zona';
$_['entry_sort_order']                           = 'Orden de Clasificación';
$_['entry_status']                               = 'Estado';
$_['entry_total']                                = 'Total';
$_['entry_reverse_authorisation']                = 'Autorización revertida:';
$_['entry_refunded']                             = 'Reintegrado:';
$_['entry_refund']                               = 'Emitir reintegro (%s):';
$_['entry_currency']                             = 'Moneda';
$_['entry_status_code']                          = 'Código de error';
$_['entry_payment_type']                         = 'Tipo de pago';
$_['entry_request']                              = 'Solicitud';
$_['entry_date_from']                            = 'Fecha desde';
$_['entry_date_to']                              = 'Fecha hasta';
$_['entry_hour']                                 = 'Hora';
$_['entry_minute']                               = 'Minuto';

$_['column_order_id']                            = 'ID de Pedido';
$_['column_transaction_reference']               = 'Referencia de transacción';
$_['column_customer']                            = 'Cliente';
$_['column_total']                               = 'Total';
$_['column_currency']                            = 'Moneda';
$_['column_settle_status']                       = 'Estado de establecimiento';
$_['column_status']                              = 'Estado';
$_['column_type']                                = 'Tipo';
$_['column_payment_type']                        = 'Tipo de pago';

$_['error_permission']                           = 'No tiene permisos para modificar este módulo';
$_['error_site_reference']                       = 'Refrencia de sitio necesaria';
$_['error_cards_accepted']                       = 'Tarjetas aceptadas es necesario';
$_['error_username']                             = 'Nombre de Usuario es necesario';
$_['error_password']                             = 'Contraseña es necesaria';
$_['error_connection']                           = 'No se pudo conectar con Secure Trading';
$_['error_data_missing']                         = 'Datos faltantes';

$_['help_refund']                                = 'Por favor incluya el punto decimal y la parte decimal del monto';
$_['help_csv_username']                          = 'Nombre de Usuario del servicio de descarga de Transacción';
$_['help_csv_password']                          = 'Contraseña del servicio de descarga de Transacción';
$_['help_total']                                 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';

$_['button_reverse_authorisation']               = 'Autorización revertida';
$_['button_refund']                              = 'Reintegrar';
$_['button_show']                                = 'Mostrar';
$_['button_download']                            = 'Descarga';
$_['button_release']                             = 'Liberar';
$_['button_rebate']                              = 'Reembolsar / reintegrar';
$_['button_void']                                = 'Autorización revertida';