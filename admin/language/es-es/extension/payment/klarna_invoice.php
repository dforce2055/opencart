<?php
// Heading
$_['heading_title']			= 'Klarna Factura';

// Text
$_['text_extension']		= 'Extensiones';
$_['text_success']			= 'Éxito: ¡Ha modificado el módulo de pago Klarna Pago!';
$_['text_edit']             = 'Editar Klarna Factura';
$_['text_klarna_invoice']	= '<a href="https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart" target="_blank"><img src="https://cdn.klarna.com/public/images/global/logos/v1/basic/global_basic_logo_std_blue-black.png?width=60&eid=opencart" alt="Klarna Factura" title="Klarna Factura" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']				= 'Funcionando';
$_['text_beta']				= 'Beta';
$_['text_sweden']			= 'Suecia';
$_['text_norway']			= 'Noruega';
$_['text_finland']			= 'Finlandia';
$_['text_denmark']			= 'Dinamarca';
$_['text_germany']			= 'Alemania';
$_['text_netherlands']		= 'Paises Bajoss';

// Entry
$_['entry_merchant']		= 'ID de Vendedor Klarna';
$_['entry_secret']			= 'Secreto Klarna';
$_['entry_server']			= 'Servidor';
$_['entry_total']			= 'Total';
$_['entry_pending_status']	= 'Estado pendiente';
$_['entry_accepted_status'] = 'Estado aceptado';
$_['entry_geo_zone']		= 'Geo Zona';
$_['entry_status']			= 'Estado';
$_['entry_sort_order']		= 'Orden de Clasificación';

// Ayuda
$_['help_merchant']			= '(id de eTienda) a utilizar con el servicio Klarna (provisto por Klarna).';
$_['help_secret']			= 'Secreto compartido a utilizar con el servicio Klarna (provisto por Klarna).';
$_['help_total']			= 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']		= 'Advertencia: ¡No tiene permisos para modificar pago con Klarna Pago!';