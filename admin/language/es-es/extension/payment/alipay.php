<?php
// Heading
$_['heading_title']                  = 'Alipay Pay';

// Text
$_['text_extension']                 = 'Extensiones';
$_['text_success']                   = 'Éxito: ¡Ha modificado sus detalles de cuenta Alipay!';
$_['text_edit']                      = 'Editar Alipay Pay';
$_['text_alipay']                    = '<a target="_BLANK" href="https://open.alipay.com"><img src="view/image/payment/alipay.png" alt="Sitio web Alipay Pay" title="Sitio web Alipay Pay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']                      = 'Funcionando';
$_['text_sandbox']                   = 'Sandbox';

// Entry
$_['entry_app_id']                   = 'ID de App';
$_['entry_merchant_private_key']     = 'Clave privada de vendedor';
$_['entry_alipay_public_key']        = 'Clave pública de Alipay';
$_['entry_test']                     = 'Modo de prueba';
$_['entry_total']                    = 'Total';
$_['entry_order_status']             = 'Estado completado';
$_['entry_geo_zone']                 = 'Geo Zona';
$_['entry_status']                   = 'Estado';
$_['entry_sort_order']               = 'Orden de Clasificación';

// Ayuda
$_['help_total']                     = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';
$_['help_alipay_setup']              = '<a target="_blank" href="http://www.opencart.cn/docs/alipay">Haga click aquí</a> para aprender cómo establecer una cuenta Alipay.';

// Error
$_['error_permission']               = 'Advertencia: ¡No tiene permisos para modificar pago con Alipay!';
$_['error_app_id']                   = 'ID de App necesario!';
$_['error_merchant_private_key']     = '¡Clave privada de vendedor necesaria!';
$_['error_alipay_public_key']        = '¡Clave pública de Alipay necesaria!';
