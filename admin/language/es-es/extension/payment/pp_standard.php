<?php
// Heading
$_['heading_title']					 = 'PayPal Pagos Standard';

// Text
$_['text_extension']				 = 'Extensiones';
$_['text_success']					 = 'Éxito: ¡Ha modificado sus detalles de cuenta PayPal!';
$_['text_edit']                      = 'Editar PayPal Pagos Standard';
$_['text_pp_standard']				 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Autorización';
$_['text_sale']						 = 'Venta'; 

// Entry
$_['entry_email']					 = 'E-Mail';
$_['entry_test']					 = 'Modo sandbox';
$_['entry_transaction']				 = 'Método de transacción';
$_['entry_debug']					 = 'Modo debug';
$_['entry_total']					 = 'Total';
$_['entry_canceled_reversal_status'] = 'Estado revocación cancelada';
$_['entry_completed_status']		 = 'Estado completado';
$_['entry_denied_status']			 = 'Estado denegado';
$_['entry_expired_status']			 = 'Estado expirado';
$_['entry_failed_status']			 = 'Estado fallado';
$_['entry_pending_status']			 = 'Estado pendiente';
$_['entry_processed_status']		 = 'Estado procesado';
$_['entry_refunded_status']			 = 'Estado reintegrado';
$_['entry_reversed_status']			 = 'Estado devuelto';
$_['entry_voided_status']			 = 'Estado anulado';
$_['entry_geo_zone']				 = 'Geo Zona';
$_['entry_status']					 = 'Estado';
$_['entry_sort_order']				 = 'Orden de Clasificación';

// Tab
$_['tab_general']					 = 'General';
$_['tab_order_status']       		 = 'Estado de pedido';

// Ayuda
$_['help_test']						 = '¿Utilizar el servidor de producción o de prueba (sandbox) para transacciones?';
$_['help_debug']			    	 = 'Registra información adicional en el registro de sistema';
$_['help_total']					 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';

// Error
$_['error_permission']				 = 'Advertencia: ¡No tiene permisos para modificar pago con PayPal!';
$_['error_email']					 = '¡E-Mail necesario!';