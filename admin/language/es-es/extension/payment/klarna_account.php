<?php
// Heading
$_['heading_title']			= 'Cuenta Klarna';

// Text
$_['text_extension']		= 'Extensiones';
$_['text_success']			= 'Éxito: ¡Ha modificado el módulo de pago Klarna!';
$_['text_edit']             = 'Editar Cuenta Klarna';
$_['text_klarna_account']	= '<a href="https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart" target="_blank"><img src="https://cdn.klarna.com/public/images/global/logos/v1/basic/global_basic_logo_std_blue-black.png?width=60&eid=opencart" alt="Klarna Cuenta" title="Klarna Cuenta" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']				= 'Funcionando';
$_['text_beta']				= 'Beta';
$_['text_sweden']			= 'Suecia';
$_['text_norway']			= 'Noruega';
$_['text_finland']			= 'Finlandia';
$_['text_denmark']			= 'Dinamarca';
$_['text_germany']			= 'Alemania';
$_['text_netherlands']		= 'Paises Bajoss';

// Entry
$_['entry_merchant']		= 'ID de Vendedor Klarna';
$_['entry_secret']			= 'Secreto Klarna';
$_['entry_server']			= 'Servidor';
$_['entry_total']			= 'Total';
$_['entry_pending_status']	= 'Estado pendiente';
$_['entry_accepted_status'] = 'Estado aceptado';
$_['entry_geo_zone']		= 'Geo Zona';
$_['entry_status']			= 'Estado';
$_['entry_sort_order']		= 'Orden de Clasificación';

// Ayuda
$_['help_merchant']			= '(id de eTienda) a utilizar con el servicio Klarna (provisto por Klarna).';
$_['help_secret']			= 'Secreto compartido a utilizar con el servicio Klarna (provisto por Klarna).';
$_['help_total']			= 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']		= 'Advertencia: ¡No tiene permisos para modificar pago con Klarna Pago!';
$_['error_pclass']			= 'No se pudo obtener pClass oara %s. Código de error: %s; Mensaje de error: %s';
$_['error_curl']			= 'Error Curl - Código: %d; Mensaje: %s';
$_['error_log']				= 'Ocurrieron errores al actualizar el mídulo. Por favor verifique el archivo de registro.';