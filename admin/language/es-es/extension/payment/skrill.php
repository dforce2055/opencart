<?php
// Heading
$_['heading_title']			  = 'Skrill';

// Text
$_['text_extension']		  = 'Extensiones';
$_['text_success']			  = 'Éxito: ¡Ha modificado sus detalles de Skrill.';
$_['text_edit']               = 'Editar Skrill';
$_['text_skrill']	     	  = '<a href="https://content.skrill.com/en/ecommerce-solutions/opencart/" target="_blank"><img src="view/image/payment/skrill.png" alt="Skrill" title="Skrill" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_email']			  = 'E-Mail';
$_['entry_secret']		      = 'Secreto';
$_['entry_total']			  = 'Total';
$_['entry_order_status']	  = 'Estado de pedido';
$_['entry_pending_status']	  = 'Pendiente Estado ';
$_['entry_canceled_status']	  = 'Cancelado Estado';
$_['entry_failed_status']	  = 'Fallado Estado';
$_['entry_chargeback_status'] = 'Devolución de Cargos Estado';
$_['entry_geo_zone']		  = 'Geo Zona';
$_['entry_status']			  = 'Estado';
$_['entry_sort_order']		  = 'Orden de Clasificación';

// Ayuda
$_['help_total']			  = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']		  = 'Advertencia: ¡No tiene permisos para modificar Skrill!';
$_['error_email']			  = '¡E-Mail necesario!';