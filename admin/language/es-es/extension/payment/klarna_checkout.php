<?php
// Heading
$_['heading_title']						 = 'Procesar pago con Klarna';

// Text
$_['text_extension']					 = 'Extensiones';
$_['text_success']						 = 'Éxito: ¡Ha modificado sus detalles de cuenta Procesar pago con Klarna!';
$_['text_edit']							 = 'Editar Procesar pago con Klarna';
$_['text_live']							 = 'Funcionando';
$_['text_test']							 = 'Prueba';
$_['text_payment_info']					 = 'Información de pago';
$_['text_na']							 = 'N/A';
$_['text_confirm_cancel']				 = '¿Está seguro de que desea cancelar esta transacción?';
$_['text_confirm_capture']				 = '¿Está seguro de que desea capturar?';
$_['text_confirm_refund']				 = '¿Está seguro de que desea reintegrar';
$_['text_confirm_extend_authorization']	 = '¿Está seguro de que desea extender la autorización?';
$_['text_confirm_release_authorization'] = '¿Está seguro de que desea liberar la autorización restante?';
$_['text_confirm_merchant_reference']	 = '¿Está seguro de que desea actualizar la referencia de vendedor 1?';
$_['text_confirm_shipping_info']		 = '¿Está seguro de que desea guardar información de envío?';
$_['text_confirm_billing_address']		 = '¿Está seguro de que desea actualizar la dirección de facturación?';
$_['text_confirm_shipping_address']		 = '¿Está seguro de que desea extender la dirección de facturación?';
$_['text_confirm_trigger_send_out']		 = '¿Está seguro de que desea disparar un nuevo despacho?';
$_['text_confirm_settlement']			 = '¿Está seguro de que desea procesar archivos de establecimiento?';
$_['text_no_capture']					 = 'Sin capturas';
$_['text_no_refund']					 = 'Sin reembolsos';
$_['text_success_action']				 = 'Éxito';
$_['text_error_generic']				 = 'Error: Ocurrió un error con su solicitud.';
$_['text_capture_shipping_info_title']	 = 'Captura %d - Info de envío';
$_['text_capture_billing_address_title'] = 'Capture %d - Dirección de facturación';
$_['text_new_capture_title']			 = 'Nueva captura';
$_['text_new_refund_title']				 = 'Nuevo reembolso';
$_['text_downloading_settlement']		 = 'Descargando archivos de establecimiento...';
$_['text_processing_orders']			 = 'Pedidos en proceso...';
$_['text_processing_order']				 = 'Pedido en proceso';
$_['text_no_files']						 = 'No hay archivos para descargar.';
$_['text_version']						 = '1.2';

// Column
$_['column_order_id']					 = 'ID de Pedido';
$_['column_capture_id']					 = 'ID de captura';
$_['column_reference']					 = 'Referencia Klarna';
$_['column_status']						 = 'Estado';
$_['column_merchant_reference_1']		 = 'Referencia de vendedor 1';
$_['column_customer_details']			 = 'Detalles de cliente';
$_['column_billing_address']			 = 'Detalles de facturación';
$_['column_shipping_address']			 = 'Detalles de envío';
$_['column_order_lines']				 = 'Detalles de item';
$_['column_amount']						 = 'Monto';
$_['column_authorization_remaining']	 = 'Autorización restante';
$_['column_authorization_expiry']		 = 'Fecha de expiración Autorización';
$_['column_item_reference']				 = 'Referencia';
$_['column_type']						 = 'Tipo';
$_['column_quantity']					 = 'Cantidad';
$_['column_quantity_unit']				 = 'Unidad de Cantidad';
$_['column_name']						 = 'Nombre';
$_['column_total_amount']				 = 'Monto total';
$_['column_unit_price']					 = 'Precio unitario';
$_['column_total_discount_amount']		 = 'Monto descontado total';
$_['column_tax_rate']					 = 'Tasa de impuestos';
$_['column_total_tax_amount']			 = 'Monto total de impuestos';
$_['column_action']						 = 'Acción';
$_['column_cancel']						 = 'Cancelar';
$_['column_capture']					 = 'Capturas';
$_['column_refund']						 = 'Reintegros';
$_['column_date']						 = 'Fecha';
$_['column_title']						 = 'Título';
$_['column_given_name']					 = 'Nombre';
$_['column_family_name']				 = 'Apellido';
$_['column_street_address']				 = 'Dirección';
$_['column_street_address2']			 = 'Dirección 2';
$_['column_city']						 = 'Ciudad';
$_['column_postal_code']				 = 'Código postal';
$_['column_region']						 = 'Región';
$_['column_country']					 = 'País';
$_['column_email']						 = 'Email';
$_['column_phone']						 = 'Teléfono';
$_['column_action']						 = 'Acción';
$_['column_shipping_info']				 = 'Info de envío';
$_['column_shipping_company']			 = 'Compañía de envío';
$_['column_shipping_method']			 = 'Método de envío';
$_['column_tracking_number']			 = 'Número de Rastreo(Tracking)';
$_['column_tracking_uri']				 = 'URI de Rastreo(Tracking)';
$_['column_return_shipping_company']	 = 'Compañía de envío de retorno';
$_['column_return_tracking_number']		 = 'Número Rastreo(Tracking) de retorno';
$_['column_return_tracking_uri']		 = 'URI de Rastreo(Tracking) de retorno';

// Entry
$_['entry_debug']						 = 'Registro de debug';
$_['entry_colour_button']				 = 'Color de fondo de botón';
$_['entry_colour_button_text']			 = 'Color de texto de botón';
$_['entry_colour_checkbox']				 = 'Color de fondo de casilla';
$_['entry_colour_checkbox_checkmark']	 = 'Color de tick de casilla';
$_['entry_colour_header']				 = 'Color de texto de encabezado';
$_['entry_colour_link']					 = 'Color de texto de vínculo';
$_['entry_separate_shipping_address']	 = 'Permitir dirección de envío separada';
$_['entry_dob_mandatory']				 = 'Fecha de nacimiento obligatoria (Sólo tiendas de UK)';
$_['entry_title_mandatory']				 = 'Título obligatorio (Sólo tiendas de UK)';
$_['entry_additional_text_box']			 = 'Permitir registrarse en boletín de noticias';

$_['entry_total']						 = 'Total';
$_['entry_order_status']				 = 'Estado de pedido';
$_['entry_order_status_authorised']		 = 'Autorizado';
$_['entry_order_status_part_captured']	 = 'Parcialmente capturado';
$_['entry_order_status_captured']		 = 'Capturado';
$_['entry_order_status_cancelled']		 = 'Cancelado';
$_['entry_order_status_refund']			 = 'Reintegro total';
$_['entry_order_status_fraud_rejected']	 = 'Fraude - Rechazado';
$_['entry_order_status_fraud_pending']	 = 'Fraude - Pendiente Decisión';
$_['entry_order_status_fraud_accepted']	 = 'Fraude - Aceptado';
$_['entry_status']						 = 'Estado';
$_['entry_terms']						 = 'Términos & Condiciones';
$_['entry_locale']						 = 'Localización';
$_['entry_currency']					 = 'Moneda';
$_['entry_merchant_id']					 = 'ID de Vendedor (MID)';
$_['entry_secret']						 = 'Secreto compartido';
$_['entry_environment']					 = 'Entorno (Producción/Prueba';
$_['entry_country']						 = 'País';
$_['entry_shipping']					 = 'Países de envío';
$_['entry_api']							 = 'Localización API';
$_['entry_shipping_company']			 = 'Compañía de envío';
$_['entry_shipping_method']				 = 'Método de envío';
$_['entry_tracking_number']				 = 'Número de Rastreo(Tracking)';
$_['entry_tracking_uri']				 = 'URI de Rastreo(Tracking)';
$_['entry_return_shipping_company']		 = 'Compañía de envío de retorno';
$_['entry_return_tracking_number']		 = 'Número Rastreo(Tracking) de retorno';
$_['entry_return_tracking_uri']			 = 'URI de Rastreo(Tracking) de retorno';
$_['entry_sftp_username']				 = 'Nombre de Usuario SFTP';
$_['entry_sftp_password']				 = 'Contraseña SFTP';
$_['entry_process_settlement']			 = 'Archivos de procesamiento de establecimientos';
$_['entry_settlement_order_status']		 = 'Estado de pedido';
$_['entry_version']						 = 'Versión de extensión';

// Ayuda
$_['help_debug']						 = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario.';
$_['help_separate_shipping_address']	 = 'En caso afirmativo, el cliente puede ingresar direcciones de facturación y entrega diferentes.';
$_['help_dob_mandatory']				 = 'En caso afirmativo, el cliente no puede saltear la fecha de nacimiento.';
$_['help_title_mandatory']				 = 'En caso negativo, el título se vuelve opcional en países que requieren un título por defecto.';
$_['help_additional_text_box']			 = 'Permitir el registro en boletín informativo dentro del iFrame para usuarios logueados.';
$_['help_total']						 = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';
$_['help_locale']						 = 'Codigo de localización para cada cuenta. Código de idioma de dos letras combinados con código de país de dos letras en acuerdo con RFC 1766 (por ejemplo en-gb para inglés británico o sv-se para sueco)';
$_['help_api']							 = 'Para usuarios europeos, Procesar pago con Klarna no provee la región de cliente. Esto significa que las opciones de envío específicas a región no funcionarán (Envíos específicos de país sí)';
$_['help_sftp_username']				 = 'Nombre de Usuario SFTP provisto por su administrador de cuentas';
$_['help_sftp_password']				 = 'Contraseña SFTP provisto por su administrador de cuentas';
$_['help_settlement_order_status']		 = 'Estado de pedido al que cambiaran pedidos de establecimiento procesados.';
$_['help_shipping']						 = 'Todos los países dentro de esta Geo Zona estarán disponibles para selección dentro del iFrame Klarna.';

// Button
$_['button_account_remove']				 = 'Quitar cuenta';
$_['button_account_add']				 = 'Agregar cuenta';
$_['button_capture']					 = 'Captura';
$_['button_refund']						 = 'Reintegrar';
$_['button_extend_authorization']		 = 'Extender';
$_['button_release_authorization']		 = 'Liberar';
$_['button_update']						 = 'Actualizar';
$_['button_add_shipping_info']			 = 'Agregar información de envío';
$_['button_trigger_send_out']			 = 'Disparar nuevo despacho';
$_['button_edit_shipping_info']			 = 'Editar info de envío';
$_['button_edit_billing_address']		 = 'Editar dirección de facturación';
$_['button_new_capture']				 = 'Nueva captura';
$_['button_new_refund']					 = 'Nuevo reembolso';
$_['button_process_settlement']			 = 'Archivos de procesamiento de establecimientos';

// Error
$_['error_warning']						 = 'Advertencia: ¡Por favor compruebe el formulario cuidadosamente en busca de errores!';
$_['error_php_version']					 = '¡Se necesita al menos versión PHP 5.4.0!';
$_['error_ssl']							 = 'Debe activar "Usar SSL" en la configuración de tienda y tener un certificado SSL instalado!';
$_['error_account_minimum']				 = 'Por favor agregue al menos una cuenta.';
$_['error_locale']						 = 'Por favor ingrese una localización válida.';
$_['error_account_currency']			 = 'Lista de cuenta contiene una o más monedas duplicadas.';
$_['error_merchant_id']					 = '¡ID de Vendedor necesaria!';
$_['error_secret']						 = '¡Secreto compartido necesario!';
$_['error_tax_warning']					 = 'Advertencia: Algunos de sus productos usan una clase de impuesto basada en dirección de pago. Procesar pago con Klarna no funccionará cuando cualquiera de ellos se ingrese en el carro de compras.';

// Tab
$_['tab_setting']						 = 'Configuraciones';
$_['tab_order_status']					 = 'Estados de pedido';
$_['tab_account']						 = 'Cuentas';
$_['tab_settlement']					 = 'Establecimiento';
