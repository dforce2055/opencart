<?php
// Heading
$_['heading_title']			   = 'Buscar transacciones';

// Text
$_['text_pp_express']		   = 'Proceso de compra con PayPal Express';
$_['text_date_search']		   = 'Buscar por fecha';
$_['text_searching']		   = 'Buscando';
$_['text_name']				   = 'Nombre';
$_['text_buyer_info']		   = 'Información de comprador';
$_['text_view']				   = 'Ver';
$_['text_format']			   = 'Formato';

// Column
$_['column_date']			   = 'Fecha';
$_['column_type']			   = 'Tipo';
$_['column_email']			   = 'Email';
$_['column_name']			   = 'Nombre';
$_['column_transid']		   = 'ID de transacción';
$_['column_status']			   = 'Estado';
$_['column_currency']		   = 'Moneda';
$_['column_amount']			   = 'Monto';
$_['column_fee']			   = 'Gasto';
$_['column_netamt']		       = 'Monto neto';
$_['column_action']		       = 'Acción';

// Entry
$_['entry_trans_all']		   = 'Todas';
$_['entry_trans_sent']		   = 'Enviadas';
$_['entry_trans_received']     = 'Recibidas';
$_['entry_trans_masspay']	   = 'Pago masivo';
$_['entry_trans_money_req']	   = 'Solicitud de dinero';
$_['entry_trans_funds_add']	   = 'Fondos añadidos';
$_['entry_trans_funds_with']   = 'Fondos retirados';
$_['entry_trans_referral']	   = 'Referencia';
$_['entry_trans_fee']		   = 'Gasto';
$_['entry_trans_subscription'] = 'Subscripción';
$_['entry_trans_dividend']     = 'Dividendo';
$_['entry_trans_billpay']      = 'Pago de factura';
$_['entry_trans_refund']       = 'Reintegro';
$_['entry_trans_conv']         = 'Conversión de moneda';
$_['entry_trans_bal_trans']	   = 'Transferencia de balance';
$_['entry_trans_reversal']	   = 'Revocación';
$_['entry_trans_shipping']	   = 'Envío';
$_['entry_trans_bal_affect']   = 'Afectando balance';
$_['entry_trans_echeque']	   = 'E Cheque';
$_['entry_date']			   = 'Fecha';
$_['entry_date_start']		   = 'Inicio';
$_['entry_date_end']		   = 'Fin';
$_['entry_date_to']			   = 'hasta';
$_['entry_transaction']		   = 'Transacción';
$_['entry_transaction_type']   = 'Tipo';
$_['entry_transaction_status'] = 'Estado';
$_['entry_email']			   = 'Email';
$_['entry_email_buyer']		   = 'Comprador';
$_['entry_email_merchant']	   = 'Recipiente';
$_['entry_receipt']			   = 'ID de recibo';
$_['entry_transaction_id']	   = 'ID de transacción';
$_['entry_invoice_no']		   = 'Número de Factura';
$_['entry_auction']			   = 'Número de item de subasta';
$_['entry_amount']			   = 'Monto';
$_['entry_recurring_id']	   = 'ID de perfil recurrente';
$_['entry_salutation']		   = 'Saludo';
$_['entry_firstname']		   = 'Nombre';
$_['entry_middlename']		   = 'Nombre';
$_['entry_lastname']		   = 'Apellido';
$_['entry_suffix']			   = 'Sufijo';
$_['entry_status_all']		   = 'Todas';
$_['entry_status_pending']	   = 'Pendiente';
$_['entry_status_processing']  = 'En Proceso';
$_['entry_status_success']	   = 'Éxito';
$_['entry_status_denied']	   = 'Denegado';
$_['entry_status_reversed']	   = 'Devuelto';