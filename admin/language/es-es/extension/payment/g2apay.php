<?php
// Heading
$_['heading_title']				 	  = 'G2APay';

// Text
$_['text_extension']				  = 'Extensiones';
$_['text_success']				 	  = 'Éxito: ¡Ha modificado sus detalles de G2APay.';
$_['text_edit']					 	  = 'Editar G2APay';
$_['text_g2apay']				 	  = '<a href="https://pay.g2a.com/" target="_blank"><img src="view/image/payment/g2apay.png" alt="G2APay" title="G2APay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_payment_info']			 	  = 'Información de pago';
$_['text_refund_status']		 	  = 'Reintegro de pago';
$_['text_order_ref']			 	  = 'Order ref';
$_['text_order_total']			 	  = 'Total autorizado';
$_['text_total_released']		 	  = 'Total librado';
$_['text_transactions']			 	  = 'Transacciones';
$_['text_column_amount']		 	  = 'Monto';
$_['text_column_type']			 	  = 'Tipo';
$_['text_column_date_added']	 	  = 'Añadido';
$_['text_refund_ok']			 	  = 'Reintegro solicitado exitosamente';
$_['text_refund_ok_order']		 	  = 'Reintegro solicitado exitosamente, monto reintegrado en su totalidad';

// Entry
$_['entry_username']			 	  = 'Nombre de Usuario';
$_['entry_secret']				 	  = 'Secreto';
$_['entry_api_hash']		     	  = 'Hash de API';
$_['entry_environment']			 	  = 'Ambiente';
$_['entry_secret_token']		 	  = 'Token Secreto';
$_['entry_ipn_url']				 	  = 'URL IPN:';
$_['entry_total']				 	  = 'Total';
$_['entry_geo_zone']			 	  = 'Geo Zona';
$_['entry_status']				 	  = 'Estado';
$_['entry_sort_order']			 	  = 'Orden de Clasificación';
$_['entry_debug']				 	  = 'Registro de debug';
$_['entry_order_status']			  = 'Estado de pedido';
$_['entry_complete_status']			  = 'Estado completado:';
$_['entry_rejected_status']			  = 'Estado rechazado:';
$_['entry_cancelled_status']		  = 'Estado cancelado:';
$_['entry_pending_status']            = 'Estado pendiente:';
$_['entry_refunded_status']			  = 'Estado reintegrado:';
$_['entry_partially_refunded_status'] = 'Estado parcialmente reintegrado:';

// Ayuda
$_['help_username']					  = 'El email utilizado para su cuenta';
$_['help_total']				 	  = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';
$_['help_debug']				 	  = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario';

// Tab
$_['tab_settings']				 	  = 'Configuraciones';
$_['tab_order_status']				  = 'Estado de pedido';

// Error
$_['error_permission']			 	  = 'Advertencia: ¡No tiene permisos para modificar G2APay!';
$_['error_email']				 	  = '¡E-Mail necesario!';
$_['error_secret']				 	  = 'Secreto necesario!';
$_['error_api_hash']			 	  = 'Hash de API necesario!';
$_['entry_status']				 	  = 'Estado';

//Button
$_['btn_refund']				 	  = 'reintegrar';

$_['g2apay_environment_live']	 	  = 'Funcionando';
$_['g2apay_environment_test']	 	  = 'Prueba';