<?php
// Heading
$_['heading_title']				         = 'Worldpay Online Payments';

// Text
$_['text_extension']				     = 'Extensiones';
$_['text_success']				         = 'Éxito: ¡Ha modificado sus detalles de cuenta Worldpay!';
$_['text_worldpay']				         = '<a href="https://online.worldpay.com/signup/ee48b6e6-d3e3-42aa-a80e-cbee3f4f8b09" target="_blank"><img src="view/image/payment/worldpay.png" alt="Worldpay" title="Worldpay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_test']					         = 'Prueba';
$_['text_live']					         = 'Funcionando';
$_['text_authenticate']			         = 'Autenticar';
$_['text_release_ok']		 	         = 'Liberación exitosa';
$_['text_release_ok_order']		         = 'Liberación exitosa, estado de pedido actualizado a exitoso - establecido';
$_['text_refund_ok']			         = 'Reembolso exitoso';
$_['text_refund_ok_order']		         = 'Reembolso exitoso, estado de pedido actualizado a reintegrado';
$_['text_void_ok']				         = 'Anulación exitosa, estado de pedido actualizado a anulado';

// Entry
$_['entry_service_key']			         = 'Clave de servicio';
$_['entry_client_key']			         = 'Clave de cliente';
$_['entry_total']				         = 'Total';
$_['entry_order_status']		         = 'Estado de pedido';
$_['entry_geo_zone']			         = 'Geo Zona';
$_['entry_status']				         = 'Estado';
$_['entry_sort_order']			         = 'Orden de Clasificación';
$_['entry_debug']				         = 'Registro de debug';
$_['entry_card']				         = 'Tarjetas de tienda';
$_['entry_secret_token']		         = 'Token Secreto';
$_['entry_webhook_url']			         = 'URL Webhook:';
$_['entry_cron_job_url']		         = 'URL Cron Job:';
$_['entry_last_cron_job_run']	         = 'Último tiempo de ejecución de cron job:';
$_['entry_success_status']		         = 'Estado Éxito:';
$_['entry_failed_status']		         = 'Estado fallado:';
$_['entry_settled_status']			     = 'Estado establecido:';
$_['entry_refunded_status']			     = 'Estado reintegrado:';
$_['entry_partially_refunded_status']	 = 'Estado parcialmente reintegrado:';
$_['entry_charged_back_status']			 = 'Cargos devueltos:';
$_['entry_information_requested_status'] = 'Estado información solicitada:';
$_['entry_information_supplied_status']	 = 'Estado información suministrada:';
$_['entry_chargeback_reversed_status']	 = 'Estado Devolución de Cargos revertido:';


$_['entry_reversed_status']			     = 'Estado devuelto:';
$_['entry_voided_status']			     = 'Estado anulado:';

// Ayuda
$_['help_total']				         = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';
$_['help_debug']				         = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario';
$_['help_secret_token']			         = 'Debería ser largo y difícil de adivinar';
$_['help_webhook_url']			         = 'Setear webhooks de Worldpay a llamar a esta URL';
$_['help_cron_job_url']			         = 'Setear un cron job que llame a esta URL';

// Tab
$_['tab_settings']				         = 'Configuraciones';
$_['tab_order_status']			         = 'Estado de pedido';

// Error
$_['error_permission']			         = 'Advertencia: ¡No tiene permisos para modificar pago con Worldpay!';
$_['error_service_key']			         = '¡Clave de servicio necesaria!';
$_['error_client_key']			         = '¡Clave de cliente necesaria!';

// Order page - payment tab
$_['text_payment_info']			         = 'Información de pago';
$_['text_refund_status']		         = 'Reintegro de pago';
$_['text_order_ref']			         = 'Referencia de pedido';
$_['text_order_total']			         = 'Total autorizado';
$_['text_total_released']		         = 'Total liberado';
$_['text_transactions']			         = 'Transacciones';
$_['text_column_amount']		         = 'Monto';
$_['text_column_type']			         = 'Tipo';
$_['text_column_date_added']	         = 'Añadido';

$_['text_confirm_refund']		         = 'Está seguro de que desea reintegrar el pago?';

$_['button_refund']				         = 'Reembolsar / reintegrar';

