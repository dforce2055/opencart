<?php
// Heading
$_['heading_title']					 = 'Realex Redirect';

// Text
$_['text_extension']				 = 'Extensiones';
$_['text_success']					 = 'Éxito: ¡Ha modificado sus detalles de cuenta Realex!';
$_['text_edit']                      = 'Editar Realex Redirect';
$_['text_live']						 = 'Funcionando';
$_['text_demo']						 = 'Demo';
$_['text_card_type']				 = 'Tipo de tarjeta';
$_['text_enabled']					 = 'Activado';
$_['text_use_default']				 = 'Usar por defecto';
$_['text_merchant_id']				 = 'ID de Vendedor';
$_['text_subaccount']				 = 'Subcuenta';
$_['text_secret']					 = 'Secreto compartido';
$_['text_card_visa']				 = 'Visa';
$_['text_card_master']				 = 'Mastercard';
$_['text_card_amex']				 = 'American Express';
$_['text_card_switch']				 = 'Switch/Maestro';
$_['text_card_laser']				 = 'Laser';
$_['text_card_diners']				 = 'Diners';
$_['text_capture_ok']				 = 'Captura exitosa';
$_['text_capture_ok_order']			 = 'Captura exitosa, estado de pedido actualizado a exitoso - establecido';
$_['text_rebate_ok']				 = 'Reembolso exitoso';
$_['text_rebate_ok_order']			 = 'Reembolso exitoso, estado de pedido actualizado a reembolsado';
$_['text_void_ok']					 = 'Anulación exitosa, estado de pedido actualizado a anulado';
$_['text_settle_auto']				 = 'Auto';
$_['text_settle_delayed']			 = 'Retrasado';
$_['text_settle_multi']				 = 'Multi';
$_['text_url_message']				 = 'Debe suministrar la URL de tienda a su administrador de cuenta Realex antes del lanzamiento';
$_['text_payment_info']				 = 'Información de pago';
$_['text_capture_status']			 = 'Pago capturado';
$_['text_void_status']				 = 'Pago anulado';
$_['text_rebate_status']			 = 'Pago reembolsado';
$_['text_order_ref']				 = 'Referencia de pedido';
$_['text_order_total']				 = 'Total autorizado';
$_['text_total_captured']			 = 'Total capturado';
$_['text_transactions']				 = 'Transacciones';
$_['text_column_amount']			 = 'Monto';
$_['text_column_type']				 = 'Tipo';
$_['text_column_date_added']		 = 'Creado';
$_['text_confirm_void']				 = '¿Está seguro de que desea anular el pago?';
$_['text_confirm_capture']			 = '¿Está seguro de que desea capture el pago?';
$_['text_confirm_rebate']			 = '¿Está seguro de que desea reembolsar el pago?';
$_['text_realex']					 = '<a target="_blank" href="http://www.realexpayments.co.uk/partner-refer?id=opencart"><img src="view/image/payment/realex.png" alt="Realex" title="Realex" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_merchant_id']				 = 'ID de Vendedor';
$_['entry_secret']					 = 'Secreto compartido';
$_['entry_rebate_password']			 = 'Contraseña de reembolso';
$_['entry_total']					 = 'Total';
$_['entry_sort_order']				 = 'Orden de clasificación';
$_['entry_geo_zone']				 = 'Geo Zona';
$_['entry_status']					 = 'Estado';
$_['entry_debug']					 = 'Registro de debug';
$_['entry_live_demo']				 = 'Producción / Demo';
$_['entry_auto_settle']				 = 'Tipo de establecimiento';
$_['entry_card_select']				 = 'Selección de tarjeta';
$_['entry_tss_check']				 = 'Comprobaciones TSS';
$_['entry_live_url']				 = 'Conexión URL de producción';
$_['entry_demo_url']				 = 'Conexión URL de demo';
$_['entry_status_success_settled']	 = 'Éxito - establecido';
$_['entry_status_success_unsettled'] = 'Éxito - no establecido';
$_['entry_status_decline']			 = 'Declinado';
$_['entry_status_decline_pending']	 = 'Declinado - auth fuera de servicio';
$_['entry_status_decline_stolen']	 = 'Declinado - tarjeta extraviada o robada';
$_['entry_status_decline_bank']		 = 'Declinado - error de banco';
$_['entry_status_void']				 = 'Anulado';
$_['entry_status_rebate']			 = 'Reembolsado';
$_['entry_notification_url']		 = 'URL de notificación';

// Ayuda
$_['help_total']					 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';
$_['help_card_select']				 = 'Solicitar al usuario que seleccone su tipo de tarjeta antes de redireccionarlo';
$_['help_notification']				 = 'Debe proveer esta URL a Realex para recibir notificaciones de pagos';
$_['help_debug']					 = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario';
$_['help_dcc_settle']				 = 'Si su cuenta tiene DCC activado, debe usar Autoestablecer';

// Tab
$_['tab_api']					     = 'Detalles de API';
$_['tab_account']		     		 = 'Cuentas';
$_['tab_order_status']				 = 'Estado de pedido';
$_['tab_payment']					 = 'Ajustes de pago';
$_['tab_advanced']					 = 'Avanzado';

// Button
$_['button_capture']				 = 'Captura';
$_['button_rebate']					 = 'Reembolsar / reintegrar';
$_['button_void']					 = 'Anular';

// Error
$_['error_merchant_id']				 = 'ID de Vendedor necesaria';
$_['error_secret']					 = 'Secreto compartidoo necesario';
$_['error_live_url']				 = 'URL de producción necesaria';
$_['error_demo_url']				 = 'URL de Demo necesaria';
$_['error_data_missing']			 = 'Datos faltantes';
$_['error_use_select_card']			 = 'Debe tener "Seleccionar Tarjeta" activado para que funcione rutear subcuentas por tipo de tarjeta';