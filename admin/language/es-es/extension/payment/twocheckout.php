<?php
// Heading
$_['heading_title']		 = '2Checkout';

// Text
$_['text_extension']	 = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta 2Checkout!';
$_['text_edit']          = 'Editar 2Checkout';
$_['text_twocheckout']	 = '<a href="https://www.2checkout.com/2co/affiliate?affiliate=1596408" target="_blank"><img src="view/image/payment/2checkout.png" alt="2Checkout" title="2Checkout" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_account']		 = 'ID de cuenta 2Checkout';
$_['entry_secret']		 = 'Palabra secreta';
$_['entry_display']		 = 'Proceso de compra directo';
$_['entry_test']		 = 'Modo de Prueba';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']	 = 'Geo Zona';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden de Clasificación';

// Ayuda
$_['help_secret']		 = 'La palabra secreta utilizada para confirmar transacciones (debe ser igual a la definida en la cuenta de vendedor en la página de configuración de cuenta).';
$_['help_total']		 = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']	 = 'Advertencia: ¡No tiene permisos para modificar pago con 2Checkout!';
$_['error_account']		 = '¡Nro. de cuenta necesario!';
$_['error_secret']		 = '¡Palabra secreta necesaria!';