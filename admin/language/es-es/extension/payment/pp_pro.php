<?php
// Heading
$_['heading_title']		 = 'PayPal Pro';

// Text
$_['text_extension']     = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta PayPal Website Payment Pro Checkout!';
$_['text_edit']          = 'Editar PayPal Pro';
$_['text_pp_pro']		 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Pago Pro" title="PayPal Website Pago Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Autorización';
$_['text_sale']			 = 'Venta';

// Entry
$_['entry_username']	 = 'Nombre de Usuario de API';
$_['entry_password']	 = 'Contraseña de API';
$_['entry_signature']	 = 'Firma de API';
$_['entry_test']		 = 'Modo de Prueba';
$_['entry_transaction']	 = 'Método de transacción:';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']	 = 'Geo Zona';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden de Clasificación';

// Ayuda
$_['help_test']			 = '¿Usar servidor de producción o prueba (sandbox) para procesar transacciones?';
$_['help_total']		 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';

// Error
$_['error_permission']	 = 'Advertencia: ¡No tiene permisos para modificar pago con PayPal Website Payment Pro Checkout!';
$_['error_username']	 = '¡Nombre de Usuario de API necesario!';
$_['error_password']	 = 'Contraseña de API necesaria!';
$_['error_signature']	 = 'Firma de API necesaria!';