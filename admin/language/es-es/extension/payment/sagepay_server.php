<?php
// Heading
$_['heading_title']			  = 'SagePay Server';

// Text
$_['text_extension']		  = 'Extensiones';
$_['text_success']			  = 'Éxito: ¡Ha modificado sus detalles de cuenta SagePay!';
$_['text_edit']               = 'Editar SagePay Server';
$_['text_sagepay_server']	  = '<a href="https://support.sagepay.com/apply/default.aspx?PartnerID=E511AF91-E4A0-42DE-80B0-09C981A3FB61" target="_blank"><img src="view/image/payment/sagepay.png" alt="SagePay" title="SagePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']				  = 'Simulador';
$_['text_test']				  = 'Prueba';
$_['text_live']				  = 'Funcionando';
$_['text_defered']			  = 'Diferido';
$_['text_authenticate']		  = 'Autenticar';
$_['text_payment']		  = 'Pago';
$_['text_release_ok']		  = 'Liberación exitosa';
$_['text_release_ok_order']	  = 'Liberación exitosa, estado de pedido actualizado a exitoso - establecido';
$_['text_rebate_ok']		  = 'Reembolso exitoso';
$_['text_rebate_ok_order']	  = 'Reembolso exitoso, estado de pedido actualizado a reembolsado';
$_['text_void_ok']			  = 'Anulación exitosa, estado de pedido actualizado a anulado';
$_['text_payment_info']		  = 'Información de pago';
$_['text_release_status']	  = 'Pago liberado';
$_['text_void_status']		  = 'Pago anulado';
$_['text_rebate_status']	  = 'Pago reembolsado';
$_['text_order_ref']		  = 'Referencia de pedido';
$_['text_order_total']		  = 'Total autorizado';
$_['text_total_released']	  = 'Total librado';
$_['text_transactions']		  = 'Transacciones';
$_['text_column_amount']	  = 'Monto';
$_['text_column_type']		  = 'Tipo';
$_['text_column_date_added']  = 'Creado';
$_['text_confirm_void']		  = '¿Está seguro de que desea anular el pago?';
$_['text_confirm_release']	  = '¿Está seguro de que desea liberar el pago?';
$_['text_confirm_rebate']	  = '¿Está seguro de que desea reembolsar el pago?';

// Entry
$_['entry_vendor']			  = 'Vendedor';
$_['entry_test']			  = 'Modo de Prueba';
$_['entry_transaction']		  = 'Método de transacción';
$_['entry_total']			  = 'Total';
$_['entry_order_status']	  = 'Estado de pedido';
$_['entry_geo_zone']		  = 'Geo Zona';
$_['entry_status']			  = 'Estado';
$_['entry_sort_order']		  = 'Orden de Clasificación';
$_['entry_debug']			  = 'Registro de debug';
$_['entry_card']			  = 'Tarjetas de tienda';
$_['entry_cron_job_token']	  = 'Token Secreto';
$_['entry_cron_job_url']	  = 'URL de Cron Job:';
$_['entry_last_cron_job_run'] = 'Última ejecución de Cron Job:';

// Ayuda
$_['help_total']			  = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';
$_['help_debug']			  = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario';
$_['help_transaction']		  = 'Método de transacción DEBE ser fijado a Pago para permitir pagos por subscripción';
$_['help_cron_job_token']	  = 'Debería ser largo y difícil de adivinar';
$_['help_cron_job_url']		  = 'Setear un cron job que llame a esta URL';

// Button
$_['button_release']		  = 'Liberar';
$_['button_rebate']			  = 'Reembolsar / reintegrar';
$_['button_void']			  = 'Anular';

// Error
$_['error_permission']		  = 'Advertencia: ¡No tiene permisos para modificar pago con SagePay!';
$_['error_vendor']			  = '¡ID de Vendedor necesaria!';