<?php
// Heading
$_['heading_title']		 = 'Perpetual Payments';

// Text
$_['text_extension']	 = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta Perpetual Payments!';
$_['text_edit']          = 'Editar Perpetual Payments';

// Entry
$_['entry_auth_id']		 = 'ID de autorización';
$_['entry_auth_pass']	 = 'Contraseña de autorización';
$_['entry_test']		 = 'Modo de Prueba';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']	 = 'Geo Zona';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden de Clasificación';

// Ayuda
$_['help_test']			 = '¿Usar este módulo en modo de prueba (SI) o producción (NO)?';
$_['help_total']		 = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']	 = 'Advertencia: ¡No tiene permisos para modificar pago con Perpetual Payments!';
$_['error_auth_id']		 = '¡ID de autorización necesaria!';
$_['error_auth_pass']	 = '¡Contraseña de autorización necesaria!';