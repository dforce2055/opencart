<?php
// Heading
$_['heading_title']                  = 'Alipay Cross-border';

// Text
$_['text_extension']                 = 'Extensiones';
$_['text_success']                   = 'Éxito: ¡Ha modificado sus detalles de cuenta Alipay!';
$_['text_edit']                      = 'Editar Alipay Pay';
$_['text_alipay_cross']              = '<a target="_BLANK" href="https://global.alipay.com"><img src="view/image/payment/alipay-cross-border.png" alt="Sitio web Alipay Pay" title="Sitio web Alipay Pay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']                      = 'Funcionando';
$_['text_sandbox']                   = 'Sandbox';

// Entry
$_['entry_app_id']                   = 'ID de afiliado';
$_['entry_merchant_private_key']     = 'Clave';
$_['entry_test']                     = 'Modo de prueba';
$_['entry_total']                    = 'Total';
$_['entry_currency']                 = 'Código de moneda';
$_['entry_order_status']             = 'Estado completado';
$_['entry_geo_zone']                 = 'Geo Zona';
$_['entry_status']                   = 'Estado';
$_['entry_sort_order']               = 'Orden de Clasificación';

// Ayuda
$_['help_total']                     = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';
$_['help_currency']                  = 'El código de moneda de pago especificado por el vendedor en el contrato. Puede agregar nuevas monedas en Sistema->Localización->Monedas si su moneda de pago no figura en la lista.';
$_['help_alipay_setup']              = '<a target="_blank" href="http://www.opencart.cn/docs/alipay">Haga click aquí</a> para aprender cómo establecer una cuenta Alipay.';

// Error
$_['error_permission']               = 'Advertencia: ¡No tiene permisos para modificar pay Alipay!';
$_['error_app_id']                   = '!ID de afiliado necesaria!';
$_['error_merchant_private_key']     = '!Clave necesaria!';
