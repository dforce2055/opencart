<?php
// Heading
$_['heading_title']                    = 'Divido';

// Text
$_['text_divido']                      = '<a href="http://divido.com" target="_blank"><img src="view/image/payment/divido.png"></a>';
$_['text_edit']                        = 'Editar Divido';
$_['text_extension']                   = 'Extensiones';
$_['text_order_info']                  = 'Info de Divido';
$_['text_success']                     = 'Éxito: ¡¡Ha modificado el módulo Divido!';
$_['text_proposal_id']                 = 'ID de propuesta';
$_['text_application_id']              = 'ID de aplicación';
$_['text_deposit_amount']              = 'Monto de depósito';

// Entry
$_['entry_order_status']               = 'Estado de pedido aprobado';
$_['entry_sort_order']                 = 'Orden de clasificación';
$_['entry_status']                     = 'Estado';
$_['entry_api_key']                    = 'Clave de API';
$_['entry_title']                      = 'Título';
$_['entry_productselection']           = 'Selección de producto';
$_['entry_planselection']              = 'Mostrar plan por defecto';
$_['entry_planlist']                   = 'Planes';
$_['entry_plans_options_all']          = 'Mostrar todos los planes';
$_['entry_plans_options_selected']     = 'Seleccionar planes por defecto';
$_['entry_products_options_all']       = 'Todos los productos';
$_['entry_products_options_selected']  = 'Sólo los productos seleccionados';
$_['entry_products_options_threshold'] = 'Todos los products por encima de un precio definido';
$_['entry_price_threshold']            = 'Límite de precio de producto';
$_['entry_cart_threshold']             = 'Límite de total de carro de compras';
$_['entry_threshold_list']             = 'Límites de plan';
$_['entry_category']                   = 'Categorías';

// Ayuda
$_['help_api_key']                     = 'Clave que lo identifica con Divido (obtenida de Divido)';
$_['help_status']                      = 'Estado del método de pago';
$_['help_order_status']                = 'Estado del pedido cuando la entidad ha aprobado';
$_['help_title']                       = 'Título de la opción de pago, mostrada en el proceso de pago';
$_['help_planselection']               = 'Elegir si se muestra el conjunto de planes por defecto o se seleccionan manualmente';
$_['help_productselection']            = 'Seleccionar qué productos se pueden financiar';
$_['help_category']                    = 'Limitar qué categorías se pueden financiar';
$_['help_cart_threshold']              = 'Límite inferior de monto en carro de compras para que Divido esté disponible';