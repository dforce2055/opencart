<?php
// Heading
$_['heading_title']			= 'Authorize.Net (SIM)';

// Text
$_['text_extension']		= 'Extensiones';
$_['text_success']			= 'Éxito: ¡Ha modificado sus detalles de cuenta Authorize.Net (SIM)!';
$_['text_edit']             = 'Editar Authorize.Net (SIM)';
$_['text_authorizenet_sim']	= '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_merchant']		= 'ID de Vendedor';
$_['entry_key']				= 'Clave de transacción';
$_['entry_callback']		= 'URL de respuesta de Relay';
$_['entry_md5']				= 'Valor de Hash MD5';
$_['entry_test']			= 'Modo de Prueba';
$_['entry_total']			= 'Total';
$_['entry_order_status']	= 'Estado de pedido';
$_['entry_geo_zone']		= 'Geo Zona';
$_['entry_status']			= 'Estado';
$_['entry_sort_order']		= 'Orden de Clasificación';

// Ayuda
$_['help_callback']			= 'Por favor inicie sesión y configure esto en <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.';
$_['help_md5']				= 'La característica de Hash MD5 le permite autenticar que una respuesta de transacción se recibe de forma segura de Authorize.Net. Por favor inicie sesión y configure esto en <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.(Opcional)';
$_['help_total']			= 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']		= 'Advertencia: ¡No tiene permisos para modificar pago con Authorize.Net (SIM)!';
$_['error_merchant']		= '¡ID de Vendedor necesaria!';
$_['error_key']				= '¡Clave de transacción necesaria!';