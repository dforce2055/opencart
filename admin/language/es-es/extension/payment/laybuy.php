<?php
// Heading
$_['heading_title']                 = 'Lay-Buy';
$_['heading_transaction_title']     = 'Transacción';

// Tab
$_['tab_settings']                  = 'Configuraciones';
$_['tab_reports']                   = 'Reportes';
$_['tab_reference']                 = 'Información de referencia';
$_['tab_customer']                  = 'Información de cliente';
$_['tab_payment']                   = 'Plan de pago';
$_['tab_modify']                    = 'Modificar plan';

// Text
$_['text_extension']                = 'Extensiones';
$_['text_success']                  = 'Éxito: ¡Ha modificado el módulo de pago Lay-Buy!';
$_['text_edit']                     = 'Editar Lay-Buy';
$_['text_laybuy']                   = '<a href="https://www.lay-buys.com" target="_blank"><img src="view/image/payment/laybuys.png" style="width:94px;height:25px" alt="Lay-Buys" title="Lay-Buys"></a>';
$_['text_cancel_success']           = 'Transacción cancelada exitosamente.';
$_['text_cancel_failure']           = 'Falló la solicitud de cancelación. ¡Por favor intente de nuevo!';
$_['text_revise_success']           = 'Solicitud de revisión exitosa.';
$_['text_revise_failure']           = 'Falló la solicitud de revisión. ¡Por favor intente de nuevo!';
$_['text_type_laybuy']              = 'Lay-Buy';
$_['text_type_buynow']              = 'Comprar-Ahora';
$_['text_status_1']                 = 'Pendiente';
$_['text_status_5']                 = 'Completado';
$_['text_status_7']                 = 'Cancelado';
$_['text_status_50']                = 'Revisión solicitada';
$_['text_status_51']                = 'Revisado';
$_['text_fetched_some']             = '%d transacción/transacciones actualizada(s) exitosamente.';
$_['text_fetched_none']             = 'No hay transacciones para actualizar.';
$_['text_transaction_details']      = 'Detalles de transacción.';
$_['text_not_found']                = 'No hay una transacción con esa ID.';
$_['text_paypal_profile_id']        = 'ID de perfil de PayPal';
$_['text_laybuy_ref_no']            = 'ID de referencia de Lay-Buy';
$_['text_order_id']                 = 'ID de Pedido';
$_['text_status']                   = 'Estado';
$_['text_amount']                   = 'Monto';
$_['text_downpayment_percent']      = 'Porcentaje de anticipo';
$_['text_month']                    = 'Mes';
$_['text_months']                   = 'Meses';
$_['text_downpayment_amount']       = 'Monto de anticipo';
$_['text_payment_amounts']          = 'Montos de pago';
$_['text_first_payment_due']        = 'Vencimiento de primer pago';
$_['text_last_payment_due']         = 'Vencimiento de último pago';
$_['text_instalment']               = 'Cuota';
$_['text_date']                     = 'Fecha';
$_['text_pp_trans_id']              = 'ID de transacción de PayPal';
$_['text_downpayment']              = 'Anticipo';
$_['text_report']                   = 'Registro de pagos';
$_['text_revise_plan']              = 'Revisar plan';
$_['texttoday']                    = 'Hoy';
$_['text_due_date']                 = 'Fecha de vencimiento';
$_['text_cancel_plan']              = 'Cancelar plan';
$_['text_firstname']                = 'Nombre';
$_['text_lastname']                 = 'Apellido';
$_['text_email']                    = 'E-Mail';
$_['text_address']                  = 'Dirección';
$_['text_suburb']                   = 'Suburbio';
$_['text_state']                    = 'Estado';
$_['text_country']                  = 'País';
$_['text_postcode']                 = 'Código postal';
$_['text_payment_info']		     	= 'Información de pago';
$_['text_no_cron_time']             = 'El cron no se ha ejecutado aún';
$_['text_comment'] 	                = 'Actualizado por Lay-Buy';
$_['text_comment_canceled'] 	    = 'Pedido canceladoo y perfil recurrente de PayPal#%s cancelado.';
$_['text_remaining'] 	            = 'Restante:';
$_['text_payment'] 	                = 'Pago';

// Column
$_['column_order_id']               = 'ID de Pedido';
$_['column_customer']               = 'Cliente';
$_['column_amount']                 = 'Monto';
$_['column_dp_percent']             = 'Porcentaje de anticipo';
$_['column_months']                 = 'Meses';
$_['column_dp_amount']              = 'Monto de anticipo';
$_['column_first_payment']          = 'Vencimiento de primer pago';
$_['column_last_payment']           = 'Vencimiento de último pago';
$_['column_status']                 = 'Estado';
$_['column_date_added']             = 'Fecha añadido';
$_['column_action']                 = 'Acción';

// Entry
$_['entry_membership_id']           = 'ID de membresía de Lay-Buys';
$_['entry_token']                   = 'Token Secreto';
$_['entry_minimum']                 = 'Anticipo mínimo (%)';
$_['entry_maximum']                 = 'Anticipo máximo (%)';
$_['entry_max_months']              = 'Meses';
$_['entry_category']                = 'Categorías permitidas';
$_['entry_product_ids']             = 'IDs de productos excluidos';
$_['entry_customer_group']          = 'Grupos de clientes permitidos';
$_['entry_logging']                 = 'Registro de debug';
$_['entry_total']                   = 'Total';
$_['entry_order_status_pending']    = 'Estado de pedido (Pendiente)';
$_['entry_order_status_canceled']   = 'Estado de pedido (Cancelado)';
$_['entry_order_status_processing'] = 'Estado de pedido (En Proceso)';
$_['entry_gateway_url']             = 'URL de Gateway';
$_['entry_api_url']                 = 'URL de API';
$_['entry_geo_zone']                = 'Geo Zona';
$_['entry_status']                  = 'Estado';
$_['entry_sort_order']              = 'Orden de Clasificación';
$_['entry_cron_url']                = 'URL de Cron Job';
$_['entry_cron_time']               = 'Última ejecución de Cron Job';
$_['entry_order_id']                = 'ID de Pedido';
$_['entry_customer']                = 'Cliente';
$_['entry_dp_percent']              = 'Porcentaje de anticipo';
$_['entry_months']                  = 'Meses';
$_['entry_date_added']              = 'Fecha añadido';

// Ayuda
$_['help_membership_id']            = 'Su número personal de membresía de cuenta Lay-Buy. (obtenido cuando registra una cuenta de vendedor en https://www.lay-buys.com/index.php/vtmob/login)';
$_['help_token']                    = 'Ingrese un token aleatorio para seguridad, o utilice el generado.';
$_['help_minimum']                  = 'Monto mínimo de depósito';
$_['help_maximum']                  = 'Monto máximo de depósito';
$_['help_months']                   = 'Cantidad máxima de meses para pagar balance';
$_['help_category']                 = 'Seleccionar para qué categorías estará disponible la opción de pago. Dejar en blanco si no hay restricciones.';
$_['help_product_ids']              = 'Agregue IDs de producto separadas por comas(,) para los cuales el método no estará disponible.';
$_['help_customer_group']           = 'El cliente debe estar en estos grupos de clientes para poder utilizar este método de pago. Dejar en blanco si no hay restricciones.';
$_['help_logging']                  = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario.';
$_['help_total']                    = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago. Must be a value with no currency sign.';
$_['help_order_status_pending']     = 'El estado del pedido luego de que el pedido del cliente ha sido creado.';
$_['help_order_status_canceled']    = 'El estado del pedido luego de que el pedido del cliente es cancelado.';
$_['help_order_status_processing']  = 'El estado del pedido luego de que el pedido del cliente es pagado.';
$_['help_cron_url']                 = 'Setear un cron job que llame a esta URL so that the reports are auto fetched.';
$_['help_cron_time']                = 'La última vez que se ejecutó la URL del cron job.';

// Error
$_['error_permission']              = 'Advertencia: ¡No tiene permisos para modificar pago con Lay-buy!';
$_['error_membership_id']           = '¡ID de membresía Lay-Buys necesaria!';
$_['error_token']                   = '¡Token Secreto Lay-Buy necesario!';
$_['error_min_deposit']             = '¡No puede exceder el monto máximo de anticipo!';

// Button
$_['button_fetch']                  = 'Traer';
$_['button_revise_plan']            = 'Revisar Plan';
$_['button_cancel_plan']            = 'Cancelar Plan';