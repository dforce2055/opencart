<?php
// Heading
$_['heading_title']		 = 'Paymate';

// Text
$_['text_extension']	 = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta Paymate!';
$_['text_edit']          = 'Editar Paymate';
$_['text_paymate']		 = '<img src="view/image/payment/paymate.png" alt="Paymate" title="Paymate" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_username']	 = 'Nombre de Usuario Paymate';
$_['entry_password']	 = 'Contraseña';
$_['entry_test']		 = 'Modo de Prueba';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']	 = 'Geo Zona';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden de Clasificación';

// Ayuda
$_['help_password']		 = 'Sólo use una contraseña aleatoria. Esta será utilizada para asegurar que no se interfiere con la información de pago luego de ser enviada al portal de pago.';
$_['help_total']		 = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']	 = 'Advertencia: ¡No tiene permisos para modificar pago con Paymate!';
$_['error_username']	 = '¡Nombre de Usuario Paymate necesario!';
$_['error_password']	 = '¡Contraseña necesaria!';