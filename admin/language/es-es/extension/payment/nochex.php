<?php
// Heading
$_['heading_title']		 = 'NOCHEX';

// Text
$_['text_extension']	 = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta NOCHEX!';
$_['text_edit']          = 'Editar NOCHEX';
$_['text_nochex']		 = '<a href="https://secure.nochex.com/apply/merchant_info.aspx?partner_id=172198798" target="_blank"><img src="view/image/payment/nochex.png" alt="NOCHEX" title="NOCHEX" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_seller']		 = 'Cuenta personal / vendedor';
$_['text_merchant']		 = 'Cuenta vendedor';

// Entry
$_['entry_email']		 = 'E-Mail';
$_['entry_account']		 = 'Cuenta Tipo';
$_['entry_merchant']	 = 'ID de Vendedor';
$_['entry_template']	 = 'Pass Plantilla';
$_['entry_test']		 = 'Prueba';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']     = 'Geo Zona';
$_['entry_status']	     = 'Estado';
$_['entry_sort_order']   = 'Orden de Clasificación';

// Ayuda
$_['help_total']	     = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']   = 'Advertencia: ¡No tiene permisos para modificar pago con NOCHEX!';
$_['error_email']	     = '¡E-Mail necesario!';
$_['error_merchant']     = '¡ID de Vendedor necesaria!';