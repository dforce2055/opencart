<?php
// Heading
$_['heading_title']		 = 'PayPal Payflow Pro';

// Text
$_['text_extension']     = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta PayPal Direct (UK)!';
$_['text_edit']          = 'Editar PayPal Payflow Pro';
$_['text_pp_payflow']	 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Autorización';
$_['text_sale']			 = 'Venta';

// Entry
$_['entry_vendor']		 = 'Vendedor';
$_['entry_user']		 = 'Usuario';
$_['entry_password']	 = 'Contraseña';
$_['entry_partner']		 = 'Afiliado';
$_['entry_test']		 = 'Modo de Prueba';
$_['entry_transaction']	 = 'Método de transacción';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']	 = 'Geo Zona';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden de Clasificación';

// Ayuda
$_['help_vendor']		 = 'Su ID de inicio de sesión que creó cuando registró una cuenta de Website Payments Pro';
$_['help_user']			 = 'Si configura uno o más usuarios adicionales en la cuenta, este valorp es el ID de usuario autorizado a procesar transacciones. Si no ha configuradoo usuarios adicionales en la cuenta, USUARIO y VENDEDOR tienen los mismos valores';
$_['help_password']		 = 'La contraseña de entre 6 y 32 caracteres que definió cuando registró su cuenta';
$_['help_partner']		 = 'La ID provista a usted por el Revendedor PayPal que lo registró para el SDK Payflow. Si compró su cuenta directamente de PayPal, utilice PayPal Pro en su lugar';
$_['help_test']			 = '¿Utilizar el servidor de producción o de prueba (sandbox) para transacciones?';
$_['help_total']		 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';

// Error
$_['error_permission']	 = 'Advertencia: ¡No tiene permisos para modificar pago con PayPal Website Payment Pro (UK)!';
$_['error_vendor']		 = '¡Vendedor necesario!';
$_['error_user']		 = '¡Usuario necesario!';
$_['error_password']	 = '¡Contraseña necesaria!';
$_['error_partner']		 = '¡Afiliado necesario!';