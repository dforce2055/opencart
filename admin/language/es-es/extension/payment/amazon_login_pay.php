<?php
//Headings
$_['heading_title']                = 'Amazon Pay';

// Text
$_['text_success']                 = 'Módulo Amazon Pay actualizado';
$_['text_ipn_url']				   = 'URL de Cron Job';
$_['text_ipn_token']			   = 'Token Secreto';
$_['text_us']					   = 'Americano';
$_['text_de']					   = 'Alemán';
$_['text_uk']                      = 'Inglés';
$_['text_fr']                      = 'Francéés';
$_['text_it']                      = 'Italiano';
$_['text_es']                      = 'Espanól';
$_['text_us_region']			   = 'Estados Unidos';
$_['text_eu_region']               = 'Euro región';
$_['text_uk_region']               = 'Reino Unido';
$_['text_live']                    = 'Funcionando';
$_['text_sandbox']                 = 'Sandbox';
$_['text_auth']		           	   = 'Autorización';
$_['text_payment']		           = 'Pago';
$_['text_extension']               = 'Extensiones';
$_['text_account']                 = 'Cuenta';
$_['text_guest']				   = 'Invitado';
$_['text_no_capture']              = '--- Sin captura automática ---';
$_['text_all_geo_zones']           = 'Todas Geo Zonas';
$_['text_button_settings']         = 'Checkout Button Configuraciones';
$_['text_colour']                  = 'Color';
$_['text_orange']                  = 'Naranja';
$_['text_tan']                     = 'Bronceado';
$_['text_white']                   = 'Blanco';
$_['text_light']                   = 'Claro';
$_['text_dark']                    = 'Oscuro';
$_['text_size']                    = 'Tamaño';
$_['text_medium']                  = 'Mediano';
$_['text_large']                   = 'Grande';
$_['text_x_large']                 = 'Extra grande';
$_['text_background']              = 'Fondo';
$_['text_edit']					   = 'Editar Amazon Pay';
$_['text_amazon_login_pay']        = '<a href="https://pay.amazon.com/uk/sp/opencart" target="_blank" title="Registrarse en Amazon Pay"><img src="view/image/payment/amazonpay.png" alt="Amazon Pay" title="Amazon Pay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_amazon_join']             = '<a href="https://pay.amazon.com/uk/sp/opencart" target="_blank" title="Registrarse en Amazon Pay"><u>Registrarse en Amazon Pay</u></a>';
$_['text_payment_info']			   = 'Información de pago';
$_['text_capture_ok']			   = 'Captura exitosa';
$_['text_capture_ok_order']		   = 'Captura exitosa, authorización capturada en su totalidad';
$_['text_refund_ok']			   = 'Reintegro solicitado exitosamente';
$_['text_refund_ok_order']		   = 'Reintegro solicitado exitosamente, monto reintegrado en su totalidad';
$_['text_cancel_ok']			   = 'Cancelación exitosa, estado de pedido cambiado a cancelado';
$_['text_capture_status']		   = 'Pago capturado';
$_['text_cancel_status']		   = 'Pago cancelado';
$_['text_refund_status']		   = 'Pago reintegrado';
$_['text_order_ref']			   = 'Referencia de pedido';
$_['text_order_total']			   = 'Total autorizado';
$_['text_total_captured']		   = 'Total capturado';
$_['text_transactions']			   = 'Transacciones';
$_['text_column_authorization_id'] = 'ID de autorización Amazon ';
$_['text_column_capture_id']	   = 'ID de captura Amazon ';
$_['text_column_refund_id']		   = 'ID de reintegro Amazon ';
$_['text_column_amount']		   = 'Monto';
$_['text_column_type']			   = 'Tipo';
$_['text_column_status']		   = 'Estado';
$_['text_column_date_added']	   = 'Fecha añadido';
$_['text_confirm_cancel']		   = '¿Está seguro de que desea cancel el pago?';
$_['text_confirm_capture']		   = '¿Está seguro de que desea capturar? el pago?';
$_['text_confirm_refund']		   = 'Está seguro de que desea reintegrar el pago?';
$_['text_minimum_total']           = 'Minimum Total de Pedidos';
$_['text_geo_zone']                = 'Geo Zona';
$_['text_status']                  = 'Estado';
$_['text_declined_codes']          = 'Test Declinar Códigos';
$_['text_sort_order']              = 'Orden de Clasificación';
$_['text_amazon_invalid']          = 'MétodoDePagoInvalido';
$_['text_amazon_rejected']         = 'AmazonRechazado';
$_['text_amazon_timeout']          = 'TiempoDeEsperaAgotado';
$_['text_amazon_no_declined']      = '--- Sin rechazo automático de autoorización ---';
$_['text_amazon_signup']		   = 'Registrarse en Amazon Pay';
$_['text_credentials']			   = 'Por favor pegue sus claves aquí (en formato JSON)';
$_['text_validate_credentials']	   = 'Validar y usar credenciales';

// Columns
$_['column_status']                = 'Estado';

//entry
$_['entry_merchant_id']            = 'ID de Vendedor';
$_['entry_access_key']             = 'Clave de acceso';
$_['entry_access_secret']          = 'Clave secreta';
$_['entry_client_id']              = 'ID de cliente';
$_['entry_client_secret']          = 'Secreto de cliente';
$_['entry_language']			   = 'Lenguaje';
$_['entry_login_pay_test']         = 'Modo de prueba';
$_['entry_login_pay_mode']         = 'Modo de pago';
$_['entry_checkout']			   = 'Modo de proceso de pago';
$_['entry_payment_region']		   = 'Pago Región';
$_['entry_capture_status']         = 'Estado de captura automática';
$_['entry_pending_status']         = 'Estado pendiente de pedido';
$_['entry_ipn_url']				   = 'URL de IPN';
$_['entry_ipn_token']			   = 'Token Secreto';
$_['entry_debug']				   = 'Registro de debug';

// Ayuda
$_['help_pay_mode']				   = 'Pago sólo disponible para mercado de EEUU';
$_['help_checkout']				   = 'Debería el botón de pago también iniciar sesión del cliente';
$_['help_capture_status']		   = 'Seleccionar estado de pedido que disparará captura automática de un pago autorizado';
$_['help_ipn_url']				   = 'Fijar esto como su URL de vendedor en Central de Vendedor amazon';
$_['help_ipn_token']			   = 'Debería ser largo y difícil de adivinar';
$_['help_minimum_total']		   = 'Debe ser mayor a 0';
$_['help_debug']				   = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario';
$_['help_declined_codes']		   = 'Sólo para propósitos de prueba';

// Order Info
$_['tab_order_adjustment']         = 'Ajuste de pedido';

// Errors
$_['error_permission']             = 'No tiene permisos para modificar este módulo';
$_['error_merchant_id']			   = 'ID de Vendedor necesaria';
$_['error_access_key']			   = 'Clave de acceso necesaria';
$_['error_access_secret']		   = 'Clave secreta necesaria';
$_['error_client_id']			   = 'ID de cliente necesaria';
$_['error_client_secret']		   = 'Clave de cliente necesaria';
$_['error_pay_mode']			   = 'Pago solo disponible para mercado de EEUU';
$_['error_minimum_total']		   = 'El mínimo de total de pedido debe ser mayor a 0';
$_['error_curreny']                = 'Su tienda debe tener la moneda %s instalada y activada';
$_['error_upload']                 = 'Subir falló';
$_['error_data_missing'] 		   = 'Faltan datos necesarios';
$_['error_credentials'] 		   = 'Por favor ingrese las claves en un foormato JSON válido';


// Buttons
$_['button_capture']			   = 'Captura';
$_['button_refund']				   = 'Reintegrar';
$_['button_cancel']				   = 'Cancelar';
