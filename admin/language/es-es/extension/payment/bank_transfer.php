<?php
// Heading
$_['heading_title']      = 'Transferencia Bancaria';

// Text
$_['text_extension']     = 'Extensiones';
$_['text_success']       = 'Éxito: ¡Ha modificado los detalles de transferencia bancaria!';
$_['text_edit']          = 'Editar Transferencia Bancaria';

// Entry
$_['entry_bank']         = 'Instrucciones de Transferencia Bancaria';
$_['entry_total']        = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']     = 'Geo Zona';
$_['entry_status']       = 'Estado';
$_['entry_sort_order']   = 'Orden de Clasificación';

// Ayuda
$_['help_total']         = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.'; 

// Error 
$_['error_permission']   = 'Advertencia: ¡No tiene permisos para modificar pagos por transferencia bancaria!';
$_['error_bank']         = '¡Instrucciones de Transferencia Bancaria necesarias!';