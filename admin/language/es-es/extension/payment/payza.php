<?php
// Heading
$_['heading_title']		 = 'Payza';

// Text
$_['text_extension']	 = 'Extensiones';
$_['text_success']		 = 'Éxito: ¡Ha modificado sus detalles de cuenta Payza!';
$_['text_edit']          = 'Editar Payza';

// Entry
$_['entry_merchant']	 = 'ID de Vendedor';
$_['entry_security']	 = 'Código de seguridad';
$_['entry_callback']	 = 'URL de alerta';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado de pedido';
$_['entry_geo_zone']	 = 'Geo Zona';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Orden de Clasificación';

// Ayuda
$_['help_callback']		 = 'Estó debe ser configurado en el panel de control de Payza. También deberá activar el "Estado IPN".';
$_['help_total']		 = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']	 = 'Advertencia: ¡No tiene permisos para modificar pago con Payza!';
$_['error_merchant']	 = '¡ID de Vendedor necesaria!';
$_['error_security']	 = '¡Código de seguridad necesario!';