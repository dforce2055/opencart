<?php
// Heading
$_['heading_title']			 = 'BluePay Redirect (Requires SSL)';

// Text
$_['text_extension']		 = 'Extensiones';
$_['text_success']			 = 'Éxito: ¡Ha modificado sus detalles de cuenta BluePay Redirect!';
$_['text_edit']              = 'Editar BluePay Redirect (Requiere SSL)';
$_['text_bluepay_redirect']	 = '<a href="http://www.bluepay.com/preferred-partner/opencart" target="_blank"><img src="view/image/payment/bluepay.jpg" alt="BluePay Redirect" title="BluePay Redirect" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']				 = 'Simulador';
$_['text_test']				 = 'Prueba';
$_['text_live']				 = 'Funcionando';
$_['text_sale']				 = 'Venta';
$_['text_authenticate']		 = 'Autorizar';
$_['text_release_ok']		 = 'Liberación exitosa';
$_['text_release_ok_order']	 = 'Liberación exitosa';
$_['text_rebate_ok']		 = 'Reembolso exitoso';
$_['text_rebate_ok_order']	 = 'Reembolso exitoso, estado de pedido actualizado a reembolsado';
$_['text_void_ok']			 = 'Anulación exitosa, estado de pedido actualizado a anulado';
$_['text_payment_info']		 = 'Información de pago';
$_['text_release_status']	 = 'Pago liberado';
$_['text_void_status']		 = 'Pago anulado';
$_['text_rebate_status']	 = 'Pago reembolsado';
$_['text_order_ref']		 = 'Referencia de pedido';
$_['text_order_total']		 = 'Total autorizado';
$_['text_total_released']	 = 'Total librado';
$_['text_transactions']		 = 'Transacciones';
$_['text_column_amount']	 = 'Monto';
$_['text_column_type']		 = 'Tipo';
$_['text_column_date_added'] = 'Creado';
$_['text_confirm_void']		 = '¿Está seguro de que desea anular el pago?';
$_['text_confirm_release']	 = '¿Está seguro de que desea liberar el pago?';
$_['text_confirm_rebate']	 = '¿Está seguro de que desea reembolsar el pago?';

// Entry
$_['entry_vendor']			 = 'ID de cuenta';
$_['entry_secret_key']		 = 'Clave secreta';
$_['entry_test']			 = 'Modo de transacción';
$_['entry_transaction']		 = 'Método de transacción';
$_['entry_total']			 = 'Total';
$_['entry_order_status']	 = 'Estado de pedido';
$_['entry_geo_zone']		 = 'Geo Zona';
$_['entry_status']			 = 'Estado';
$_['entry_sort_order']		 = 'Orden de Clasificación';
$_['entry_debug']			 = 'Registro de debug';
$_['entry_card']			 = 'Tarjetas de tienda';

// Ayuda
$_['help_total']			 = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';
$_['help_debug']			 = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario';
$_['help_transaction']		 = 'La venta se cargará al cliente inmediatamente. La autorización pondrá los fondos en espera para futura captura.';
$_['help_cron_job_token']	 = 'Elija un token que sea largo y difícil de adivinar';
$_['help_cron_job_url']		 = 'Setear un cron job que llamará a esta URL';

// Button
$_['button_release']		 = 'Liberar';
$_['button_rebate']			 = 'Reembolsar / reintegrar';
$_['button_void']			 = 'Anular';

// Error
$_['error_permission']		 = 'Advertencia: ¡No tiene permisos para modificar pago con BluePay!';
$_['error_account_id']		 = '¡ID de cuenta necesaria!';
$_['error_secret_key']		 = '¡Clave secreta necesaria!';