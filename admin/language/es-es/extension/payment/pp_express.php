<?php
// Heading
$_['heading_title']					 = 'Proceso de compra con PayPal Express';

// Text
$_['text_extension']				 = 'Extensiones';
$_['text_success']				 	 = 'Éxito: ¡Ha modificado sus detalles de cuenta Proceso de compra con PayPal Express!';
$_['text_edit']                      = 'Editar Proceso de compra con PayPal Express';
$_['text_pp_express']				 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Autorización';
$_['text_sale']						 = 'Venta';
$_['text_signup']                    = 'Registrarse en PayPal - guarde sus ajustes primero ya que está página se refrescará';
$_['text_sandbox']                   = 'Registrarse en PayPal Sandbox - guarde sus ajustes primero ya que está página se refrescará';
$_['text_configure_live']            = 'Configurar producción';
$_['text_configure_sandbox']         = 'Configure Sandbox';
$_['text_show_advanced']             = 'Mostrar avanzado';
$_['text_show_quick_setup']          = 'Mostrar configuración rápida';
$_['text_quick_setup']             	 = 'Configuración rápida - Vincule una cuenta existente de PayPal o cree una nueva para comenzar a aceptar pagos en minutos';
$_['text_paypal_consent']		 	 = 'Al utilizar la herramienta de configuración rapida, le da permiso a PayPal recibir información de su tienda';
$_['text_success_connect']			 = 'Éxito: ¡Ha conectado su cuenta de PayPal!';
$_['text_preferred_main']		 	 = 'Le da a sus clientes una experiencia de proceso de pago simplificada en varios dispositivos que los mantiene locales a su sitio web a lo largo del proceso de autorización de pago';
$_['text_learn_more']			 	 = '(Conocer más)';
$_['text_preferred_li_1']			 = 'Comienze a aceptar PayPal en tres clicks';
$_['text_preferred_li_2']			 = 'Aceptar pagos de alrededor del mundo';
$_['text_preferred_li_3']			 = 'Ofrecer atajo a Express Checkout, permitiendo a clientes procesar sus pagos directamente desde su página de canasta';
$_['text_preferred_li_4']			 = 'Mejore la conversión con PayPal One Touch y proceso de pago En-Contexto';
$_['text_connect_paypal']			 = 'Conectar con PayPal';
$_['text_incontext_not_supported']	 = '* No soportado con proceso de pago En-Contexto';
$_['text_retrieve']	 				 = 'Se han ingresado sus datos desde PayPal';
$_['text_enable_button']			 = 'Recomendamos ofrecer atajo de proceso de pagos PayPal Express para maximizar la conversión, esto permite a los clientes usar su libreta de direcciones de PayPal y <strong>un proceso de pagos en tan poco como tres toques</strong> desde la página de canasta. Presione activar para instalar la extensión y acceder al administrador de diseños, deberá agregar "Botón de proceso de pago con PayPal Express" al diseño del proceso de pagos';

// Entry
$_['entry_username']				 = 'Nombre de Usuario de API';
$_['entry_password']				 = 'Contraseña de API';
$_['entry_signature']				 = 'Firma de API';
$_['entry_sandbox_username']		 = 'Nombre de usuario de Sandbox de API';
$_['entry_sandbox_password']		 = 'Contraseña de Sandbox de API';
$_['entry_sandbox_signature']		 = 'Firma de Sandbox de API';
$_['entry_ipn']						 = 'URL IPN';
$_['entry_test']					 = 'Modo de prueba (Sandbox)';
$_['entry_debug']					 = 'Registro de debug';
$_['entry_currency']				 = 'Moneda por defecto';
$_['entry_recurring_cancel']	     = 'Permitir a los clientes cancelar pagos recurrentes desde el área de cuenta';
$_['entry_transaction']		         = 'Tipo de establecimiento';
$_['entry_total']					 = 'Total';
$_['entry_geo_zone']				 = 'Geo Zona';
$_['entry_status']					 = 'Estado';
$_['entry_sort_order']				 = 'Orden de Clasificación';
$_['entry_canceled_reversal_status'] = 'Estado revocación cancelada';
$_['entry_completed_status']		 = 'Estado completado';
$_['entry_denied_status']			 = 'Estado denegado';
$_['entry_expired_status']			 = 'Estado expirado';
$_['entry_failed_status']			 = 'Estado fallado';
$_['entry_pending_status']			 = 'Estado pendiente';
$_['entry_processed_status']		 = 'Estado procesado';
$_['entry_refunded_status']			 = 'Estado reintegrado';
$_['entry_reversed_status']			 = 'Estado devuelto';
$_['entry_voided_status']			 = 'Estado anulado';
$_['entry_allow_notes']				 = 'Permitir notas';
$_['entry_colour']	      			 = 'Color de fondo de página';
$_['entry_logo']					 = 'Logo';
$_['entry_incontext']				 = 'Desactivar proceso de pagos En-Contexto';

// Tab
$_['tab_api']				         = 'Detalles de API';
$_['tab_order_status']				 = 'Estado de pedido';
$_['tab_checkout']					 = 'Proceso de pago';

// Ayuda
$_['help_ipn']						 = 'Requerido para subscripciones';
$_['help_total']					 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';
$_['help_logo']						 = 'Máximo 750px(ancho) x 90px(alto)<br />Debería usar un logo únicamente si tiene SSL configurado.';
$_['help_colour']					 = 'Código de color HTML de 6 caracteres';
$_['help_currency']					 = 'Usado para búsqueda de transacciones';

// Error
$_['error_permission']				 = 'Advertencia: ¡No tiene permisos para modificar pago con Proceso de compra con PayPal Express!';
$_['error_username']				 = '¡Nombre de Usuario de API necesario!';
$_['error_password']				 = '¡Contraseña de API necesaria!';
$_['error_signature']				 = '¡Firma de API necesaria!';
$_['error_sandbox_username']	 	 = '¡Nombre de usuario de Sandbox de API necesario!';
$_['error_sandbox_password']		 = '¡Contraseña de Sandbox de API necesario!';
$_['error_sandbox_signature']		 = '¡Firma de Sandbox de API necesaria!';
$_['error_api']						 = 'Error de autorización de PayPal';
$_['error_api_sandbox']				 = 'Error de autorización de Sandbox de PayPal';
$_['error_consent']				 	 = 'Para utilizar la configuración rápida, debe darle permiso a PayPal de usar su información de tienda';
