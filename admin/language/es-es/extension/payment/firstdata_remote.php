<?php
// Heading
$_['heading_title']					 = 'API de Servicio Web First Data EMEA';

// Text
$_['text_firstdata_remote']			 = '<img src="view/image/payment/firstdata.png" alt="First Data" title="First Data" style="border: 1px solid #EEEEEE;" />';
$_['text_extension']					 = 'Extensiones';
$_['text_success']					 = 'Éxito: ¡Ha modificado sus detalles de cuenta First Data!';
$_['text_edit']                      = 'Editar API de Servicio Web First Data EMEA';
$_['text_card_type']				 = 'Tipo de tarjeta';
$_['text_enabled']					 = 'Activado';
$_['text_merchant_id']				 = 'ID de tienda';
$_['text_subaccount']				 = 'Subcuenta';
$_['text_user_id']					 = 'ID de usuario';
$_['text_capture_ok']				 = 'Captura exitosa';
$_['text_capture_ok_order']			 = 'Captura exitosa, estado de pedido actualizado a exitoso - establecido';
$_['text_refund_ok']				 = 'Reintegro exitoso';
$_['text_refund_ok_order']			 = 'Reintegro exitoso, estado de pedido actualizado a reintegrado';
$_['text_void_ok']					 = 'Anulación exitosa, estado de pedido actualizado a anulado';
$_['text_settle_auto']				 = 'Venta';
$_['text_settle_delayed']			 = 'Pre auth';
$_['text_mastercard']				 = 'Mastercard';
$_['text_visa']						 = 'Visa';
$_['text_diners']					 = 'Diners';
$_['text_amex']						 = 'American Express';
$_['text_maestro']					 = 'Maestro';
$_['text_payment_info']				 = 'Información de pago';
$_['text_capture_status']			 = 'Pago capturado';
$_['text_void_status']				 = 'Pago anulado';
$_['text_refund_status']			 = 'Pago reintegrado';
$_['text_order_ref']				 = 'Referencia de pedido';
$_['text_order_total']				 = 'Total autorizado';
$_['text_total_captured']			 = 'Total capturado';
$_['text_transactions']				 = 'Transacciones';
$_['text_column_amount']			 = 'Monto';
$_['text_column_type']				 = 'Tipo';
$_['text_column_date_added']		 = 'Creado';
$_['text_confirm_void']				 = '¿Está seguro de que desea anular el pago?';
$_['text_confirm_capture']			 = '¿Está seguro de que desea capture el pago?';
$_['text_confirm_refund']			 = 'Está seguro de que desea reintegrar el pago?';

// Entry
$_['entry_certificate_path']		 = 'Ruta de certificado';
$_['entry_certificate_key_path']	 = 'Ruta de clave privada';
$_['entry_certificate_key_pw']		 = 'Contraseña de clave privada';
$_['entry_certificate_ca_path']		 = 'Ruta de CA';
$_['entry_merchant_id']				 = 'ID de tienda';
$_['entry_user_id']					 = 'ID de usuario';
$_['entry_password']				 = 'Contraseña';
$_['entry_total']					 = 'Total';
$_['entry_sort_order']				 = 'Orden de clasificación';
$_['entry_geo_zone']				 = 'Geo Zona';
$_['entry_status']					 = 'Estado';
$_['entry_debug']					 = 'Registro de debug';
$_['entry_auto_settle']				 = 'Tipo de establecimiento';
$_['entry_status_success_settled']	 = 'Éxito - establecido';
$_['entry_status_success_unsettled'] = 'Éxito - no establecido';
$_['entry_status_decline']			 = 'Declinar';
$_['entry_status_void']				 = 'Anulado';
$_['entry_status_refund']			 = 'Reintegrado';
$_['entry_enable_card_store']		 = 'Activar tokens de almacenamiento de tarjetas';
$_['entry_cards_accepted']			 = 'Tipos de tarjeta aceptados';

// Ayuda
$_['help_total']					 = 'El total de compra que debe alcanzar el pedido antes de que se active este método de pago';
$_['help_certificate']				 = 'Claves privadas y certificados deberían ser almacenadas fuera de sus carpetas web públicas';
$_['help_card_select']				 = 'Solicite al usuario que seleccione su tipo de tarjeta antes de ser redireccionado';
$_['help_notification']				 = 'Debe proveer esta URL a First Data para obtener notificaciones de pago';
$_['help_debug']					 = 'Activar debug escribirá datos sensibles a un archivo de registro. Siempre debería desactivarlo salvo que se le instruya lo contrario .';
$_['help_settle']					 = 'Si utiliza pre-auth debe completar una acción post-auth dentro de 3-5 días o su transacción será descartada';

// Tab
$_['tab_account']					 = 'Info de API';
$_['tab_order_status']				 = 'Estado de pedido';
$_['tab_payment']					 = 'Ajustes de pago';

// Button
$_['button_capture']				 = 'Captura';
$_['button_refund']					 = 'Reintegrar';
$_['button_void']					 = 'Anular';

// Error
$_['error_merchant_id']				 = 'ID de tienda necesaria';
$_['error_user_id']					 = 'ID de usuario necesaria';
$_['error_password']				 = 'Contraseña necesaria';
$_['error_certificate']				 = 'Ruta de certificado necesaria';
$_['error_key']						 = 'Clave de certificado necesaria';
$_['error_key_pw']					 = 'Contraseña de clave de certificado necesaria';
$_['error_ca']						 = 'Autoridad de Certificados (CA) necesaria';