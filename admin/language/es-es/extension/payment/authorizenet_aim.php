<?php
// Heading
$_['heading_title']         = 'Authorize.Net (AIM)';

// Text
$_['text_extension']        = 'Extensiones';
$_['text_success']          = 'Éxito: ¡Ha modificado sus detalles de cuenta Authorize.Net (AIM)!';
$_['text_edit']             = 'Editar Authorize.Net (AIM)';
$_['text_test']             = 'Prueba';
$_['text_live']             = 'Funcionando';
$_['text_authorization']    = 'Autorización';
$_['text_capture']          = 'Captura';
$_['text_authorizenet_aim'] = '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_login']           = 'ID de inicio de sesión';
$_['entry_key']             = 'Clave de transacción';
$_['entry_hash']            = 'Hash MD5';
$_['entry_server']          = 'Servidor de transacción';
$_['entry_mode']            = 'Modo de transacción';
$_['entry_method']          = 'Método de transacción';
$_['entry_total']           = 'Total';
$_['entry_order_status']    = 'Estado de pedido';
$_['entry_geo_zone']        = 'Geo Zona';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Orden de Clasificación';

// Ayuda
$_['help_total']            = 'El total de compra que debe alcanzar el pedido para que se habilite este método de pago.';

// Error
$_['error_permission']      = 'Advertencia: ¡No tiene permisos para modificar pago con Authorize.Net (SIM)!';
$_['error_login']           = '¡ID de inicio de sesión necesaria!';
$_['error_key']             = '¡Clave de transacción necesaria!';