<?php
// Heading
$_['heading_title']    = 'Cupón';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el total de cupones!';
$_['text_edit']        = 'Editar Cupón';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el total de cupones!';
