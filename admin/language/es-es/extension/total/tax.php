<?php
// Heading
$_['heading_title']    = 'Impuestos';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: Has modificado el total de impuestos!';
$_['text_edit']        = 'Editar Total de impuestos';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el total de impuestos!';