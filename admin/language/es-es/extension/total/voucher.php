<?php
// Heading
$_['heading_title']    = 'Cupón de regalo';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el total de cupones de regalo!';
$_['text_edit']        = 'Editar total de cupones de regalo';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar el total de cupones de regalo!';