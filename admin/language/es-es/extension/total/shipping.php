<?php
// Heading
$_['heading_title']    = 'Envío';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el total de envíos!';
$_['text_edit']        = 'Editar total de Envíos';

// Entry
$_['entry_estimator']  = 'Estimador de Envío';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el total de envíos!';
