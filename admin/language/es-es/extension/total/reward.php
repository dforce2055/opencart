<?php
// Heading
$_['heading_title']    = ' Puntos de Recompensa';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el total de puntos de recompensa!';
$_['text_edit']        = 'Editar total de Puntos de Recompensa';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el total de puntos de recompensa!';