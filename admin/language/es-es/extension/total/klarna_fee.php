<?php
// Heading
$_['heading_title']    = 'Gastos Klarna';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito:¡Has modificado el total de Gastos Klarna!';
$_['text_edit']        = 'Editar total de Gastos Klarna';
$_['text_sweden']      = 'Suecia';
$_['text_norway']      = 'Noruega';
$_['text_finland']     = 'Finlandia';
$_['text_denmark']     = 'Dinamarca';
$_['text_germany']     = 'Alemania';
$_['text_netherlands'] = 'Paises Bajos';

// Entry
$_['entry_total']      = 'Total de Pedidos';
$_['entry_fee']        = 'Factura del gasto';
$_['entry_tax_class']  = 'Clase de Impuesto';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el total de Gastos Klarna!';
