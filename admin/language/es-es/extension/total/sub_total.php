<?php
// Heading
$_['heading_title']    = 'Sub-Total';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el total de sub-total!';
$_['text_edit']        = 'Editar total de Sub-Total';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: No tiene permisos para modificar el total de sub-total!';