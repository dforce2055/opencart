<?php
// Heading
$_['heading_title']    = 'Gastos administrativos';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado total de gastos administrativos!';
$_['text_edit']        = 'Editar total de gastos administrativos';

// Entry
$_['entry_total']      = 'Total de Pedidos';
$_['entry_fee']        = 'Gasto';
$_['entry_tax_class']  = 'Clase de Impuesto';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Ayuda
$_['help_total']       = 'El total de pago debe llegar antes de que se desactive este total del pedido.';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar total de gastos administrativos!';
