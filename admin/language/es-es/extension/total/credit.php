<?php
// Heading
$_['heading_title']    = 'Crédito de la Tienda';

// Text
$_['text_extension']   = 'Extensiones';
$_['text_success']     = 'Éxito: ¡Has modificado el crédito total de la tienda!';
$_['text_edit']        = 'Editar el Crédito Total de la Tienda';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de Clasificación';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar el crédito total de la tienda!';
