<?php
// Heading
$_['heading_title']     = 'Vouchers de Regalo';

// Text
$_ ['text_success'] = 'Se han modificado los Vouchers de Regalo.';
$_ ['text_list'] = 'Lista de Vouchers de Regalo';
$_ ['text_add'] = 'Agregar Voucher de Regalo';
$_ ['text_edit'] = 'Editar Voucher de Regalo';
$_ ['text_sent'] = 'El Voucher de Regalo ha sido enviado.';

// Column
$_['column_name'] = 'Nombre del Voucher';
$_['column_code'] = 'Código';
$_['column_from'] = 'De';
$_['column_to'] = 'Para';
$_['column_theme'] = 'Tema';
$_['column_amount'] = 'Cantidad';
$_['column_status'] = 'Estado';
$_['column_order_id'] = 'Pedido ID';
$_['column_customer'] = 'Cliente';
$_['column_date_added'] = 'Fecha de Alta';
$_['column_action'] = 'Acción';

// Entry
$_['entry_code'] = 'Código';
$_['entry_from_name'] = 'De Nombre';
$_['entry_from_email'] = 'De Email';
$_['entry_to_name'] = 'Para Nombre';
$_['entry_to_email'] = 'Para Email';
$_['entry_theme'] = 'Tema';
$_['entry_message'] = 'Mensaje';
$_['entry_amount'] = 'Cantidad';
$_['entry_status'] = 'Estado';

// Ayuda
$_['help_code'] = 'Código del cliente para activar el Voucher.';

// Error
$_['error_selection'] = 'No hay Vouchers seleccionados.';
$_['error_permission'] = 'Sin permiso para modificar Vouchers.';
$_['error_exists'] = 'El Código del Voucher ya está en uso.';
$_['error_code'] = 'El Código debe contener entre 3 y 10 caractéres.';
$_['error_to_name'] = 'El Nombre del Destinatario debe contener entre 1 y 64 caractéres. ';
$_['error_from_name'] = 'El Nombre debe contener entre 1 y 64 caractéres.';
$_['error_email'] = 'Email inválido.';
$_['error_amount'] = 'La cantidad debe ser mayor que 0 igual a 1.';
$_['error_order'] = 'Este Voucher no puede ser eliminado, ya que es parte de un <a href="%s"> Pedido </a>';