<?php
// Heading
$_['heading_title']     = 'Tipos de Vouchers de Regalo';

// Text
$_['text_success'] = 'Se han modificado los temas de Vouchers de Regalo.';
$_['text_list'] = 'Voucher por Temas';
$_['text_add'] = 'Agregar Voucher Temático';
$_['text_edit'] = 'Editar Voucher Temático';

// Column
$_['column_name'] = 'Nombre del Tema';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre del Tema';
$_['entry_description'] = 'Descripción';
$_['entry_image'] = 'Imagen';

// Error
$_['error_permission'] = 'Sin permiso para modificar los Temas de Vouchers.';
$_['error_name'] = 'El Nombre del Tema debe contener entre 3 y 32 caractéres.';
$_['error_image'] = 'Imagen Obligatoria.';
$_['error_voucher'] = 'Este tema de Vouchers no se puede eliminar ya que se asigna actualmente a los Vouchers% s.';