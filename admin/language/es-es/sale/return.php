<?php
// Heading
$_['heading_title']       = 'Devoluciones de Producto';

// Text
$_['text_success'] = 'Se han modificado las Devoluciones.';
$_['text_list'] = 'Lista de Productos Devuelto';
$_['text_add'] = 'Agregar Productos Devueltos';
$_['text_edit'] = 'Editar Devolución del Producto';
$_['text_opened'] = 'Abierto';
$_['text_unopened'] = 'Sin abrir';
$_['text_order'] = 'Solicitar Información';
$_['text_product'] = 'Información del producto; Razón de la Devolución.';
$_['text_history'] = 'Agregar Historial Devolución';

// Columna
$_['column_return_id'] = 'Devolución ID';
$_['column_order_id'] = 'Pedido ID';
$_['column_customer'] = 'Cliente';
$_['column_product'] = 'Producto';
$_['column_model'] = 'Modelo';
$_['column_status'] = 'Estado';
$_['column_date_added'] = 'Fecha Alta';
$_['column_date_modified'] = 'Fecha Modificación';
$_['column_comment'] = 'Comentario';
$_['column_notify'] = 'Notificar Cliente';
$_['column_action'] = 'Acción';

// Entrada
$_['entry_customer'] = 'Cliente';
$_['entry_order_id'] = 'Pedido ID';
$_['entry_date_ordered'] = 'Fecha del Pedido';
$_['entry_firstname'] = 'Nombre';
$_['entry_lastname'] = 'Apellido';
$_['entry_email'] = 'Email';
$_['entry_telephone'] = 'Teléfono';
$_['entry_product'] = 'Producto';
$_['entry_model'] = 'Modelo';
$_['entry_quantity'] = 'Cantidad';
$_['entry_opened'] = 'Abierto';
$_['entry_comment'] = 'Comentario';
$_['entry_return_reason'] = 'Razones de Devolución';
$_['entry_return_action'] = 'Acción De Devolución';
$_['entry_return_status'] = 'Estado de Devoluciones';
$_['entry_notify'] = 'Notificar al cliente';
$_['entry_return_id'] = 'Devolución ID';
$_['entry_date_added'] = 'Fecha Alta';
$_['entry_date_modified'] = 'Fecha Modificación';

// Ayuda
$_['help_product'] = '(Autocompletar)';

// Error
$_['error_warning'] = 'El Formulario contiene Errores.';
$_['error_permission'] = 'Sin permiso para modificar las Devoluciones.';
$_['error_order_id'] = 'El Pedido requiere identificación.';
$_['error_firstname'] = 'El Nombre debe contener entre 1 y 32 caractéres.';
$_['error_lastname'] = 'El Apellido debe contener entre 1 y 32 caractéres.';
$_['error_email'] = 'Email inválido.';
$_['error_telephone'] = 'El Teléfono debe contener entre 3 y 32 caractéres.';
$_['error_product'] = 'El Nombre del Producto debe contener entre 3 y 255 caractéres.';
$_['error_model'] = 'El Modelo Producto debe contener entre 3 y 64 caractéres.';