<?php
// Heading
$_['heading_title']         = 'Clientes';

// Text
$_['text_success'] = 'Se han modificado los Clientes.';
$_['text_list'] = 'Lista de Clientes ';
$_['text_add'] = 'Agregar Cliente ';
$_['text_edit'] = 'Editar Cliente';
$_['text_default'] = 'Por defecto';
$_['text_balance'] = 'Balance';
$_['text_add_ban_ip'] = 'Agregar IP prohibida';
$_['text_remove_ban_ip'] = 'Eliminar IP prohibida';

// Column
$_['column_name'] = 'Nombre del Cliente';
$_['column_email'] = 'Email';
$_['column_customer_group'] = 'Grupo de Clientes';
$_['column_status'] = 'Estado';
$_['column_date_added'] = 'Fecha Alta';
$_['column_comment'] = 'Comentario';
$_['column_description'] = 'Descripción';
$_['column_amount'] = 'Cantidad';
$_['column_points'] = 'Puntos';
$_['column_ip'] = 'IP';
$_['column_total'] = 'Las cuentas totales';
$_['column_action'] = 'Acción';

// Entry
$_['entry_customer_group'] = 'Grupo de Clientes';
$_['entry_firstname'] = 'Nombre';
$_['entry_lastname'] = 'Apellido';
$_['entry_email'] = 'Email';
$_['entry_telephone'] = 'Teléfono';
$_['entry_fax'] = 'Fax';
$_['entry_newsletter'] = 'Boletín de Noticias';
$_['entry_status'] = 'Estado';
$_['entry_approved'] = 'Aprobado';
$_['entry_safe'] = 'Segura';
$_['entry_password'] = 'contraseña';
$_['entry_confirm'] = 'Confirmar';
$_['entry_company'] = 'Empresa';
$_['entry_address_1'] = 'Dirección 1';
$_['entry_address_2'] = 'Dirección 2';
$_['entry_city'] = 'Ciudad';
$_['entry_postcode'] = 'Código Postal';
$_['entry_country'] = 'País';
$_['entry_zone'] = 'Provincia/Estado';
$_['entry_default'] = 'Dirección Por defecto';
$_['entry_comment'] = 'Comentario';
$_['entry_description'] = 'Descripción';
$_['entry_amount'] = 'Cantidad';
$_['entry_points'] = 'Puntos';
$_['entry_name'] = 'Nombre del Cliente';
$_['entry_ip'] = 'IP';
$_['entry_date_added'] = 'Fecha Alta';

// Ayuda
$_['help_safe'] = 'Definir en true para evitar que este cliente sea capturado por el sistema de lucha contra el fraude';
$_['help_points'] = 'Usar menos para quitar puntos';

// Error
$_['error_warning'] = 'El Formulario contiene Errores.';
$_['error_permission'] = 'Sin permiso para modificar Clientes.';
$_['error_exists'] = 'Email ya registrado.';
$_['error_firstname'] = 'El Nombre debe contener entre 1 y 32 caractéres.';
$_['error_lastname'] = 'El Apellido debe contner entre 1 y 32 caractéres.';
$_['error_email'] = 'El Email es inválido.';
$_['error_telephone'] = 'El Teléfono debe contener entre 3 y 32 caractéres.';
$_['error_password'] = 'La Contraseña debe contener entre 4 y 20 caractéres.';
$_['error_confirm'] = 'La Contraseña y su Confirmación no coinciden.';
$_['error_address_1'] = 'La Dirección 1 debe contener entre 3 y 128 caractéres.';
$_['error_city'] = 'La Ciudad debe contener entre 2 y 128 caractéres.';
$_['error_postcode'] = 'El Código Postal debe contener entre 2 y 10 caractéres de este país.';
$_['error_country'] = 'Seleccionar País.';
$_['error_zone'] = 'Seleccionar Provincia/Estado.';
$_['error_custom_field'] = 'Obligtorio% s.';
$_['error_comment'] = 'Comentario Obligatorio.';