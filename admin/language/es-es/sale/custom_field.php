<?php
// Heading
$_['heading_title']         = 'Campos Personalizados';

// Text
$_['text_success']          = 'Se han modificado los Campos Personalizados.';
$_['text_list']             = 'Lista de Campos Personalizados';
$_['text_add']              = 'Agregar Campos Personalizados';
$_['text_edit']             = 'Editar Campos Personalizados';
$_['text_choose']           = 'Elegir';
$_['text_select']           = 'Seleccionar';
$_['text_radio']            = 'Radio';
$_['text_checkbox']         = 'Checkbox';
$_['text_input']            = 'Entrada';
$_['text_text']             = 'Texto';
$_['text_textarea']         = 'Area de Texto';
$_['text_file']             = 'Archivo';
$_['text_date']             = 'Fecha';
$_['text_datetime']         = 'Fecha y Hora';
$_['text_time']             = 'Hora';
$_['text_account']          = 'Cuenta';
$_['text_address']          = 'Dirección';

// Column
$_['column_name']           = 'Nombre Campos Personalizados';
$_['column_location']       = 'Localzación';
$_['column_type']           = 'Tipo';
$_['column_sort_order']     = 'Ordenar';
$_['column_action']         = 'Acción';

// Entry
$_['entry_name']            = 'Nombre de los Campos Personalizados';
$_['entry_location']        = 'Localización';
$_['entry_type']            = 'Tipo';
$_['entry_value']           = 'Importancia';
$_['entry_custom_value']    = 'Valor de los Campos Personalizados';
$_['entry_customer_group']  = 'Grupo de Clientes';
$_['entry_necesario']        = 'Obligatorio';
$_['entry_status']          = 'Estado';
$_['entry_sort_order']      = 'Ordenar';

// Ayuda
$_['help_sort_order'] = 'Usar menos para contar hacia atrás.';

// Error
$_['error_permission'] = 'Sin permiso para modificar los Campos Personalizados.';
$_['error_name'] = 'El Nombre del Campo Personalizado debe contener entre 1 y 128 caractéres.';
$_['tipo_error'] = 'Los Valores de los Campos Personalizados son Obligatorios.';
$_['error_custom_value'] = 'El Nombre del Campo Personalizado debe contener entre 1 y 128 caractéres.';
