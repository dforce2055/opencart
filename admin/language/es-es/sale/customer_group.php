<?php
// Heading
$_['heading_title']     = 'Grupos de Clientes';

// Text
$_['text_success'] = 'Se han modificado los Grupos de Clientes';
$_['text_list'] = 'Lista de Grupos de Clientes';
$_['text_add'] = 'Agregar Grupo de Clientes';
$_['text_edit'] = 'Editar Grupo de Clientes';

// Column
$_['column_name'] = 'Nombre del Grupo de Clientes';
$_['column_sort_order'] = 'Orden';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre del Grupo de Clientes';
$_['entry_description'] = 'Descripción';
$_['entry_approval'] = 'Aprobar Clientes Nuevos';
$_['entry_sort_order'] = 'Orden';

// Ayuda
$_['help_approval'] = 'Los clientes deben ser aprobados por el Administrador antes de que puedan Iniciar Sesión.';

// Error
$_['error_permission'] = 'Sin permiso para modificar los Grupos de Clientes.';
$_['error_name'] = 'El Nombre del Grupo de Clientes debe contener entre 3 y 32 caractéres.';
$_['error_default'] = 'Este Grupo de Clientes no puede ser eliminado, ya que actualmente está asignado como el Grupo por Defecto.';
$_['error_store'] = 'Este Grupo de Clientes no puede ser eliminado, ya que está actualmente asignado a %s Comercios.';
$_['error_customer'] = 'Este Grupo de Clientes no puede ser eliminado, ya que está asignado actualmente a %s clientes.';
$_['error_seller'] = 'Este Grupo de Clientes no puede ser eliminado, ya que está asignado actualmente a %s vendedores.';
