<?php
// Heading
$_['heading_title']                           = 'FraudLabs Pro';

// Text
$_['text_fraud']                              = 'Anti-Fraud';
$_['text_success']                            = 'Éxito: ¡Has modificado FraudLabs Pro!.';
$_['text_edit']                               = 'Configuraciones';
$_['text_signup']                             = 'FraudLabsPro es un servicio de detección de fraudes. Si no tiene una clave para la API, puede <a href="http://www.fraudlabspro.com/plan?ref=1730" target="_blank"><u>registrarse aquí</u></a>.';
$_['text_id']                                 = 'FraudLabs Pro ID';
$_['text_ip_address']                         = 'Dirección de IP';
$_['text_ip_net_speed']                       = 'IP Velocidad de la Red';
$_['text_ip_isp_name']                        = 'IP Nombre del ISP';
$_['text_ip_usage_type']                      = 'IP Tipo de Uso';
$_['text_ip_domain']                          = 'IP Dominio';
$_['text_ip_time_zone']                       = 'IP Zona Horaria';
$_['text_ip_location']                        = 'IP Localización';
$_['text_ip_distance']                        = 'IP Distancia';
$_['text_ip_latitude']                        = 'IP Latitud';
$_['text_ip_longitude']                       = 'IP Longitud';
$_['text_risk_country']                       = 'País Alto Riesgo';
$_['text_free_email']                         = 'Gratis Email';
$_['text_ship_forward']                       = 'Evnío Adelantado';
$_['text_using_proxy']                        = 'Usando Proxy';
$_['text_bin_found']                          = 'BIN Encontrado';
$_['text_email_blacklist']                    = 'Lista Negra de Correos Electrónicos';
$_['text_credit_card_blacklist']              = 'Lista Negra de Tarjetas de Crédito';
$_['text_score']                              = 'FraudLabsPro Puntaje';
$_['text_status']                             = 'FraudLabs Pro Estado';
$_['text_message']                            = 'Mensaje';
$_['text_transaction_id']                     = 'Transaction ID';
$_['text_credits']                     		  = 'Balance';
$_['text_error']                              = 'Error:';
$_['text_flp_upgrade']                        = '<a href="http://www.fraudlabspro.com/plan" target="_blank">[Mejorar las Prestaciones]</a>';
$_['text_flp_merchant_area']                  = 'Por favor inicie sesión en <a href="http://www.fraudlabspro.com/login" target="_blank">FraudLabs Pro Zona Comercial</a> para más información sobre de este pedido.';


// Entry
$_['entry_status']                            = 'Estado';
$_['entry_key']                               = 'API Key';
$_['entry_score']                             = 'Puntuación de Riesgo';
$_['entry_order_status']                      = 'Order Estado';
$_['entry_review_status']                     = 'Review Estado';
$_['entry_approve_status']                    = 'Aprobar Estado';
$_['entry_reject_status']                     = 'Reject Estado';
$_['entry_simulate_ip']                       = 'Simulate IP';

// Ayuda
$_['help_order_status']                       = 'Los pedidos que tengan un puntaje por encima de su puntaje de riesgo establecido, se les asignará el estado de riesgo y no se les permitirá alcanzar el estado completo automáticamente.';
$_['help_review_status']                      = 'Pedidos that marked as review by FraudLabs Pro will be assigned this order status.';
$_['help_approve_status']                     = 'Pedidos that marked as approve by FraudLabs Pro will be assigned this order status.';
$_['help_reject_status']                      = 'Pedidos that marked as reject by FraudLabs Pro will be assigned this order status.';
$_['help_simulate_ip']                        = 'Simulate the visitor IP address for testing. Leave blank for production run.';
$_['help_fraudlabspro_id']                    = 'Unique identifier to identify a transaction screened by FraudLabs Pro system.';
$_['help_ip_address']                         = 'Dirección de IP.';
$_['help_ip_net_speed']                       = 'Connection speed.';
$_['help_ip_isp_name']                        = 'Estimated ISP de la dirección de IP.';
$_['help_ip_usage_type']                      = 'Estimated usage type de la dirección de IP. E.g, ISP, Commercial, Residential.';
$_['help_ip_domain']                          = 'Estimated domain name de la dirección de IP.';
$_['help_ip_time_zone']                       = 'Estimated time zone de la dirección de IP.';
$_['help_ip_location']                        = 'Estimated location de la dirección de IP.';
$_['help_ip_distance']                        = 'Distancia from IP address to Billing Localización.';
$_['help_ip_latitude']                        = 'Estimated latitude de la dirección de IP.';
$_['help_ip_longitude']                       = 'Estimated longitude de la dirección de IP.';
$_['help_risk_country']                       = 'Whether IP address or billing address country is in the latest high risk list.';
$_['help_free_email']                         = 'Whether Email is from free Email provider.';
$_['help_ship_forward']                       = 'Si la dirección de envío está en la base de datos de correos electrónicos conocidos.';
$_['help_using_proxy']                        = 'Whether IP address is from Proxy Anonimo Server.';
$_['help_bin_found']                          = 'Whether the BIN information matches our BIN list.';
$_['help_email_blacklist']                    = 'Whether the email address is in our blacklist database.';
$_['help_credit_card_blacklist']              = 'Whether the crédito card is in our blacklist database.';
$_['help_score']                              = 'Risk score, 0 (low risk) - 100 (high risk).';
$_['help_status']                             = 'FraudLabs Pro status.';
$_['help_message']                            = 'FraudLabs Pro error message description.';
$_['help_transaction_id']                     = 'Click the link to view the details fraud analysis.';
$_['help_credits']                            = 'Balance of queries in your account after this transaction.';

// Error
$_['error_permission']                        = 'Advertencia: No tiene permisos para modificar FraudLabs Pro settings.';
$_['error_key']		                          = 'Se requiere Clave de Licencia.';
