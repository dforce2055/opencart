<?php
// Heading
$_['heading_title']    = 'Klarna Cuota';

// Text
$_['text_total'] = 'Pedidos Totales';
$_['text_success'] = 'Se ha modificado el total de Cuota de Klarna.';
$_['text_edit'] = 'Editar Klarna Cuota Total';
$_['text_sweden'] = 'Suecia';
$_['text_norway'] = 'de Noruega';
$_['text_finland'] = 'Finlandiaia';
$_['text_denmark'] = 'Dinamarca';
$_['text_germany'] = 'Alemania';
$_['text_netherlands'] = 'Países Bajos';

// Entrada
$_['entry_total'] = 'Pedido total';
$_['entry_fee'] = 'Factura Cuota';
$_['entry_tax_class'] = 'Clase de Impuesto';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Pedido';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Total de Tarifa Klarna.';