<?php
// Heading
$_['heading_title']    = 'Cupón';

// Text
$_['text_total'] = 'Total';
$_['text_success'] = 'Se ha modificado el Total de Cupón';
$_['text_edit'] = 'Editar Cupón';

// Entrada
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Orden';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Total de los Cupones.';