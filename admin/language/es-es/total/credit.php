<?php
// Heading
$_['heading_title']    = 'Crédito del Comercio';

// Text
$_['text_total'] = 'Total';
$_['text_success'] = 'Se ha modificado el Total de Crédito del Comercio.';
$_['text_edit'] = 'Editar Crédito del Comercio';

// Entrada
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Orden';

// Error
$_['error_permission'] = 'Sin permiso para modificar Total de Crédito del Comercio.';