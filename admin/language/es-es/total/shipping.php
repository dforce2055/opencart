<?php
// Heading
$_['heading_title']    = 'Envío';

// Text
$_['text_total'] = 'Total para el Envío';
$_['text_success'] = 'Se ha modificado el Total del Envío.';
$_['text_edit'] = 'Editar Total del Envío Total';

// Entrada
$_['entry_estimator'] = 'Estimador de Envío';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Pedidos';

// Error
$_['error_permission'] = 'Sin permiso para modificar Total del Envío.';