<?php
// Text
$_['text_subject'] = '%s - ¡Su cuenta ah sido activada!';
$_['text_welcome'] = ' Bienvenido y gracias por registrarse como %s!';
$_['text_login']   = 'Su cuenta ha sido creada, puede iniciar sesión usando su dirección de E-mail y contraseña visitando nuestro sitio web o desde la siguiente URL:';
$_['text_service'] = 'Al iniciar sesión, podrá acceder a otros servicios, incluidos la revisión de pedidos anteriores, la impresión de facturas y la edición de la información de su cuenta.';
$_['text_thanks']  = 'Gracias,';
