<?php
// Text
$_['text_subject'] = '%s - Solicitud de Recupero de Contraseña';
$_['text_greeting'] = 'Se solicitó una nueva contraseña para de Administrador %s.';
$_['text_change'] = 'Para restablecer la contraseña, haga click en el siguiente enlace:';
$_['text_ip'] = 'La IP utilizada para hacer esta Solicitud fue: %s';
