<?php
// Text
$_['text_approve_subject'] = '%s - Cuenta Activada.';
$_['text_approve_welcome'] = 'Bienvenidos y gracias por registrarse en %s.';
$_['text_approve_login'] = 'Su cuenta ha sido creada, puede iniciar sesión usando su dirección de E-mail y contraseña visitando nuestro sitio web o desde la siguiente URL:';
$_['text_approve_services'] = 'Al iniciar Sesión, se podrá acceder a otros servicios, como la revisión de los Pedidos anteriores, las facturas de impresión y edición de la información de la cuenta.';
$_['text_approve_thanks'] = 'Gracias,';
$_['text_transaction_subject'] = '%s - Cuenta de Crédito';
$_['text_transaction_received'] = 'Se ha recibido un crédito de %s.';
$_['text_transaction_total'] = 'El importe total del crédito es de %s.' . "\ n \ n". 'El crédito se deducirá automáticamente de la próxima Compra. ';
$_['text_reward_subject'] = '%s - Puntos de recompensa';
$_['text_reward_received'] = 'Se han recibido%s Puntos de recompensa.';
$_['text_reward_total'] = 'El número total de Puntos de Recompensa es de %s.';
