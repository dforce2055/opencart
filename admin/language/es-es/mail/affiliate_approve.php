<?php
// Text
$_['text_subject']  = '%s - ¡Su cuenta de afiliado ha sido activada!';
$_['text_welcome']  = ' Bienvenido y gracias por registrarse como %s!';
$_['text_login']    = 'Su cuenta ha sido creada, puede iniciar sesión usando su dirección de E-mail y contraseña visitando nuestro sitio web o desde la siguiente URL:';
$_['text_services'] = 'al iniciar sesión, podrá generar códigos de seguimiento, rastrear pagos de comisiones y editar la información de su cuenta.';
$_['text_thanks']   = 'Gracias,';
