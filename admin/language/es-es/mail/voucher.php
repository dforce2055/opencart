<?php
// Text
$_['text_subject'] = 'Se le ha enviado un Voucher de Regalo de %s';
$_['text_greeting'] = 'Felicitaciones, ha recibido un Voucher de Regalo por valor de %s';
$_['text_from'] = 'Este Voucher de Regalo ha sido enviado por %s';
$_['text_message'] = 'Con el siguiente mensaje ';
$_['text_redeem'] = 'Para canjear este Voucher de Regalo, anotar el código siguiente <b> %s </ b> a continuación, hacer click en el link de abajo y comprar el producto en el que desea utilizar este Voucher de Regalo. Se puede introducir el código del Cupón de regalo en la página del Carro de Compras antes de hacer click en Comprar. ';
$_['text_footer'] = 'Responder a este Email si tiene dudas.';
