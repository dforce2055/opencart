<?php
// Text
$_['text_approve_subject']      = '¡%s - Tu cuenta ha sido activada!';
$_['text_approve_welcome']      = 'Bienvenido y gracias por registrarte en %s!';
$_['text_approve_login']        = 'Su cuenta "%s" ha sido creada, puede iniciar sesión usando su dirección de E-mail y contraseña visitando nuestro sitio web o desde la siguiente URL:';
$_['text_approve_services']     = 'Al iniciar sesión, podrá acceder a otros servicios, como vender productos, revisión de pedidos, administrar clientes y la edición de la información de su cuenta.';
$_['text_approve_thanks']       = 'Gracias,';
$_['text_transaction_subject']  = '%s - Crédito de la Cuenta';
$_['text_transaction_received'] = '¡Has recibido %s Crédito!';
$_['text_transaction_total']    = 'Ahora el monto total de su Crédito es %s.' . "\n\n" . 'El Crédito de su cuenta se deducirá automáticamente en su próxima compra';
$_['text_reward_subject']       = '%s - Puntos de Recompensa';
$_['text_reward_received']      = '¡Has recibido %s Puntos de Recompensa!';
$_['text_reward_total']         = 'Ahora su número total de Puntos de Recompensa es %s.';
?>
