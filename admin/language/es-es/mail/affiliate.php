<?php
// Text
$_['text_approve_subject'] = '%s - La cuenta de Afiliado ha sido activada.';
$_['text_approve_welcome'] = 'Bienvenidos y gracias por registrarse en %s.';
$_['text_approve_login'] = 'Su cuenta ha sido creada, puede iniciar sesión usando su dirección de E-mail y contraseña visitando nuestro sitio web o desde la siguiente URL:';
$_['text_approve_services'] = 'Al iniciar la sesión, es posible generar códigos de seguimiento, pagos de comisiones y editar la información de la cuenta.';
$_['text_approve_thanks'] = 'Gracias,';
$_['text_transaction_subject'] = '%s - Comisión de Afiliados';
$_['text_transaction_received'] = 'Se ha recibido comisión%s.';
$_['text_transaction_total'] = 'El importe total de la comisión es de%s.';
