<?php
// Text
$_['text_subject'] = '%s - ¡Su cuenta fue denegada!';
$_['text_welcome'] = '¡Bienvenido y gracias por registrarse como %s!';
$_['text_denied']  = 'Lamentablemente su solicitud ha sido denegada. Para más información contactese con el dueño de la tienda desde aquí:';
$_['text_thanks']  = 'Gracias,';
