<?php
// Text
$_['text_subject'] = '%s - Devolución actualizada %s';
$_['text_return_id'] = 'Devolución ID:';
$_['text_date_added'] = 'Fecha de Devolución:';
$_['text_return_status'] = 'La Devolución ha sido actualizada al siguiente estado:';
$_['text_comment'] = 'Los comentarios de la declaración son:';
$_['text_footer'] = 'Responda este Email si tiene alguna duda.';
