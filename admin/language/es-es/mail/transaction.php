<?php
// Text
$_['text_subject']  = '%s - Crédito de Afiliado';
$_['text_received'] = 'Has recibido %s crédito!';
$_['text_total']    = 'El monto total de crédito ahora es %s.';
$_['text_credit']   = 'El crédito de su cuenta, sera deducido en la proxima compra.';
