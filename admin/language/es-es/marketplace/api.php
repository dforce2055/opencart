<?php
// Heading
$_['heading_title']    = 'OpenCart Tienda API';

// Text
$_['text_success']     = 'Éxito: ¡Has modificado la información de tu API!';
$_['text_signup']      = 'Ingrese la información de su API de OpenCart que la puede Obtener de <a href="https://www.opencart.com/index.php?route=account/store" target="_blank" class="alert-link">aquí</a>.';

// Entry
$_['entry_username']   = 'Nombre de Usuario';
$_['entry_secret']     = 'Secreto';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar la tienda de API!';
$_['error_username']   = '¡Nombre de Usuario es requirido!';
$_['error_secret']     = '¡Secreto requerido!';
