<?php
// Text
$_['text_success']     = 'Éxito: ¡Has modificado extensiones!';
$_['text_unzip']       = 'Extrayendo Archivos!';
$_['text_move']        = '¡Copiando archivos!';
$_['text_xml']         = '¡Aplicando modificaciones!';
$_['text_remove']      = '¡Removiendo archivos temporarios!';

// Error
$_['error_permission'] = 'Advertencia: ¡No tiene permisos para modificar extensiones!';
$_['error_install']    = '¡La instalación de la Extensión se esta realizando, por favor espere unos segundos antes de intentar instalar una aplicación nueva!';
$_['error_unzip']      = 'El archivo Zip no se puede abrir!';
$_['error_file']       = '¡El Archivo de instalación no se pudo encontrar!';
$_['error_directory']  = '¡El Directorio de Instalación no se pudo encontrar!';
$_['error_code']       = '¡Es requerido Unicode para modificar XML!';
$_['error_xml']        = '¡Modificación %s ya esta siendo usada!';
$_['error_exists']     = '¡El archivo %s ya existe!';
$_['error_allowed']    = '¡El directorio %s no se puede escribir!';
