<?php
// Heading
$_['heading_title']     = 'Modificaciones';

// Text
$_['text_success']      = 'Éxito: ¡Has modificado modificaciones!';
$_['text_refresh']      = 'Siempre que habilite / deshabilite o elimine una modificación, tiene que hacer click en el boton para actulizar y reconstruir el cache!!';
$_['text_list']         = 'Lista de Modificaciones';

// Column
$_['column_name']       = 'Nombre de la Modificación';
$_['column_author']     = 'Autor';
$_['column_version']    = 'Versión';
$_['column_status']     = 'Estado';
$_['column_date_added'] = 'Fecha añadido';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar las Modificaciones!';
