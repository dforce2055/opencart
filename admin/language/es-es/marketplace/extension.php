<?php
// Heading
$_['heading_title'] = 'Extensiones';

// Text
$_['text_success']  = 'Éxito: Has modificado extensiones!';
$_['text_list']     = 'Lista de Extensiones';
$_['text_type']     = 'Seleccione el tipo de extensión';
$_['text_filter']   = 'Filtro';
