<?php
// Heading
$_['heading_title']      = 'Extensión Tienda';

// Text
$_['text_success']       = 'Éxito: ¡Has modificado las extensiones!';
$_['text_list']          = 'Lista de Extensión ';
$_['text_filter']        = 'Filtro';
$_['text_search']        = 'Buscar extensiones y temas';
$_['text_category']      = 'Categorías';
$_['text_all']           = 'Todas';
$_['text_theme']         = 'Temas';
$_['text_marketplace']   = 'Tiendas';
$_['text_language']      = 'Lenguajes';
$_['text_payment']       = 'Pago';
$_['text_shipping']      = 'Envío';
$_['text_module']        = 'Módulos';
$_['text_total']         = 'Total de Pedidos';
$_['text_feed']          = 'Cuotads';
$_['text_report']        = 'Reportes';
$_['text_other']         = 'Otros';
$_['text_free']          = 'Gratis';
$_['text_paid']          = 'Pagado';
$_['text_purchased']     = 'Comprado';
$_['text_date_modified'] = 'Fecha Modificación';
$_['text_date_added']    = 'Fecha añadido';
$_['text_rating']        = 'Rating';
$_['text_reviews']       = 'Opinión';
$_['text_compatibility'] = 'Compatibilidad';
$_['text_downloaded']    = 'Descargables';
$_['text_member_since']  = 'Miembro desde:';
$_['text_price']         = 'Precio';
$_['text_partner']       = 'Desarrollado por Socios de OpenCart';
$_['text_support']       = '12 Meses de soporte gratis';
$_['text_documentation'] = 'Documentación Incluida';
$_['text_sales']         = 'Ventas';
$_['text_comment']       = 'Comentarios';
$_['text_download']      = 'Descargando';
$_['text_install']       = 'Instalando';
$_['text_comment_add']   = 'Deje su comentario';
$_['text_write']         = 'Esciba su comentario aquí..';
$_['text_purchase']      = '¡Por favor confirme quien es!';
$_['text_pin']           = 'Por favor ingrese su número PIN de 4 dígitos. Este número PIN es para proteger su cuenta.';
$_['text_secure']        = 'No le dé su PIN a nadie, incluidos los desarrolladores. Si necesita instalar una extensión, entonces debe enviarle al vendedor por correo electrónico la extensión requerida.';
$_['text_name']          = 'Nombre de la Descarga';
$_['text_progress']      = 'Progreso';
$_['text_available']     = 'Instalaciones disponibles';
$_['text_action']        = 'Acción';

// Entry
$_['entry_pin']          = 'PIN';

// Tab
$_['tab_description']    = 'Descripción';
$_['tab_documentation']  = 'Documentación';
$_['tab_download']       = 'Descarga';
$_['tab_comment']        = 'Comentario';

// Button
$_['button_opencart']    = 'API de Tienda';
$_['button_purchase']    = 'Comprar';
$_['button_view_all']    = 'Ver todas las extensiones';
$_['button_get_support'] = 'Obtener Soporte';
$_['button_comment']     = 'Comentario';
$_['button_reply']       = 'Respuesta';

// Error
$_['error_permission']   = 'Advertencia: ¡No tiene permisos para modificar las extensiones!';
$_['error_opencart']     = 'Advertencia: ¡Debe ingresar información e su API de OpenCart antes de realizar cualquier compra!';
$_['error_install']      = '¡La instalación de la Extensión se esta realizando, por favor espere unos segundos antes de intentar instalar una aplicación nueva!';
$_['error_purchase']     = '¡La Extensión no se pudo comprar!';
$_['error_download']     = '¡La Extensión no se pudo descargar!';
