<?php
// Heading
$_['heading_title']        				= 'OpenBay Pro';

// Buttons
$_['button_retry']						= 'Reintentar';
$_['button_update']						= 'Actualizar';
$_['button_patch']						= 'Ruta';
$_['button_faq']						= 'Ver Preguntas Frecuentes';

// Tab
$_['tab_setting']						= 'Configuraciones';
$_['tab_update']						= 'Actualizaciones de Software';
$_['tab_developer']						= 'Desarrollador ';

// Text
$_['text_dashboard']         			= 'Panel de Control';
$_['text_success']         				= 'Éxito: ¡Las Configuraciones fueron guardadas';
$_['text_products']          			= 'Artículos';
$_['text_orders']          				= 'Pedidos';
$_['text_manage']          				= 'Administrar';
$_['text_help']                     	= 'Ayuda';
$_['text_tutorials']                    = 'Tutoriales';
$_['text_suggestions']                  = 'Ideas';
$_['text_version_latest']               = 'Estas corriendo la ultima versión';
$_['text_version_check']     			= 'Controlar la versión del Software';
$_['text_version_installed']    		= 'Versión instalada de OpenBay Pro: v';
$_['text_version_current']        		= 'Su versión es';
$_['text_version_available']        	= 'La última es';
$_['text_language']             		= 'Lenguaje de la API';
$_['text_getting_messages']     		= 'Obtener mensajes de OpenBay Pro';
$_['text_complete']     				= 'Completar';
$_['text_patch_complete']           	= 'Parche aplicado';
$_['text_updated']						= 'El modulo ha sido actualizado (v.%s)';
$_['text_update_description']			= 'La herramienta de actualización, va a realizar cambios en el sistema de archivos de la tienda. Asegurese de hacer un respaldo completo de su base de datos y sistema de archivos, antes de actualizar.';
$_['text_patch_description']			= 'Si sube los archivos de manera manual, es necesario que ejecute el parche para completar la actualización';
$_['text_clear_faq']                    = 'Borrar ventanas emergentes de Preguntas Frecuentes';
$_['text_clear_faq_complete']           = 'Las notificaciones volveran a mostrarse';
$_['text_install_success']              = 'La tienda ha sido instalada';
$_['text_uninstall_success']            = 'La tienda ha sido eliminada';
$_['text_title_messages']               = 'Mensajes &amp; notificaciones';
$_['text_marketplace_shipped']			= 'El estado del pedido se actualizara para enviarlo a la tienda';
$_['text_action_warning']				= 'Esta acción es peligrosa, por eso esta protegida por contraseña.';
$_['text_check_new']					= 'Comprobar si existe una nueva versión';
$_['text_downloading']					= 'Descargando archivos de actualización';
$_['text_extracting']					= 'Extrayendo Archivos';
$_['text_running_patch']				= 'Ejecutar parche de actualización';
$_['text_fail_patch']					= 'No se puede extraer archivos de actualización';
$_['text_updated_ok']					= 'Actualización completa, se ha instalado una nueva versión ';
$_['text_check_server']					= 'Controlar requerimientos del servidor';
$_['text_version_ok']					= 'El Software esta actualizado, la versión instalada es ';
$_['text_remove_files']					= 'La eliminación de archivos ya no es necesaria';
$_['text_confirm_backup']				= 'Asegurese de tener un respaldo completo antes de continuar';
$_['text_software_update']				= 'OpenBay Pro actualización de software';
$_['text_patch_option']				    = 'Manual patching';

// Column
$_['column_name']          				= 'Nombre del Plugin(complemento)';
$_['column_status']        				= 'Estado';
$_['column_action']        				= 'Acción';

// Entry
$_['entry_patch']            			= 'Parche de actualización manual';
$_['entry_courier']						= 'Servicio de Mensajería';
$_['entry_courier_other']           	= 'Otro Servicio de Mensajería';
$_['entry_tracking']                	= 'Rastreo(Tracking) #';
$_['entry_empty_data']					= '¿Datos de la tienda vacios';
$_['entry_password_prompt']				= 'Por favor, ingrese la contraseña para borrar los datos';
$_['entry_update']						= 'Actualización fácil en 1 click';
$_['entry_beta']						= 'Esar una versión Beta';

// Error
$_['error_admin']             			= 'Se espera el directorio de administación';
$_['error_failed']						= 'Fallo la carga ¿Reintentar?';
$_['error_tracking_id_format']			= 'El ID de rastreo no puede contener caractéres > ó <';
$_['error_tracking_courier']			= 'Debe seleccionar un servicio de Mensajería si desea agregar un ID de rastreo';
$_['error_tracking_custom']				= 'Por favor, deje el campo de mensajería si desea utilizar un servicio de mensajería personalizado';
$_['error_permission']					= 'No tiene permisos para modificar la extensión de OpenBay Pro';
$_['error_file_delete']					= 'No se pudieron eliminar estos archivos, debe eliminarlos manualmente';
$_['error_mkdir']						= 'La función de PHP "mkdir" esta deshabilitada, contactese con su provedor de hosting.';
$_['error_openssl_encrypt']            	= 'La función de PHP "openssl_encrypt" esta deshabilitada. contactese con su proveedor de hosting.';
$_['error_openssl_decrypt']            	= 'La función de PHP "openssl_decrypt" esta deshabilitada. contactese con su proveedor de hosting.';
$_['error_fopen']             			= 'La función de PHP "fopen" esta deshabilitada. contactese con su proveedor de hosting.';
$_['error_url_fopen']             		= 'La directiva "allow_url_fopen" directive esta deshabilitada en el servidor - No podra importar imagenes a los productos importados de eBay';
$_['error_curl']                        = 'PHP library "CURL" esta deshabilitada. contactese con su proveedor de hosting.';
$_['error_zip']                         = 'La extensión ZIP necesita ser cargada. contactese con su proveedor de hosting.';
$_['error_mbstring']               		= 'La libreria de PHP "mb strings" esta deshabilitada. contactese con su proveedor de hosting.';
$_['error_oc_version']             		= 'Su versión de OpenCart no esta probada ara funcionar con este modulo. Puede experimentar algunos problemas.';

// Ayuda
$_['help_clear_faq']					= 'Mostrar todas las notificaciones de ayuda de nuevo';
$_['help_empty_data']					= '¡Esto puede causar daños graves, no lo use si no sabe lo que esta haciendo!';
$_['help_easy_update']					= 'Click en actualizar para instalar la ultima versión de OpenBay Pro automaticamente';
$_['help_patch']						= 'Click para ejecutar los scripts de parche';
$_['help_beta']							= '¡Cuidado! La versión beta es la última versión de desarrollo. Puede que no sea estable y contenga errores.';
