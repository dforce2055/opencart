<?php
// Heading
$_['heading_title']     = 'Instalador de Extensiones';

// Text
$_['text_progress']     = 'Progreso de Instalación';
$_['text_upload']       = 'Subir sus extensiones';
$_['text_history']      = 'Historial de instalaciones';
$_['text_success']      = 'Éxito: ¡La extensión fue instalada!';
$_['text_install']      = 'Instalando';

// Column
$_['column_filename']   = 'Nombre de Archivo';
$_['column_date_added'] = 'Fecha añadido';
$_['column_action']     = 'Acción';

// Entry
$_['entry_upload']      = 'Subir Archivo';
$_['entry_progress']    = 'Progreso';

// Ayuda
$_['help_upload']       = 'Requiere un archivo de modificación con extensión \'.ocmod.zip\'.';

// Error
$_['error_permission']  = 'Advertencia:¡No tiene permisos para modificar extensiones!';
$_['error_install']     = '¡La instalación de la Extensión se esta realizando, por favor espere unos segundos antes de intentar instalar una aplicación nueva!';
$_['error_upload']      = '¡El archivo no se puede subir!';
$_['error_filetype']    = '¡Tipo de archivo invalido!';
$_['error_file']        = '¡El Archivo no se pudo encontrar!';
