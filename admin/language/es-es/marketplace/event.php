<?php
// Heading
$_['heading_title']     = 'Eventos';

// Text
$_['text_success']      = 'Éxito: Has modificado events!';
$_['text_list']         = 'Lista de Eventos';
$_['text_event']        = 'Los eventos son utilizados por las extensiones para anular funcionalidad predeterminada de su tienda. Si tiene problemas puede habilitar / deshabilitar los eventos desde aquí.';
$_['text_info']         = 'Información de Evento';
$_['text_trigger']      = 'Disparador';
$_['text_action']       = 'Acción';

// Column
$_['column_code']       = 'Código de Evento';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Orden de Clasificación';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permisos para modificar extensiones!';
