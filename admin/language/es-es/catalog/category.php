<?php
// Heading
$_ ['heading_title'] = 'Rubros';

// Text
$_ ['text_success'] = 'Se han modificado las Rubros.';
$_['text_list'] = 'Lista de Rubros';
$_ ['text_add'] = 'Agregar Rubro';
$_ ['text_edit'] = 'Editar Rubro';
$_ ['text_default'] = 'Por Defecto';

//Column
$_ ['column_name'] = 'Nombre del Rubros';
$_ ['column_sort_order'] = 'Orden';
$_ ['column_action'] = 'Acción';

// Entry
$_['entry_name']             = 'Nombre del Rubro';
$_ ['entry_description'] = 'Descripción';
$_ ['entry_meta_title'] = 'Título Meta Tag';
$_ ['entry_meta_keyword'] = 'Palabras Clave Meta Tag';
$_ ['entry_meta_description'] = 'Meta Descripción de la etiqueta';
$_ ['entry_keyword'] = 'SEO palabra clave';
$_ ['entry_parent'] = 'Padre';
$_ ['entry_filter'] = 'Filtros';
$_ ['entry_store'] = 'Comercios';
$_ ['entry_image'] = 'Imagen';
$_ ['entry_top'] = 'Top';
$_ ['entry_column'] = 'Columnas';
$_ ['entry_sort_order'] = 'Orden';
$_ ['entry_status'] ='Estado';
$_ ['entry_layout'] ='Ignorar Layout';

// Ayuda
$_ ['help_filter'] = '(Autocompletar)';
$_ ['help_keyword'] = 'No utilizar espacios, reemplazarlos con -. La palabra clave debe ser única.';
$_ ['help_top'] = 'Mostrar en la barra de menú superior. Sólo funciona para los Rubros Padres ';
$_ ['help_column'] = 'Número de columnas que se utilizará para los 3 Rubros inferiores. Sólo funciona para los Rubros Padres';

//Error
$_ ['error_warning'] = 'El Formulario contiene Errores.';
$_ ['error_permission'] = 'Error: Sin permiso para modificar los Rubros.';
$_ ['error_name'] = 'El Nombre del Rubro debe contener entre 2 y 32 caractéres.';
$_ ['error_meta_title'] = 'El Meta Título debe contener entre 3 y 255 caractéres.';
$_ ['error_keyword'] = 'Palabra clave SEO ya en uso.';

//Sellers
$_['text_approved']         = 'La categoría ha sido aprobada';
$_['entry_approve']           = 'Aprobar:';
$_['approve_enabled']        = 'Aprobado';
$_['approve_disabled']       = 'Sin Aprobar';
$_['approve_success']        = 'Has aprobado (%s) paquetes(s';
$_['column_approve']          = 'Aprobar';
$_['column_seller']       = 'Vendedor';
$_['column_approved']       = 'Aprobado';
