<?php
// Heading
$_['heading_title'] = 'Comentarios';

// Text
$_['text_success'] = 'Se han modificado los Comentarios.';
$_['text_list'] = 'Lista de Comentarios ';
$_['text_add'] = 'Agregar Comentario';
$_['text_edit'] = 'Editar Comentario ';

// Column
$_['column_product'] = 'Producto';
$_['column_author'] = 'Autor';
$_['column_rating'] = 'Valoración';
$_['column_status'] = 'Estado';
$_['column_date_added'] = 'Fecha Alta';
$_['column_action'] = 'Acción';

// Entry
$_['entry_product'] = 'Producto';
$_['entry_author'] = 'Autor';
$_['entry_rating'] = 'Valoración';
$_['entry_status'] = 'Estado';
$_['entry_text'] = 'Texto';
$_['entry_date_added'] = 'Fecha de Alta';

// Ayuda
$_['help_product'] = '(Autocompletar)';

// Error
$_['error_permission'] = 'Sin permiso para modificar los Comentarios.';
$_['error_product'] = 'Producto Obligatorio.';
$_['error_author'] = 'El Autor debe contener entre 3 y 64 caractéres.';
$_['ERROR_TEXT'] = 'El Texto del Comentario debe contener al menos 1 caracter.';
$_['error_rating'] = 'La Valoración del Comentario es Obligatoria.';