<?php
// Heading
$_ ['heading_title'] = 'Perfiles Recurrentes';

// Text
$_['text_success'] = 'Se han modificado los Perfiles Recurrentes.';
$_['text_list'] = 'Lista de Perfiles Recurrentes';
$_['text_add'] = 'Agregar Perfil Recurrente';
$_['text_edit'] = 'Editar Perfil recurrentes';
$_['text_day'] = 'Día';
$_['text_week'] = 'Semana';
$_['text_semi_month'] = 'Quincena';
$_['text_month'] = 'Mes';
$_['text_year'] = 'Año';
$_['text_recurring'] = '<p> <i class = "fa fa-info-circle"> </ i> cantidades recurrentes se calculan por la frecuencia y ciclos. </ p> <p> Por ejemplo, si utilizar una frecuencia de "semana" y un ciclo de "2", entonces el usuario será facturado cada 2 semanas. </ p> <p> La duración es el número de veces que el usuario va a hacer un pago, ajustar a 0 para hacer pagos hasta que se cancelen. </ p>';
$_['text_profile'] = 'Perfil Recurrente';
$_['text_trial'] = 'Trial Perfil';

// Entry
$_['entry_name'] = 'Nombre';
$_['entry_price'] = 'Precio';
$_['entry_duration'] = 'Duración';
$_['entry_cycle'] = 'Ciclo';
$_['entry_frequency'] ='Frecuencia';
$_['entry_trial_price'] ='Precio Trial';
$_['entry_trial_duration'] = 'Duración del Trial';
$_['entry_trial_status'] = 'Estado de Prueba';
$_['entry_trial_cycle'] = 'Ciclo de Prueba';
$_['entry_trial_frequency'] = 'Frecuencia de Prueba';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Orden';

// Column
$_['column_name'] = 'Nombre';
$_['column_sort_order'] = 'Orden';
$_['column_action'] = 'Acción';

// Error
$_['error_warning'] = 'El Formulario contiene errores.';
$_['error_permission'] = 'Sin permiso para modificar perfiles recurrentes.';
$_['error_name'] = 'El Nombre del Perfil debe contener entre 3 y 255 caractéres.';
$_['error_product'] = 'Este perfil recurrente no puede suprimirse, ya que está asignado actualmente a los productos de% s.';