<?php
// Heading
$_ ['heading_title'] = 'Opciones';

// Text
$_ ['text_success'] = 'Se han modificado las Opciones.';
$_ ['text_list'] = 'Lista de Opciones';
$_ ['text_add'] = 'Agregar Opciones';
$_ ['text_edit'] = 'Editar Opciones';
$_ ['text_choose'] = 'Elegir';
$_ ['text_select'] = 'Seleccionar';
$_ ['text_radio'] = 'Radio';
$_ ['text_checkbox'] = 'Casilla';
$_ ['text_image'] = 'Imagen';
$_ ['text_input'] = 'Entrada';
$_ ['text_text'] = 'Texto';
$_ ['text_textarea'] = 'Textarea';
$_ ['text_file'] = 'Archivo';
$_ ['text_date'] = 'Fecha';
$_ ['text_datetime'] = 'Fecha y Hora';
$_ ['text_time'] = 'Tiempo';

// Column
$_ ['column_name'] = 'Nombre de la Opción';
$_ ['column_sort_order'] = 'Orden';
$_ ['column_action'] = 'Acción';

// Entry
$_ ['entry_name'] = 'Nombre de la Opción';
$_ ['entry_type'] = 'Tipo';
$_ ['entry_option_value'] = 'Nombre del Valor de la Opción';
$_ ['entry_image'] = 'Imagen';
$_ ['entry_sort_order'] = 'Orden';

// Error
$_ ['error_permission'] = 'Sin permiso para modificar las Opciones.';
$_ ['error_name'] = 'El Nombre de la Opción debe contener entre 1 y 128 caractéres.';
$_ ['tipo_error'] = 'Los valores de Opción son Obligatorios.';
$_ ['error_option_value'] = 'El Nombre del Valor de la Opción debe contener entre 1 y 128 caractéres.';
$_ ['error_product'] = 'Esta opción no se puede eliminar ya que está asignada actualmente a los productos de% s.';

//Sellers
$_['entry_approve']           = 'Aprobar:';
$_['approve_enabled']        = 'Aprobado';
$_['approve_disabled']       = 'Sin Aprobar';
$_['approve_success']        = 'Has aprobado (%s) paquetes(s';
$_['column_approve']          = 'Aprobar';
$_['column_seller']       = 'Vendedor';
$_['column_approved']       = 'Aprobado';
