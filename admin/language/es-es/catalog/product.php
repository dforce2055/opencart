<?php
// Heading
$_['heading_title'] = 'Productos';

// Text
$_['text_success'] = 'Se han modificado los Productos.';
$_['text_list'] = 'Lista de productos';
$_['text_add'] = 'Agregar producto';
$_['text_edit'] = 'Editar producto ';
$_['text_plus'] = '+';
$_['text_minus'] = '-';
$_['text_default'] = 'Por defecto';
$_['text_option'] = 'Opción';
$_['text_option_value'] = 'Valor de la Opción';
$_['text_percent'] = 'Porcentaje';
$_['text_amount'] = 'Cantidad Fija';

// Column
$_['column_name'] = 'Nombre';
$_['column_model'] = 'Modelo';
$_['column_image'] = 'Imagen';
$_['column_price'] = 'Precio';
$_['column_quantity'] = 'Cantidad';
$_['column_status'] = 'Estado';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre';
$_['entry_description'] = 'Descripción';
$_['entry_meta_title'] = 'Meta Título';
$_['entry_meta_keyword'] = 'Meta Palabras';
$_['entry_meta_description'] = 'Meta Descripción';
$_['entry_keyword'] = 'SEO URL';
$_['entry_model'] = 'Modelo';
$_['entry_sku'] = 'Código de Barras';
$_['entry_upc'] = 'UPC';
$_['entry_ean'] = 'EAN';
$_['entry_jan'] = 'Jan';
$_['entry_isbn'] = 'ISBN';
$_['entry_mpn'] = 'MPN';
$_['entry_location'] = 'Ubicación';
$_['entry_shipping'] = 'Envío';
$_['entry_manufacturer'] = 'Marca';
$_['entry_store'] = 'Comercios';
$_['entry_date_available'] = 'Fecha de Disponibilidad';
$_['entry_quantity'] = 'Stock';
$_['entry_minimum'] = 'Cantidad mínima en Pedido';
$_['entry_stock_status'] = 'Estado Agotado';
$_['entry_price'] = 'Precio';
$_['entry_tax_class'] = 'Clase de Impuesto ';
$_['entry_points'] = 'Puntos';
$_['entry_option_points'] = 'Puntos';
$_['entry_subtract'] = 'Restar del Stock';
$_['entry_weight_class'] = 'Tipo de Peso';
$_['entry_weight'] = 'Peso';
$_['entry_dimension'] ='Dimensiones (L x An x Al)';
$_['entry_length_class'] = 'Clase de Medida';
$_['entry_length'] = 'Longitud';
$_['entry_width'] = 'Ancho';
$_['entry_height'] = 'Altura';
$_['entry_image'] = 'Imagen';
$_['entry_customer_group'] = 'Grupo de Clientes';
$_['entry_date_start'] = 'Fecha de Inicio';
$_['entry_date_end'] = 'Fecha Fin';
$_['entry_priority'] = 'Prioridad';
$_['entry_attribute'] = 'Atributo';
$_['entry_attribute_group'] = 'Grupo de Atributos';
$_['entry_text'] = 'Texto';
$_['entry_option'] = 'Opción';
$_['entry_option_value'] ='Valor de la Opción';
$_['entry_necesario'] = 'Obligatorio';
$_['entry_status'] ='Estado';
$_['entry_sort_order'] = 'Orden';
$_['entry_category'] = 'Rubros';
$_['entry_filter'] = 'Filtros';
$_['entry_download'] = 'Descargas';
$_['entry_related'] = 'Productos Relacionados';
$_['entry_tag'] = 'Etiquetas del Producto';
$_['entry_reward'] = 'Puntos de recompensa';
$_['entry_layout'] = 'Diseño';
$_['entry_recurring'] = 'Recurrente Perfil';

// Ayuda
$_['help_keyword'] = 'No utilizar espacios, reemplazarlos con -. La Palabra Clave debe ser única.';
$_['help_sku'] = 'Código de Barras SKU(Stock Keeping Unit)';
$_['help_upc'] = 'Código Universal de Producto';
$_['help_ean'] = 'Código Numérico Europeo';
$_['help_jan'] = 'Código japonés';
$_['help_isbn'] = 'Código para Libros';
$_['help_mpn'] = 'Código de Marca';
$_['help_manufacturer'] = '(Autocompletar)';
$_['help_minimum'] = 'Mínimo del Pedido';
$_['help_stock_status'] = 'Estado se muestra cuando un producto está fuera de stock ';
$_['help_points'] = 'El número de puntos necesarios para comprar este artículo. Si no se desea comprar este producto con puntos dejar en 0.';
$_['help_category'] = '(Autocompletar)';
$_['help_filter'] = '(Autocompletar)';
$_['help_download'] = '(Autocompletar)';
$_['help_related'] = '(Autocompletar)';
$_['help_tag'] = 'Separadas por comas';

// Error
$_['error_warning'] = 'El Formulario contiene Errores.';
$_['error_permission'] = 'Sin permiso para modificar productos.';
$_['error_name'] = 'El Nombre del producto debe contener entre 3 y 255 caractéres.';
$_['error_meta_title'] = 'El Meta Título debe contener entre 3 y 255 caractéres.';
$_['error_model'] = 'El Modelo Producto debe contener entre 1 y 64 caractéres.';
$_['error_keyword'] = 'La Palabra Clave SEO ya está en uso.';

//Faltantes
$_['entry_additional_image'] = 'Imágenes Adicionales';

//Sellers
$_['column_seller']          = 'Vendedor';
$_['entry_seller_name']          = 'Nombre del Vendedor:';
$_['entry_approve']           = 'Aprobar:';
$_['approve_enabled']        = 'Aprobado';
$_['approve_disabled']       = 'Sin Aprobar';
$_['approve_success']        = 'Has aprobado (%s) paquetes(s)';
$_['column_approve']          = 'Aprobar';
$_['column_seller']          = 'Vendedor';
$_['tab_seller_links']          = 'Enlaces del Vendedor';
$_['entry_seller_name']          = 'Nombre del Vendedor:';
$_['button_seller']          = 'Agregar Vendedor';
