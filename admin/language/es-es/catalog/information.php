<?php
// Heading
$_['heading_title'] = 'Información';

// Text
$_['text_success'] = 'Se ha modificado la Información.';
$_['text_list'] = 'Lista de Información ';
$_['text_add'] = 'Agregar Información ';
$_['text_edit'] = 'Editar Información';
$_['text_default'] = 'Por Defecto';

// Column
$_['column_title'] = 'Título de la Información';
$_['column_sort_order'] = 'Orden';
$_['column_action'] = 'Acción';

// Entry
$_['entry_title'] = 'Título de la Información';
$_['entry_description'] = 'Descripción';
$_['entry_store'] = 'Comercios';
$_['entry_meta_title'] = 'Título Meta';
$_['entry_meta_keyword'] = 'Palabras Clave Meta';
$_['entry_meta_description'] = 'Meta Descripción de la Etiqueta';
$_['entry_keyword'] = 'SEO palabra clave';
$_['entry_bottom'] = 'Menu del footer';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Orden';
$_['entry_layout'] = 'Ignorar Diseño';

// Ayuda
$_['help_keyword'] = 'No utilizar espacios, reemplazarlos con -. La Palabra Clave debe ser única.';
$_['help_bottom'] = 'Mostrar en el pie de página.';

// Error
$_['error_warning'] = 'El Formulario contiene Errores.';
$_['error_permission'] = 'Sin permiso para modificar la información.';
$_['error_title'] = 'El Título de la Información debe contener entre 3 y 64 caractéres.';
$_['error_description'] = 'La Descripción debe contener más de 3 caractéres.';
$_['error_meta_title'] = 'El Meta Título debe contener entre 3 y 255 caractéres.';
$_['error_keyword'] = 'La Palabra Clave SEO ya está en uso.';
$_['error_account'] = 'Esta página de información no puede ser eliminada, ya que actualmente está asignada como los Términos para crear una Cuenta de Usuario.';
$_['error_checkout'] = 'Esta página de información no puede ser eliminada, ya que actualmente está asignada como los Términos para realizar una Compra.';
$_['error_affiliate'] = 'Esta página de información no puede ser eliminada, ya que actualmente está asignada como los Términos para las Cuentas de Afiliados.';
$_['error_return'] = 'Esta página de información no puede ser eliminada, ya que actualmente está asignada como los Términos para realizar Devoluciones.';
$_['error_store'] = 'Esta página de información no se puede eliminar como actualmente utilizado por los Comercios% s.';