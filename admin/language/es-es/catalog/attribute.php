<?php
// Heading
$_['heading_title']          = 'Atributos';

// Text
$_['text_success']           = 'Se han modificado los Atributos.';
$_['text_list']              = 'Lista de Atributos';
$_['text_add']               = 'Nuevo Atributo';
$_['text_edit']              = 'Editar Atributo';

// Column
$_['column_name']            = 'Nombre de Atributo';
$_['column_attribute_group'] = 'Grupo de Atributos';
$_['column_sort_order']      = 'Orden';
$_['column_action']          = 'Acción';

// Entry
$_['entry_name']            = 'Nombre de Atributo';
$_['entry_attribute_group'] = 'Grupo de Atributos';
$_['entry_sort_order']      = 'Orden';

// Error
$_['error_permission']      = 'Sin permiso para modificar los Atributos.';
$_['error_name']            = 'El Nombre del Atributo debe contener entre 3 y 64 caractéres.';
$_['error_product']         = 'Este Atributo no puede ser eliminado, ya que está asignado actualmente a Productos.';


//Sellers
$_['entry_approve']           = 'Aprobar:';
$_['approve_enabled']        = 'Aprobado';
$_['approve_disabled']       = 'Sin Aprobar';
$_['approve_success']        = 'Has aprobado (%s) paquetes(s';
$_['column_approve']          = 'Aprobar';
$_['column_seller']       = 'Vendedor';
$_['column_approved']       = 'Aprobado';
