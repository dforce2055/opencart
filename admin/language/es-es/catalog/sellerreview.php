<?php
// Heading
$_['heading_title']       = 'Opinión';

// Text
$_['text_success']      = 'Éxito: ¡Pudo modificar la Opinión!';

// Column
$_['column_product']    = 'ID Pedido';
$_['column_author']     = 'Autor';
$_['column_rating']     = 'Valoración';
$_['column_status']     = 'Estado';
$_['column_date_added'] = 'Fecha de agregación';
$_['column_action']     = 'Acción';

// Entry
$_['entry_product']     = 'Producto:<br/><span class="help">(Autocompletar)</span>';
$_['entry_author']      = 'Autor:';
$_['entry_rating']      = 'Valoración:';
$_['entry_status']      = 'Estado:';
$_['entry_text']        = 'Texto:';
$_['entry_good']        = 'Bueno';
$_['entry_bad']         = 'Malo';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permiso para modificar la Opinión!';
$_['error_product']     = 'Advertencia: ¡Se requiere un Producto!';
$_['error_author']      = 'Advertencia: ¡El Autor debe tener entre 3 y 64 caractéres!';
$_['error_text']        = 'Advertencia: ¡El texto de la Opinión debe tener al menos 1 car&aoacute;cter!';
$_['error_rating']      = 'Advertencia: !LA Opinión debe tener una Valoración!';
?>
