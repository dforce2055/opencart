<?php
// Heading
$_ ['heading_title'] = 'Categorías';

// Text
$_ ['text_success'] = 'Se han modificado las Categorías.';
$_['text_list'] = 'Lista de Categoría';
$_ ['text_add'] = 'Agregar Categoría';
$_ ['text_edit'] = 'Editar Categoría';
$_ ['text_default'] = 'Por Defecto';

//Column
$_ ['column_name'] = 'Nombre de la Categoría';
$_ ['column_sort_order'] = 'Orden';
$_ ['column_action'] = 'Acción';

// Entry
$_['entry_name']             = 'Nombre de la Categoría';
$_ ['entry_description'] = 'Descripción';
$_ ['entry_meta_title'] = 'Título Meta Tag';
$_ ['entry_meta_keyword'] = 'Palabras Clave Meta Tag';
$_ ['entry_meta_description'] = 'Meta Descripción de la etiqueta';
$_ ['entry_keyword'] = 'SEO palabra clave';
$_ ['entry_parent'] = 'Padre';
$_ ['entry_filter'] = 'Filtros';
$_ ['entry_store'] = 'Comercios';
$_ ['entry_image'] = 'Imagen';
$_ ['entry_top'] = 'Top';
$_ ['entry_column'] = 'Columnas';
$_ ['entry_sort_order'] = 'Orden';
$_ ['entry_status'] ='Estado';
$_ ['entry_layout'] ='Ignorar Layout';

// Ayuda
$_ ['help_filter'] = '(Autocompletar)';
$_ ['help_keyword'] = 'No utilizar espacios, reemplazarlos con -. La palabra clave debe ser única.';
$_ ['help_top'] = 'Mostrar en la barra de menú superior. Sólo funciona para las Categorías Padres ';
$_ ['help_column'] = 'Número de columnas que se utilizará para los 3 Categorías inferiores. Sólo funciona para las Categorías Padres';

//Error
$_ ['error_warning'] = 'El Formulario contiene Errores.';
$_ ['error_permission'] = 'Sin permiso para modificar las Categorías.';
$_ ['error_name'] = 'El Nombre de la Categoría debe contener entre 2 y 32 caractéres.';
$_ ['error_meta_title'] = 'El Meta Título debe contener entre 3 y 255 caractéres.';
$_ ['error_keyword'] = 'Palabra clave SEO ya en uso.';

$_ ['text_seleccionar'] = 'Seleccionar';
$_ ['entry_rubro'] = 'Rubro';

//Sellers
$_['text_approved']         = 'La categoría ha sido aprobada';
$_['entry_approve']           = 'Aprobar:';
$_['approve_enabled']        = 'Aprobado';
$_['approve_disabled']       = 'Sin Aprobar';
$_['approve_success']        = 'Has aprobado (%s) paquetes(s';
$_['column_approve']          = 'Aprobar';
$_['column_seller']       = 'Vendedor';
$_['column_approved']       = 'Aprobado';
