<?php
// Heading
$_['heading_title'] = 'Marcas';

// Text
$_['text_success'] = 'Se han modificado las Marcas.';
$_['text_list'] = 'Lista de Marcas';
$_['text_add'] = 'Agregar Marca';
$_['text_edit'] = 'Editar Marca';
$_['text_default'] = 'Por Defecto';
$_['text_percent'] = 'Porcentaje';
$_['text_amount'] = 'Cantidad Fija';

// Column
$_['column_name'] = 'Marca';
$_['column_sort_order'] = 'Orden';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Marca';
$_['entry_store'] = 'Comercios';
$_['entry_keyword'] = 'SEO palabra clave';
$_['entry_image'] = 'Imagen';
$_['entry_sort_order'] = 'Orden';
$_['entry_type'] = 'Tipo';

// help
$_['help_keyword'] = 'No utilizar espacios, reemplazar los espacios con -. La Palabra Clave debe ser única.';

// Error
$_['error_permission'] = 'Sin permiso para modificar las Marcas.';
$_['error_name'] = 'El Nombre de la Marca debe contener entre 2 y 64 caractéres.';
$_['error_keyword'] = 'Palabra clave SEO ya en uso.';
$_['error_product'] = 'Esta marca no puede ser eliminada, ya que está asignada actualmente a los productos de % s.';