<?php
// Heading
$_['heading_title']      = 'Comisión';

// Text
$_['text_success']       = 'Éxito: ¡Pudo modificar la comisión!';
$_['text_fixed_rate']    = 'Monto fijo';
$_['text_percentage']    = 'Porcentaje';

// Column
$_['column_name']			= 'Nombre de la Comisión';
$_['column_type']  			= 'Tipo de Comisión';
$_['column_commission'] 	= 'Porcentaje de Comisión';
$_['column_total_agents'] 	= 'Total de Vendedores';
$_['column_sort_order'] 	= 'Clasificar por Orden';
$_['column_action']     	= 'Acción';

$_['column_product_limit']		= 'Límite del Producto';

// Entry
$_['entry_product_limit']		= 'Límite del Producto:';

$_['entry_name']		= 'Nombre de la Comisión:';
$_['button_insert']		= 'Insertar';

$_['entry_type']		= 'Tipo de Comisión:';
$_['entry_commission']	= 'Porcentaje de Comisión:';
$_['entry_sort_order']  = 'Clasificar por Orden:';

// Error
$_['error_permission']  			= 'Advertencia: ¡No tiene permiso para modificar la Comisión!';
$_['error_delete']     				= 'Advertencia: ¡Esta Comisión no se puede eliminar ya que actualmente esta asignada a los agentes %s!';
$_['error_necesario_data']  			= 'Advertencia: ¡Por favor complete todos los datos requeridos!';
$_['error_commission_name']  		= '¡El nombre de la Comisión debe tener entre 3 y 64 caractres!';
$_['error_commission']   			= 'La Comisión debe ser números';


// ZMULTISELLER
$_['entry_amount']          	= 'Monto:';
$_['entry_discount']          	= 'Descuento Especial:<br/><span class="help">Esto sobrescribirá su descuento especial por defecto en productos. Si no desea sobreescribir a continiación deje en blanco o en 0.</span>';
$_['entry_duration']          	= 'Duración:';
$_['tab_general']          	= 'General';
$_['tab_plan']          	= 'Plan';
$_['text_default_comission_rate']          	= 'Porcentaje de Comisión por defecto';

?>
