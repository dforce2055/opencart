<?php
// Heading
$_['heading_title']    = 'Google SiteMaps';

// Text
$_['text_feed']        = 'Cuotads';
$_['text_success']     = 'Se ha modificado el Cuotad de Google SiteMaps.';
$_['text_edit']        = 'Editar Google SiteMaps';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_data_feed']  = 'Url del Cuotad de Datos';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Cuotad de Google SiteMaps.';
