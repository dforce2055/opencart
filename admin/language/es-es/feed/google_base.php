<?php
// Heading
$_['heading_title']    = 'Google Base';

// Text
$_['text_feed']        = 'Cuotads';
$_['text_success']     = 'Se ha moficado el Cuotad de Google Base.';
$_['text_edit']        = 'Editar Google Base';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_data_feed']  = 'Url del Cuotad Datos';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Cuotad de Google Base.';