<?php
// Heading
$_['heading_title']     = 'Grupos de Usuarios';

// Text
$_['text_success'] = 'Se han modificado los Grupos de Usuarios.';
$_['text_list'] = 'Lista de Grupos de Usuarios';
$_['text_add'] = 'Agregar Grupo de Usuarios';
$_['text_edit'] = 'Editar Grupo de Usuarios';

// Columna
$_['column_name'] = 'Nombre del Grupo de Usuarios';
$_['column_action'] = 'Acción';

// Entrada
$_['entry_name'] = 'Nombre del Grupo de Usuarios';
$_['entry_access'] = 'Permiso de Acceso';
$_['entry_modify'] = 'Modificar Permiso';
$_['entry_sellers_permissions']     = "Permisos de Vendedor";

// Error
$_['error_permission'] = 'Sin permiso para modificar Grupos de Usuarios.';
$_['error_name'] = 'El Nombre del Grupo de Usuarios debe contener entre 3 y 64 caractéres.';
$_['error_user'] = 'Este Grupo de Usuarios no se puede eliminar ya que está asignado actualmente a los usuarios% s.';
