<?php
// Heading
$_['heading_title']     = 'Usuarios';

// Text
$_['text_success'] = 'Se han modificado los Usuarios.';
$_['text_list'] = 'Lista de Usuarios';
$_['text_add'] = 'Agregar Usuario';
$_['text_edit'] = 'Editar Usuario';

// Columna
$_['column_username'] = 'Nombre de usuario';
$_['column_status'] = 'Estado';
$_['column_date_added'] = 'Fecha Alta';
$_['column_action'] = 'Acción';

// Entrada
$_['entry_username'] = 'Nombre de Usuario';
$_['entry_user_group'] = 'Grupo de Usuarios';
$_['entry_password'] = 'Contraseña';
$_['entry_confirm'] = 'Confirmar';
$_['entry_firstname'] = 'Nombre';
$_['entry_lastname'] = 'Apellido';
$_['entry_email'] = 'Email';
$_['entry_image'] = 'Imagen';
$_['entry_status'] = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar los usuarios.';
$_['error_account'] = 'No se puede eliminar la propia cuenta.';
$_['error_exists'] = 'Nombre de Usuario ya existente.';
$_['error_username'] = 'El Nombre de Usuario debe contener entre 3 y 20 caractéres.';
$_['error_password'] = 'La contraseña debe contener entre 4 y 20 caractéres.';
$_['error_confirm'] = 'La Contraseña y su Confirmación no coinciden.';
$_['error_firstname'] = 'El Nombre debe contener entre 1 y 32 caractéres.';
$_['error_lastname'] = 'El Apellido debe contener entre 1 y 32 caractéres.';