<?php
// Heading
$_['heading_title']        = 'APIs';

// Text
$_['text_success'] = 'Se ha modificado la API.';
$_['text_list'] = 'Lista de APIs';
$_['text_add'] = 'Agregar API';
$_['text_edit'] = 'Editar API';

// Columna
$_['column_username'] = 'Nombre de Usuario';
$_['column_status'] = 'Estado';
$_['column_date_added'] = 'Fecha de Alta';
$_['column_date_modified'] = 'Fecha de Modificación';
$_['column_action'] = 'Acción';

// Entrada
$_['entry_username'] = 'Nombre de Usuario';
$_['entry_password'] = 'Contraseña';
$_['entry_status'] = 'Estado';

// Error
$_['error_permission'] = 'Sin permiso para modificar la API.';
$_['error_username'] = 'El Nombre de Usuario debe contener entre 3 y 20 caractéres.';
$_['error_password'] = 'La Contraseña debe contener entre 3 y 256 caractéres.';