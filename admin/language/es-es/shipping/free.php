<?php
// Heading
$_['heading_title']    = 'Envío Gratuito';

// Text
$_['text_shipping'] = 'Envío Gratuito';
$_['text_success'] = 'Se ha modificado el Envío Gratuito.';
$_['text_edit'] = 'Editar Envío Gratuito ';

// Entrada
$_['entry_total'] = 'Total';
$_['entry_geo_zone'] = 'GeoZona';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Orden';

// Ayuda
$_['help_total'] = 'Cantidad Mínima para que el Envío Gratuito esté disponible.';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Envío Gratuito.';