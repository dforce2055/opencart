<?php
// Heading
$_['heading_title']     = 'Por Artículo';

// Text
$_['text_shipping'] = 'Envío';
$_['text_success'] = 'Se ha modificado el Envío basado en Artículos.';
$_['text_edit'] = 'Editar Envío basado en Artículos';

// Entrada
$_['entry_cost'] = 'Costo';
$_['entry_tax_class'] = 'Clase de Impuesto';
$_['entry_geo_zone'] = 'GeoZona';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Orden';

// Error
$_['error_permission'] = 'Sin permiso para modificar el Envío basado en Artículos.';