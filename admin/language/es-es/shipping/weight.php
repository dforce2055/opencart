<?php
// Heading
$_['heading_title']    = 'Envío basado en Peso';

// Text
$_['text_shipping'] = 'Envío';
$_['text_success'] = 'Se ha modificado el Envío basado en el Peso.';
$_['text_edit'] = 'Editar Envío Basado en el Peso';

// Entrada
$_['entry_rate'] = 'Tarifas';
$_['entry_tax_class'] = 'Clase de Impuesto';
$_['entry_geo_zone'] = 'GeoZona';
$_['entry_status'] = 'Estado';
$_['entry_sort_order'] = 'Orden';

// Ayuda
$_['help_rate'] = 'Ejemplo: 5: 10.00,7: 12,00 Peso: Costo, Peso: Costo, etc.';

// Error
$_['error_permission'] = 'Sin permiso para modificar el envío Basado en el Peso.';