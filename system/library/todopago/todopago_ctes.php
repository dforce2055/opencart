<?PHP
define('TP_VERSION', '1.1.0');
define('TP_LOGDIR', DIR_LOGS.'error.log');
define('TP_LOG_LEVEL', 'debug');
define('TP_PLUGIN_NAME', 'OPENCART3');
define('TP_JS_TEST', 'https://developers.todopago.com.ar/resources/v2/TPBSAForm.min.js');
define('TP_JS_PROD', 'https://forms.todopago.com.ar/resources/v2/TPBSAForm.min.js');
